package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.ClientService;
import innovar.io.person.dto.ClientDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PersonApplication.host)
public class ClientController extends BaseController {
    @Autowired
    ClientService clientService;

    @PostMapping("/client")
    public AppResponse createClient(@RequestBody() ClientDto clientDto){
        return Response(()-> clientService.createClient((clientDto)));
    }
    @PutMapping("/client/{id}")
    public AppResponse updateClient(@PathVariable() String id, @RequestBody() ClientDto clientDto){
        return Response(()-> clientService.updateClient((id),(clientDto)));
    }
    @GetMapping("/client/{id}")
    public AppResponse getClient(@PathVariable() String id){
        return Response(()-> clientService.getClient(id));
    }

    @GetMapping("/clients")
    public AppResponse getAllClient(){
        return Response(()-> clientService.getAllClient());
    }

    @GetMapping("/ListClients")
    public AppResponse getListClient(){
        return Response(()-> clientService.getListClient());
    }

    @DeleteMapping("/client/{id}")
    public AppResponse disabledClient(@PathVariable() String id){
        return Response(()-> clientService.disabledClient(id));
    }

    @GetMapping("/findClientDocumentIdentity/{code},{number}")
    public AppResponse findClientDocumentIndentity(@PathVariable String code,@PathVariable String number){
        return Response(()-> clientService.findClientDocumentIdentity(code,number));
    }

}
