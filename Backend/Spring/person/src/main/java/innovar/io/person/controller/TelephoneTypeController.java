package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.TelephoneTypeService;
import innovar.io.person.dto.TelephoneTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PersonApplication.host)
public class TelephoneTypeController extends BaseController {
    @Autowired
    TelephoneTypeService telephoneTypeService;

    @PostMapping("/telephonetype")
    public AppResponse createTelephoneType(@RequestBody() TelephoneTypeDto telephoneTypeDto){
        return Response(()-> telephoneTypeService.createTelephoneType((telephoneTypeDto)));
    }

    @PutMapping("/telephonetype/{id}")
    public AppResponse updateTelephoneType(@PathVariable() String id, @RequestBody() TelephoneTypeDto telephoneTypeDto){
        return Response(()-> telephoneTypeService.updateTelephoneType((id),(telephoneTypeDto)));
    }

    @GetMapping("/telephonetype/{id}")
    public AppResponse getTelephoneType(@PathVariable() String id){
        return Response(()-> telephoneTypeService.getTelephoneType(id));
    }

    @GetMapping("/telephonetypes")
    public AppResponse getAllTelephoneType(){
        return Response(()-> telephoneTypeService.getAllTelephoneType());
    }


    @GetMapping("/ListTelephoneTypes")
    public AppResponse getListTelephoneType(){
        return Response(()-> telephoneTypeService.getListTelephoneType());
    }

    @DeleteMapping("/telephonetype/{id}")
    public AppResponse disabledTelephoneType(@PathVariable() String id){
        return Response(()-> telephoneTypeService.disabledTelephoneType(id));
    }
}
