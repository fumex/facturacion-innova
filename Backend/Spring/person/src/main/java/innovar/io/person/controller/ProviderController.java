package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.ProviderService;
import innovar.io.person.dto.ProviderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PersonApplication.host)
public class ProviderController extends BaseController {
    @Autowired
    ProviderService providerService;

    @PostMapping("/provider")
    public AppResponse createProvider(@RequestBody() ProviderDto providerDto){
        return Response(()-> providerService.createProvider((providerDto)));
    }
    @PutMapping("/provider/{id}")
    public AppResponse updateProvider(@PathVariable() String id, @RequestBody() ProviderDto providerDto){
        return Response(()-> providerService.updateProvider((id),(providerDto)));
    }
    @GetMapping("/provider/{id}")
    public AppResponse getProvider(@PathVariable() String id){
        return Response(()-> providerService.getProvider(id));
    }

    @GetMapping("/providers")
    public AppResponse getAllProvider(){
        return Response(()-> providerService.getAllProvider());
    }

    @GetMapping("/ListProviders")
    public AppResponse getListProvider(){
        return Response(()-> providerService.getListProvider());
    }

    @DeleteMapping("/provider/{id}")
    public AppResponse disabledProvider(@PathVariable() String id){
        return Response(()-> providerService.disabledProvider(id));
    }
}
