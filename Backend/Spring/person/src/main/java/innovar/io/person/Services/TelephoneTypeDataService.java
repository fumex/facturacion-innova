package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Telephonetype;

import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class TelephoneTypeDataService extends RepositoryPostgressService<Telephonetype> {

    public TelephoneTypeDataService(DSLContext dsl){super(dsl);}
    /* public List<TelephoneType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, TelephoneType.class);
    }
    */

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Telephonetype.class);
    }

    @Override
    public innovar.io.person.models.tables.Telephonetype getTable() {
        return Tables.TELEPHONETYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.TELEPHONETYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.TELEPHONETYPE.ISACTIVE;
    }
}
