/*
 * This file is generated by jOOQ.
 */
package innovar.io.person.models.tables;


import innovar.io.person.models.Indexes;
import innovar.io.person.models.Keys;
import innovar.io.person.models.Public;
import innovar.io.person.models.tables.records.PadronpersonRecord;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import javax.annotation.Generated;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Padronperson extends TableImpl<PadronpersonRecord> {

    private static final long serialVersionUID = -1355168078;

    /**
     * The reference instance of <code>public.padronperson</code>
     */
    public static final Padronperson PADRONPERSON = new Padronperson();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PadronpersonRecord> getRecordType() {
        return PadronpersonRecord.class;
    }

    /**
     * The column <code>public.padronperson.id</code>.
     */
    public final TableField<PadronpersonRecord, String> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.VARCHAR(24).nullable(false), this, "");

    /**
     * The column <code>public.padronperson.companyid</code>.
     */
    public final TableField<PadronpersonRecord, String> COMPANYID = createField(DSL.name("companyid"), org.jooq.impl.SQLDataType.VARCHAR(24), this, "");

    /**
     * The column <code>public.padronperson.padronid</code>.
     */
    public final TableField<PadronpersonRecord, String> PADRONID = createField(DSL.name("padronid"), org.jooq.impl.SQLDataType.VARCHAR(24), this, "");

    /**
     * The column <code>public.padronperson.createdon</code>.
     */
    public final TableField<PadronpersonRecord, Timestamp> CREATEDON = createField(DSL.name("createdon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.padronperson.createdby</code>.
     */
    public final TableField<PadronpersonRecord, String> CREATEDBY = createField(DSL.name("createdby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.padronperson.updatedon</code>.
     */
    public final TableField<PadronpersonRecord, Timestamp> UPDATEDON = createField(DSL.name("updatedon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.padronperson.updatedby</code>.
     */
    public final TableField<PadronpersonRecord, String> UPDATEDBY = createField(DSL.name("updatedby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.padronperson.isactive</code>.
     */
    public final TableField<PadronpersonRecord, Boolean> ISACTIVE = createField(DSL.name("isactive"), org.jooq.impl.SQLDataType.BOOLEAN, this, "");

    /**
     * The column <code>public.padronperson.class</code>.
     */
    public final TableField<PadronpersonRecord, String> CLASS = createField(DSL.name("class"), org.jooq.impl.SQLDataType.VARCHAR(200), this, "");

    /**
     * Create a <code>public.padronperson</code> table reference
     */
    public Padronperson() {
        this(DSL.name("padronperson"), null);
    }

    /**
     * Create an aliased <code>public.padronperson</code> table reference
     */
    public Padronperson(String alias) {
        this(DSL.name(alias), PADRONPERSON);
    }

    /**
     * Create an aliased <code>public.padronperson</code> table reference
     */
    public Padronperson(Name alias) {
        this(alias, PADRONPERSON);
    }

    private Padronperson(Name alias, Table<PadronpersonRecord> aliased) {
        this(alias, aliased, null);
    }

    private Padronperson(Name alias, Table<PadronpersonRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Padronperson(Table<O> child, ForeignKey<O, PadronpersonRecord> key) {
        super(child, key, PADRONPERSON);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.PADRONPERSON_PKEY);
    }

    @Override
    public UniqueKey<PadronpersonRecord> getPrimaryKey() {
        return Keys.PADRONPERSON_PKEY;
    }

    @Override
    public List<UniqueKey<PadronpersonRecord>> getKeys() {
        return Arrays.<UniqueKey<PadronpersonRecord>>asList(Keys.PADRONPERSON_PKEY);
    }

    @Override
    public Padronperson as(String alias) {
        return new Padronperson(DSL.name(alias), this);
    }

    @Override
    public Padronperson as(Name alias) {
        return new Padronperson(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Padronperson rename(String name) {
        return new Padronperson(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Padronperson rename(Name name) {
        return new Padronperson(name, null);
    }

    // -------------------------------------------------------------------------
    // Row9 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row9<String, String, String, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row9) super.fieldsRow();
    }
}
