package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.TaxRegimeDataService;
import innovar.io.person.dto.TaxRegimeDto;

import innovar.io.person.models.tables.pojos.Taxregime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class TaxRegimeService extends BaseBussinesService {
    @Autowired
    TaxRegimeDataService baseTaxRegimeService;

    public TaxRegimeDto createTaxRegime(TaxRegimeDto typeRegimeTaxDto){
        Taxregime taxRegime =map(typeRegimeTaxDto , Taxregime.class);
        List<Taxregime> taxRegimes = baseTaxRegimeService.findCode(typeRegimeTaxDto.code);
        if(taxRegimes.size()>0){
            String id = taxRegimes.get(0).getId();
            Taxregime taxRegimeUp =  baseTaxRegimeService.findById(id);
            taxRegimeUp.setActivatedate(new Timestamp(new Date().getTime()));
            baseTaxRegimeService.update(id,taxRegimeUp);
            return map(taxRegimeUp, TaxRegimeDto.class);
        }else{
            if(taxRegime.getIsactive()==null){
                taxRegime.setIsactive(true);
            }
            taxRegime.setActivatedate(new Timestamp(new Date().getTime()));
            taxRegime.setInactivatedate(null);
            baseTaxRegimeService.insert(taxRegime);
            return map(taxRegime, TaxRegimeDto.class);
        }

    }

    public List<TaxRegimeDto> createListTaxRegime(List<TaxRegimeDto> taxRegimeDtos){
        List<Taxregime> taxRegimes = mapAll(taxRegimeDtos, Taxregime.class);
        TaxRegimeDto taxRegimeDto = new TaxRegimeDto();
        taxRegimes.stream().forEach((p)->{
            taxRegimeDto.setCode(p.getCode());
            taxRegimeDto.setName(p.getName());
            taxRegimeDto.setAbbreviation(p.getAbbreviation());
            if(p.getIsactive()==null){
                taxRegimeDto.setIsactive(true);
            }else{
                taxRegimeDto.setIsactive(p.getIsactive());
            }
            createTaxRegime(taxRegimeDto);
        });
        return mapAll(taxRegimes, TaxRegimeDto.class);
    }

    public TaxRegimeDto updateTaxRegime(String id, TaxRegimeDto typeDocumentIdentityDto){
        Taxregime taxRegime=map(typeDocumentIdentityDto, Taxregime.class);
        Taxregime a = baseTaxRegimeService.findById(id);
        taxRegime.setActivatedate(a.getActivatedate());
        taxRegime.setInactivatedate(a.getInactivatedate());
        taxRegime.setUpdatedon(new Timestamp(new Date().getTime()));
        baseTaxRegimeService.update(id, taxRegime);
        return map(taxRegime, TaxRegimeDto.class);
    }
    public TaxRegimeDto getTaxRegime(String id){
        Taxregime taxRegimeServiceById = baseTaxRegimeService.findById(id);
        return map(taxRegimeServiceById, TaxRegimeDto.class);
    }
    public TaxRegimeDto disabledTaxRegime(String id){
        Taxregime taxRegimeServiceById = baseTaxRegimeService.findById(id);
        taxRegimeServiceById.setInactivatedate(new Timestamp(new Date().getTime()));
        baseTaxRegimeService.update(id,taxRegimeServiceById);
        return map(baseTaxRegimeService.disabled(id), TaxRegimeDto.class);
    }
    public List<TaxRegimeDto> getAllTaxRegimes(){
        List<Taxregime> taxRegimes = baseTaxRegimeService.findIntoList();
        return mapAll(taxRegimes, TaxRegimeDto.class);
    }

    public List<TaxRegimeDto> getListTaxRegimes(){
        List<Taxregime> taxRegimes = baseTaxRegimeService.listAll();
        return mapAll(taxRegimes, TaxRegimeDto.class);
    }

}
