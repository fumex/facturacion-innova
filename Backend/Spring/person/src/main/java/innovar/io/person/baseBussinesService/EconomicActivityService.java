package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.EconomicActivityDataService;
import innovar.io.person.dto.EconomicActivityDto;

import innovar.io.person.models.tables.pojos.Economicactivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class EconomicActivityService  extends BaseBussinesService {
    @Autowired
    EconomicActivityDataService baseEconomicActivityDataService;

    public EconomicActivityDto createEconomicActivity(EconomicActivityDto economicActivityDto){
        Economicactivity economicActivity = map(economicActivityDto, Economicactivity.class);
        List<Economicactivity> economicActivities  =baseEconomicActivityDataService.findCode(economicActivityDto.code);
        if(economicActivities.size()>0){
            String id = economicActivities.get(0).getId();
            Economicactivity economicAct = baseEconomicActivityDataService.findById(id);
            economicAct.setActivatedate(new Timestamp(new Date().getTime()));
            baseEconomicActivityDataService.update(id, economicAct);
            return  map(economicAct, EconomicActivityDto.class);
        }else{
            if(economicActivity.getIsactive()==null){
                economicActivity.setIsactive(true);
            }
            economicActivity.setActivatedate(new Timestamp(new Date().getTime()));
            economicActivity.setInactivatedate(null);
            baseEconomicActivityDataService.insert((economicActivity));
            return map(economicActivity, EconomicActivityDto.class);
        }

    }
    public List<EconomicActivityDto> createListEconomicActivity(List<EconomicActivityDto> economicActivityDtos){
        List<Economicactivity> economicActivities = mapAll(economicActivityDtos, Economicactivity.class);
        EconomicActivityDto economicActivityDto = new EconomicActivityDto();
        economicActivities.stream().forEach((p)->{
            economicActivityDto.setCode(p.getCode());
            economicActivityDto.setName(p.getName());
            if(p.getIsactive()==null){
                economicActivityDto.setIsactive(true);
            }else{
                economicActivityDto.setIsactive(p.getIsactive());
            }
            createEconomicActivity(economicActivityDto);
        });
        return mapAll(economicActivities, EconomicActivityDto.class);
    }

    public EconomicActivityDto updateEconomicActivity(String id, EconomicActivityDto economicActivityDto){
        Economicactivity economicActivity = map(economicActivityDto,Economicactivity.class);
        Economicactivity a = baseEconomicActivityDataService.findById(id);
        economicActivity.setActivatedate(a.getActivatedate());
        economicActivity.setInactivatedate(a.getInactivatedate());
        if(economicActivityDto.getIsactive()==null){
            economicActivityDto.setIsactive(true);
        }
        baseEconomicActivityDataService.update(id,economicActivity);
        return map(economicActivity,EconomicActivityDto.class);
    }

    public EconomicActivityDto getEconomicActivity(String id){
        Economicactivity economicActivity = baseEconomicActivityDataService.findById(id);
        return map(economicActivity,EconomicActivityDto.class);
    }

    public EconomicActivityDto disableEconomicActivity(String id){
        Economicactivity economicActivity = baseEconomicActivityDataService.findById(id);
        economicActivity.setInactivatedate(new Timestamp( new Date().getTime()));
        baseEconomicActivityDataService.update(id,economicActivity);
        return map( baseEconomicActivityDataService.disabled(id), EconomicActivityDto.class);
    }

    public List<EconomicActivityDto> getAllEconomicActivity(){
        List<Economicactivity> economicActivities = baseEconomicActivityDataService.findIntoList();
        return mapAll(economicActivities,EconomicActivityDto.class);
    }

    public List<EconomicActivityDto> getListEconomicActivity(){
        List<Economicactivity> economicActivities = baseEconomicActivityDataService.listAll();
        return mapAll(economicActivities,EconomicActivityDto.class);
    }
}
