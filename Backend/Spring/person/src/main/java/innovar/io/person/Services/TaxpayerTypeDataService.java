package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Taxpayertype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaxpayerTypeDataService extends RepositoryPostgressService<Taxpayertype> {

    public TaxpayerTypeDataService (DSLContext dsl){ super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Taxpayertype.class);
    }

    @Override
    public innovar.io.person.models.tables.Taxpayertype getTable() {
        return Tables.TAXPAYERTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.TAXPAYERTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.TAXPAYERTYPE.ISACTIVE;
    }

    /*public List<TaxpayerType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, TaxpayerType.class);
    }*/
}
