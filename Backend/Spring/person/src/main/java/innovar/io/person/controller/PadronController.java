package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.PadronService;
import innovar.io.person.dto.PadronDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class PadronController extends BaseController {
    @Autowired
    PadronService padronService;

    @PostMapping("/padron")
    public AppResponse createPadron(@RequestBody() PadronDto padronDto){
        return Response(()-> padronService.createPadron((padronDto)));
    }

    @PostMapping("/padronListCreate")
    public AppResponse createListPadron(@RequestBody() List<PadronDto> padronDtos){
        return Response(()->padronService.createListPadron(padronDtos));
    }

    @PutMapping("/padron/{id}")
    public AppResponse updatePadron(@PathVariable() String id, @RequestBody() PadronDto padronDto){
        return Response(()-> padronService.updatePadron((id),(padronDto)));
    }

    @GetMapping("/padron/{id}")
    public AppResponse getPadron(@PathVariable() String id){
        return Response(()-> padronService.getPadron(id));
    }

    @GetMapping("/padrones")
    public AppResponse getAllPadrons(){
        return Response(()-> padronService.getAllPadron());
    }

    @GetMapping("/ListPadrones")
    public AppResponse getListPadrons(){
        return Response(()-> padronService.getListPadron());
    }

    @DeleteMapping("/padron/{id}")
    public AppResponse disabledPadron(@PathVariable() String id){
        return Response(()-> padronService.disabledPadron(id));
    }
}
