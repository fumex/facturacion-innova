package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.PadronDataService;
import innovar.io.person.dto.PadronDto;

import innovar.io.person.models.tables.pojos.Padron;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class PadronService extends BaseBussinesService {
    @Autowired
    PadronDataService basePadronDataService;

    public PadronDto createPadron(PadronDto padronDto){
        Padron padron =map(padronDto , Padron.class);
        List<Padron> padrons = basePadronDataService.findCode(padronDto.code);
        if (padrons.size() > 0) {
            String id = padrons.get(0).getId();
            Padron pattern1 = basePadronDataService.findById(id);
            pattern1.setActivatedate(new Timestamp(new Date().getTime()));
            basePadronDataService.update(id,pattern1);
            return map(pattern1, PadronDto.class);
        }else {
            if(padron.getIsactive()==null){
                padron.setIsactive(true);
            }
            padron.setActivatedate(new Timestamp(new Date().getTime()));
            padron.setInactivatedate(null);
            basePadronDataService.insert(padron);
            return map(padron, PadronDto.class);
        }

    }

    public List<PadronDto> createListPadron(List<PadronDto> padronDtos){
        List<Padron> padrons = mapAll(padronDtos, Padron.class);
        PadronDto padronDto = new PadronDto();
        padrons.stream().forEach((p)->{
            padronDto.setCode(p.getCode());
            padronDto.setName(p.getName());
            if(p.getIsactive()==null){
                padronDto.setIsactive(true);
            }else{
                padronDto.setIsactive(p.getIsactive());
            }
            createPadron(padronDto);
        });
        return mapAll(padrons, PadronDto.class);
    }

    public PadronDto updatePadron(String id, PadronDto padronDto){
        Padron padron=map(padronDto, Padron.class);
        Padron a = basePadronDataService.findById(id);
        padron.setActivatedate(a.getActivatedate());
        padron.setInactivatedate(a.getInactivatedate());
        padron.setUpdatedon(new Timestamp(new Date().getTime()));
        if(padron.getIsactive()==null){
            padron.setIsactive(true);
        }
        basePadronDataService.update(id, padron);
        return map(padron, PadronDto.class);
    }
    public PadronDto getPadron(String id){
        Padron padron = basePadronDataService.findById(id);
        return map(padron, PadronDto.class);
    }
    public PadronDto disabledPadron(String id){
        Padron padron = basePadronDataService.findById(id);
        padron.setInactivatedate(new Timestamp(new Date().getTime()));
        padron.setUpdatedon(new Timestamp(new Date().getTime()));
        basePadronDataService.update(id,padron);
        return map(basePadronDataService.disabled(id), PadronDto.class);
    }
    public List<PadronDto> getAllPadron(){
        List<Padron> padrons = basePadronDataService.findIntoList();
        return mapAll(padrons, PadronDto.class);
    }

    public List<PadronDto> getListPadron(){
        List<Padron> padrons = basePadronDataService.listAll();
        return mapAll(padrons, PadronDto.class);
    }
}
