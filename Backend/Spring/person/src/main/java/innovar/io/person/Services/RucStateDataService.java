package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Rucstate;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RucStateDataService extends RepositoryPostgressService<Rucstate> {

    public RucStateDataService(DSLContext dsl) {
        super(dsl);
    }

    public List findCode(String code) {
        return find(getTable().CODE.eq(code)).fetchInto(Rucstate.class);
    }

    @Override
    public innovar.io.person.models.tables.Rucstate getTable() {
        return Tables.RUCSTATE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.RUCSTATE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.RUCSTATE.ISACTIVE;
    }
    /*public List<RucState> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, RucState.class);
    }
     */
}
