package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.PhoneDataService;

import innovar.io.person.dto.PhoneDto;

import innovar.io.person.models.tables.pojos.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class PhoneService extends BaseBussinesService {
    @Autowired
    PhoneDataService basePhoneDataService;

    public PhoneDto createPhone(PhoneDto phoneDto){
        Phone phone =map(phoneDto , Phone.class);
        if(phone.getIsactive()==null){
            phone.setIsactive(true);
        }
        basePhoneDataService.insert(phone);
        return map(phone, PhoneDto.class);
    }

    public PhoneDto updatePhone(String id, PhoneDto phoneDto){
        Phone phone=map(phoneDto, Phone.class);
        Phone a = basePhoneDataService.findById(id);
        phone.setUpdatedon(new Timestamp(new Date().getTime()));
        if(phone.getIsactive()==null){
            phone.setIsactive(true);
        }
        basePhoneDataService.update(id, phone);
        return map(phone, PhoneDto.class);
    }
    public PhoneDto getPhone(String id){
        Phone phone = basePhoneDataService.findById(id);
        return map(phone, PhoneDto.class);
    }
    public PhoneDto disabledPhone(String id){
        Phone phone = basePhoneDataService.findById(id);
        phone.setUpdatedon(new Timestamp(new Date().getTime()));
        basePhoneDataService.update(id,phone);
        return map(basePhoneDataService.disabled(id), PhoneDto.class);
    }

    public List<PhoneDto> getAllPhone(){
        List<Phone> phones = basePhoneDataService.findIntoList();
        return mapAll(phones, PhoneDto.class);
    }
    public List<PhoneDto> getListPhone(){
        List<Phone> phones = basePhoneDataService.listAll();
        return mapAll(phones, PhoneDto.class);
    }

}
