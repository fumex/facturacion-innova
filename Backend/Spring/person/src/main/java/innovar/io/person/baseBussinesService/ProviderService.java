package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.ProviderDataService;
import innovar.io.person.dto.ProviderDto;
import innovar.io.person.models.tables.pojos.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ProviderService extends BaseBussinesService {
    @Autowired
    ProviderDataService providerDataService;

    public ProviderDto createProvider(ProviderDto providerDto){
        Provider provider = map(providerDto, Provider.class);
        if(provider.getIsactive()==null){
            provider.setIsactive(true);
        }
        providerDataService.insert((provider));
        return map(provider, ProviderDto.class);
    }

    public ProviderDto updateProvider(String id, ProviderDto providerDto){
        Provider provider= map(providerDto,Provider.class);
        provider.setUpdatedon(new Timestamp(new Date().getTime()));
        if(provider.getIsactive()==null){
            provider.setIsactive(true);
        }
        providerDataService.update(id, provider);
        return  map(provider, ProviderDto.class);
    }
    public ProviderDto getProvider(String id){
        Provider provider= providerDataService.findById(id);
        return map(provider, ProviderDto.class);
    }

    public List<ProviderDto> getAllProvider(){
        List<Provider> providers= providerDataService.findIntoList();
        return  mapAll(providers, ProviderDto.class);
    }

    public List<ProviderDto> getListProvider(){
        List<Provider> providers= providerDataService.listAll();
        return  mapAll(providers, ProviderDto.class);
    }

    public ProviderDto disabledProvider(String id){
        Provider provider= providerDataService.findById(id);
        provider.setUpdatedon(new Timestamp(new Date().getTime()));
        providerDataService.update(id, provider);
        return map(providerDataService.disabled(id),ProviderDto.class);
    }
}
