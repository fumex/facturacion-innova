package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Padronperson;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

@Service
public class PadronPersonDataService extends RepositoryPostgressService<Padronperson> {

    public PadronPersonDataService(DSLContext dsl){ super(dsl); }

    @Override
    public innovar.io.person.models.tables.Padronperson getTable() {
        return Tables.PADRONPERSON;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PADRONPERSON.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PADRONPERSON.ISACTIVE;
    }
}
