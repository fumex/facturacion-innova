package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.FiscalAddressConditionTypeService;
import innovar.io.person.dto.FiscalAddressConditionTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class FiscalAddressConditionTypeController extends BaseController {
    @Autowired
    FiscalAddressConditionTypeService fiscalAddressConditionTypeService;

    @PostMapping("/fiscaladdressconditiontype")
    public AppResponse createEconomicActivity(@RequestBody()FiscalAddressConditionTypeDto fiscalAddressConditionTypeDto){
        return Response(()-> fiscalAddressConditionTypeService.createFiscalAddressConditionType((fiscalAddressConditionTypeDto)));
    }

    @PostMapping("/fiscaladdressconditiontypeListCreate")
    public AppResponse createListFiscalAddressConditionType(@RequestBody() List<FiscalAddressConditionTypeDto> fiscalAddressConditionTypeDtos){
        return Response(()->fiscalAddressConditionTypeService.createListFiscalAddressConditionType(fiscalAddressConditionTypeDtos));
    }

    @PutMapping("/fiscaladdressconditiontype/{id}")
    public AppResponse updateFiscalAddressConditionType(@PathVariable() String id, @RequestBody() FiscalAddressConditionTypeDto fiscalAddressConditionTypeDto){
        return Response(()-> fiscalAddressConditionTypeService.updateFiscalAddressConditionType((id),(fiscalAddressConditionTypeDto)));
    }

    @GetMapping("/fiscaladdressconditiontype/{id}")
    public AppResponse getFiscalAddressConditionType(@PathVariable() String id){
        return Response(()-> fiscalAddressConditionTypeService.getFiscalAddressConditionType(id));
    }

    @GetMapping("/fiscaladdressconditiontypes")
    public AppResponse getAllFiscalAddressConditionTypes(){
        return Response(()-> fiscalAddressConditionTypeService.getAllFiscalAddressConditionType());
    }

    @GetMapping("/ListFiscalAddressConditionTypes")
    public AppResponse getListFiscalAddressConditionTypes(){
        return Response(()-> fiscalAddressConditionTypeService.getListFiscalAddressConditionType());
    }

    @DeleteMapping("/fiscaladdressconditiontype/{id}")
    public AppResponse disabledFiscalAddressConditionType(@PathVariable() String id){
        return Response(()-> fiscalAddressConditionTypeService.disableFiscalAddressConditionType(id));
    }
}
