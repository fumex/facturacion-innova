package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.TaxpayerTypeDataService;
import innovar.io.person.dto.TaxpayerTypeDto;

import innovar.io.person.models.tables.pojos.Taxpayertype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class TaxpayerTypeService extends BaseBussinesService {
    @Autowired
    TaxpayerTypeDataService baseTaxpayerTypeDataService;

    public TaxpayerTypeDto createTaxpayerType(TaxpayerTypeDto taxpayerTypeDto){
        Taxpayertype taxpayerType =  map(taxpayerTypeDto, Taxpayertype.class);
        List<Taxpayertype> taxpayerTypeList = baseTaxpayerTypeDataService.findCode(taxpayerTypeDto.code);
        if(taxpayerTypeList.size()>0){
            String id = taxpayerTypeList.get(0).getId();
            Taxpayertype taxpayerTypeUp = baseTaxpayerTypeDataService.findById(id);
            taxpayerTypeUp.setActivatedate(new Timestamp(new Date().getTime()));
            baseTaxpayerTypeDataService.update(id,taxpayerTypeUp);
            return map(taxpayerTypeUp, TaxpayerTypeDto.class );
        }else{
            if(taxpayerType.getIsactive()==null){
                taxpayerType.setIsactive(true);
            }
            taxpayerType.setActivatedate(new Timestamp(new Date().getTime()));
            taxpayerType.setInactivatedate(null);
            baseTaxpayerTypeDataService.insert(taxpayerType);
            return map(taxpayerType, TaxpayerTypeDto.class);
        }
    }

    public List<TaxpayerTypeDto> createListTaxpayerType(List<TaxpayerTypeDto> taxpayerTypeDtos){
        List<Taxpayertype> taxpayerTypes = mapAll(taxpayerTypeDtos, Taxpayertype.class);
        TaxpayerTypeDto taxpayerTypeDto = new TaxpayerTypeDto();
        taxpayerTypes.stream().forEach((p)->{
            taxpayerTypeDto.setCode(p.getCode());
            taxpayerTypeDto.setName(p.getName());
            if(p.getIsactive()==null){
                taxpayerTypeDto.setIsactive(true);
            }else{
                taxpayerTypeDto.setIsactive(p.getIsactive());
            }
            createTaxpayerType(taxpayerTypeDto);
        });
        return mapAll(taxpayerTypes, TaxpayerTypeDto.class);
    }

    public TaxpayerTypeDto updateTaxpayerType(String id, TaxpayerTypeDto taxpayerTypeDto){
        Taxpayertype taxpayerType =  map(taxpayerTypeDto, Taxpayertype.class);
        Taxpayertype a = baseTaxpayerTypeDataService.findById(id);
        taxpayerType.setActivatedate(a.getActivatedate());
        taxpayerType.setInactivatedate(a.getInactivatedate());
        if(taxpayerType.getIsactive()==null){
            taxpayerType.setIsactive(true);
        }
        baseTaxpayerTypeDataService.update(id, taxpayerType);
        return map(taxpayerType, TaxpayerTypeDto.class);
    }

    public TaxpayerTypeDto getTaxpayerType(String id){
        Taxpayertype taxpayerType =  baseTaxpayerTypeDataService.findById(id);
        return map(taxpayerType, TaxpayerTypeDto.class);
    }

    public List<TaxpayerTypeDto> getAllTaxpayerType(){
        List<Taxpayertype> taxpayerTypes = baseTaxpayerTypeDataService.findIntoList();
        return mapAll(taxpayerTypes, TaxpayerTypeDto.class);
    }
    public TaxpayerTypeDto disabledTaxpayerType(String id){
        Taxpayertype taxpayerType= baseTaxpayerTypeDataService.findById(id);
        taxpayerType.setInactivatedate(new Timestamp(new Date().getTime()));
        baseTaxpayerTypeDataService.update(id,taxpayerType);
        return map(baseTaxpayerTypeDataService.disabled(id), TaxpayerTypeDto.class);
    }

    public List<TaxpayerTypeDto> getListTaxpayerType(){
        List<Taxpayertype> taxpayerTypes = baseTaxpayerTypeDataService.listAll();
        return mapAll(taxpayerTypes, TaxpayerTypeDto.class);
    }
}
