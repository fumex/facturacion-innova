package innovar.io.person.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PhonePersonDto {
    public String id;
    public String personid;
    public String phoneid;
    public Boolean isactive;
}
