package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.TelephoneTypeDataService;
import innovar.io.person.dto.TelephoneTypeDto;

import innovar.io.person.models.tables.pojos.Telephonetype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class TelephoneTypeService extends BaseBussinesService {
    @Autowired
    TelephoneTypeDataService baseTelephoneTypeService;

    public TelephoneTypeDto createTelephoneType(TelephoneTypeDto telephoneTypeDto){
        Telephonetype telephoneType =map(telephoneTypeDto , Telephonetype.class);
        baseTelephoneTypeService.insert(telephoneType);
        if(telephoneType.getIsactive()==null){
            telephoneType.setIsactive(true);
        }
        return map(telephoneType, TelephoneTypeDto.class);
    }

    public TelephoneTypeDto updateTelephoneType(String id, TelephoneTypeDto telephoneTypeDto){
        Telephonetype telephoneType=map(telephoneTypeDto, Telephonetype.class);
        Telephonetype a = baseTelephoneTypeService.findById(id);
        telephoneType.setUpdatedon(new Timestamp(new Date().getTime()));
        if(telephoneType.getIsactive()==null){
            telephoneType.setIsactive(true);
        }
        baseTelephoneTypeService.update(id, telephoneType);
        return map(telephoneType, TelephoneTypeDto.class);
    }
    public TelephoneTypeDto getTelephoneType(String id){
        Telephonetype telephoneType = baseTelephoneTypeService.findById(id);
        return map(telephoneType, TelephoneTypeDto.class);
    }
    public TelephoneTypeDto disabledTelephoneType(String id){
        Telephonetype telephoneType = baseTelephoneTypeService.findById(id);
        baseTelephoneTypeService.update(id,telephoneType);
        return map(baseTelephoneTypeService.disabled(id), TelephoneTypeDto.class);
    }
    public List<TelephoneTypeDto> getAllTelephoneType(){
        List<Telephonetype> telephoneTypes = baseTelephoneTypeService.findIntoList();
        return mapAll(telephoneTypes, TelephoneTypeDto.class);
    }

    public List<TelephoneTypeDto> getListTelephoneType(){
        List<Telephonetype> telephoneTypes = baseTelephoneTypeService.listAll();
        return mapAll(telephoneTypes, TelephoneTypeDto.class);
    }
}
