package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.person.Services.CompanyDataService;
import innovar.io.person.dto.ClientDto;
import innovar.io.person.dto.CompanyDto;


import innovar.io.person.models.tables.pojos.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class CompanyService  extends BaseBussinesService{
    @Autowired
    CompanyDataService baseCompanyDataService;

    public CompanyDto createCompany(CompanyDto companyDto){
        Company company = map(companyDto,Company.class);
        if(company.getIsactive()==null){
            company.setIsactive(true);
        }
        baseCompanyDataService.insert((company));
        return map(company, CompanyDto.class);
    }

    public CompanyDto updateCompany(String id, CompanyDto companyDto){
        Company company=map(companyDto,Company.class);
        company.setUpdatedon(new Timestamp(new Date().getTime()));
        if(company.getIsactive()==null){
            company.setIsactive(true);
        }
        baseCompanyDataService.update(id, company);
        return  map(company, CompanyDto.class);
    }

    public CompanyDto getCompany(String id){
        Company company = baseCompanyDataService.findById(id);
        return map(company, CompanyDto.class);
    }

    public CompanyDto disabledCompany(String id){
        Company company =  baseCompanyDataService.findById(id);
        company.setUpdatedon(new Timestamp(new Date().getTime()));
        baseCompanyDataService.update(id,company);
        return map(baseCompanyDataService.disabled(id), CompanyDto.class);
    }
    public List<CompanyDto> getAllCompany(){
        List<Company> companies= baseCompanyDataService.findIntoList();
        return mapAll(companies, CompanyDto.class);
    }

    public List<CompanyDto> getListCompany(){
        List<Company> companies= baseCompanyDataService.listAll();
        return mapAll(companies, CompanyDto.class);
    }
}
