package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.FiscalAddressConditionTypeDataService;

import innovar.io.person.dto.FiscalAddressConditionTypeDto;

import innovar.io.person.models.tables.pojos.Fiscaladdressconditiontype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class FiscalAddressConditionTypeService extends BaseBussinesService {
    @Autowired
    FiscalAddressConditionTypeDataService baseFiscalAddressConditionTypeDataService;

    public FiscalAddressConditionTypeDto createFiscalAddressConditionType(FiscalAddressConditionTypeDto fiscalAddressConditionTypeDto){
        Fiscaladdressconditiontype fiscalAddressConditionType = map(fiscalAddressConditionTypeDto, Fiscaladdressconditiontype.class);
        List<Fiscaladdressconditiontype> fiscalAddressConditionTypeList  = baseFiscalAddressConditionTypeDataService.findCode(fiscalAddressConditionTypeDto.code);
        if(fiscalAddressConditionTypeList.size()>0){
            String id = fiscalAddressConditionTypeList.get(0).getId();
            Fiscaladdressconditiontype a = baseFiscalAddressConditionTypeDataService.findById(id);
            a.setActivatedate(new Timestamp(new Date().getTime()));
            baseFiscalAddressConditionTypeDataService.update(id, a);
            return  map(a, FiscalAddressConditionTypeDto.class);
        }else{
            if(fiscalAddressConditionType.getIsactive()==null){
                fiscalAddressConditionType.setIsactive(true);
            }
            fiscalAddressConditionType.setActivatedate(new Timestamp(new Date().getTime()));
            fiscalAddressConditionType.setInactivatedate(null);
            baseFiscalAddressConditionTypeDataService.insert((fiscalAddressConditionType));
            return map(fiscalAddressConditionType, FiscalAddressConditionTypeDto.class);
        }
    }
    public List<FiscalAddressConditionTypeDto> createListFiscalAddressConditionType(List<FiscalAddressConditionTypeDto> fiscalAddressConditionTypeDtos){
        List<Fiscaladdressconditiontype> fiscalAddressConditionTypes = mapAll(fiscalAddressConditionTypeDtos, Fiscaladdressconditiontype.class);
        FiscalAddressConditionTypeDto fiscalAddressConditionTypeDto = new FiscalAddressConditionTypeDto();
        fiscalAddressConditionTypes.stream().forEach((p)->{
            fiscalAddressConditionTypeDto.setCode(p.getCode());
            fiscalAddressConditionTypeDto.setName(p.getName());
            if(p.getIsactive()==null){
                fiscalAddressConditionTypeDto.setIsactive(true);
            }else{
                fiscalAddressConditionTypeDto.setIsactive(p.getIsactive());
            }
            createFiscalAddressConditionType(fiscalAddressConditionTypeDto);
        });
        return mapAll(fiscalAddressConditionTypes, FiscalAddressConditionTypeDto.class);
    }

    public FiscalAddressConditionTypeDto updateFiscalAddressConditionType(String id, FiscalAddressConditionTypeDto fiscalAddressConditionTypeDto){
        Fiscaladdressconditiontype fiscalAddressConditionType = map(fiscalAddressConditionTypeDto,Fiscaladdressconditiontype.class);
        Fiscaladdressconditiontype a = baseFiscalAddressConditionTypeDataService.findById(id);
        fiscalAddressConditionType.setActivatedate(a.getActivatedate());
        fiscalAddressConditionType.setInactivatedate(a.getInactivatedate());
        if(fiscalAddressConditionType.getIsactive()==null){
            fiscalAddressConditionType.setIsactive(true);
        }
        baseFiscalAddressConditionTypeDataService.update(id,fiscalAddressConditionType);
        return map(fiscalAddressConditionType,FiscalAddressConditionTypeDto.class);
    }

    public FiscalAddressConditionTypeDto getFiscalAddressConditionType(String id){
        Fiscaladdressconditiontype fiscalAddressConditionType = baseFiscalAddressConditionTypeDataService.findById(id);
        return map(fiscalAddressConditionType,FiscalAddressConditionTypeDto.class);
    }

    public FiscalAddressConditionTypeDto disableFiscalAddressConditionType(String id){
        Fiscaladdressconditiontype fiscalAddressConditionType = baseFiscalAddressConditionTypeDataService.findById(id);
        fiscalAddressConditionType.setInactivatedate(new Timestamp(new Date().getTime()));
        baseFiscalAddressConditionTypeDataService.update(id,fiscalAddressConditionType);
        return map( baseFiscalAddressConditionTypeDataService.disabled(id), FiscalAddressConditionTypeDto.class);
    }

    public List<FiscalAddressConditionTypeDto> getAllFiscalAddressConditionType(){
        List<Fiscaladdressconditiontype> fiscalAddressConditionTypes = baseFiscalAddressConditionTypeDataService.findIntoList();
        return mapAll(fiscalAddressConditionTypes,FiscalAddressConditionTypeDto.class);
    }

    public List<FiscalAddressConditionTypeDto> getListFiscalAddressConditionType(){
        List<Fiscaladdressconditiontype> fiscalAddressConditionTypes = baseFiscalAddressConditionTypeDataService.listAll();
        return mapAll(fiscalAddressConditionTypes,FiscalAddressConditionTypeDto.class);
    }
}
