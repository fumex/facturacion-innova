package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Documentidentitytype;

import innovar.io.person.models.tables.pojos.Economicactivity;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentIdentityTypeDataService extends RepositoryPostgressService<Documentidentitytype> {

    public DocumentIdentityTypeDataService(DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Documentidentitytype.class);
    }

    @Override
    public innovar.io.person.models.tables.Documentidentitytype getTable() {
        return Tables.DOCUMENTIDENTITYTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.DOCUMENTIDENTITYTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.DOCUMENTIDENTITYTYPE.ISACTIVE;
    }

   /* public List<DocumentIdentityType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, DocumentIdentityType.class);
    }

    */
}
