package innovar.io.person.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TelephoneTypeDto {
    public String id;
    public String code;
    public String name;
    public Boolean isactive;
}
