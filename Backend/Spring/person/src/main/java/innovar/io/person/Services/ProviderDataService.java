package innovar.io.person.Services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Client;
import innovar.io.person.models.tables.pojos.Provider;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderDataService extends RepositoryPostgressService<Provider> {

    public List findProviderDocumentNumber(String number){
        return find(getTable().DOCUMENTIDNUMBER.eq(number)).fetchInto(Provider.class);
    }
    public ProviderDataService(DSLContext dsl){
        super(dsl);
    }
    @Override
    public innovar.io.person.models.tables.Provider getTable() {
        return Tables.PROVIDER;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PROVIDER.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PROVIDER.ISACTIVE;
    }
}
