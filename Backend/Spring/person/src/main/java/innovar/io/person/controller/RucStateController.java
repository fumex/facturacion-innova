package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.RucStateService;
import innovar.io.person.dto.RucStateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class RucStateController extends BaseController {
    @Autowired
    RucStateService rucStateService;

    @PostMapping("/rucstate")
    public AppResponse createRucState(@RequestBody() RucStateDto rucStateDto){
        return Response(()->rucStateService.createRucState((rucStateDto)));
    }

    @PostMapping("/rucstateListCreate")
    public AppResponse createListRucState(@RequestBody() List<RucStateDto> rucStateDtos){
        return Response(()->rucStateService.createListRucStates(rucStateDtos));
    }

    @PutMapping("/rucstate/{id}")
    public AppResponse updateRucState(@PathVariable() String id, @RequestBody() RucStateDto rucStateDto){
        return Response(()->rucStateService.updateRucState((id),(rucStateDto)));
    }

    @GetMapping("/rucstate/{id}")
    public AppResponse getRucState(@PathVariable() String id){
        return Response(()->rucStateService.getRucState(id));
    }

    @GetMapping("/rucstates")
    public AppResponse getAllRucState(){
        return Response(()->rucStateService.getAllRucStates());
    }

    @GetMapping("/ListRucStates")
    public AppResponse getListRucState(){
        return Response(()->rucStateService.getListRucStates());
    }

    @DeleteMapping("/rucstate/{id}")
    public AppResponse disabledRucState(@PathVariable() String id){
        return Response(()->rucStateService.disableRucState(id));
    }
}
