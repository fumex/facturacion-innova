package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Phone;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

@Service
public class PhoneDataService  extends RepositoryPostgressService<Phone> {
    public PhoneDataService(DSLContext dsl){
        super(dsl);
    }
    @Override
    public innovar.io.person.models.tables.Phone getTable() {
        return Tables.PHONE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PHONE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PHONE.ISACTIVE;
    }
}
