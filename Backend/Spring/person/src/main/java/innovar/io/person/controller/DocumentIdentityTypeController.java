package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.DocumentIdentityTypeService;

import innovar.io.person.dto.DocumentIdentityTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class DocumentIdentityTypeController extends BaseController {
    @Autowired
    DocumentIdentityTypeService documentIdentityTypeService;

    @PostMapping("/documentidentitytype")
    public AppResponse createDocumentIdentityType(@RequestBody() DocumentIdentityTypeDto typeDocumentIdentityDto){
        return Response(()-> documentIdentityTypeService.createDocumentIdentityType((typeDocumentIdentityDto)));
    }

    @PostMapping("/documentidentitytypeListCreate")
    public AppResponse createListDocumentIdentityType(@RequestBody() List<DocumentIdentityTypeDto> economicActivityCompanyDtos){
        return Response(()->documentIdentityTypeService.createListDocumentIdentityType(economicActivityCompanyDtos));
    }

    @PutMapping("/documentidentitytype/{id}")
    public AppResponse updateDocumentIdentityType(@PathVariable() String id,@RequestBody() DocumentIdentityTypeDto typeDocumentIdentityDto){
        return Response(()-> documentIdentityTypeService.updateDocumentIdentityType((id),(typeDocumentIdentityDto)));
    }
    @GetMapping("/documentidentitytype/{id}")
    public AppResponse getDocumentIdentityType(String id){return Response(()-> documentIdentityTypeService.getDocumentIdentityType(id));}

    @GetMapping("/documentidentitytypes")
    public AppResponse getAllDocuments(){
        return Response(()-> documentIdentityTypeService.getAllDocumentIdentityTypes());
    }

    @GetMapping("/ListDocumentIdentityTypes")
    public AppResponse getListDocuments(){
        return Response(()-> documentIdentityTypeService.getListDocumentIdentityTypes());
    }

    @DeleteMapping("/documentidentitytype/{id}")
    public AppResponse disabledDocument(@PathVariable() String id){
        return Response(()-> documentIdentityTypeService.disabledDocumentIdentityType(id));
    }
}
