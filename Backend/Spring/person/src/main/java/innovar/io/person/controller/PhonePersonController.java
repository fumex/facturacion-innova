package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.PhonePersonService;
import innovar.io.person.dto.PhonePersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class PhonePersonController extends BaseController {
    @Autowired
    PhonePersonService phonePersonService;

    @PostMapping("/phoneperson")
    public AppResponse createPhonePerson(@RequestBody() PhonePersonDto phoneDto){
        return Response(()-> phonePersonService.createPhonePerson((phoneDto)));
    }

    @PostMapping("/phonepersonListCreate")
    public AppResponse createListPhonePerson(@RequestBody() List<PhonePersonDto> phonePersonDtos){
        return Response(()->phonePersonService.createListPhonePerson(phonePersonDtos));
    }

    @PutMapping("/phoneperson/{id}")
    public AppResponse updatePhonePerson(@PathVariable() String id, @RequestBody() PhonePersonDto phoneDto){
        return Response(()-> phonePersonService.updatePhonePerson((id),(phoneDto)));
    }

    @GetMapping("/phoneperson/{id}")
    public AppResponse getPhonePerson(@PathVariable() String id){
        return Response(()-> phonePersonService.getPhonePerson(id));
    }

    @GetMapping("/phonepersons")
    public AppResponse getAllPhonePerson(){
        return Response(()-> phonePersonService.getAllPhonePerson());
    }


    @DeleteMapping("/phoneperson/{id}")
    public AppResponse disabledPhonePerson(@PathVariable() String id){
        return Response(()-> phonePersonService.disabledPhonePerson(id));
    }

    @GetMapping("/findPhonesPerson/{id}")
    public AppResponse findPhonePerson(@PathVariable() String id){
        return Response(()->phonePersonService.findPhonesPerson(id));
    }
}
