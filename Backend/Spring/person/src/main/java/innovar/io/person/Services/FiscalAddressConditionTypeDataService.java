package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;

import innovar.io.person.models.tables.pojos.Fiscaladdressconditiontype;
import innovar.io.person.models.tables.pojos.Padron;
import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FiscalAddressConditionTypeDataService extends RepositoryPostgressService<Fiscaladdressconditiontype>{

    public FiscalAddressConditionTypeDataService(DSLContext dsl){ super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Fiscaladdressconditiontype.class);
    }

    @Override
    public innovar.io.person.models.tables.Fiscaladdressconditiontype getTable() {
        return Tables.FISCALADDRESSCONDITIONTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.FISCALADDRESSCONDITIONTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.FISCALADDRESSCONDITIONTYPE.ISACTIVE;
    }

    /* public List<FiscalAddressConditionType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, FiscalAddressConditionType.class);
    }
    */
}
