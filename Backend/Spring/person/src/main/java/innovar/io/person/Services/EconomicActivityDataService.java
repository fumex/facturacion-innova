package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Economicactivity;
import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EconomicActivityDataService extends RepositoryPostgressService<Economicactivity> {

    public EconomicActivityDataService (DSLContext dsl){ super(dsl);}
    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Economicactivity.class);
    }

    @Override
    public innovar.io.person.models.tables.Economicactivity getTable() {
        return Tables.ECONOMICACTIVITY;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.ECONOMICACTIVITY.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.ECONOMICACTIVITY.ISACTIVE;
    }

   /* public List<EconomicActivity> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query,EconomicActivity.class);
    }

    */
}
