package innovar.io.person.Services;



import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Taxregime;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaxRegimeDataService extends RepositoryPostgressService<Taxregime> {

    public TaxRegimeDataService(DSLContext dsl){ super (dsl);}

    /*public List<TaxRegime> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, TaxRegime.class);
    }*/

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Taxregime.class);
    }

    @Override
    public innovar.io.person.models.tables.Taxregime getTable() {
        return Tables.TAXREGIME;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.TAXREGIME.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.TAXREGIME.ISACTIVE;
    }
}
