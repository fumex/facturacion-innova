package innovar.io.person.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PersonDto {
    public String id;
    public String documentidnumber;
    public String taxpayertypecode;
    public String documentidcode;
    public String name;
}
