package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Phoneperson;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhonePersonDataService extends RepositoryPostgressService<Phoneperson> {

    public PhonePersonDataService (DSLContext dsl){
        super(dsl);
    }
    public List findPersonPhones(String idPerson){
        return find(getTable().PERSONID.eq(idPerson)).fetchInto(Phoneperson.class);
    }
    @Override
    public innovar.io.person.models.tables.Phoneperson getTable() {
        return Tables.PHONEPERSON;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PHONEPERSON.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PHONEPERSON.ISACTIVE;
    }
    /*public List<PhonePerson> findPersonPhones(String idPerson){
        Query query= new Query();
        query.addCriteria(Criteria.where("personId").is(idPerson));
        return mongoTemplate.find(query, PhonePerson.class);
    }*/
}
