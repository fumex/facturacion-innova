package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.CompanyService;
import innovar.io.person.dto.CompanyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PersonApplication.host)
public class CompanyController extends BaseController {
    @Autowired
    CompanyService companyService;

    @PostMapping("/company")
    public AppResponse createCompany(@RequestBody()CompanyDto companyDto){
        return Response(()->companyService.createCompany((companyDto)));
    }
    @PutMapping("/company/{id}")
    public AppResponse updateCompany(@PathVariable() String id, @RequestBody() CompanyDto companyDto){
        return Response(()->companyService.updateCompany((id),(companyDto)));
    }
    @GetMapping("/company/{id}")
    public AppResponse getCompany(@PathVariable() String id){
        return Response(()->companyService.getCompany(id));
    }

    @GetMapping("/companies")
    public AppResponse getAllCompanies(){
        return Response(()->companyService.getAllCompany());
    }

    @GetMapping("/ListCompanies")
    public AppResponse getListCompanies(){
        return Response(()->companyService.getListCompany());
    }

    @DeleteMapping("/company/{id}")
    public AppResponse disabledCompany(@PathVariable() String id){
        return Response(()->companyService.disabledCompany(id));
    }

}
