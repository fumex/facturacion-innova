package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Client;
import innovar.io.person.models.tables.pojos.Company;

import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CompanyDataService extends RepositoryPostgressService<Company> {
    public CompanyDataService(DSLContext dsl){ super(dsl);}

    public List findCompanyDocumentNumber(String number){
        return find(getTable().DOCUMENTIDNUMBER.eq(number)).fetchInto(Company.class);
    }
    @Override
    public innovar.io.person.models.tables.Company getTable() {
        return Tables.COMPANY;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.COMPANY.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.COMPANY.ISACTIVE;
    }

}
