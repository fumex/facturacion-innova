package innovar.io.person.models.tables.pojos;

import innovar.io.core.models.BaseModel;

public class Person extends BaseModel {
    protected String    documentidnumber;
    protected String    taxpayertypecode;
    protected String    documentidcode;
    protected String    name;
    protected String    tradename;
}
