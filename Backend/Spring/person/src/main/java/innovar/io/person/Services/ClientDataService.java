package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Client;

import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientDataService extends RepositoryPostgressService<Client> {

    public ClientDataService (DSLContext dsl) {super(dsl);}

    public List findClientDocumentIdentity(String codeDocumentIdentity, String number){
        return find(getTable().DOCUMENTIDCODE.eq(codeDocumentIdentity)
                .and(getTable().DOCUMENTIDNUMBER.eq(number)))
                .fetchInto(Client.class);
    }
    public List findClientDocumentNumber(String number){
        return find(getTable().DOCUMENTIDNUMBER.eq(number)).fetchInto(Client.class);
    }

    @Override
    public innovar.io.person.models.tables.Client getTable() {
        return Tables.CLIENT;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.CLIENT.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.CLIENT.ISACTIVE;
    }

    /*public List<Client> findClientDocumentIdentity(String codeDocumentIdentity, String number){
        Query query= new Query();
        query.addCriteria(Criteria.where("documentIdCode").is(codeDocumentIdentity).and("documentIdNumber").is(number));
        return mongoTemplate.find(query, Client.class);
    }*/
}
