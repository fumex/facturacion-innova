package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.EconomicActivityCompanyDataService;
import innovar.io.person.dto.EconomicActivityCompanyDto;


import innovar.io.person.models.tables.pojos.Economicactivitycompany;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class EconomicActivityCompanyService extends BaseBussinesService {
    @Autowired
    EconomicActivityCompanyDataService baseEconomicActivityCompany;

    public EconomicActivityCompanyDto createEconomicActivityCompany(EconomicActivityCompanyDto economicActivityCompanyDto){
        Economicactivitycompany economicActivityCompany =map(economicActivityCompanyDto , Economicactivitycompany.class);
        if(economicActivityCompany.getIsactive()==null){
            economicActivityCompany.setIsactive(true);
        }
        baseEconomicActivityCompany.insert(economicActivityCompany);
        return map(economicActivityCompany, EconomicActivityCompanyDto.class);
    }

    public List<EconomicActivityCompanyDto> createListEconomicActivityCompany(List<EconomicActivityCompanyDto> economicActivityCompanyDtos){
        List<Economicactivitycompany> economicActivityCompanies = mapAll(economicActivityCompanyDtos, Economicactivitycompany.class);
        EconomicActivityCompanyDto economicActivityCompanyDto = new EconomicActivityCompanyDto();
        economicActivityCompanies.stream().forEach((p)->{
            economicActivityCompanyDto.setCompanyid(p.getCompanyid());
            economicActivityCompanyDto.setEconomicactivityid(p.getEconomicactivityid());
            if(p.getIsactive()==null){
                economicActivityCompanyDto.setIsactive(true);
            }
            else{
                economicActivityCompanyDto.setIsactive(p.getIsactive());
            }
            createEconomicActivityCompany(economicActivityCompanyDto);
        });
        return mapAll(economicActivityCompanies, EconomicActivityCompanyDto.class);
    }

    public EconomicActivityCompanyDto updateEconomicActivityCompany(String id, EconomicActivityCompanyDto economicActivityCompanyDto){
        Economicactivitycompany economicActivityCompany=map(economicActivityCompanyDto, Economicactivitycompany.class);
        Economicactivitycompany a =baseEconomicActivityCompany.findById(id);
        economicActivityCompany.setUpdatedon(new Timestamp(new Date().getTime()));
        if(economicActivityCompany.getIsactive()==null){
            economicActivityCompany.setIsactive(true);
        }
        baseEconomicActivityCompany.update(id, economicActivityCompany);
        return map(economicActivityCompany, EconomicActivityCompanyDto.class);
    }
    public EconomicActivityCompanyDto getEconomicActivityCompany(String id){
        Economicactivitycompany economicActivityCompany = baseEconomicActivityCompany.findById(id);
        return map(economicActivityCompany, EconomicActivityCompanyDto.class);
    }
    public EconomicActivityCompanyDto disabledEconomicActivityCompany(String id){
        Economicactivitycompany economicActivityCompany = baseEconomicActivityCompany.findById(id);
        baseEconomicActivityCompany.update(id,economicActivityCompany);
        return map(baseEconomicActivityCompany.disabled(id), EconomicActivityCompanyDto.class);
    }
    public List<EconomicActivityCompanyDto> getAllEconomicActivityCompany(){
        List<Economicactivitycompany> economicActivityCompanies = baseEconomicActivityCompany.listAll();
        return mapAll(economicActivityCompanies, EconomicActivityCompanyDto.class);
    }
}
