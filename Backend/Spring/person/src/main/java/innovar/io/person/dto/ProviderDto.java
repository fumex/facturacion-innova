package innovar.io.person.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProviderDto extends PersonDto {
    public String tradename;
    public boolean foreigntradeactivityflag;
    public String providertype;
    public Boolean isactive;
}
