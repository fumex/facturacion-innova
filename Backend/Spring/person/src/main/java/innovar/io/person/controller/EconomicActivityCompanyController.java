package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.EconomicActivityCompanyService;
import innovar.io.person.dto.EconomicActivityCompanyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class EconomicActivityCompanyController extends BaseController {
    @Autowired
    EconomicActivityCompanyService economicActivityCompanyService;

    @PostMapping("/economicactivitycompany")
    public AppResponse createEconomicActivityCompany(@RequestBody() EconomicActivityCompanyDto economicActivityCompanyDto){
        return Response(()-> economicActivityCompanyService.createEconomicActivityCompany((economicActivityCompanyDto)));
    }

    @PostMapping("/economicactivitycompanyListCreate")
    public AppResponse createListEconomicActivityCompany(@RequestBody() List<EconomicActivityCompanyDto> economicActivityCompanyDtos){
        return Response(()->economicActivityCompanyService.createListEconomicActivityCompany(economicActivityCompanyDtos));
    }

    @PutMapping("/economicactivitycompany/{id}")
    public AppResponse updateEconomicActivityCompany(@PathVariable() String id, @RequestBody() EconomicActivityCompanyDto economicActivityCompanyDto){
        return Response(()-> economicActivityCompanyService.updateEconomicActivityCompany((id),(economicActivityCompanyDto)));
    }

    @GetMapping("/economicactivitycompany/{id}")
    public AppResponse getEconomicActivityCompany(@PathVariable() String id){
        return Response(()-> economicActivityCompanyService.getEconomicActivityCompany(id));
    }

    @GetMapping("/economicactivitycompanies")
    public AppResponse getAllEconomicActivityCompany(){
        return Response(()-> economicActivityCompanyService.getAllEconomicActivityCompany());
    }

    @DeleteMapping("/economicactivitycompanies/{id}")
    public AppResponse disabledEconomicActivityCompany(@PathVariable() String id){
        return Response(()-> economicActivityCompanyService.disabledEconomicActivityCompany(id));
    }
}
