package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.TaxRegimeService;
import innovar.io.person.dto.TaxRegimeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class TaxRegimeController extends BaseController {
    @Autowired
    TaxRegimeService taxRegimeService;

    @PostMapping("/taxregime")
    public AppResponse createTaxRegime(@RequestBody() TaxRegimeDto regimeTaxDto){
        return Response(()-> taxRegimeService.createTaxRegime((regimeTaxDto)));
    }

    @PostMapping("/taxregimeListCreate")
    public AppResponse createListTaxRegime(@RequestBody() List<TaxRegimeDto> taxRegimeDtos){
        return Response(()->taxRegimeService.createListTaxRegime(taxRegimeDtos));
    }

    @PutMapping("/taxregime/{id}")
    public AppResponse updateTaxRegime(@PathVariable() String id, @RequestBody() TaxRegimeDto regimeTaxDto){
        return Response(()-> taxRegimeService.updateTaxRegime((id),(regimeTaxDto)));
    }

    @GetMapping("/taxregime/{id}")
    public AppResponse getTaxRegime(@PathVariable() String id){
        return Response(()-> taxRegimeService.getTaxRegime(id));
    }

    @GetMapping("/taxregimes")
    public AppResponse getAllTaxRegime(){
        return Response(()-> taxRegimeService.getAllTaxRegimes());
    }

    @GetMapping("/ListTaxRegimes")
    public AppResponse getListTaxRegime(){
        return Response(()-> taxRegimeService.getListTaxRegimes());
    }

    @DeleteMapping("/taxregime/{id}")
    public AppResponse disabledTaxRegime(@PathVariable() String id){
        return Response(()-> taxRegimeService.disabledTaxRegime(id));
    }
}
