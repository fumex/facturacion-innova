/*
 * This file is generated by jOOQ.
 */
package innovar.io.person.models.tables.pojos;


import innovar.io.core.models.BaseModel;

import javax.annotation.Generated;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Padronperson extends BaseModel implements Serializable {

    private static final long serialVersionUID = 2133191807;


    private String    companyid;
    private String    padronid;

    private String    class_;

    public Padronperson() {}

    public Padronperson(Padronperson value) {
        this.id = value.id;
        this.companyid = value.companyid;
        this.padronid = value.padronid;
        this.createdon = value.createdon;
        this.createdby = value.createdby;
        this.updatedon = value.updatedon;
        this.updatedby = value.updatedby;
        this.isactive = value.isactive;
        this.class_ = value.class_;
    }

    public Padronperson(
        String    id,
        String    companyid,
        String    padronid,
        Timestamp createdon,
        String    createdby,
        Timestamp updatedon,
        String    updatedby,
        Boolean   isactive,
        String    class_
    ) {
        this.id = id;
        this.companyid = companyid;
        this.padronid = padronid;
        this.createdon = createdon;
        this.createdby = createdby;
        this.updatedon = updatedon;
        this.updatedby = updatedby;
        this.isactive = isactive;
        this.class_ = class_;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyid() {
        return this.companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String getPadronid() {
        return this.padronid;
    }

    public void setPadronid(String padronid) {
        this.padronid = padronid;
    }

    public Timestamp getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Timestamp createdon) {
        this.createdon = createdon;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Timestamp getUpdatedon() {
        return this.updatedon;
    }

    public void setUpdatedon(Timestamp updatedon) {
        this.updatedon = updatedon;
    }

    public String getUpdatedby() {
        return this.updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getClass_() {
        return this.class_;
    }

    public void setClass_(String class_) {
        this.class_ = class_;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Padronperson (");

        sb.append(id);
        sb.append(", ").append(companyid);
        sb.append(", ").append(padronid);
        sb.append(", ").append(createdon);
        sb.append(", ").append(createdby);
        sb.append(", ").append(updatedon);
        sb.append(", ").append(updatedby);
        sb.append(", ").append(isactive);
        sb.append(", ").append(class_);

        sb.append(")");
        return sb.toString();
    }
}
