package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.PhoneDataService;
import innovar.io.person.Services.PhonePersonDataService;
import innovar.io.person.dto.PhonePersonDto;

import innovar.io.person.models.tables.pojos.Phone;
import innovar.io.person.models.tables.pojos.Phoneperson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PhonePersonService extends BaseBussinesService {

    @Autowired
    PhonePersonDataService basePhonePersonDataService;
    @Autowired
    PhoneDataService basePhoneDataService;

    public PhonePersonDto createPhonePerson(PhonePersonDto phonePersonDto){
        Phoneperson phonePerson =map(phonePersonDto , Phoneperson.class);
        if(phonePerson.getIsactive()==null){
            phonePerson.setIsactive(true);
        }
        basePhonePersonDataService.insert(phonePerson);
        return map(phonePerson, PhonePersonDto.class);
    }
    public List<PhonePersonDto> createListPhonePerson(List<PhonePersonDto> phonePersonDtos){
        List<Phoneperson> phonePersonList = mapAll(phonePersonDtos, Phoneperson.class);
        PhonePersonDto phonePersonDto = new PhonePersonDto();
        phonePersonList.stream().forEach((p)->{
            phonePersonDto.setPersonid(p.getPersonid());
            phonePersonDto.setPhoneid(p.getPhoneid());
            if(p.getIsactive()==null){
                phonePersonDto.setIsactive(true);
            }else{
                phonePersonDto.setIsactive(p.getIsactive());
            }
            createPhonePerson(phonePersonDto);
        });
        return mapAll(phonePersonList, PhonePersonDto.class);
    }

    public PhonePersonDto updatePhonePerson(String id, PhonePersonDto phonePersonDto){
        Phoneperson phoneperson=map(phonePersonDto, Phoneperson.class);
        Phoneperson a = basePhonePersonDataService.findById(id);
        phoneperson.setUpdatedon(new Timestamp(new Date().getTime()));
        if(phoneperson.getIsactive()==null){
            phoneperson.setIsactive(true);
        }
        basePhonePersonDataService.update(id, phoneperson);
        return map(phoneperson, PhonePersonDto.class);
    }
    public PhonePersonDto getPhonePerson(String id){
        Phoneperson phonePerson = basePhonePersonDataService.findById(id);
        return map(phonePerson, PhonePersonDto.class);
    }
    public PhonePersonDto disabledPhonePerson(String id){
        Phoneperson phonePerson = basePhonePersonDataService.findById(id);
        basePhonePersonDataService.update(id,phonePerson);
        return map(basePhonePersonDataService.disabled(id), PhonePersonDto.class);
    }
    public List<PhonePersonDto> getAllPhonePerson(){
        List<Phoneperson> phonePersons = basePhonePersonDataService.listAll();
        return mapAll(phonePersons, PhonePersonDto.class);
    }
    public List<Phone> findPhonesPerson(String idPerson){
        List<Phoneperson> list = basePhonePersonDataService.findPersonPhones(idPerson);
        List<Phone> phones = new ArrayList<Phone>();
        list.stream().forEach((p)->{
            Phone phone = basePhoneDataService.findById(p.getPhoneid());
            if(phone.getIsactive()==true) {
                phones.add(phone);
            }
        });
        return mapAll(phones,Phone.class);
    }
}
