package innovar.io.person.Services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Economicactivitycompany;

import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EconomicActivityCompanyDataService extends RepositoryPostgressService<Economicactivitycompany> {

    public EconomicActivityCompanyDataService(DSLContext dsl){ super(dsl);}

    @Override
    public innovar.io.person.models.tables.Economicactivitycompany getTable() {
        return Tables.ECONOMICACTIVITYCOMPANY;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.ECONOMICACTIVITYCOMPANY.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.ECONOMICACTIVITYCOMPANY.ISACTIVE;
    }
}
