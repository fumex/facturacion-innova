/*
 * This file is generated by jOOQ.
 */
package innovar.io.person.models.tables;


import innovar.io.person.models.Indexes;
import innovar.io.person.models.Keys;
import innovar.io.person.models.Public;
import innovar.io.person.models.tables.records.EconomicactivityRecord;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import javax.annotation.Generated;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Economicactivity extends TableImpl<EconomicactivityRecord> {

    private static final long serialVersionUID = -245003346;

    /**
     * The reference instance of <code>public.economicactivity</code>
     */
    public static final Economicactivity ECONOMICACTIVITY = new Economicactivity();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<EconomicactivityRecord> getRecordType() {
        return EconomicactivityRecord.class;
    }

    /**
     * The column <code>public.economicactivity.id</code>.
     */
    public final TableField<EconomicactivityRecord, String> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.VARCHAR(24).nullable(false), this, "");

    /**
     * The column <code>public.economicactivity.code</code>.
     */
    public final TableField<EconomicactivityRecord, String> CODE = createField(DSL.name("code"), org.jooq.impl.SQLDataType.VARCHAR(4), this, "");

    /**
     * The column <code>public.economicactivity.name</code>.
     */
    public final TableField<EconomicactivityRecord, String> NAME = createField(DSL.name("name"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.economicactivity.activatedate</code>.
     */
    public final TableField<EconomicactivityRecord, Timestamp> ACTIVATEDATE = createField(DSL.name("activatedate"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.economicactivity.inactivatedate</code>.
     */
    public final TableField<EconomicactivityRecord, Timestamp> INACTIVATEDATE = createField(DSL.name("inactivatedate"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.economicactivity.createdon</code>.
     */
    public final TableField<EconomicactivityRecord, Timestamp> CREATEDON = createField(DSL.name("createdon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.economicactivity.createdby</code>.
     */
    public final TableField<EconomicactivityRecord, String> CREATEDBY = createField(DSL.name("createdby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.economicactivity.updatedon</code>.
     */
    public final TableField<EconomicactivityRecord, Timestamp> UPDATEDON = createField(DSL.name("updatedon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.economicactivity.updatedby</code>.
     */
    public final TableField<EconomicactivityRecord, String> UPDATEDBY = createField(DSL.name("updatedby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.economicactivity.isactive</code>.
     */
    public final TableField<EconomicactivityRecord, Boolean> ISACTIVE = createField(DSL.name("isactive"), org.jooq.impl.SQLDataType.BOOLEAN, this, "");

    /**
     * The column <code>public.economicactivity.class</code>.
     */
    public final TableField<EconomicactivityRecord, String> CLASS = createField(DSL.name("class"), org.jooq.impl.SQLDataType.VARCHAR(200), this, "");

    /**
     * Create a <code>public.economicactivity</code> table reference
     */
    public Economicactivity() {
        this(DSL.name("economicactivity"), null);
    }

    /**
     * Create an aliased <code>public.economicactivity</code> table reference
     */
    public Economicactivity(String alias) {
        this(DSL.name(alias), ECONOMICACTIVITY);
    }

    /**
     * Create an aliased <code>public.economicactivity</code> table reference
     */
    public Economicactivity(Name alias) {
        this(alias, ECONOMICACTIVITY);
    }

    private Economicactivity(Name alias, Table<EconomicactivityRecord> aliased) {
        this(alias, aliased, null);
    }

    private Economicactivity(Name alias, Table<EconomicactivityRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Economicactivity(Table<O> child, ForeignKey<O, EconomicactivityRecord> key) {
        super(child, key, ECONOMICACTIVITY);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.ECONOMICACTIVITY_PKEY);
    }

    @Override
    public UniqueKey<EconomicactivityRecord> getPrimaryKey() {
        return Keys.ECONOMICACTIVITY_PKEY;
    }

    @Override
    public List<UniqueKey<EconomicactivityRecord>> getKeys() {
        return Arrays.<UniqueKey<EconomicactivityRecord>>asList(Keys.ECONOMICACTIVITY_PKEY);
    }

    @Override
    public Economicactivity as(String alias) {
        return new Economicactivity(DSL.name(alias), this);
    }

    @Override
    public Economicactivity as(Name alias) {
        return new Economicactivity(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Economicactivity rename(String name) {
        return new Economicactivity(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Economicactivity rename(Name name) {
        return new Economicactivity(name, null);
    }

    // -------------------------------------------------------------------------
    // Row11 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row11<String, String, String, Timestamp, Timestamp, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row11) super.fieldsRow();
    }
}
