package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.ClientDataService;
import innovar.io.person.dto.ClientDto;
import innovar.io.person.models.tables.pojos.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ClientService extends BaseBussinesService {
    @Autowired
    ClientDataService baseClientDataService;

    public ClientDto createClient(ClientDto clientDto) {
        Client client = map(clientDto, Client.class);
        if(client.getIsactive()==null){
            client.setIsactive(true);
        }
        baseClientDataService.insert((client));
        return map(client, ClientDto.class);
    }

    public ClientDto updateClient(String id, ClientDto clientDto){
        Client client= map(clientDto,Client.class);
        client.setUpdatedon(new Timestamp(new Date().getTime()));
        if(client.getIsactive()==null){
            client.setIsactive(true);
        }
        baseClientDataService.update(id, client);
        return  map(client, ClientDto.class);
    }
    public ClientDto getClient(String id){
        Client client= baseClientDataService.findById(id);
        return map(client, ClientDto.class);
    }

    public List<ClientDto> getAllClient(){
        List<Client> client= baseClientDataService.findIntoList();
        return  mapAll(client, ClientDto.class);
    }

    public List<ClientDto> getListClient(){
        List<Client> client= baseClientDataService.listAll();
        return  mapAll(client, ClientDto.class);
    }

    public ClientDto disabledClient(String id){
        Client client= baseClientDataService.findById(id);
        client.setUpdatedon(new Timestamp(new Date().getTime()));
        baseClientDataService.update(id, client);
        return map(baseClientDataService.disabled(id),ClientDto.class);
    }

    public List<ClientDto> findClientDocumentIdentity(String codeDocumentIdentity, String number){
        List<Client> client= baseClientDataService.findClientDocumentIdentity(codeDocumentIdentity, number);
        return mapAll(client, ClientDto.class);
    }


}
