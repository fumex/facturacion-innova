package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.DocumentIdentityTypeDataService;
import innovar.io.person.dto.DocumentIdentityTypeDto;

import innovar.io.person.models.tables.pojos.Documentidentitytype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

@Service
public class DocumentIdentityTypeService extends BaseBussinesService {
    @Autowired
    DocumentIdentityTypeDataService baseDocumentIdentityTypeDataService;

    public DocumentIdentityTypeDto createDocumentIdentityType(DocumentIdentityTypeDto typeDocumentIdentityDto){
        Documentidentitytype typeDocumentIdentity = map(typeDocumentIdentityDto, Documentidentitytype.class);
        List<Documentidentitytype> typeDocumentIdentities = baseDocumentIdentityTypeDataService.findCode(typeDocumentIdentityDto.code);
        if(typeDocumentIdentities.size()>0){
            String id = typeDocumentIdentities.get(0).getId();
            Documentidentitytype typeDocumentIdentity1 = baseDocumentIdentityTypeDataService.findById(id);
            typeDocumentIdentity1.setActivatedate(new Timestamp(new Date().getTime()));
            baseDocumentIdentityTypeDataService.update(id, typeDocumentIdentity1);
            return map(typeDocumentIdentity1, DocumentIdentityTypeDto.class);
        }else{
            if(typeDocumentIdentity.getIsactive()==null){
                typeDocumentIdentity.setIsactive(true);
            }
            typeDocumentIdentity.setActivatedate(new Timestamp(new Date().getTime()));
            typeDocumentIdentity.setInactivatedate(null);
            baseDocumentIdentityTypeDataService.insert(typeDocumentIdentity);
            return map(typeDocumentIdentity, DocumentIdentityTypeDto.class);
        }
    }

    public List<DocumentIdentityTypeDto> createListDocumentIdentityType(List<DocumentIdentityTypeDto> documentIdentityTypeDtos){
        List<Documentidentitytype> documentIdentityTypes = mapAll(documentIdentityTypeDtos, Documentidentitytype.class);
        DocumentIdentityTypeDto documentIdentityTypeDto = new DocumentIdentityTypeDto();
        documentIdentityTypes.stream().forEach((p)->{
            documentIdentityTypeDto.setCode(p.getCode());
            documentIdentityTypeDto.setName(p.getName());
            if(p.getIsactive()==null){
                documentIdentityTypeDto.setIsactive(true);
            }else{
                documentIdentityTypeDto.setIsactive(p.getIsactive());
            }
            createDocumentIdentityType(documentIdentityTypeDto);
        });
        return mapAll(documentIdentityTypes, DocumentIdentityTypeDto.class);
    }

    public DocumentIdentityTypeDto updateDocumentIdentityType(String id, DocumentIdentityTypeDto typeDocumentIdentityDto){
        Documentidentitytype typeDocumentIdentity=map(typeDocumentIdentityDto, Documentidentitytype.class);
         Documentidentitytype a = baseDocumentIdentityTypeDataService.findById(id);
        typeDocumentIdentity.setActivatedate(a.getActivatedate());
        typeDocumentIdentity.setInactivatedate(a.getInactivatedate());
        typeDocumentIdentity.setUpdatedon(new Timestamp(new Date().getTime()));
        if(typeDocumentIdentity.getIsactive()==null){
            typeDocumentIdentity.setIsactive(true);
        }
        baseDocumentIdentityTypeDataService.update(id, typeDocumentIdentity);
        return map(typeDocumentIdentity, DocumentIdentityTypeDto.class);
    }
    public DocumentIdentityTypeDto getDocumentIdentityType(String id){
        Documentidentitytype typeDocumentIdentity = baseDocumentIdentityTypeDataService.findById(id);
        return map(typeDocumentIdentity, DocumentIdentityTypeDto.class);
    }
    public DocumentIdentityTypeDto disabledDocumentIdentityType(String id){
        Documentidentitytype typeDocumentIdentity = baseDocumentIdentityTypeDataService.findById(id);
        typeDocumentIdentity.setInactivatedate(new Timestamp(new Date().getTime()));
        baseDocumentIdentityTypeDataService.update(id,typeDocumentIdentity);
        return map(baseDocumentIdentityTypeDataService.disabled(id) , DocumentIdentityTypeDto.class);
    }
     public List<DocumentIdentityTypeDto> getAllDocumentIdentityTypes(){
        List<Documentidentitytype> typeDocumentIdentities = baseDocumentIdentityTypeDataService.findIntoList();
        return mapAll(typeDocumentIdentities, DocumentIdentityTypeDto.class);
     }

    public List<DocumentIdentityTypeDto> getListDocumentIdentityTypes(){
        List<Documentidentitytype> typeDocumentIdentities = baseDocumentIdentityTypeDataService.listAll();
        return mapAll(typeDocumentIdentities, DocumentIdentityTypeDto.class);
    }

}
