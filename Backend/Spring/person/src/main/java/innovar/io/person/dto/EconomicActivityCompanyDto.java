package innovar.io.person.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class EconomicActivityCompanyDto  {
    public String companyid;
    public String economicactivityid;
    public Boolean isactive;
}
