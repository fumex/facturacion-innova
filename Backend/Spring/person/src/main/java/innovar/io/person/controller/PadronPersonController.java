package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.PadronPersonService;
import innovar.io.person.dto.PadronPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class PadronPersonController extends BaseController {
    @Autowired
    PadronPersonService padronPersonService;

    @PostMapping("/padronperson")
    public AppResponse createPadronPerson(@RequestBody() PadronPersonDto padronPersonDto){
        return Response(()-> padronPersonService.createPadronPerson((padronPersonDto)));
    }

    @PostMapping("/padronpersonListCreate")
    public AppResponse createListPadronPerson(@RequestBody() List<PadronPersonDto> padronPersonDtos){
        return Response(()->padronPersonService.createListPadronPerson(padronPersonDtos));
    }


    @PutMapping("/padronperson/{id}")
    public AppResponse updatePadronPerson(@PathVariable() String id, @RequestBody() PadronPersonDto padronPersonDto){
        return Response(()-> padronPersonService.updatePadronPerson((id),(padronPersonDto)));
    }

    @GetMapping("/padronperson/{id}")
    public AppResponse getEconomicActivityCompany(@PathVariable() String id){
        return Response(()-> padronPersonService.getPadronPerson(id));
    }

    @GetMapping("/padronpersons")
    public AppResponse getAllEconomicActivityCompany(){
        return Response(()-> padronPersonService.getAllPadronPerson());
    }

    @DeleteMapping("/padronperson/{id}")
    public AppResponse disabledEconomicActivityCompany(@PathVariable() String id){
        return Response(()-> padronPersonService.disabledPadronPerson(id));
    }
}
