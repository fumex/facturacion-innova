package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.EconomicActivityService;
import innovar.io.person.dto.EconomicActivityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class EconomicActivityController extends BaseController {
    @Autowired
    EconomicActivityService economicActivityService;

    @PostMapping("/economicactivity")
    public AppResponse createEconomicActivity(@RequestBody()EconomicActivityDto economicActivityDto){
        return Response(()->economicActivityService.createEconomicActivity((economicActivityDto)));
    }

    @PostMapping("/economicactivityListCreate")
    public AppResponse createListEconomicActivity(@RequestBody() List<EconomicActivityDto> economicActivityDtos){
        return Response(()->economicActivityService.createListEconomicActivity(economicActivityDtos));
    }

    @PutMapping("/economicactivity/{id}")
    public AppResponse updateEconomicActivity(@PathVariable() String id, @RequestBody() EconomicActivityDto economicActivityDto){
        return Response(()->economicActivityService.updateEconomicActivity((id),(economicActivityDto)));
    }

    @GetMapping("/economicactivity/{id}")
    public AppResponse getEconomicActivity(@PathVariable() String id){
        return Response(()->economicActivityService.getEconomicActivity(id));
    }

    @GetMapping("/economicactivities")
    public AppResponse getAllEconomicActivity(){
        return Response(()->economicActivityService.getAllEconomicActivity());
    }

    @GetMapping("/ListEconomicActivities")
    public AppResponse getListEconomicActivity(){
        return Response(()->economicActivityService.getListEconomicActivity());
    }

    @DeleteMapping("/economicactivity/{id}")
    public AppResponse disabledEconomicActivity(@PathVariable() String id){
        return Response(()->economicActivityService.disableEconomicActivity(id));
    }



}
