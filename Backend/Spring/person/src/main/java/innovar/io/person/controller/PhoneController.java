package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.PhoneService;
import innovar.io.person.dto.PhoneDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PersonApplication.host)
public class PhoneController extends BaseController {
    @Autowired
    PhoneService phoneService;

    @PostMapping("/phone")
    public AppResponse createPhone(@RequestBody() PhoneDto phoneDto){
        return Response(()-> phoneService.createPhone((phoneDto)));
    }

    @PutMapping("/phone/{id}")
    public AppResponse updatePhone(@PathVariable() String id, @RequestBody() PhoneDto phoneDto){
        return Response(()-> phoneService.updatePhone((id),(phoneDto)));
    }

    @GetMapping("/phone/{id}")
    public AppResponse getPhone(@PathVariable() String id){
        return Response(()-> phoneService.getPhone(id));
    }

    @GetMapping("/phones")
    public AppResponse getAllPhone(){
        return Response(()-> phoneService.getAllPhone());
    }

    @GetMapping("/ListPhones")
    public AppResponse getListPhone(){
        return Response(()-> phoneService.getListPhone());
    }

    @DeleteMapping("/phone/{id}")
    public AppResponse disabledPhone(@PathVariable() String id){
        return Response(()-> phoneService.disabledPhone(id));
    }
}
