package innovar.io.person;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "innovar.io")
@EntityScan(basePackages = "innovar.io")
public class PersonApplication {
    public static  final String host= "/api/person";
    public static void main(String[] args) {
        SpringApplication.run(PersonApplication.class, args);
    }

}
