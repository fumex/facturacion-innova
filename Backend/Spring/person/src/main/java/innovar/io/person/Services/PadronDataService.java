package innovar.io.person.Services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.person.models.Tables;
import innovar.io.person.models.tables.pojos.Padron;

import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PadronDataService extends RepositoryPostgressService<Padron> {

    public PadronDataService( DSLContext dsl){ super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Padron.class);
    }

    @Override
    public innovar.io.person.models.tables.Padron getTable() {
        return Tables.PADRON;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PADRON.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PADRON.ISACTIVE;
    }
    /*public List<Padron> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, Padron.class);
    }*/
}
