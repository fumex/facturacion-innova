package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.PadronPersonDataService;
import innovar.io.person.dto.PadronPersonDto;

import innovar.io.person.models.tables.pojos.Padronperson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class PadronPersonService extends BaseBussinesService {
    @Autowired
    PadronPersonDataService basePadronPerson;

    public PadronPersonDto createPadronPerson(PadronPersonDto padronPersonDto){
        Padronperson padronperson =map(padronPersonDto , Padronperson.class);
        if(padronperson.getIsactive()==null){
            padronperson.setIsactive(true);
        }
        basePadronPerson.insert(padronperson);
        return map(padronperson, PadronPersonDto.class);
    }

    public List<PadronPersonDto> createListPadronPerson(List<PadronPersonDto> padronPersonDtos){
        List<Padronperson> padronPersonList = mapAll(padronPersonDtos, Padronperson.class);
        PadronPersonDto padronPersonDto = new PadronPersonDto();
        padronPersonList.stream().forEach((p)->{
            padronPersonDto.setCompanyid(p.getCompanyid());
            padronPersonDto.setPadronid(p.getPadronid());
            if(p.getIsactive()==null){
                padronPersonDto.setIsactive(true);
            }else{
                padronPersonDto.setIsactive(p.getIsactive());
            }
            createPadronPerson(padronPersonDto);
        });
        return mapAll(padronPersonList, PadronPersonDto.class);
    }

    public PadronPersonDto updatePadronPerson(String id, PadronPersonDto padronPersonDto){
        Padronperson padronPerson=map(padronPersonDto, Padronperson.class);
        Padronperson a = basePadronPerson.findById(id);
        padronPerson.setUpdatedon(new Timestamp(new Date().getTime()));
        if(padronPerson.getIsactive()==null){
            padronPerson.setIsactive(true);
        }
        basePadronPerson.update(id, padronPerson);
        return map(padronPerson, PadronPersonDto.class);
    }
    public PadronPersonDto getPadronPerson(String id){
        Padronperson padronPerson = basePadronPerson.findById(id);
        return map(padronPerson, PadronPersonDto.class);
    }
    public PadronPersonDto disabledPadronPerson(String id){
        Padronperson padronPerson = basePadronPerson.findById(id);
        basePadronPerson.update(id,padronPerson);
        return map(basePadronPerson.disabled(id), PadronPersonDto.class);
    }
    public List<PadronPersonDto> getAllPadronPerson(){
        List<Padronperson> padronPersonActives = basePadronPerson.listAll();
        return mapAll(padronPersonActives, PadronPersonDto.class);
    }
}
