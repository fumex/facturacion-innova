package innovar.io.person.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.person.Services.RucStateDataService;
import innovar.io.person.dto.RucStateDto;

import innovar.io.person.models.tables.pojos.Rucstate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class RucStateService extends BaseBussinesService {
    @Autowired
    RucStateDataService baseRucStateDataService;

    public RucStateDto createRucState(RucStateDto rucStateDto){
        Rucstate rucState = map(rucStateDto, Rucstate.class);
        List<Rucstate> rucStates =  baseRucStateDataService.findCode(rucStateDto.code);
        if(rucStates.size()>0){
            String id = rucStates.get(0).getId();
            Rucstate rucState1 = baseRucStateDataService.findById(id);
            rucState1.setActivatedate(new Timestamp(new Date().getTime()));
            baseRucStateDataService.update(id,rucState1);
            return map(rucState1, RucStateDto.class);
        }else{
            if(rucState.getIsactive()==null){
                rucState.setIsactive(true);
            }
            rucState.setActivatedate(new Timestamp(new Date().getTime()));
            rucState.setInactivatedate(null);
            baseRucStateDataService.insert((rucState));
            return map(rucState, RucStateDto.class);
        }
    }

    public List<RucStateDto> createListRucStates(List<RucStateDto> rucStateDtos){
        List<Rucstate> rucStates = mapAll(rucStateDtos, Rucstate.class);
        RucStateDto rucStateDto = new RucStateDto();
        rucStates.stream().forEach((p)->{
            rucStateDto.setCode(p.getCode());
            rucStateDto.setName(p.getName());
            rucStateDto.setConcept(p.getConcept());
            rucStateDto.setAction(p.getAction());
            if(p.getIsactive()==null){
                rucStateDto.setIsactive(true);
            }else{
                rucStateDto.setIsactive(p.getIsactive());
            }
            createRucState(rucStateDto);
        });
        return mapAll(rucStates, RucStateDto.class);
    }

    public RucStateDto updateRucState(String id, RucStateDto rucStateDto){
        Rucstate rucState =  map(rucStateDto,Rucstate.class);
        Rucstate a = baseRucStateDataService.findById(id);
        rucState.setActivatedate(a.getActivatedate());
        rucState.setInactivatedate(a.getInactivatedate());
        baseRucStateDataService.update(id, rucState);
        return map(rucState,RucStateDto.class);
    }

    public RucStateDto getRucState(String id){
        Rucstate rucState =  baseRucStateDataService.findById(id);
        return map(rucState, RucStateDto.class);
    }

    public List<RucStateDto> getAllRucStates(){
        List<Rucstate> rucStates = baseRucStateDataService.findIntoList();
        return mapAll(rucStates, RucStateDto.class);
    }

    public List<RucStateDto> getListRucStates(){
        List<Rucstate> rucStates = baseRucStateDataService.listAll();
        return mapAll(rucStates, RucStateDto.class);
    }

    public RucStateDto disableRucState(String id){
        Rucstate rucState= baseRucStateDataService.findById(id);
        rucState.setInactivatedate(new Timestamp(new Date().getTime()));
        baseRucStateDataService.update(id, rucState);
        return map(baseRucStateDataService.disabled(id), RucStateDto.class);
    }

}
