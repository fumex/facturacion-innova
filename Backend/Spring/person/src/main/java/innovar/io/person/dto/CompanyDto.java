package innovar.io.person.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CompanyDto extends PersonDto{
    public Date activitystartdate;
    public Date electronictransmitterdate;
    public boolean foreigntradeactivityflag;
    public String fiscaladdressconditionstypecode;
    public String tradename;
    public String taxregimecode;
    public String rucstatuscode;
    public Boolean isactive;
}
