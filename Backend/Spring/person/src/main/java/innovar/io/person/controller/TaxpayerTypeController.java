package innovar.io.person.controller;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.person.PersonApplication;
import innovar.io.person.baseBussinesService.TaxpayerTypeService;
import innovar.io.person.dto.TaxpayerTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonApplication.host)
public class TaxpayerTypeController extends BaseController {
    @Autowired
    TaxpayerTypeService taxpayerTypeService;

    @PostMapping("/taxpayertype")
    public AppResponse createTaxpayerType(@RequestBody() TaxpayerTypeDto typeContributorDto){
        return Response(()-> taxpayerTypeService.createTaxpayerType(typeContributorDto));
    }

    @PostMapping("/taxpayertypeListCreate")
    public AppResponse createListTaxpayerType(@RequestBody() List<TaxpayerTypeDto> taxpayerTypeDtos){
        return Response(()->taxpayerTypeService.createListTaxpayerType(taxpayerTypeDtos));
    }

    @PutMapping("/taxpayertype/{id}")
    public AppResponse updateTaxpayerType(@PathVariable String id ,@RequestBody() TaxpayerTypeDto typeContributorDto){
        return Response(()-> taxpayerTypeService.updateTaxpayerType(id,typeContributorDto));
    }
    @GetMapping("/taxpayertype/{id}")
    public AppResponse getTaxpayerType(@PathVariable String id){
        return Response(()-> taxpayerTypeService.getTaxpayerType(id));
    }

    @GetMapping("/taxpayertypes")
    public AppResponse getAllTaxpayerType(){
        return Response(()-> taxpayerTypeService.getAllTaxpayerType());
    }

    @GetMapping("/ListTaxpayerTypes")
    public AppResponse getListTaxpayerType(){
        return Response(()-> taxpayerTypeService.getListTaxpayerType());
    }

    @DeleteMapping("/taxpayertype/{id}")
    public AppResponse disabledTTaxpayerType(@PathVariable String id){
        return Response(()-> taxpayerTypeService.disabledTaxpayerType(id));
    }

}
