package innovar.io.soapclient;

import innovar.io.soapclient.sunat.BillService;
import innovar.io.soapclient.sunat.BillService_Service;
import innovar.io.soapclient.util.HeaderHandlerResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.DataSource;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.FileOutputStream;

@SpringBootApplication
public class SoapclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoapclientApplication.class, args);
        sendBill_example();
    }

    public static void sendBill_example(){
        try{
            BillService_Service service = new BillService_Service();
            service.setHandlerResolver(new HeaderHandlerResolver());
            BillService port = service.getBillServicePort();
            //
            String fileName = "20100066603-01-F001-1.zip";
            DataSource fds = new FileDataSource("/home/gregori/ZIP/20100066603-01-F001-1.zip");
            DataHandler contentFile =  new DataHandler(fds);
            byte[] result = port.sendBill(fileName,contentFile,"");
            FileOutputStream fileOutputStream = new FileOutputStream("/home/gregori/ZIP/respuesta.zip");
            fileOutputStream.write(result);
            fileOutputStream.close();
            System.out.println("Result =  es"+result);
        }catch (SOAPFaultException ex){
            System.out.println(ex.getFault().getFaultCode());
            System.out.println(ex.getFault().getFaultString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
