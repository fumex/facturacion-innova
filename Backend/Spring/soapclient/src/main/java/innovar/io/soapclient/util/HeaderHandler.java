package innovar.io.soapclient.util;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Set;

public class HeaderHandler implements SOAPHandler<SOAPMessageContext> {
    private final static String login = "20100066603MODDATOS";
    private final static String pwd = "moddatos";

    @Override
    public Set<QName> getHeaders(){
        System.out.println("Client : getHeaders()........");
        return  null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context){
        System.out.println("Client : handleMessage().............");
        Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if(outboundProperty.booleanValue()){
            try{
                String prefix = "wsse";
                String uri = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
                SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
                if(envelope.getHeader()!=null){
                  envelope.getHeader().detachNode();
                }
                SOAPHeader header = envelope.addHeader();
                SOAPElement security = header.addChildElement("Security",prefix, uri);
                SOAPElement usernameToken = security.addChildElement("UsernameToken",prefix);
                SOAPElement username = usernameToken.addChildElement("Username", prefix);
                username.addTextNode(login);
                SOAPElement password = usernameToken.addChildElement("Password", prefix);
                password.addTextNode(pwd);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }else{
            try {

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return outboundProperty;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context){
        System.out.println("Client : handleFault().........");
        return true;
    }

    @Override
    public void close(MessageContext context){
        System.out.println("Client : close()........");
    }
}
