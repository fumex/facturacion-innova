package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;

import innovar.io.product.baseBussinesService.TaxTypeService;
import innovar.io.product.dto.TaxTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class TaxTypeController extends BaseController {

    @Autowired
    TaxTypeService taxTypeService;

    @PostMapping("/taxtype")
    public AppResponse createTaxType(@RequestBody() TaxTypeDto taxTypeDto) {
        return Response(() -> taxTypeService.createTaxType((taxTypeDto)));
    }

    @PostMapping("/taxtypeListCreate")
    public AppResponse createListTaxType(@RequestBody() List<TaxTypeDto> taxTypeDtos) {
        return Response(() -> taxTypeService.createListTaxTypes(taxTypeDtos));
    }

    @PutMapping("taxtype/{id}")
    public AppResponse updateTaxType(@PathVariable() String id, @RequestBody() TaxTypeDto taxTypeDto) {
        return Response(() -> taxTypeService.updateTaxType((id), (taxTypeDto)));
    }

    @GetMapping("/taxtype/{id}")
    public AppResponse getTaxType(@PathVariable() String id) {
        return Response(() -> taxTypeService.getTaxType(id));
    }

    @GetMapping("/taxtypes")
    public AppResponse getAllTaxType() {
        return Response(() -> taxTypeService.getAllTaxType());
    }

    @GetMapping("/listTaxtypes")
    public AppResponse getListTaxType() {
        return Response(() -> taxTypeService.getListTaxType());
    }

    @DeleteMapping("/taxtype/{id}")
    public AppResponse disabledTaxType(@PathVariable() String id) {
        return Response(() -> taxTypeService.disableTaxType(id));
    }
}
