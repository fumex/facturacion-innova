package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.product.dto.ProductPriceTaxDto;
import innovar.io.product.models.tables.pojos.Productpricetax;
import innovar.io.product.services.ProductPriceTaxDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ProductPriceTaxService extends BaseBussinesService {
    @Autowired
    ProductPriceTaxDataService productPriceTaxDataService;

    public ProductPriceTaxDto createProductPriceTax(ProductPriceTaxDto productPriceTaxDto){
        Productpricetax productpricetax = map(productPriceTaxDto, Productpricetax.class);
        if(productpricetax.isactive==null){
            productpricetax.setIsactive(true);
        }
        productPriceTaxDataService.insert((productpricetax));
        return map(productpricetax, ProductPriceTaxDto.class);
    }

    public List<ProductPriceTaxDto> createListProductPriceTax(List<ProductPriceTaxDto> productPriceTaxDtos){
        List<Productpricetax> productpricetaxes = mapAll(productPriceTaxDtos, Productpricetax.class);
        ProductPriceTaxDto productPriceTaxDto = new ProductPriceTaxDto();
        productpricetaxes.stream().forEach((p)->{
            productPriceTaxDto.setIdproductprice(p.getIdproductprice());
            productPriceTaxDto.setIgvinvolvementtypecode(p.getIgvinvolvementtypecode());
            productPriceTaxDto.setIscinvolvementtypecode(p.getIscinvolvementtypecode());
            productPriceTaxDto.setTaxtypecode(p.getTaxtypecode());
            productPriceTaxDto.setUnittaxamount(p.getUnittaxamount());
            if(p.getIsactive()==null){
                productPriceTaxDto.setIsactive(true);
            }else{
                productPriceTaxDto.setIsactive(p.getIsactive());
            }
            createProductPriceTax(productPriceTaxDto);
        });
        return mapAll(productpricetaxes,ProductPriceTaxDto.class);
    }

    public ProductPriceTaxDto updateProductPriceTax(String id, ProductPriceTaxDto productPriceTaxDto){
        Productpricetax productpricetax = map(productPriceTaxDto,Productpricetax.class);
        productpricetax.setUpdatedon(new Timestamp(new Date().getTime()));
        if(productpricetax.isactive==null){
            productpricetax.setIsactive(true);
        }
        productPriceTaxDataService.update(id, productpricetax);
        return  map(productpricetax, ProductPriceTaxDto.class);
    }
    public ProductPriceTaxDto getProductPriceTax(String id){
        Productpricetax productpricetax= productPriceTaxDataService.findById(id);
        return map(productpricetax, ProductPriceTaxDto.class);
    }

    public List<ProductPriceTaxDto> getAllProductPriceTaxes(){
        List<Productpricetax> productpricestax= productPriceTaxDataService.findIntoList();
        return  mapAll(productpricestax, ProductPriceTaxDto.class);
    }

    public List<ProductPriceTaxDto> getListProductPriceTaxes(){
        List<Productpricetax> productpricetaxes= productPriceTaxDataService.listAll();
        return  mapAll(productpricetaxes, ProductPriceTaxDto.class);
    }

    public ProductPriceTaxDto disabledProductPriceTax(String id){
        Productpricetax productpricetax= productPriceTaxDataService.findById(id);
        productpricetax.setUpdatedon(new Timestamp(new Date().getTime()));
        productPriceTaxDataService.update(id, productpricetax);
        return map(productPriceTaxDataService.disabled(id),ProductPriceTaxDto.class);
    }
}
