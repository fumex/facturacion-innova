/*
 * This file is generated by jOOQ.
 */
package innovar.io.product.models.tables.pojos;


import innovar.io.core.models.BaseModel;

import javax.annotation.Generated;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Productdetail extends BaseModel implements Serializable {

    private static final long serialVersionUID = -1422219682;


    private String    batch;
    private String    brand;
    private String    color;
    private String    dimension;
    private String    model;
    private String    serie;
    private Date      expirationdate;
    private Date      productiondate;
    private String    productcode;
    private String    unitcode;

    private String    class_;

    public Productdetail() {}

    public Productdetail(Productdetail value) {
        this.id = value.id;
        this.batch = value.batch;
        this.brand = value.brand;
        this.color = value.color;
        this.dimension = value.dimension;
        this.model = value.model;
        this.serie = value.serie;
        this.expirationdate = value.expirationdate;
        this.productiondate = value.productiondate;
        this.productcode = value.productcode;
        this.unitcode = value.unitcode;
        this.createdon = value.createdon;
        this.createdby = value.createdby;
        this.updatedon = value.updatedon;
        this.updatedby = value.updatedby;
        this.isactive = value.isactive;
        this.class_ = value.class_;
    }

    public Productdetail(
        String    id,
        String    batch,
        String    brand,
        String    color,
        String    dimension,
        String    model,
        String    serie,
        Date      expirationdate,
        Date      productiondate,
        String    productcode,
        String    unitcode,
        Timestamp createdon,
        String    createdby,
        Timestamp updatedon,
        String    updatedby,
        Boolean   isactive,
        String    class_
    ) {
        this.id = id;
        this.batch = batch;
        this.brand = brand;
        this.color = color;
        this.dimension = dimension;
        this.model = model;
        this.serie = serie;
        this.expirationdate = expirationdate;
        this.productiondate = productiondate;
        this.productcode = productcode;
        this.unitcode = unitcode;
        this.createdon = createdon;
        this.createdby = createdby;
        this.updatedon = updatedon;
        this.updatedby = updatedby;
        this.isactive = isactive;
        this.class_ = class_;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBatch() {
        return this.batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDimension() {
        return this.dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerie() {
        return this.serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Date getExpirationdate() {
        return this.expirationdate;
    }

    public void setExpirationdate(Date expirationdate) {
        this.expirationdate = expirationdate;
    }

    public Date getProductiondate() {
        return this.productiondate;
    }

    public void setProductiondate(Date productiondate) {
        this.productiondate = productiondate;
    }

    public String getProductcode() {
        return this.productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public String getUnitcode() {
        return this.unitcode;
    }

    public void setUnitcode(String unitcode) {
        this.unitcode = unitcode;
    }

    public Timestamp getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Timestamp createdon) {
        this.createdon = createdon;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Timestamp getUpdatedon() {
        return this.updatedon;
    }

    public void setUpdatedon(Timestamp updatedon) {
        this.updatedon = updatedon;
    }

    public String getUpdatedby() {
        return this.updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getClass_() {
        return this.class_;
    }

    public void setClass_(String class_) {
        this.class_ = class_;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Productdetail (");

        sb.append(id);
        sb.append(", ").append(batch);
        sb.append(", ").append(brand);
        sb.append(", ").append(color);
        sb.append(", ").append(dimension);
        sb.append(", ").append(model);
        sb.append(", ").append(serie);
        sb.append(", ").append(expirationdate);
        sb.append(", ").append(productiondate);
        sb.append(", ").append(productcode);
        sb.append(", ").append(unitcode);
        sb.append(", ").append(createdon);
        sb.append(", ").append(createdby);
        sb.append(", ").append(updatedon);
        sb.append(", ").append(updatedby);
        sb.append(", ").append(isactive);
        sb.append(", ").append(class_);

        sb.append(")");
        return sb.toString();
    }
}
