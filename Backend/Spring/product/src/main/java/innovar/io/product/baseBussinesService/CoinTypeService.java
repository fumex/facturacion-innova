package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.product.dto.CoinTypeDto;

import innovar.io.product.models.tables.pojos.Cointype;
import innovar.io.product.services.CoinTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class CoinTypeService extends BaseBussinesService {
    @Autowired
    CoinTypeDataService baseCoinTypeDataService;

    public CoinTypeDto createCoinType(CoinTypeDto coinTypeDto){
        Cointype coinType = map(coinTypeDto, Cointype.class);
        List<Cointype> coinTypes = baseCoinTypeDataService.findCode(coinTypeDto.getCode());
        if(coinTypes.size()>0){
            String id = coinTypes.get(0).getId();
            Cointype coinTypeId = baseCoinTypeDataService.findById(id);
            coinTypeId.setActivateddate(new Timestamp(new Date().getTime()));
            baseCoinTypeDataService.update(id,coinTypeId);
            return map(coinTypeId, CoinTypeDto.class);
        }else{
            if(coinType.isactive==null){
                coinType.setIsactive(true);
            }
            coinType.setActivateddate(new Timestamp(new Date().getTime()));
            coinType.setInactivateddate(new Timestamp(new Date().getTime()));
            baseCoinTypeDataService.insert((coinType));
            return map(coinType, CoinTypeDto.class);
        }
    }

    public List<CoinTypeDto> createListCoinType(List<CoinTypeDto> taxTypeDtos){
        List<Cointype> coinTypes = mapAll(taxTypeDtos, Cointype.class);
        CoinTypeDto coinTypeDto = new CoinTypeDto();
        coinTypes.stream().forEach((p)->{
            coinTypeDto.setCode(p.getCode());
            coinTypeDto.setName(p.getName());
            coinTypeDto.setBadge(p.getBadge());
            coinTypeDto.setNumber(p.getNumber());
            if(coinTypeDto.getIsactive()==null){
                coinTypeDto.setIsactive(true);
            }else{
                coinTypeDto.setIsactive(p.getIsactive());
            }
            createCoinType(coinTypeDto);
        });
        return mapAll(coinTypes,CoinTypeDto.class);
    }

    public CoinTypeDto updateCoinType(String id, CoinTypeDto coinTypeDto){
        Cointype coinType = map(coinTypeDto, Cointype.class);
        Cointype a = baseCoinTypeDataService.findById(id);
        coinType.setActivateddate(a.getActivateddate());
        coinType.setInactivateddate(a.getInactivateddate());
        if(coinType.isactive==null){
            coinType.setIsactive(true);
        }
        baseCoinTypeDataService.update(id,coinType);
        return map(coinType, CoinTypeDto.class);
    }

    public CoinTypeDto getCoinType(String id){
        Cointype coinType = baseCoinTypeDataService.findById(id);
        return map(coinType, CoinTypeDto.class);
    }

    public CoinTypeDto disableCoinType(String id){
        Cointype coinType = baseCoinTypeDataService.findById(id);
        coinType.setInactivateddate(new Timestamp(new Date().getTime()));
        baseCoinTypeDataService.update(id,coinType);
        return map( baseCoinTypeDataService.disabled(id), CoinTypeDto.class);
    }

    public List<CoinTypeDto> getAllCoinTypes(){
        List<Cointype> coinTypes = baseCoinTypeDataService.findIntoList();
        return mapAll(coinTypes, CoinTypeDto.class);
    }

    public List<CoinTypeDto> getListCoinTypes(){
        List<Cointype> coinTypes = baseCoinTypeDataService.listAll();
        return mapAll(coinTypes, CoinTypeDto.class);
    }
}
