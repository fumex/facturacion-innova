package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.UnitMeasurementDto;
import innovar.io.product.models.tables.pojos.Unitmeasurement;
import innovar.io.product.services.UnitMeasurementDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class UnitMeasurementService  extends BaseBussinesService {
    @Autowired
    UnitMeasurementDataService baseUnitMeasurementDataService;

    public UnitMeasurementDto createUnitMeasurement(UnitMeasurementDto measurementDto){
        Unitmeasurement unitmeasurement = map(measurementDto, Unitmeasurement.class);
        List<Unitmeasurement> unitmeasurements = baseUnitMeasurementDataService.findCode(measurementDto.getCode());
        if(unitmeasurements.size()>0){
            String id = unitmeasurements.get(0).getId();
            Unitmeasurement unitmeasurementById = baseUnitMeasurementDataService.findById(id);
            unitmeasurementById.setActivateddate(new Timestamp(new Date().getTime()));
            baseUnitMeasurementDataService.update(id,unitmeasurementById);
            return map(unitmeasurementById, UnitMeasurementDto.class);
        }else{
            if(unitmeasurement.isactive==null){
                unitmeasurement.setIsactive(true);
            }
            unitmeasurement.setActivateddate(new Timestamp(new Date().getTime()));
            unitmeasurement.setInactivateddate(new Timestamp(new Date().getTime()));
            baseUnitMeasurementDataService.insert((unitmeasurement));
            return map(unitmeasurement, UnitMeasurementDto.class);
        }
    }

    public List<UnitMeasurementDto> createListUnitMeasurements(List<UnitMeasurementDto> unitMeasurementDtos){
        List<Unitmeasurement> unitmeasurements = mapAll(unitMeasurementDtos, Unitmeasurement.class);
        UnitMeasurementDto unitMeasurementDto = new UnitMeasurementDto();
        unitmeasurements.stream().forEach((p)->{
            unitMeasurementDto.setCode(p.getCode());
            unitMeasurementDto.setName(p.getName());
            unitMeasurementDto.setAbbreviation(p.getAbbreviation());
            unitMeasurementDto.setIsactive(p.getIsactive());
            createUnitMeasurement(unitMeasurementDto);
        });
        return mapAll(unitmeasurements,UnitMeasurementDto.class);
    }

    public UnitMeasurementDto updateUnitMeasurement(String id, UnitMeasurementDto unitMeasurementDto){
        Unitmeasurement unitmeasurement = map(unitMeasurementDto, Unitmeasurement.class);
        Unitmeasurement a = baseUnitMeasurementDataService.findById(id);
        unitmeasurement.setActivateddate(a.getActivateddate());
        unitmeasurement.setInactivateddate(a.getInactivateddate());
        baseUnitMeasurementDataService.update(id,unitmeasurement);
        return map(unitmeasurement, UnitMeasurementDto.class);
    }

    public UnitMeasurementDto getUnitMeasurement(String id){
        Unitmeasurement unitmeasurement = baseUnitMeasurementDataService.findById(id);
        return map(unitmeasurement, UnitMeasurementDto.class);
    }

    public UnitMeasurementDto disableUnitMeasurement(String id){
        Unitmeasurement unitmeasurement = baseUnitMeasurementDataService.findById(id);
        unitmeasurement.setInactivateddate(new Timestamp(new Date().getTime()));
        baseUnitMeasurementDataService.update(id,unitmeasurement);
        return map( baseUnitMeasurementDataService.disabled(id), UnitMeasurementDto.class);
    }

    public List<UnitMeasurementDto> getAllUnitMeasurement(){
        List<Unitmeasurement> unitmeasurements = baseUnitMeasurementDataService.findIntoList();
        return mapAll(unitmeasurements, UnitMeasurementDto.class);
    }

    public List<UnitMeasurementDto> getListUnitMeasurement(){
        List<Unitmeasurement> unitmeasurements = baseUnitMeasurementDataService.listAll();
        return mapAll(unitmeasurements, UnitMeasurementDto.class);
    }
}
