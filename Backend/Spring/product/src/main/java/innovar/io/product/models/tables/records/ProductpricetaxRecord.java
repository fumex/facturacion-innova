/*
 * This file is generated by jOOQ.
 */
package innovar.io.product.models.tables.records;


import innovar.io.product.models.tables.Productpricetax;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record12;
import org.jooq.Row12;
import org.jooq.impl.UpdatableRecordImpl;

import javax.annotation.Generated;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProductpricetaxRecord extends UpdatableRecordImpl<ProductpricetaxRecord> implements Record12<String, String, String, String, String, BigDecimal, Timestamp, String, Timestamp, String, Boolean, String> {

    private static final long serialVersionUID = -765321636;

    /**
     * Setter for <code>public.productpricetax.id</code>.
     */
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.productpricetax.id</code>.
     */
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>public.productpricetax.idproductprice</code>.
     */
    public void setIdproductprice(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.productpricetax.idproductprice</code>.
     */
    public String getIdproductprice() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.productpricetax.igvinvolvementtypecode</code>.
     */
    public void setIgvinvolvementtypecode(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.productpricetax.igvinvolvementtypecode</code>.
     */
    public String getIgvinvolvementtypecode() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.productpricetax.iscinvolvementtypecode</code>.
     */
    public void setIscinvolvementtypecode(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.productpricetax.iscinvolvementtypecode</code>.
     */
    public String getIscinvolvementtypecode() {
        return (String) get(3);
    }

    /**
     * Setter for <code>public.productpricetax.taxtypecode</code>.
     */
    public void setTaxtypecode(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>public.productpricetax.taxtypecode</code>.
     */
    public String getTaxtypecode() {
        return (String) get(4);
    }

    /**
     * Setter for <code>public.productpricetax.unittaxamount</code>.
     */
    public void setUnittaxamount(BigDecimal value) {
        set(5, value);
    }

    /**
     * Getter for <code>public.productpricetax.unittaxamount</code>.
     */
    public BigDecimal getUnittaxamount() {
        return (BigDecimal) get(5);
    }

    /**
     * Setter for <code>public.productpricetax.createdon</code>.
     */
    public void setCreatedon(Timestamp value) {
        set(6, value);
    }

    /**
     * Getter for <code>public.productpricetax.createdon</code>.
     */
    public Timestamp getCreatedon() {
        return (Timestamp) get(6);
    }

    /**
     * Setter for <code>public.productpricetax.createdby</code>.
     */
    public void setCreatedby(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>public.productpricetax.createdby</code>.
     */
    public String getCreatedby() {
        return (String) get(7);
    }

    /**
     * Setter for <code>public.productpricetax.updatedon</code>.
     */
    public void setUpdatedon(Timestamp value) {
        set(8, value);
    }

    /**
     * Getter for <code>public.productpricetax.updatedon</code>.
     */
    public Timestamp getUpdatedon() {
        return (Timestamp) get(8);
    }

    /**
     * Setter for <code>public.productpricetax.updatedby</code>.
     */
    public void setUpdatedby(String value) {
        set(9, value);
    }

    /**
     * Getter for <code>public.productpricetax.updatedby</code>.
     */
    public String getUpdatedby() {
        return (String) get(9);
    }

    /**
     * Setter for <code>public.productpricetax.isactive</code>.
     */
    public void setIsactive(Boolean value) {
        set(10, value);
    }

    /**
     * Getter for <code>public.productpricetax.isactive</code>.
     */
    public Boolean getIsactive() {
        return (Boolean) get(10);
    }

    /**
     * Setter for <code>public.productpricetax.class</code>.
     */
    public void setClass_(String value) {
        set(11, value);
    }

    /**
     * Getter for <code>public.productpricetax.class</code>.
     */
    public String getClass_() {
        return (String) get(11);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record12 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row12<String, String, String, String, String, BigDecimal, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row12) super.fieldsRow();
    }

    @Override
    public Row12<String, String, String, String, String, BigDecimal, Timestamp, String, Timestamp, String, Boolean, String> valuesRow() {
        return (Row12) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Productpricetax.PRODUCTPRICETAX.ID;
    }

    @Override
    public Field<String> field2() {
        return Productpricetax.PRODUCTPRICETAX.IDPRODUCTPRICE;
    }

    @Override
    public Field<String> field3() {
        return Productpricetax.PRODUCTPRICETAX.IGVINVOLVEMENTTYPECODE;
    }

    @Override
    public Field<String> field4() {
        return Productpricetax.PRODUCTPRICETAX.ISCINVOLVEMENTTYPECODE;
    }

    @Override
    public Field<String> field5() {
        return Productpricetax.PRODUCTPRICETAX.TAXTYPECODE;
    }

    @Override
    public Field<BigDecimal> field6() {
        return Productpricetax.PRODUCTPRICETAX.UNITTAXAMOUNT;
    }

    @Override
    public Field<Timestamp> field7() {
        return Productpricetax.PRODUCTPRICETAX.CREATEDON;
    }

    @Override
    public Field<String> field8() {
        return Productpricetax.PRODUCTPRICETAX.CREATEDBY;
    }

    @Override
    public Field<Timestamp> field9() {
        return Productpricetax.PRODUCTPRICETAX.UPDATEDON;
    }

    @Override
    public Field<String> field10() {
        return Productpricetax.PRODUCTPRICETAX.UPDATEDBY;
    }

    @Override
    public Field<Boolean> field11() {
        return Productpricetax.PRODUCTPRICETAX.ISACTIVE;
    }

    @Override
    public Field<String> field12() {
        return Productpricetax.PRODUCTPRICETAX.CLASS;
    }

    @Override
    public String component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getIdproductprice();
    }

    @Override
    public String component3() {
        return getIgvinvolvementtypecode();
    }

    @Override
    public String component4() {
        return getIscinvolvementtypecode();
    }

    @Override
    public String component5() {
        return getTaxtypecode();
    }

    @Override
    public BigDecimal component6() {
        return getUnittaxamount();
    }

    @Override
    public Timestamp component7() {
        return getCreatedon();
    }

    @Override
    public String component8() {
        return getCreatedby();
    }

    @Override
    public Timestamp component9() {
        return getUpdatedon();
    }

    @Override
    public String component10() {
        return getUpdatedby();
    }

    @Override
    public Boolean component11() {
        return getIsactive();
    }

    @Override
    public String component12() {
        return getClass_();
    }

    @Override
    public String value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getIdproductprice();
    }

    @Override
    public String value3() {
        return getIgvinvolvementtypecode();
    }

    @Override
    public String value4() {
        return getIscinvolvementtypecode();
    }

    @Override
    public String value5() {
        return getTaxtypecode();
    }

    @Override
    public BigDecimal value6() {
        return getUnittaxamount();
    }

    @Override
    public Timestamp value7() {
        return getCreatedon();
    }

    @Override
    public String value8() {
        return getCreatedby();
    }

    @Override
    public Timestamp value9() {
        return getUpdatedon();
    }

    @Override
    public String value10() {
        return getUpdatedby();
    }

    @Override
    public Boolean value11() {
        return getIsactive();
    }

    @Override
    public String value12() {
        return getClass_();
    }

    @Override
    public ProductpricetaxRecord value1(String value) {
        setId(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value2(String value) {
        setIdproductprice(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value3(String value) {
        setIgvinvolvementtypecode(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value4(String value) {
        setIscinvolvementtypecode(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value5(String value) {
        setTaxtypecode(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value6(BigDecimal value) {
        setUnittaxamount(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value7(Timestamp value) {
        setCreatedon(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value8(String value) {
        setCreatedby(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value9(Timestamp value) {
        setUpdatedon(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value10(String value) {
        setUpdatedby(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value11(Boolean value) {
        setIsactive(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord value12(String value) {
        setClass_(value);
        return this;
    }

    @Override
    public ProductpricetaxRecord values(String value1, String value2, String value3, String value4, String value5, BigDecimal value6, Timestamp value7, String value8, Timestamp value9, String value10, Boolean value11, String value12) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ProductpricetaxRecord
     */
    public ProductpricetaxRecord() {
        super(Productpricetax.PRODUCTPRICETAX);
    }

    /**
     * Create a detached, initialised ProductpricetaxRecord
     */
    public ProductpricetaxRecord(String id, String idproductprice, String igvinvolvementtypecode, String iscinvolvementtypecode, String taxtypecode, BigDecimal unittaxamount, Timestamp createdon, String createdby, Timestamp updatedon, String updatedby, Boolean isactive, String class_) {
        super(Productpricetax.PRODUCTPRICETAX);

        set(0, id);
        set(1, idproductprice);
        set(2, igvinvolvementtypecode);
        set(3, iscinvolvementtypecode);
        set(4, taxtypecode);
        set(5, unittaxamount);
        set(6, createdon);
        set(7, createdby);
        set(8, updatedon);
        set(9, updatedby);
        set(10, isactive);
        set(11, class_);
    }
}
