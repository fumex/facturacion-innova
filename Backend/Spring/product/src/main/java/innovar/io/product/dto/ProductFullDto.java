package innovar.io.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProductFullDto {
    public  String    id;
    private String    barcode;
    private String    code;
    private String    denomination;
    private String    description;
    private Integer   stock;
    private String    sunatcode;
    private String    categorycode;
    public Boolean    isactive;
    public ProductPriceDto productPriceDto;
    public List<ProductPriceTaxDto> productPriceTaxDtos;
    public List<ProductPriceChargeDiscountDto> productPriceChargeDiscountDtos;
}
