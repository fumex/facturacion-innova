package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.SunatCodeDto;
import innovar.io.product.models.tables.pojos.Sunatcode;
import innovar.io.product.services.SunatCodeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class SunatCodeService extends BaseBussinesService {

    @Autowired
    SunatCodeDataService baseSunatCodeDataService;

    public SunatCodeDto createSunatCode(SunatCodeDto sunatCodeDto){
        Sunatcode sunatcode = map(sunatCodeDto, Sunatcode.class);
        List<Sunatcode> sunatcodes = baseSunatCodeDataService.findCode(sunatCodeDto.getCode());
        if(sunatcodes.size()>0){
            String id = sunatcodes.get(0).getId();
            Sunatcode sunatcodeId = baseSunatCodeDataService.findById(id);
            sunatcodeId.setActivateddate(new Timestamp(new Date().getTime()));
            baseSunatCodeDataService.update(id,sunatcodeId);
            return map(sunatcodeId, SunatCodeDto.class);
        }else{
            if(sunatcode.isactive==null){
                sunatcode.setIsactive(true);
            }
            sunatcode.setActivateddate(new Timestamp(new Date().getTime()));
            sunatcode.setInactivateddate(new Timestamp(new Date().getTime()));
            baseSunatCodeDataService.insert((sunatcode));
            return map(sunatcode, SunatCodeDto.class);
        }
    }

    public List<SunatCodeDto> createListSunatCodes(List<SunatCodeDto> sunatCodeDtos){
        List<Sunatcode> iscinvolvementtypes = mapAll(sunatCodeDtos, Sunatcode.class);
        SunatCodeDto sunatCodeDto = new SunatCodeDto();
        iscinvolvementtypes.stream().forEach((p)->{
            sunatCodeDto.setCode(p.getCode());
            sunatCodeDto.setName(p.getName());
            sunatCodeDto.setIsactive(p.getIsactive());
            createSunatCode(sunatCodeDto);
        });
        return mapAll(iscinvolvementtypes,SunatCodeDto.class);
    }

    public SunatCodeDto updateSunatCode(String id, SunatCodeDto sunatCodeDto){
        Sunatcode sunatcode = map(sunatCodeDto, Sunatcode.class);
        Sunatcode a = baseSunatCodeDataService.findById(id);
        sunatcode.setActivateddate(a.getActivateddate());
        sunatcode.setInactivateddate(a.getInactivateddate());
        baseSunatCodeDataService.update(id,sunatcode);
        return map(sunatcode, SunatCodeDto.class);
    }

    public SunatCodeDto getSunatCode(String id){
        Sunatcode sunatcode = baseSunatCodeDataService.findById(id);
        return map(sunatcode, SunatCodeDto.class);
    }

    public SunatCodeDto disableSunatCode(String id){
        Sunatcode iscinvolvementtype = baseSunatCodeDataService.findById(id);
        iscinvolvementtype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseSunatCodeDataService.update(id,iscinvolvementtype);
        return map( baseSunatCodeDataService.disabled(id), SunatCodeDto.class);
    }

    public List<SunatCodeDto> getAllSunatCode(){
        List<Sunatcode> sunatcodes = baseSunatCodeDataService.findIntoList();
        return mapAll(sunatcodes, SunatCodeDto.class);
    }

    public List<SunatCodeDto> getListSunatCode(){
        List<Sunatcode> sunatcodes = baseSunatCodeDataService.listAll();
        return mapAll(sunatcodes, SunatCodeDto.class);
    }
}
