package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.product.dto.CategoryDto;
import innovar.io.product.models.tables.pojos.Category;
import innovar.io.product.services.CategoryDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class CategoryService extends BaseBussinesService {

    @Autowired
    CategoryDataService baseCategoryDataService;

    public CategoryDto createCategory(CategoryDto categoryDto){
        Category category = map(categoryDto, Category.class);
        List<Category> categories = baseCategoryDataService.findCode(categoryDto.getCode());
        if(categories.size()>0){
            String id = categories.get(0).getId();
            Category categoryId = baseCategoryDataService.findById(id);
            categoryId.setActivateddate(new Timestamp(new Date().getTime()));
            baseCategoryDataService.update(id,categoryId);
            return map(categoryId, CategoryDto.class);
        }else{
            if(category.isactive==null){
                category.setIsactive(true);
            }
            category.setActivateddate(new Timestamp(new Date().getTime()));
            category.setInactivateddate(new Timestamp(new Date().getTime()));
            baseCategoryDataService.insert((category));
            return map(category, CategoryDto.class);
        }
    }

    public List<CategoryDto> createListCategories(List<CategoryDto> categoryDtos){
        List<Category> categories = mapAll(categoryDtos, Category.class);
        CategoryDto categoryDto = new CategoryDto();
        categories.stream().forEach((p)->{
            categoryDto.setCode(p.getCode());
            categoryDto.setName(p.getName());
            categoryDto.setIsactive(p.getIsactive());
            createCategory(categoryDto);
        });
        return mapAll(categories,CategoryDto.class);
    }

    public CategoryDto updateCategory(String id, CategoryDto categoryDto){
        Category category = map(categoryDto, Category.class);
        Category a = baseCategoryDataService.findById(id);
        category.setActivateddate(a.getActivateddate());
        category.setInactivateddate(a.getInactivateddate());
        baseCategoryDataService.update(id,category);
        return map(category, CategoryDto.class);
    }

    public CategoryDto getCategory(String id){
        Category category = baseCategoryDataService.findById(id);
        return map(category, CategoryDto.class);
    }

    public CategoryDto disableCategory(String id){
        Category category = baseCategoryDataService.findById(id);
        category.setInactivateddate(new Timestamp(new Date().getTime()));
        baseCategoryDataService.update(id,category);
        return map( baseCategoryDataService.disabled(id), CategoryDto.class);
    }

    public List<CategoryDto> getAllCategory(){
        List<Category> categories = baseCategoryDataService.findIntoList();
        return mapAll(categories, CategoryDto.class);
    }

    public List<CategoryDto> getListCategory(){
        List<Category> categories = baseCategoryDataService.listAll();
        return mapAll(categories, CategoryDto.class);
    }
}
