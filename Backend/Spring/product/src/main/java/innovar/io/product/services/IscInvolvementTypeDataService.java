package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;

import innovar.io.product.models.tables.pojos.Iscinvolvementtype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IscInvolvementTypeDataService extends RepositoryPostgressService<Iscinvolvementtype> {

    public IscInvolvementTypeDataService (DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Iscinvolvementtype.class);
    }

    @Override
    public innovar.io.product.models.tables.Iscinvolvementtype getTable() {
        return Tables.ISCINVOLVEMENTTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.ISCINVOLVEMENTTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.ISCINVOLVEMENTTYPE.ISACTIVE;
    }
}
