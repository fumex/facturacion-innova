package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.baseBussinesService.IscInvolvementTypeService;
import innovar.io.product.dto.IscInvolvementTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class IscInvolvementTypeController extends BaseController {
    @Autowired
    IscInvolvementTypeService iscInvolvementTypeService;

    @PostMapping("/iscinvolvementtype")
    public AppResponse createIscInvolvementType(@RequestBody() IscInvolvementTypeDto iscInvolvementTypeDto) {
        return Response(() -> iscInvolvementTypeService.createIscInvolvementType((iscInvolvementTypeDto)));
    }

    @PostMapping("/iscinvolvementtypeListCreate")
    public AppResponse createListIscInvolvementType(@RequestBody() List<IscInvolvementTypeDto> iscInvolvementTypeDtos) {
        return Response(() -> iscInvolvementTypeService.createListIscInvolvementTypes(iscInvolvementTypeDtos));
    }

    @PutMapping("iscinvolvementtype/{id}")
    public AppResponse updateIscInvolvementType(@PathVariable() String id, @RequestBody() IscInvolvementTypeDto igvInvolvementTypeDto) {
        return Response(() -> iscInvolvementTypeService.updateIscInvolvementType((id), (igvInvolvementTypeDto)));
    }

    @GetMapping("/iscinvolvementtype/{id}")
    public AppResponse getIscInvolvementType(@PathVariable() String id) {
        return Response(() -> iscInvolvementTypeService.getIscInvolvementType(id));
    }

    @GetMapping("/iscinvolvementtypes")
    public AppResponse getAllIscInvolvementType() {
        return Response(() -> iscInvolvementTypeService.getAllIscInvolvementType());
    }

    @GetMapping("/listIscinvolvementtypes")
    public AppResponse getListIscInvolvementType() {
        return Response(() -> iscInvolvementTypeService.getListIscInvolvementType());
    }

    @DeleteMapping("/iscinvolvementtype/{id}")
    public AppResponse disabledIscInvolvementType(@PathVariable() String id) {
        return Response(() -> iscInvolvementTypeService.disableIscInvolvementType(id));
    }
}
