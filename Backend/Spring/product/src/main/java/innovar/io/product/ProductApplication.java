package innovar.io.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "innovar.io")
@EntityScan(basePackages = "innovar.io")
public class ProductApplication {
    public static  final String host= "/api/product";
    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class, args);
    }

}
