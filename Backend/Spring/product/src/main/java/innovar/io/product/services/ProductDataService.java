package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.dto.ProductDto;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Product;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductDataService  extends RepositoryPostgressService<Product> {

    public ProductDataService (DSLContext dsl){ super(dsl);}

    public List findProductBarCode(String barCode){
        return find(getTable().BARCODE.eq(barCode)).fetchInto(ProductDto.class);
    }
    @Override
    public innovar.io.product.models.tables.Product getTable() {
        return Tables.PRODUCT;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PRODUCT.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PRODUCT.ISACTIVE;
    }
}
