package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.ProductDetailDto;
import innovar.io.product.models.tables.pojos.Productdetail;
import innovar.io.product.services.ProductDetailDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ProductDetailService extends BaseBussinesService {

    @Autowired
    ProductDetailDataService productDetailDataService;

    public ProductDetailDto createProductDetail(ProductDetailDto productDetailDto){
        Productdetail productdetail = map(productDetailDto, Productdetail.class);
        if(productdetail.isactive==null){
            productdetail.setIsactive(true);
        }
        productDetailDataService.insert((productdetail));
        return map(productdetail, ProductDetailDto.class);
    }

    public ProductDetailDto updateProductDetail(String id, ProductDetailDto productDetailDto){
        Productdetail productdetail = map(productDetailDto,Productdetail.class);
        productdetail.setUpdatedon(new Timestamp(new Date().getTime()));
        if(productdetail.isactive==null){
            productdetail.setIsactive(true);
        }
        productDetailDataService.update(id, productdetail);
        return  map(productdetail, ProductDetailDto.class);
    }
    public ProductDetailDto getProductDetail(String id){
        Productdetail productdetail= productDetailDataService.findById(id);
        return map(productdetail, ProductDetailDto.class);
    }

    public List<ProductDetailDto> getAllProductDetails(){
        List<Productdetail> productdetails= productDetailDataService.findIntoList();
        return  mapAll(productdetails, ProductDetailDto.class);
    }

    public List<ProductDetailDto> getListProductDetails(){
        List<Productdetail> productdetails= productDetailDataService.listAll();
        return  mapAll(productdetails, ProductDetailDto.class);
    }

    public ProductDetailDto disabledProductDetail(String id){
        Productdetail productdetail= productDetailDataService.findById(id);
        productdetail.setUpdatedon(new Timestamp(new Date().getTime()));
        productDetailDataService.update(id, productdetail);
        return map(productDetailDataService.disabled(id),ProductDetailDto.class);
    }
}
