package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;

import innovar.io.product.baseBussinesService.UnitSalePriceTypeService;
import innovar.io.product.dto.UnitSalePriceTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class UnitSalePriceTypeController extends BaseController {
    @Autowired
    UnitSalePriceTypeService unitSalePriceTypeService;

    @PostMapping("/unitsalepricetype")
    public AppResponse createUnitSalePriceType(@RequestBody() UnitSalePriceTypeDto unitSalePriceTypeDto) {
        return Response(() -> unitSalePriceTypeService.createUnitSalePriceType((unitSalePriceTypeDto)));
    }

    @PostMapping("/unitsalepricetypeListCreate")
    public AppResponse createListUnitSalePriceType(@RequestBody() List<UnitSalePriceTypeDto> unitSalePriceTypeDtos) {
        return Response(() -> unitSalePriceTypeService.createListUnitSalePriceTypes(unitSalePriceTypeDtos));
    }

    @PutMapping("unitsalepricetype/{id}")
    public AppResponse updateUnitSalePriceType(@PathVariable() String id, @RequestBody() UnitSalePriceTypeDto unitSalePriceTypeDto) {
        return Response(() -> unitSalePriceTypeService.updateUnitSalePriceType((id), (unitSalePriceTypeDto)));
    }

    @GetMapping("/unitsalepricetype/{id}")
    public AppResponse getUnitSalePriceType(@PathVariable() String id) {
        return Response(() -> unitSalePriceTypeService.getUnitSalePriceType(id));
    }

    @GetMapping("/unitsalepricetypes")
    public AppResponse getAllUnitSalePriceType() {
        return Response(() -> unitSalePriceTypeService.getAllUnitSalePriceType());
    }

    @GetMapping("/listUnitsalepricetype")
    public AppResponse getListUnitSalePriceType() {
        return Response(() -> unitSalePriceTypeService.getListUnitSalePriceType());
    }

    @DeleteMapping("/unitsalepricetype/{id}")
    public AppResponse disabledUnitSalePriceType(@PathVariable() String id) {
        return Response(() -> unitSalePriceTypeService.disableUnitSalePriceType(id));
    }
}
