package innovar.io.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProductPriceDto {
    private String     id;
    private String     coincode;
    private String     productcode;
    private BigDecimal unitsalevalue;
    private BigDecimal unitbaseamount;
    private BigDecimal unitsaleprice;
    private String     unitsalepricetypecode;
    private Boolean    isactive;
}
