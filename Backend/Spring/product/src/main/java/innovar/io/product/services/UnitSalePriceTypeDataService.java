package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Unitmeasurement;
import innovar.io.product.models.tables.pojos.Unitsalepricetype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitSalePriceTypeDataService extends RepositoryPostgressService<Unitsalepricetype> {

    public UnitSalePriceTypeDataService (DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Unitsalepricetype.class);
    }

    @Override
    public innovar.io.product.models.tables.Unitsalepricetype getTable() {
        return Tables.UNITSALEPRICETYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.UNITSALEPRICETYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.UNITSALEPRICETYPE.ISACTIVE;
    }
}
