package innovar.io.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CoinTypeDto {
    public String    id;
    private String    code;
    private String    name;
    private String    badge;
    private Integer   number;
    public Boolean  isactive;
}
