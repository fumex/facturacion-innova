package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.product.dto.ProductPriceDto;
import innovar.io.product.models.tables.pojos.Productprice;
import innovar.io.product.services.ProductPriceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ProductPriceService extends BaseBussinesService {
    @Autowired
    ProductPriceDataService productPriceDataService;
    public ProductPriceDto createProductPrice(ProductPriceDto productPriceDto){
        Productprice productprice = map(productPriceDto, Productprice.class);
        if(productprice.isactive==null){
            productprice.setIsactive(true);
        }
        productPriceDataService.insert((productprice));
        return map(productprice, ProductPriceDto.class);
    }

    public List<ProductPriceDto> createListProductPrice(List<ProductPriceDto> productPriceDtos){
        List<Productprice> productprices = mapAll(productPriceDtos, Productprice.class);
        ProductPriceDto productPriceDto = new ProductPriceDto();
        productprices.stream().forEach((p)->{
            productPriceDto.setCoincode(p.getCoincode());
            productPriceDto.setProductcode(p.getProductcode());
            productPriceDto.setUnitsalevalue(p.getUnitsalevalue());
            productPriceDto.setUnitbaseamount(p.getUnitbaseamount());
            productPriceDto.setUnitsaleprice(p.getUnitsaleprice());
            productPriceDto.setUnitsalepricetypecode(p.getUnitsalepricetypecode());
            if(p.getIsactive()==null){
                productPriceDto.setIsactive(true);
            }else{
                productPriceDto.setIsactive(p.getIsactive());
            }
            createProductPrice(productPriceDto);
        });
        return mapAll(productprices,ProductPriceDto.class);
    }

    public ProductPriceDto updateProductPrice(String id, ProductPriceDto productPriceDto){
        Productprice productprice = map(productPriceDto,Productprice.class);
        productprice.setUpdatedon(new Timestamp(new Date().getTime()));
        if(productprice.isactive==null){
            productprice.setIsactive(true);
        }
        productPriceDataService.update(id, productprice);
        return  map(productprice, ProductPriceDto.class);
    }
    public ProductPriceDto getProductPrice(String id){
        Productprice productprice= productPriceDataService.findById(id);
        return map(productprice, ProductPriceDto.class);
    }

    public List<ProductPriceDto> getAllProductPrices(){
        List<Productprice> productprices= productPriceDataService.findIntoList();
        return  mapAll(productprices, ProductPriceDto.class);
    }

    public List<ProductPriceDto> getListProductPrices(){
        List<Productprice> productprices= productPriceDataService.listAll();
        return  mapAll(productprices, ProductPriceDto.class);
    }

    public ProductPriceDto disabledProductPrice(String id){
        Productprice productprice= productPriceDataService.findById(id);
        productprice.setUpdatedon(new Timestamp(new Date().getTime()));
        productPriceDataService.update(id, productprice);
        return map(productPriceDataService.disabled(id),ProductPriceDto.class);
    }
}
