package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.baseBussinesService.CategoryService;
import innovar.io.product.dto.CategoryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class CategoryController extends BaseController {
    @Autowired
    CategoryService categoryService;

    @PostMapping("/category")
    public AppResponse createTaxType(@RequestBody() CategoryDto categoryDto){
        return Response(()-> categoryService.createCategory((categoryDto)));
    }

    @PostMapping("/categoryListCreate")
    public AppResponse createListTaxType(@RequestBody() List<CategoryDto> categoryDtoList){
        return Response(()->categoryService.createListCategories(categoryDtoList));
    }

    @PutMapping("/category/{id}")
    public AppResponse updateTaxType(@PathVariable() String id, @RequestBody() CategoryDto categoryDto){
        return Response(()-> categoryService.updateCategory((id),(categoryDto)));
    }

    @GetMapping("/category/{id}")
    public AppResponse getTaxType(@PathVariable() String id){
        return Response(()-> categoryService.getCategory(id));
    }

    @GetMapping("/categories")
    public AppResponse getAllTaxTypes(){
        return Response(()-> categoryService.getAllCategory());
    }

    @GetMapping("/ListCategory")
    public AppResponse getListTaxTypes(){
        return Response(()-> categoryService.getListCategory());
    }

    @DeleteMapping("/category/{id}")
    public AppResponse disabledTaxType(@PathVariable() String id){
        return Response(()-> categoryService.disableCategory(id));
    }
}
