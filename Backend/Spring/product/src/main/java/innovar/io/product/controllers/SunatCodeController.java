package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.baseBussinesService.SunatCodeService;
import innovar.io.product.dto.SunatCodeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class SunatCodeController extends BaseController {
    @Autowired
    SunatCodeService sunatCodeService;

    @PostMapping("/sunatcode")
    public AppResponse createSunatCode(@RequestBody() SunatCodeDto sunatCodeDto) {
        return Response(() -> sunatCodeService.createSunatCode((sunatCodeDto)));
    }

    @PostMapping("/sunatcodeListCreate")
    public AppResponse createListSunatCode(@RequestBody() List<SunatCodeDto> sunatCodeDtos) {
        return Response(() -> sunatCodeService.createListSunatCodes(sunatCodeDtos));
    }

    @PutMapping("sunatcode/{id}")
    public AppResponse updateSunatCode(@PathVariable() String id, @RequestBody() SunatCodeDto sunatCodeDto) {
        return Response(() -> sunatCodeService.updateSunatCode((id), (sunatCodeDto)));
    }

    @GetMapping("/sunatcode/{id}")
    public AppResponse getSunatCode(@PathVariable() String id) {
        return Response(() -> sunatCodeService.getSunatCode(id));
    }

    @GetMapping("/sunatcodes")
    public AppResponse getAllSunatCode() {
        return Response(() -> sunatCodeService.getAllSunatCode());
    }

    @GetMapping("/listSunatCodes")
    public AppResponse getListSunatCode() {
        return Response(() -> sunatCodeService.getListSunatCode());
    }

    @DeleteMapping("/sunatcode/{id}")
    public AppResponse disabledSunatCode(@PathVariable() String id) {
        return Response(() -> sunatCodeService.disableSunatCode(id));
    }
}
