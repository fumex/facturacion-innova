/*
 * This file is generated by jOOQ.
 */
package innovar.io.product.models.tables.records;


import innovar.io.product.models.tables.Category;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record11;
import org.jooq.Row11;
import org.jooq.impl.UpdatableRecordImpl;

import javax.annotation.Generated;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CategoryRecord extends UpdatableRecordImpl<CategoryRecord> implements Record11<String, String, String, Timestamp, Timestamp, Timestamp, String, Timestamp, String, Boolean, String> {

    private static final long serialVersionUID = 761980094;

    /**
     * Setter for <code>public.category.id</code>.
     */
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.category.id</code>.
     */
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>public.category.code</code>.
     */
    public void setCode(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.category.code</code>.
     */
    public String getCode() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.category.name</code>.
     */
    public void setName(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.category.name</code>.
     */
    public String getName() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.category.activateddate</code>.
     */
    public void setActivateddate(Timestamp value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.category.activateddate</code>.
     */
    public Timestamp getActivateddate() {
        return (Timestamp) get(3);
    }

    /**
     * Setter for <code>public.category.inactivateddate</code>.
     */
    public void setInactivateddate(Timestamp value) {
        set(4, value);
    }

    /**
     * Getter for <code>public.category.inactivateddate</code>.
     */
    public Timestamp getInactivateddate() {
        return (Timestamp) get(4);
    }

    /**
     * Setter for <code>public.category.createdon</code>.
     */
    public void setCreatedon(Timestamp value) {
        set(5, value);
    }

    /**
     * Getter for <code>public.category.createdon</code>.
     */
    public Timestamp getCreatedon() {
        return (Timestamp) get(5);
    }

    /**
     * Setter for <code>public.category.createdby</code>.
     */
    public void setCreatedby(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>public.category.createdby</code>.
     */
    public String getCreatedby() {
        return (String) get(6);
    }

    /**
     * Setter for <code>public.category.updatedon</code>.
     */
    public void setUpdatedon(Timestamp value) {
        set(7, value);
    }

    /**
     * Getter for <code>public.category.updatedon</code>.
     */
    public Timestamp getUpdatedon() {
        return (Timestamp) get(7);
    }

    /**
     * Setter for <code>public.category.updatedby</code>.
     */
    public void setUpdatedby(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>public.category.updatedby</code>.
     */
    public String getUpdatedby() {
        return (String) get(8);
    }

    /**
     * Setter for <code>public.category.isactive</code>.
     */
    public void setIsactive(Boolean value) {
        set(9, value);
    }

    /**
     * Getter for <code>public.category.isactive</code>.
     */
    public Boolean getIsactive() {
        return (Boolean) get(9);
    }

    /**
     * Setter for <code>public.category.class</code>.
     */
    public void setClass_(String value) {
        set(10, value);
    }

    /**
     * Getter for <code>public.category.class</code>.
     */
    public String getClass_() {
        return (String) get(10);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record11 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row11<String, String, String, Timestamp, Timestamp, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row11) super.fieldsRow();
    }

    @Override
    public Row11<String, String, String, Timestamp, Timestamp, Timestamp, String, Timestamp, String, Boolean, String> valuesRow() {
        return (Row11) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Category.CATEGORY.ID;
    }

    @Override
    public Field<String> field2() {
        return Category.CATEGORY.CODE;
    }

    @Override
    public Field<String> field3() {
        return Category.CATEGORY.NAME;
    }

    @Override
    public Field<Timestamp> field4() {
        return Category.CATEGORY.ACTIVATEDDATE;
    }

    @Override
    public Field<Timestamp> field5() {
        return Category.CATEGORY.INACTIVATEDDATE;
    }

    @Override
    public Field<Timestamp> field6() {
        return Category.CATEGORY.CREATEDON;
    }

    @Override
    public Field<String> field7() {
        return Category.CATEGORY.CREATEDBY;
    }

    @Override
    public Field<Timestamp> field8() {
        return Category.CATEGORY.UPDATEDON;
    }

    @Override
    public Field<String> field9() {
        return Category.CATEGORY.UPDATEDBY;
    }

    @Override
    public Field<Boolean> field10() {
        return Category.CATEGORY.ISACTIVE;
    }

    @Override
    public Field<String> field11() {
        return Category.CATEGORY.CLASS;
    }

    @Override
    public String component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getCode();
    }

    @Override
    public String component3() {
        return getName();
    }

    @Override
    public Timestamp component4() {
        return getActivateddate();
    }

    @Override
    public Timestamp component5() {
        return getInactivateddate();
    }

    @Override
    public Timestamp component6() {
        return getCreatedon();
    }

    @Override
    public String component7() {
        return getCreatedby();
    }

    @Override
    public Timestamp component8() {
        return getUpdatedon();
    }

    @Override
    public String component9() {
        return getUpdatedby();
    }

    @Override
    public Boolean component10() {
        return getIsactive();
    }

    @Override
    public String component11() {
        return getClass_();
    }

    @Override
    public String value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getCode();
    }

    @Override
    public String value3() {
        return getName();
    }

    @Override
    public Timestamp value4() {
        return getActivateddate();
    }

    @Override
    public Timestamp value5() {
        return getInactivateddate();
    }

    @Override
    public Timestamp value6() {
        return getCreatedon();
    }

    @Override
    public String value7() {
        return getCreatedby();
    }

    @Override
    public Timestamp value8() {
        return getUpdatedon();
    }

    @Override
    public String value9() {
        return getUpdatedby();
    }

    @Override
    public Boolean value10() {
        return getIsactive();
    }

    @Override
    public String value11() {
        return getClass_();
    }

    @Override
    public CategoryRecord value1(String value) {
        setId(value);
        return this;
    }

    @Override
    public CategoryRecord value2(String value) {
        setCode(value);
        return this;
    }

    @Override
    public CategoryRecord value3(String value) {
        setName(value);
        return this;
    }

    @Override
    public CategoryRecord value4(Timestamp value) {
        setActivateddate(value);
        return this;
    }

    @Override
    public CategoryRecord value5(Timestamp value) {
        setInactivateddate(value);
        return this;
    }

    @Override
    public CategoryRecord value6(Timestamp value) {
        setCreatedon(value);
        return this;
    }

    @Override
    public CategoryRecord value7(String value) {
        setCreatedby(value);
        return this;
    }

    @Override
    public CategoryRecord value8(Timestamp value) {
        setUpdatedon(value);
        return this;
    }

    @Override
    public CategoryRecord value9(String value) {
        setUpdatedby(value);
        return this;
    }

    @Override
    public CategoryRecord value10(Boolean value) {
        setIsactive(value);
        return this;
    }

    @Override
    public CategoryRecord value11(String value) {
        setClass_(value);
        return this;
    }

    @Override
    public CategoryRecord values(String value1, String value2, String value3, Timestamp value4, Timestamp value5, Timestamp value6, String value7, Timestamp value8, String value9, Boolean value10, String value11) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached CategoryRecord
     */
    public CategoryRecord() {
        super(Category.CATEGORY);
    }

    /**
     * Create a detached, initialised CategoryRecord
     */
    public CategoryRecord(String id, String code, String name, Timestamp activateddate, Timestamp inactivateddate, Timestamp createdon, String createdby, Timestamp updatedon, String updatedby, Boolean isactive, String class_) {
        super(Category.CATEGORY);

        set(0, id);
        set(1, code);
        set(2, name);
        set(3, activateddate);
        set(4, inactivateddate);
        set(5, createdon);
        set(6, createdby);
        set(7, updatedon);
        set(8, updatedby);
        set(9, isactive);
        set(10, class_);
    }
}
