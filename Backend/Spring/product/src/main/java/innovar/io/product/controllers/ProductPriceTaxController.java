package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.ProductApplication;

import innovar.io.product.baseBussinesService.ProductPriceTaxService;
import innovar.io.product.dto.ProductPriceDto;
import innovar.io.product.dto.ProductPriceTaxDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class ProductPriceTaxController extends BaseController {
    @Autowired
    ProductPriceTaxService productPriceTaxService;

    @PostMapping("/productpricetax")
    public AppResponse createProductPriceTax(@RequestBody() ProductPriceTaxDto productPriceTaxDto){
        return Response(()-> productPriceTaxService.createProductPriceTax((productPriceTaxDto)));
    }

    @PostMapping("/listCreateProductPriceTax")
    public AppResponse createListProductPriceTax(@RequestBody() List<ProductPriceTaxDto> productPriceTaxDtos){
        return Response(()->productPriceTaxService.createListProductPriceTax(productPriceTaxDtos));
    }

    @PutMapping("/productpricetax/{id}")
    public AppResponse updateProductPriceTax(@PathVariable() String id, @RequestBody() ProductPriceTaxDto productPriceTaxDto){
        return Response(()-> productPriceTaxService.updateProductPriceTax((id),(productPriceTaxDto)));
    }

    @GetMapping("/productpricetax/{id}")
    public AppResponse getProductPriceTax(@PathVariable() String id){
        return Response(()-> productPriceTaxService.getProductPriceTax(id));
    }

    @GetMapping("/productpricetaxes")
    public AppResponse getAllProductPriceTaxes(){
        return Response(()-> productPriceTaxService.getAllProductPriceTaxes());
    }

    @GetMapping("/ListProductpricetaxes")
    public AppResponse getListProductPriceTaxes(){
        return Response(()-> productPriceTaxService.getListProductPriceTaxes());
    }

    @DeleteMapping("/productpricetax/{id}")
    public AppResponse disabledProductPriceTax(@PathVariable() String id){
        return Response(()-> productPriceTaxService.disabledProductPriceTax(id));
    }
}
