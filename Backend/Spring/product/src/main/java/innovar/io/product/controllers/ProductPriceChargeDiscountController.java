package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.ProductApplication;

import innovar.io.product.baseBussinesService.ProductPriceChargeDiscountService;
import innovar.io.product.dto.ProductPriceChargeDiscountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class ProductPriceChargeDiscountController extends BaseController {
    @Autowired
    ProductPriceChargeDiscountService productPriceChargeDiscountService;

    @PostMapping("/productpricechargediscount")
    public AppResponse createProductPriceChargeDiscount(@RequestBody() ProductPriceChargeDiscountDto productPriceChargeDiscountDto){
        return Response(()-> productPriceChargeDiscountService.createProductPriceChargeDiscount((productPriceChargeDiscountDto)));
    }

    @PostMapping("/listCreateProductPriceChargeDiscount")
    public AppResponse createListProductPriceChargeDiscount(@RequestBody() List<ProductPriceChargeDiscountDto> productPriceChargeDiscountDtos){
        return Response(()->productPriceChargeDiscountService.createListProductPriceChargeDiscount(productPriceChargeDiscountDtos));
    }

    @PutMapping("/productpricechargediscount/{id}")
    public AppResponse updateProductPriceChargeDiscount(@PathVariable() String id, @RequestBody() ProductPriceChargeDiscountDto productPriceChargeDiscountDto){
        return Response(()-> productPriceChargeDiscountService.updateProductPriceChargeDiscount((id),(productPriceChargeDiscountDto)));
    }

    @GetMapping("/productpricechargediscount/{id}")
    public AppResponse getProductPriceChargeDiscount(@PathVariable() String id){
        return Response(()-> productPriceChargeDiscountService.getProductPriceChargeDiscount(id));
    }

    @GetMapping("/productpricechargediscounts")
    public AppResponse getAllProductPriceChargeDiscount(){
        return Response(()-> productPriceChargeDiscountService.getAllProductPriceChargeDiscount());
    }

    @GetMapping("/ListProductpricechargediscount")
    public AppResponse getListProductPriceChargeDiscount(){
        return Response(()-> productPriceChargeDiscountService.getListProductPriceChargeDiscount());
    }

    @DeleteMapping("/productpricechargediscount/{id}")
    public AppResponse disabledProductPriceChargeDiscount(@PathVariable() String id){
        return Response(()-> productPriceChargeDiscountService.disabledProductPriceChargeDiscount(id));
    }
}
