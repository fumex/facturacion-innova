package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.dto.ProductPriceChargeDiscountDto;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Productpricechargediscount;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductPriceChargeDiscountDataService extends RepositoryPostgressService<Productpricechargediscount> {

    public ProductPriceChargeDiscountDataService(DSLContext dsl){ super(dsl);}

    public List findProductPriceChargeDiscount(String idProductPrice){
        return find(getTable().IDPRODUCTPRICE.eq(idProductPrice)).fetchInto(ProductPriceChargeDiscountDto.class);
    }

    @Override
    public innovar.io.product.models.tables.Productpricechargediscount getTable() {
        return Tables.PRODUCTPRICECHARGEDISCOUNT;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PRODUCTPRICECHARGEDISCOUNT.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PRODUCTPRICECHARGEDISCOUNT.ISACTIVE;
    }
}
