package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.ProductApplication;

import innovar.io.product.baseBussinesService.ProductPriceService;

import innovar.io.product.dto.ProductPriceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class ProductPriceController extends BaseController {
    @Autowired
    ProductPriceService productPriceService;

    @PostMapping("/productprice")
    public AppResponse createProductPrice(@RequestBody() ProductPriceDto productPriceDto){
        return Response(()-> productPriceService.createProductPrice((productPriceDto)));
    }

    @PostMapping("/listCreateProductPrice")
    public AppResponse createListProductPrice(@RequestBody() List<ProductPriceDto> productPriceDtos){
        return Response(()->productPriceService.createListProductPrice(productPriceDtos));
    }

    @PutMapping("/productprice/{id}")
    public AppResponse updateProductPrice(@PathVariable() String id, @RequestBody() ProductPriceDto productPriceDto){
        return Response(()-> productPriceService.updateProductPrice((id),(productPriceDto)));
    }

    @GetMapping("/productprice/{id}")
    public AppResponse getProductPrice(@PathVariable() String id){
        return Response(()-> productPriceService.getProductPrice(id));
    }

    @GetMapping("/productprices")
    public AppResponse getAllProductPrices(){
        return Response(()-> productPriceService.getAllProductPrices());
    }

    @GetMapping("/ListProductprices")
    public AppResponse getListProductPrices(){
        return Response(()-> productPriceService.getListProductPrices());
    }

    @DeleteMapping("/productprice/{id}")
    public AppResponse disabledProductPrice(@PathVariable() String id){
        return Response(()-> productPriceService.disabledProductPrice(id));
    }
}
