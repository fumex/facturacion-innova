package innovar.io.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProductPriceChargeDiscountDto {
    private String     id;
    private String     idproductprice;
    private String     discountchargetypecode;
    private BigDecimal unitchargediscountamount;
    private Boolean    isactivate;
}
