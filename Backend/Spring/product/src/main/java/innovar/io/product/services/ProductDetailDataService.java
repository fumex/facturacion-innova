package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Productdetail;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

@Service
public class ProductDetailDataService extends RepositoryPostgressService<Productdetail> {

    public ProductDetailDataService(DSLContext dsl){ super(dsl);}

    @Override
    public innovar.io.product.models.tables.Productdetail getTable() {
        return Tables.PRODUCTDETAIL;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PRODUCTDETAIL.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PRODUCTDETAIL.ISACTIVE;
    }
}
