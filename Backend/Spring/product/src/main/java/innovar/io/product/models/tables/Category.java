/*
 * This file is generated by jOOQ.
 */
package innovar.io.product.models.tables;


import innovar.io.product.models.Indexes;
import innovar.io.product.models.Keys;
import innovar.io.product.models.Public;
import innovar.io.product.models.tables.records.CategoryRecord;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import javax.annotation.Generated;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Category extends TableImpl<CategoryRecord> {

    private static final long serialVersionUID = 1895268621;

    /**
     * The reference instance of <code>public.category</code>
     */
    public static final Category CATEGORY = new Category();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<CategoryRecord> getRecordType() {
        return CategoryRecord.class;
    }

    /**
     * The column <code>public.category.id</code>.
     */
    public final TableField<CategoryRecord, String> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.VARCHAR(24).nullable(false), this, "");

    /**
     * The column <code>public.category.code</code>.
     */
    public final TableField<CategoryRecord, String> CODE = createField(DSL.name("code"), org.jooq.impl.SQLDataType.VARCHAR(10), this, "");

    /**
     * The column <code>public.category.name</code>.
     */
    public final TableField<CategoryRecord, String> NAME = createField(DSL.name("name"), org.jooq.impl.SQLDataType.VARCHAR(100), this, "");

    /**
     * The column <code>public.category.activateddate</code>.
     */
    public final TableField<CategoryRecord, Timestamp> ACTIVATEDDATE = createField(DSL.name("activateddate"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.category.inactivateddate</code>.
     */
    public final TableField<CategoryRecord, Timestamp> INACTIVATEDDATE = createField(DSL.name("inactivateddate"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.category.createdon</code>.
     */
    public final TableField<CategoryRecord, Timestamp> CREATEDON = createField(DSL.name("createdon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.category.createdby</code>.
     */
    public final TableField<CategoryRecord, String> CREATEDBY = createField(DSL.name("createdby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.category.updatedon</code>.
     */
    public final TableField<CategoryRecord, Timestamp> UPDATEDON = createField(DSL.name("updatedon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.category.updatedby</code>.
     */
    public final TableField<CategoryRecord, String> UPDATEDBY = createField(DSL.name("updatedby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.category.isactive</code>.
     */
    public final TableField<CategoryRecord, Boolean> ISACTIVE = createField(DSL.name("isactive"), org.jooq.impl.SQLDataType.BOOLEAN, this, "");

    /**
     * The column <code>public.category.class</code>.
     */
    public final TableField<CategoryRecord, String> CLASS = createField(DSL.name("class"), org.jooq.impl.SQLDataType.VARCHAR(200), this, "");

    /**
     * Create a <code>public.category</code> table reference
     */
    public Category() {
        this(DSL.name("category"), null);
    }

    /**
     * Create an aliased <code>public.category</code> table reference
     */
    public Category(String alias) {
        this(DSL.name(alias), CATEGORY);
    }

    /**
     * Create an aliased <code>public.category</code> table reference
     */
    public Category(Name alias) {
        this(alias, CATEGORY);
    }

    private Category(Name alias, Table<CategoryRecord> aliased) {
        this(alias, aliased, null);
    }

    private Category(Name alias, Table<CategoryRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Category(Table<O> child, ForeignKey<O, CategoryRecord> key) {
        super(child, key, CATEGORY);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.CATEGORY_CODE_KEY, Indexes.CATEGORY_PKEY);
    }

    @Override
    public UniqueKey<CategoryRecord> getPrimaryKey() {
        return Keys.CATEGORY_PKEY;
    }

    @Override
    public List<UniqueKey<CategoryRecord>> getKeys() {
        return Arrays.<UniqueKey<CategoryRecord>>asList(Keys.CATEGORY_PKEY, Keys.CATEGORY_CODE_KEY);
    }

    @Override
    public Category as(String alias) {
        return new Category(DSL.name(alias), this);
    }

    @Override
    public Category as(Name alias) {
        return new Category(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Category rename(String name) {
        return new Category(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Category rename(Name name) {
        return new Category(name, null);
    }

    // -------------------------------------------------------------------------
    // Row11 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row11<String, String, String, Timestamp, Timestamp, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row11) super.fieldsRow();
    }
}
