package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.dto.ProductPriceDto;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.Product;
import innovar.io.product.models.tables.pojos.Productprice;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductPriceDataService extends RepositoryPostgressService<Productprice> {

    public ProductPriceDataService(DSLContext dsl){super(dsl);}

    public List findProductPrice(String code){
        return find(getTable().PRODUCTCODE.eq(code)).fetchInto(ProductPriceDto.class);
    }
    @Override
    public innovar.io.product.models.tables.Productprice getTable() {
        return Tables.PRODUCTPRICE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PRODUCTPRICE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PRODUCTPRICE.ISACTIVE;
    }
}
