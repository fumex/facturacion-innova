package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.ProductPriceChargeDiscountDto;

import innovar.io.product.models.tables.pojos.Productpricechargediscount;
import innovar.io.product.services.ProductPriceChargeDiscountDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ProductPriceChargeDiscountService extends BaseBussinesService {
    @Autowired
    ProductPriceChargeDiscountDataService productPriceChargeDiscountDataService;

    public ProductPriceChargeDiscountDto createProductPriceChargeDiscount(ProductPriceChargeDiscountDto productPriceChargeDiscountDto){
        Productpricechargediscount productpricechargediscount = map(productPriceChargeDiscountDto, Productpricechargediscount.class);
        if(productpricechargediscount.isactive==null){
            productpricechargediscount.setIsactive(true);
        }
        productPriceChargeDiscountDataService.insert((productpricechargediscount));
        return map(productpricechargediscount, ProductPriceChargeDiscountDto.class);
    }

    public List<ProductPriceChargeDiscountDto> createListProductPriceChargeDiscount(List<ProductPriceChargeDiscountDto> productPriceChargeDiscountDtos){
        List<Productpricechargediscount> productpricechargediscounts = mapAll(productPriceChargeDiscountDtos, Productpricechargediscount.class);
        ProductPriceChargeDiscountDto productPriceChargeDiscountDto = new ProductPriceChargeDiscountDto();
        productpricechargediscounts.stream().forEach((p)->{
            productPriceChargeDiscountDto.setIdproductprice(p.getIdproductprice());
            productPriceChargeDiscountDto.setDiscountchargetypecode(p.getDiscountchargetypecode());
            productPriceChargeDiscountDto.setUnitchargediscountamount(p.getUnitchargediscountamount());
            if(p.getIsactive()==null){
                productPriceChargeDiscountDto.setIsactivate(true);
            }else{
                productPriceChargeDiscountDto.setIsactivate(p.getIsactive());
            }
            createProductPriceChargeDiscount(productPriceChargeDiscountDto);
        });
        return mapAll(productpricechargediscounts,ProductPriceChargeDiscountDto.class);
    }

    public ProductPriceChargeDiscountDto updateProductPriceChargeDiscount(String id, ProductPriceChargeDiscountDto productPriceChargeDiscountDto){
        Productpricechargediscount product = map(productPriceChargeDiscountDto,Productpricechargediscount.class);
        product.setUpdatedon(new Timestamp(new Date().getTime()));
        if(product.isactive==null){
            product.setIsactive(true);
        }
        productPriceChargeDiscountDataService.update(id, product);
        return  map(product, ProductPriceChargeDiscountDto.class);
    }
    public ProductPriceChargeDiscountDto getProductPriceChargeDiscount(String id){
        Productpricechargediscount productpricechargediscount= productPriceChargeDiscountDataService.findById(id);
        return map(productpricechargediscount, ProductPriceChargeDiscountDto.class);
    }

    public List<ProductPriceChargeDiscountDto> getAllProductPriceChargeDiscount(){
        List<Productpricechargediscount> productpricechargediscountList= productPriceChargeDiscountDataService.findIntoList();
        return  mapAll(productpricechargediscountList, ProductPriceChargeDiscountDto.class);
    }

    public List<ProductPriceChargeDiscountDto> getListProductPriceChargeDiscount(){
        List<Productpricechargediscount> productpricechargediscounts= productPriceChargeDiscountDataService.listAll();
        return  mapAll(productpricechargediscounts, ProductPriceChargeDiscountDto.class);
    }

    public ProductPriceChargeDiscountDto disabledProductPriceChargeDiscount(String id){
        Productpricechargediscount productpricechargediscount= productPriceChargeDiscountDataService.findById(id);
        productpricechargediscount.setUpdatedon(new Timestamp(new Date().getTime()));
        productPriceChargeDiscountDataService.update(id, productpricechargediscount);
        return map(productPriceChargeDiscountDataService.disabled(id),ProductPriceChargeDiscountDto.class);
    }
}
