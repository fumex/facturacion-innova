package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.IgvInvolvementTypeDto;
import innovar.io.product.models.tables.pojos.Igvinvolvementtype;
import innovar.io.product.services.IgvInvolvementTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class IgvInvolvementTypeService extends BaseBussinesService {
    @Autowired
    IgvInvolvementTypeDataService baseIgvInvolvementTypeDataService;

    public IgvInvolvementTypeDto createIgvInvolvementType(IgvInvolvementTypeDto igvInvolvementTypeDto){
        Igvinvolvementtype igv = map(igvInvolvementTypeDto, Igvinvolvementtype.class);
        List<Igvinvolvementtype> igvinvolvementtypes = baseIgvInvolvementTypeDataService.findCode(igvInvolvementTypeDto.getCode());
        if(igvinvolvementtypes.size()>0){
            String id = igvinvolvementtypes.get(0).getId();
            Igvinvolvementtype igvinvolvementtype = baseIgvInvolvementTypeDataService.findById(id);
            igvinvolvementtype.setActivateddate(new Timestamp(new Date().getTime()));
            baseIgvInvolvementTypeDataService.update(id,igvinvolvementtype);
            return map(igvinvolvementtype, IgvInvolvementTypeDto.class);
        }else{
            if(igv.isactive==null){
                igv.setIsactive(true);
            }
            igv.setActivateddate(new Timestamp(new Date().getTime()));
            igv.setInactivateddate(new Timestamp(new Date().getTime()));
            baseIgvInvolvementTypeDataService.insert((igv));
            return map(igv, IgvInvolvementTypeDto.class);
        }
    }

    public List<IgvInvolvementTypeDto> createListIgvInvolvementTypes(List<IgvInvolvementTypeDto> categoryDtos){
        List<Igvinvolvementtype> igvinvolvementtypes = mapAll(categoryDtos, Igvinvolvementtype.class);
        IgvInvolvementTypeDto igvInvolvementTypeDto = new IgvInvolvementTypeDto();
        igvinvolvementtypes.stream().forEach((p)->{
            igvInvolvementTypeDto.setCode(p.getCode());
            igvInvolvementTypeDto.setName(p.getName());
            igvInvolvementTypeDto.setIsactive(p.getIsactive());
            igvInvolvementTypeDto.setTaxcode(p.getTaxcode());
            createIgvInvolvementType(igvInvolvementTypeDto);
        });
        return mapAll(igvinvolvementtypes,IgvInvolvementTypeDto.class);
    }

    public IgvInvolvementTypeDto updateIgvInvolvementType(String id, IgvInvolvementTypeDto igvInvolvementTypeDto){
        Igvinvolvementtype igvinvolvementtype = map(igvInvolvementTypeDto, Igvinvolvementtype.class);
        Igvinvolvementtype a = baseIgvInvolvementTypeDataService.findById(id);
        System.out.println(a);
        igvinvolvementtype.setActivateddate(a.getActivateddate());
        igvinvolvementtype.setInactivateddate(a.getInactivateddate());
        baseIgvInvolvementTypeDataService.update(id,igvinvolvementtype);
        return map(igvinvolvementtype, IgvInvolvementTypeDto.class);
    }

    public IgvInvolvementTypeDto getIgvInvolvementType(String id){
        Igvinvolvementtype dataServiceById = baseIgvInvolvementTypeDataService.findById(id);
        return map(dataServiceById, IgvInvolvementTypeDto.class);
    }

    public IgvInvolvementTypeDto disableIgvInvolvementType(String id){
        Igvinvolvementtype igvinvolvementtype = baseIgvInvolvementTypeDataService.findById(id);
        igvinvolvementtype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseIgvInvolvementTypeDataService.update(id,igvinvolvementtype);
        return map( baseIgvInvolvementTypeDataService.disabled(id), IgvInvolvementTypeDto.class);
    }

    public List<IgvInvolvementTypeDto> getAllIgvInvolvementType(){
        List<Igvinvolvementtype> igvinvolvementtypes = baseIgvInvolvementTypeDataService.findIntoList();
        return mapAll(igvinvolvementtypes, IgvInvolvementTypeDto.class);
    }

    public List<IgvInvolvementTypeDto> getListIgvInvolvementType(){
        List<Igvinvolvementtype> igvinvolvementtypes = baseIgvInvolvementTypeDataService.listAll();
        return mapAll(igvinvolvementtypes, IgvInvolvementTypeDto.class);
    }
}
