package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.*;
import innovar.io.product.models.tables.pojos.Product;
import innovar.io.product.services.ProductDataService;
import innovar.io.product.services.ProductPriceChargeDiscountDataService;
import innovar.io.product.services.ProductPriceDataService;
import innovar.io.product.services.ProductPriceTaxDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

@Service
public class ProductService extends BaseBussinesService {
    @Autowired
    ProductDataService productDataService;
    @Autowired
    ProductPriceService productPriceService;
    @Autowired
    ProductPriceDataService productPriceDataService;
    @Autowired
    ProductPriceTaxDataService productPriceTaxDataService;
    @Autowired
    ProductPriceChargeDiscountDataService productPriceChargeDiscountDataService;

    public ProductDto createProduct(ProductDto productDto){
        Product product = map(productDto, Product.class);
        if(product.isactive==null){
            product.setIsactive(true);
        }
        productDataService.insert((product));
        return map(product, ProductDto.class);
    }

    public ProductDto updateProduct(String id, ProductDto productDto){
        Product product = map(productDto,Product.class);
        product.setUpdatedon(new Timestamp(new Date().getTime()));
        if(product.isactive==null){
            product.setIsactive(true);
        }
        productDataService.update(id, product);
        return  map(product, ProductDto.class);
    }
    public ProductDto getProduct(String id){
        Product product= productDataService.findById(id);
        return map(product, ProductDto.class);
    }

    public List<ProductDto> getAllProduct(){
        List<Product> products= productDataService.findIntoList();
        return  mapAll(products, ProductDto.class);
    }

    public List<ProductDto> getListProduct(){
        List<Product> products= productDataService.listAll();
        return  mapAll(products, ProductDto.class);
    }

    public ProductDto disabledProduct(String id){
        Product product= productDataService.findById(id);
        product.setUpdatedon(new Timestamp(new Date().getTime()));
        productDataService.update(id, product);
        return map(productDataService.disabled(id),ProductDto.class);
    }

    public List<ProductFullDto> listProductFull(){
        List<ProductFullDto> productFullDtos = new ArrayList<>();
        List<ProductDto> productDtos = getAllProduct();
        for(int i=0; i<productDtos.size();i++){
            productFullDtos.add(getProductFullId(productDtos.get(i).getId()));
        }
        return productFullDtos;
    }

    public ProductFullDto getProductFullBarCode(String barCode){
        ProductFullDto productFullDto = new ProductFullDto();
        List<ProductDto> productDtos = productDataService.findProductBarCode(barCode);
        if(productDtos != null || productDtos.size() != 0){
            ProductDto productDto = getProduct(productDtos.get(0).getId());
            productFullDto.setId(productDto.getId());
            productFullDto.setBarcode(productDto.getBarcode());
            productFullDto.setCode(productDto.getCode());
            productFullDto.setDenomination(productDto.getDenomination());
            productFullDto.setDescription(productDto.getDescription());
            productFullDto.setStock(productDto.getStock());
            productFullDto.setSunatcode(productDto.getSunatcode());
            productFullDto.setCategorycode(productDto.getCategorycode());
            productFullDto.setIsactive(productDto.getIsactive());
            productFullDto.setProductPriceDto(getProductPriceDto(productDtos.get(0).getId()));
            String idProductPrice = productFullDto.getProductPriceDto().getId();
            productFullDto.setProductPriceTaxDtos(getProductPriceTaxDto(idProductPrice));
            productFullDto.setProductPriceChargeDiscountDtos(getProductPriceChargeDiscountDto(idProductPrice));
            return productFullDto;
        }
        return null;
    }
    public ProductFullDto getProductFullId(String id){
        ProductFullDto productFullDto = new ProductFullDto();
        ProductDto product = getProduct(id);
        if(product != null){
            ProductDto productDto = getProduct(product.getId());
            productFullDto.setId(productDto.getId());
            productFullDto.setBarcode(productDto.getBarcode());
            productFullDto.setCode(productDto.getCode());
            productFullDto.setDenomination(productDto.getDenomination());
            productFullDto.setDescription(productDto.getDescription());
            productFullDto.setStock(productDto.getStock());
            productFullDto.setSunatcode(productDto.getSunatcode());
            productFullDto.setCategorycode(productDto.getCategorycode());
            productFullDto.setIsactive(productDto.getIsactive());
            productFullDto.setProductPriceDto(getProductPriceDto(product.getId()));
            String idProductPrice = productFullDto.getProductPriceDto().getId();
            productFullDto.setProductPriceTaxDtos(getProductPriceTaxDto(idProductPrice));
            productFullDto.setProductPriceChargeDiscountDtos(getProductPriceChargeDiscountDto(idProductPrice));
            return productFullDto;
        }
        return null;
    }

    public ProductPriceDto getProductPriceDto(String productCode){
        List<ProductPriceDto> productPrice = productPriceDataService.findProductPrice(productCode);
        return productPriceService.getProductPrice(productPrice.get(0).getId());

    }

    public List<ProductPriceTaxDto> getProductPriceTaxDto(String idProductPrice){
        List<ProductPriceTaxDto> productPriceTaxDtos = productPriceTaxDataService.finProductPriceTax(idProductPrice);
        return  productPriceTaxDtos;
    }

    public List<ProductPriceChargeDiscountDto> getProductPriceChargeDiscountDto(String idProductPrice){
        return productPriceChargeDiscountDataService.findProductPriceChargeDiscount(idProductPrice);

    }



}
