package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;

import innovar.io.core.controller.BaseController;
import innovar.io.product.baseBussinesService.IgvInvolvementTypeService;
import innovar.io.product.dto.IgvInvolvementTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;
import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class IgvInvolvementTypeController extends BaseController {
    @Autowired
    IgvInvolvementTypeService igvInvolvementTypeService;

    @PostMapping("/igvinvolvementtype")
    public AppResponse createIgvInvolvementType(@RequestBody() IgvInvolvementTypeDto igvInvolvementTypeDto){
        return Response(()-> igvInvolvementTypeService.createIgvInvolvementType((igvInvolvementTypeDto)));
    }

    @PostMapping("/igvinvolvementtypeListCreate")
    public AppResponse createListIgvInvolvementType(@RequestBody() List<IgvInvolvementTypeDto> igvInvolvementTypeDtos){
        return Response(()-> igvInvolvementTypeService.createListIgvInvolvementTypes(igvInvolvementTypeDtos));
    }

    @PutMapping("/igvinvolvementtype/{id}")
    public AppResponse updateIgvInvolvementType(@PathVariable() String id, @RequestBody() IgvInvolvementTypeDto igvInvolvementTypeDto){
        return Response(()-> igvInvolvementTypeService.updateIgvInvolvementType((id),(igvInvolvementTypeDto)));
    }

    @GetMapping("/igvinvolvementtype/{id}")
    public AppResponse getIgvInvolvementType(@PathVariable() String id){
        return Response(()-> igvInvolvementTypeService.getIgvInvolvementType(id));
    }

    @GetMapping("/igvinvolvementtypes")
    public AppResponse getAllIgvInvolvementType(){
        return Response(()-> igvInvolvementTypeService.getAllIgvInvolvementType());
    }

    @GetMapping("/listIgvinvolvementtype")
    public AppResponse getListIgvInvolvementType(){
        return Response(()-> igvInvolvementTypeService.getListIgvInvolvementType());
    }

    @DeleteMapping("/igvinvolvementtype/{id}")
    public AppResponse disabledIgvInvolvementType(@PathVariable() String id){
        return Response(()-> igvInvolvementTypeService.disableIgvInvolvementType(id));
    }
}
