package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.baseBussinesService.DiscountChargeTypeService;
import innovar.io.product.dto.CategoryDto;
import innovar.io.product.dto.DiscountChargeTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class DiscountChargeTypeController extends BaseController {
    @Autowired
    DiscountChargeTypeService discountChargeTypeService;

    @PostMapping("/discountchargetype")
    public AppResponse createDiscountChargeType(@RequestBody() DiscountChargeTypeDto discountChargeTypeDtoo){
        return Response(()-> discountChargeTypeService.createDiscountChargeType((discountChargeTypeDtoo)));
    }

    @PostMapping("/discountchargetypeListCreate")
    public AppResponse createListDiscountChargeType(@RequestBody() List<DiscountChargeTypeDto> discountChargeTypeDtos){
        return Response(()-> discountChargeTypeService.createListDiscountChargeTypes(discountChargeTypeDtos));
    }

    @PutMapping("/discountchargetype/{id}")
    public AppResponse updateDiscountChargeType(@PathVariable() String id, @RequestBody() DiscountChargeTypeDto discountChargeTypeDto){
        return Response(()-> discountChargeTypeService.updateDiscountChargeType((id),(discountChargeTypeDto)));
    }

    @GetMapping("/discountchargetype/{id}")
    public AppResponse getDiscountChargeType(@PathVariable() String id){
        return Response(()-> discountChargeTypeService.getDiscountChargeType(id));
    }

    @GetMapping("/discountchargetypes")
    public AppResponse getAllDiscountChargeType(){
        return Response(()-> discountChargeTypeService.getAllDiscountChargeType());
    }

    @GetMapping("/ListDiscountChargeType")
    public AppResponse getListDiscountChargeType(){
        return Response(()-> discountChargeTypeService.getListDiscountChargeType());
    }

    @GetMapping("/listDiscountChargeTypeItem")
    public AppResponse getListDiscountChargeTypeItem(){
        return Response(()-> discountChargeTypeService.listDiscountChargeTypeItem());
    }

    @DeleteMapping("/discountchargetype/{id}")
    public AppResponse disabledDiscountChargeType(@PathVariable() String id){
        return Response(()-> discountChargeTypeService.disableDiscountChargeType(id));
    }
}
