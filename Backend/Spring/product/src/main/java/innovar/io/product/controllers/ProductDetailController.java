package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.baseBussinesService.ProductDetailService;
import innovar.io.product.dto.ProductDetailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;
@RestController
@RequestMapping(ProductApplication.host)
public class ProductDetailController extends BaseController {
    @Autowired
    ProductDetailService productDetailService;

    @PostMapping("/productdetail")
    public AppResponse createProductDetail(@RequestBody() ProductDetailDto productDetailDto){
        return Response(()-> productDetailService.createProductDetail((productDetailDto)));
    }

    @PutMapping("/productdetail/{id}")
    public AppResponse updateProductDetail(@PathVariable() String id, @RequestBody() ProductDetailDto productDetailDto){
        return Response(()-> productDetailService.updateProductDetail((id),(productDetailDto)));
    }

    @GetMapping("/productdetail/{id}")
    public AppResponse getProductDetail(@PathVariable() String id){
        return Response(()-> productDetailService.getProductDetail(id));
    }

    @GetMapping("/productdetails")
    public AppResponse getAllProductDetail(){
        return Response(()-> productDetailService.getAllProductDetails());
    }

    @GetMapping("/ListProductdetails")
    public AppResponse getListProductDetail(){
        return Response(()-> productDetailService.getListProductDetails());
    }

    @DeleteMapping("/productdetail/{id}")
    public AppResponse disabledProductDetail(@PathVariable() String id){
        return Response(()-> productDetailService.disabledProductDetail(id));
    }
}
