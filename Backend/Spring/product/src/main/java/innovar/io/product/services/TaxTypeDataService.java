package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Taxtype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaxTypeDataService extends RepositoryPostgressService<Taxtype> {

    public TaxTypeDataService (DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Taxtype.class);
    }

    @Override
    public innovar.io.product.models.tables.Taxtype getTable() {
        return Tables.TAXTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.TAXTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.TAXTYPE.ISACTIVE;
    }
}
