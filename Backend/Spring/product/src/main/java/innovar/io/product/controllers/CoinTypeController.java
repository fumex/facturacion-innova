package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.product.baseBussinesService.CoinTypeService;
import innovar.io.product.dto.CoinTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class CoinTypeController extends BaseController {
    @Autowired
    CoinTypeService coinTypeService;

    @PostMapping("/cointype")
    public AppResponse createCoinType(@RequestBody() CoinTypeDto coinTypeDto){
        return Response(()-> coinTypeService.createCoinType((coinTypeDto)));
    }

    @PostMapping("/cointypeListCreate")
    public AppResponse createListCoinType(@RequestBody() List<CoinTypeDto> coinTypeDtos){
        return Response(()->coinTypeService.createListCoinType(coinTypeDtos));
    }

    @PutMapping("/cointype/{id}")
    public AppResponse updateCoinType(@PathVariable() String id, @RequestBody() CoinTypeDto coinTypeDto){
        return Response(()-> coinTypeService.updateCoinType((id),(coinTypeDto)));
    }

    @GetMapping("/cointype/{id}")
    public AppResponse getCoinType(@PathVariable() String id){
        return Response(()-> coinTypeService.getCoinType(id));
    }

    @GetMapping("/cointypes")
    public AppResponse getAllCoinTypes(){
        return Response(()-> coinTypeService.getAllCoinTypes());
    }

    @GetMapping("/ListCoinTypes")
    public AppResponse getListCoinTypes(){
        return Response(()-> coinTypeService.getListCoinTypes());
    }

    @DeleteMapping("/cointype/{id}")
    public AppResponse disabledCoinType(@PathVariable() String id){
        return Response(()-> coinTypeService.disableCoinType(id));
    }
}
