package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Sunatcode;
import innovar.io.product.models.tables.pojos.Unitmeasurement;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitMeasurementDataService extends RepositoryPostgressService<Unitmeasurement> {

    public UnitMeasurementDataService (DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Unitmeasurement.class);
    }

    @Override
    public innovar.io.product.models.tables.Unitmeasurement getTable() {
        return Tables.UNITMEASUREMENT;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.UNITMEASUREMENT.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.UNITMEASUREMENT.ISACTIVE;
    }

}
