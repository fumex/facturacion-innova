package innovar.io.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProductPriceTaxDto {
    private String      id;
    private String     idproductprice;
    private String     igvinvolvementtypecode;
    private String     iscinvolvementtypecode;
    private String     taxtypecode;
    private BigDecimal unittaxamount;
    public Boolean     isactive;
}
