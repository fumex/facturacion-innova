package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Iscinvolvementtype;
import innovar.io.product.models.tables.pojos.Sunatcode;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SunatCodeDataService  extends RepositoryPostgressService<Sunatcode> {

    public SunatCodeDataService(DSLContext dsl){ super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Sunatcode.class);
    }

    @Override
    public innovar.io.product.models.tables.Sunatcode getTable() {
        return Tables.SUNATCODE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.SUNATCODE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.SUNATCODE.ISACTIVE;
    }
}
