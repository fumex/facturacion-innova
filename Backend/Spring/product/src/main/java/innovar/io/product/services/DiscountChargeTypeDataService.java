package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Category;
import innovar.io.product.models.tables.pojos.Discountchargetype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountChargeTypeDataService extends RepositoryPostgressService<Discountchargetype> {

    public DiscountChargeTypeDataService (DSLContext dsl){ super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Discountchargetype.class);
    }

    public List findDiscountChargeItem(){

        return find(getTable().LEVEL.eq("Item").and(getTable().ISACTIVE.eq(true))).fetchInto(Discountchargetype.class);
    }

    @Override
    public innovar.io.product.models.tables.Discountchargetype getTable() {
        return Tables.DISCOUNTCHARGETYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.DISCOUNTCHARGETYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.DISCOUNTCHARGETYPE.ISACTIVE;
    }
}
