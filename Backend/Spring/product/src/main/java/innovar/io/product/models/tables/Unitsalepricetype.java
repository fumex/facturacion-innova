/*
 * This file is generated by jOOQ.
 */
package innovar.io.product.models.tables;


import innovar.io.product.models.Indexes;
import innovar.io.product.models.Keys;
import innovar.io.product.models.Public;
import innovar.io.product.models.tables.records.UnitsalepricetypeRecord;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import javax.annotation.Generated;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Unitsalepricetype extends TableImpl<UnitsalepricetypeRecord> {

    private static final long serialVersionUID = -736314768;

    /**
     * The reference instance of <code>public.unitsalepricetype</code>
     */
    public static final Unitsalepricetype UNITSALEPRICETYPE = new Unitsalepricetype();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<UnitsalepricetypeRecord> getRecordType() {
        return UnitsalepricetypeRecord.class;
    }

    /**
     * The column <code>public.unitsalepricetype.id</code>.
     */
    public final TableField<UnitsalepricetypeRecord, String> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.VARCHAR(24).nullable(false), this, "");

    /**
     * The column <code>public.unitsalepricetype.code</code>.
     */
    public final TableField<UnitsalepricetypeRecord, String> CODE = createField(DSL.name("code"), org.jooq.impl.SQLDataType.VARCHAR(2), this, "");

    /**
     * The column <code>public.unitsalepricetype.name</code>.
     */
    public final TableField<UnitsalepricetypeRecord, String> NAME = createField(DSL.name("name"), org.jooq.impl.SQLDataType.VARCHAR(100), this, "");

    /**
     * The column <code>public.unitsalepricetype.activateddate</code>.
     */
    public final TableField<UnitsalepricetypeRecord, Timestamp> ACTIVATEDDATE = createField(DSL.name("activateddate"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.unitsalepricetype.inactivateddate</code>.
     */
    public final TableField<UnitsalepricetypeRecord, Timestamp> INACTIVATEDDATE = createField(DSL.name("inactivateddate"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.unitsalepricetype.createdon</code>.
     */
    public final TableField<UnitsalepricetypeRecord, Timestamp> CREATEDON = createField(DSL.name("createdon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.unitsalepricetype.createdby</code>.
     */
    public final TableField<UnitsalepricetypeRecord, String> CREATEDBY = createField(DSL.name("createdby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.unitsalepricetype.updatedon</code>.
     */
    public final TableField<UnitsalepricetypeRecord, Timestamp> UPDATEDON = createField(DSL.name("updatedon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.unitsalepricetype.updatedby</code>.
     */
    public final TableField<UnitsalepricetypeRecord, String> UPDATEDBY = createField(DSL.name("updatedby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.unitsalepricetype.isactive</code>.
     */
    public final TableField<UnitsalepricetypeRecord, Boolean> ISACTIVE = createField(DSL.name("isactive"), org.jooq.impl.SQLDataType.BOOLEAN, this, "");

    /**
     * The column <code>public.unitsalepricetype.class</code>.
     */
    public final TableField<UnitsalepricetypeRecord, String> CLASS = createField(DSL.name("class"), org.jooq.impl.SQLDataType.VARCHAR(200), this, "");

    /**
     * Create a <code>public.unitsalepricetype</code> table reference
     */
    public Unitsalepricetype() {
        this(DSL.name("unitsalepricetype"), null);
    }

    /**
     * Create an aliased <code>public.unitsalepricetype</code> table reference
     */
    public Unitsalepricetype(String alias) {
        this(DSL.name(alias), UNITSALEPRICETYPE);
    }

    /**
     * Create an aliased <code>public.unitsalepricetype</code> table reference
     */
    public Unitsalepricetype(Name alias) {
        this(alias, UNITSALEPRICETYPE);
    }

    private Unitsalepricetype(Name alias, Table<UnitsalepricetypeRecord> aliased) {
        this(alias, aliased, null);
    }

    private Unitsalepricetype(Name alias, Table<UnitsalepricetypeRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Unitsalepricetype(Table<O> child, ForeignKey<O, UnitsalepricetypeRecord> key) {
        super(child, key, UNITSALEPRICETYPE);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.UNITSALEPRICETYPE_CODE_KEY, Indexes.UNITSALEPRICETYPE_CODE_KEY1, Indexes.UNITSALEPRICETYPE_PKEY);
    }

    @Override
    public UniqueKey<UnitsalepricetypeRecord> getPrimaryKey() {
        return Keys.UNITSALEPRICETYPE_PKEY;
    }

    @Override
    public List<UniqueKey<UnitsalepricetypeRecord>> getKeys() {
        return Arrays.<UniqueKey<UnitsalepricetypeRecord>>asList(Keys.UNITSALEPRICETYPE_PKEY, Keys.UNITSALEPRICETYPE_CODE_KEY, Keys.UNITSALEPRICETYPE_CODE_KEY1);
    }

    @Override
    public Unitsalepricetype as(String alias) {
        return new Unitsalepricetype(DSL.name(alias), this);
    }

    @Override
    public Unitsalepricetype as(Name alias) {
        return new Unitsalepricetype(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Unitsalepricetype rename(String name) {
        return new Unitsalepricetype(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Unitsalepricetype rename(Name name) {
        return new Unitsalepricetype(name, null);
    }

    // -------------------------------------------------------------------------
    // Row11 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row11<String, String, String, Timestamp, Timestamp, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row11) super.fieldsRow();
    }
}
