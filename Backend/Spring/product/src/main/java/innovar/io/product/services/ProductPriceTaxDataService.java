package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.dto.ProductPriceTaxDto;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Productpricetax;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductPriceTaxDataService extends RepositoryPostgressService<Productpricetax> {
    public ProductPriceTaxDataService(DSLContext dsl){super(dsl);}

    public List finProductPriceTax(String idProductPrice){
        return find(getTable().IDPRODUCTPRICE.eq(idProductPrice)).fetchInto(ProductPriceTaxDto.class);
    }
    @Override
    public innovar.io.product.models.tables.Productpricetax getTable() {
        return Tables.PRODUCTPRICETAX;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PRODUCTPRICETAX.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PRODUCTPRICETAX.ISACTIVE;
    }
}
