/*
 * This file is generated by jOOQ.
 */
package innovar.io.product.models.tables.records;


import innovar.io.product.models.tables.Productdetail;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record17;
import org.jooq.Row17;
import org.jooq.impl.UpdatableRecordImpl;

import javax.annotation.Generated;
import java.sql.Date;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProductdetailRecord extends UpdatableRecordImpl<ProductdetailRecord> implements Record17<String, String, String, String, String, String, String, Date, Date, String, String, Timestamp, String, Timestamp, String, Boolean, String> {

    private static final long serialVersionUID = 818245218;

    /**
     * Setter for <code>public.productdetail.id</code>.
     */
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.productdetail.id</code>.
     */
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>public.productdetail.batch</code>.
     */
    public void setBatch(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.productdetail.batch</code>.
     */
    public String getBatch() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.productdetail.brand</code>.
     */
    public void setBrand(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.productdetail.brand</code>.
     */
    public String getBrand() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.productdetail.color</code>.
     */
    public void setColor(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.productdetail.color</code>.
     */
    public String getColor() {
        return (String) get(3);
    }

    /**
     * Setter for <code>public.productdetail.dimension</code>.
     */
    public void setDimension(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>public.productdetail.dimension</code>.
     */
    public String getDimension() {
        return (String) get(4);
    }

    /**
     * Setter for <code>public.productdetail.model</code>.
     */
    public void setModel(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>public.productdetail.model</code>.
     */
    public String getModel() {
        return (String) get(5);
    }

    /**
     * Setter for <code>public.productdetail.serie</code>.
     */
    public void setSerie(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>public.productdetail.serie</code>.
     */
    public String getSerie() {
        return (String) get(6);
    }

    /**
     * Setter for <code>public.productdetail.expirationdate</code>.
     */
    public void setExpirationdate(Date value) {
        set(7, value);
    }

    /**
     * Getter for <code>public.productdetail.expirationdate</code>.
     */
    public Date getExpirationdate() {
        return (Date) get(7);
    }

    /**
     * Setter for <code>public.productdetail.productiondate</code>.
     */
    public void setProductiondate(Date value) {
        set(8, value);
    }

    /**
     * Getter for <code>public.productdetail.productiondate</code>.
     */
    public Date getProductiondate() {
        return (Date) get(8);
    }

    /**
     * Setter for <code>public.productdetail.productcode</code>.
     */
    public void setProductcode(String value) {
        set(9, value);
    }

    /**
     * Getter for <code>public.productdetail.productcode</code>.
     */
    public String getProductcode() {
        return (String) get(9);
    }

    /**
     * Setter for <code>public.productdetail.unitcode</code>.
     */
    public void setUnitcode(String value) {
        set(10, value);
    }

    /**
     * Getter for <code>public.productdetail.unitcode</code>.
     */
    public String getUnitcode() {
        return (String) get(10);
    }

    /**
     * Setter for <code>public.productdetail.createdon</code>.
     */
    public void setCreatedon(Timestamp value) {
        set(11, value);
    }

    /**
     * Getter for <code>public.productdetail.createdon</code>.
     */
    public Timestamp getCreatedon() {
        return (Timestamp) get(11);
    }

    /**
     * Setter for <code>public.productdetail.createdby</code>.
     */
    public void setCreatedby(String value) {
        set(12, value);
    }

    /**
     * Getter for <code>public.productdetail.createdby</code>.
     */
    public String getCreatedby() {
        return (String) get(12);
    }

    /**
     * Setter for <code>public.productdetail.updatedon</code>.
     */
    public void setUpdatedon(Timestamp value) {
        set(13, value);
    }

    /**
     * Getter for <code>public.productdetail.updatedon</code>.
     */
    public Timestamp getUpdatedon() {
        return (Timestamp) get(13);
    }

    /**
     * Setter for <code>public.productdetail.updatedby</code>.
     */
    public void setUpdatedby(String value) {
        set(14, value);
    }

    /**
     * Getter for <code>public.productdetail.updatedby</code>.
     */
    public String getUpdatedby() {
        return (String) get(14);
    }

    /**
     * Setter for <code>public.productdetail.isactive</code>.
     */
    public void setIsactive(Boolean value) {
        set(15, value);
    }

    /**
     * Getter for <code>public.productdetail.isactive</code>.
     */
    public Boolean getIsactive() {
        return (Boolean) get(15);
    }

    /**
     * Setter for <code>public.productdetail.class</code>.
     */
    public void setClass_(String value) {
        set(16, value);
    }

    /**
     * Getter for <code>public.productdetail.class</code>.
     */
    public String getClass_() {
        return (String) get(16);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record17 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row17<String, String, String, String, String, String, String, Date, Date, String, String, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row17) super.fieldsRow();
    }

    @Override
    public Row17<String, String, String, String, String, String, String, Date, Date, String, String, Timestamp, String, Timestamp, String, Boolean, String> valuesRow() {
        return (Row17) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Productdetail.PRODUCTDETAIL.ID;
    }

    @Override
    public Field<String> field2() {
        return Productdetail.PRODUCTDETAIL.BATCH;
    }

    @Override
    public Field<String> field3() {
        return Productdetail.PRODUCTDETAIL.BRAND;
    }

    @Override
    public Field<String> field4() {
        return Productdetail.PRODUCTDETAIL.COLOR;
    }

    @Override
    public Field<String> field5() {
        return Productdetail.PRODUCTDETAIL.DIMENSION;
    }

    @Override
    public Field<String> field6() {
        return Productdetail.PRODUCTDETAIL.MODEL;
    }

    @Override
    public Field<String> field7() {
        return Productdetail.PRODUCTDETAIL.SERIE;
    }

    @Override
    public Field<Date> field8() {
        return Productdetail.PRODUCTDETAIL.EXPIRATIONDATE;
    }

    @Override
    public Field<Date> field9() {
        return Productdetail.PRODUCTDETAIL.PRODUCTIONDATE;
    }

    @Override
    public Field<String> field10() {
        return Productdetail.PRODUCTDETAIL.PRODUCTCODE;
    }

    @Override
    public Field<String> field11() {
        return Productdetail.PRODUCTDETAIL.UNITCODE;
    }

    @Override
    public Field<Timestamp> field12() {
        return Productdetail.PRODUCTDETAIL.CREATEDON;
    }

    @Override
    public Field<String> field13() {
        return Productdetail.PRODUCTDETAIL.CREATEDBY;
    }

    @Override
    public Field<Timestamp> field14() {
        return Productdetail.PRODUCTDETAIL.UPDATEDON;
    }

    @Override
    public Field<String> field15() {
        return Productdetail.PRODUCTDETAIL.UPDATEDBY;
    }

    @Override
    public Field<Boolean> field16() {
        return Productdetail.PRODUCTDETAIL.ISACTIVE;
    }

    @Override
    public Field<String> field17() {
        return Productdetail.PRODUCTDETAIL.CLASS;
    }

    @Override
    public String component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getBatch();
    }

    @Override
    public String component3() {
        return getBrand();
    }

    @Override
    public String component4() {
        return getColor();
    }

    @Override
    public String component5() {
        return getDimension();
    }

    @Override
    public String component6() {
        return getModel();
    }

    @Override
    public String component7() {
        return getSerie();
    }

    @Override
    public Date component8() {
        return getExpirationdate();
    }

    @Override
    public Date component9() {
        return getProductiondate();
    }

    @Override
    public String component10() {
        return getProductcode();
    }

    @Override
    public String component11() {
        return getUnitcode();
    }

    @Override
    public Timestamp component12() {
        return getCreatedon();
    }

    @Override
    public String component13() {
        return getCreatedby();
    }

    @Override
    public Timestamp component14() {
        return getUpdatedon();
    }

    @Override
    public String component15() {
        return getUpdatedby();
    }

    @Override
    public Boolean component16() {
        return getIsactive();
    }

    @Override
    public String component17() {
        return getClass_();
    }

    @Override
    public String value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getBatch();
    }

    @Override
    public String value3() {
        return getBrand();
    }

    @Override
    public String value4() {
        return getColor();
    }

    @Override
    public String value5() {
        return getDimension();
    }

    @Override
    public String value6() {
        return getModel();
    }

    @Override
    public String value7() {
        return getSerie();
    }

    @Override
    public Date value8() {
        return getExpirationdate();
    }

    @Override
    public Date value9() {
        return getProductiondate();
    }

    @Override
    public String value10() {
        return getProductcode();
    }

    @Override
    public String value11() {
        return getUnitcode();
    }

    @Override
    public Timestamp value12() {
        return getCreatedon();
    }

    @Override
    public String value13() {
        return getCreatedby();
    }

    @Override
    public Timestamp value14() {
        return getUpdatedon();
    }

    @Override
    public String value15() {
        return getUpdatedby();
    }

    @Override
    public Boolean value16() {
        return getIsactive();
    }

    @Override
    public String value17() {
        return getClass_();
    }

    @Override
    public ProductdetailRecord value1(String value) {
        setId(value);
        return this;
    }

    @Override
    public ProductdetailRecord value2(String value) {
        setBatch(value);
        return this;
    }

    @Override
    public ProductdetailRecord value3(String value) {
        setBrand(value);
        return this;
    }

    @Override
    public ProductdetailRecord value4(String value) {
        setColor(value);
        return this;
    }

    @Override
    public ProductdetailRecord value5(String value) {
        setDimension(value);
        return this;
    }

    @Override
    public ProductdetailRecord value6(String value) {
        setModel(value);
        return this;
    }

    @Override
    public ProductdetailRecord value7(String value) {
        setSerie(value);
        return this;
    }

    @Override
    public ProductdetailRecord value8(Date value) {
        setExpirationdate(value);
        return this;
    }

    @Override
    public ProductdetailRecord value9(Date value) {
        setProductiondate(value);
        return this;
    }

    @Override
    public ProductdetailRecord value10(String value) {
        setProductcode(value);
        return this;
    }

    @Override
    public ProductdetailRecord value11(String value) {
        setUnitcode(value);
        return this;
    }

    @Override
    public ProductdetailRecord value12(Timestamp value) {
        setCreatedon(value);
        return this;
    }

    @Override
    public ProductdetailRecord value13(String value) {
        setCreatedby(value);
        return this;
    }

    @Override
    public ProductdetailRecord value14(Timestamp value) {
        setUpdatedon(value);
        return this;
    }

    @Override
    public ProductdetailRecord value15(String value) {
        setUpdatedby(value);
        return this;
    }

    @Override
    public ProductdetailRecord value16(Boolean value) {
        setIsactive(value);
        return this;
    }

    @Override
    public ProductdetailRecord value17(String value) {
        setClass_(value);
        return this;
    }

    @Override
    public ProductdetailRecord values(String value1, String value2, String value3, String value4, String value5, String value6, String value7, Date value8, Date value9, String value10, String value11, Timestamp value12, String value13, Timestamp value14, String value15, Boolean value16, String value17) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        value13(value13);
        value14(value14);
        value15(value15);
        value16(value16);
        value17(value17);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ProductdetailRecord
     */
    public ProductdetailRecord() {
        super(Productdetail.PRODUCTDETAIL);
    }

    /**
     * Create a detached, initialised ProductdetailRecord
     */
    public ProductdetailRecord(String id, String batch, String brand, String color, String dimension, String model, String serie, Date expirationdate, Date productiondate, String productcode, String unitcode, Timestamp createdon, String createdby, Timestamp updatedon, String updatedby, Boolean isactive, String class_) {
        super(Productdetail.PRODUCTDETAIL);

        set(0, id);
        set(1, batch);
        set(2, brand);
        set(3, color);
        set(4, dimension);
        set(5, model);
        set(6, serie);
        set(7, expirationdate);
        set(8, productiondate);
        set(9, productcode);
        set(10, unitcode);
        set(11, createdon);
        set(12, createdby);
        set(13, updatedon);
        set(14, updatedby);
        set(15, isactive);
        set(16, class_);
    }
}
