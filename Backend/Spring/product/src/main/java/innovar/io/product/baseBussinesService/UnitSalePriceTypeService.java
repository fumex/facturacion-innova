package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.UnitSalePriceTypeDto;
import innovar.io.product.models.tables.pojos.Unitsalepricetype;
import innovar.io.product.services.UnitSalePriceTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class UnitSalePriceTypeService extends BaseBussinesService {

    @Autowired
    UnitSalePriceTypeDataService baseUnitSalePriceTypeDataService;

    public UnitSalePriceTypeDto createUnitSalePriceType(UnitSalePriceTypeDto unitSalePriceTypeDto){
        Unitsalepricetype unitsalepricetype = map(unitSalePriceTypeDto, Unitsalepricetype.class);
        List<Unitsalepricetype> unitsalepricetypes = baseUnitSalePriceTypeDataService.findCode(unitSalePriceTypeDto.getCode());
        if(unitsalepricetypes.size()>0){
            String id = unitsalepricetypes.get(0).getId();
            Unitsalepricetype unitSalePriceTypeById = baseUnitSalePriceTypeDataService.findById(id);
            unitSalePriceTypeById.setActivateddate(new Timestamp(new Date().getTime()));
            baseUnitSalePriceTypeDataService.update(id,unitSalePriceTypeById);
            return map(unitSalePriceTypeById, UnitSalePriceTypeDto.class);
        }else{
            if(unitsalepricetype.isactive==null){
                unitsalepricetype.setIsactive(true);
            }
            unitsalepricetype.setActivateddate(new Timestamp(new Date().getTime()));
            unitsalepricetype.setInactivateddate(new Timestamp(new Date().getTime()));
            baseUnitSalePriceTypeDataService.insert((unitsalepricetype));
            return map(unitsalepricetype, UnitSalePriceTypeDto.class);
        }
    }

    public List<UnitSalePriceTypeDto> createListUnitSalePriceTypes(List<UnitSalePriceTypeDto> unitSalePriceTypeDtos){
        List<Unitsalepricetype> unitsalepricetypes = mapAll(unitSalePriceTypeDtos, Unitsalepricetype.class);
        UnitSalePriceTypeDto unitSalePriceTypeDto = new UnitSalePriceTypeDto();
        unitsalepricetypes.stream().forEach((p)->{
            unitSalePriceTypeDto.setCode(p.getCode());
            unitSalePriceTypeDto.setName(p.getName());
            unitSalePriceTypeDto.setIsactive(p.getIsactive());
            createUnitSalePriceType(unitSalePriceTypeDto);
        });
        return mapAll(unitsalepricetypes,UnitSalePriceTypeDto.class);
    }

    public UnitSalePriceTypeDto updateUnitSalePriceType(String id, UnitSalePriceTypeDto unitSalePriceTypeDto){
        Unitsalepricetype unitsalepricetype = map(unitSalePriceTypeDto, Unitsalepricetype.class);
        Unitsalepricetype a = baseUnitSalePriceTypeDataService.findById(id);
        unitsalepricetype.setActivateddate(a.getActivateddate());
        unitsalepricetype.setInactivateddate(a.getInactivateddate());
        baseUnitSalePriceTypeDataService.update(id,unitsalepricetype);
        return map(unitsalepricetype, UnitSalePriceTypeDto.class);
    }

    public UnitSalePriceTypeDto getUnitSalePriceType(String id){
        Unitsalepricetype unitsalepricetype = baseUnitSalePriceTypeDataService.findById(id);
        return map(unitsalepricetype, UnitSalePriceTypeDto.class);
    }

    public UnitSalePriceTypeDto disableUnitSalePriceType(String id){
        Unitsalepricetype unitsalepricetype = baseUnitSalePriceTypeDataService.findById(id);
        unitsalepricetype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseUnitSalePriceTypeDataService.update(id,unitsalepricetype);
        return map( baseUnitSalePriceTypeDataService.disabled(id), UnitSalePriceTypeDto.class);
    }

    public List<UnitSalePriceTypeDto> getAllUnitSalePriceType(){
        List<Unitsalepricetype> unitsalepricetypes = baseUnitSalePriceTypeDataService.findIntoList();
        return mapAll(unitsalepricetypes, UnitSalePriceTypeDto.class);
    }

    public List<UnitSalePriceTypeDto> getListUnitSalePriceType(){
        List<Unitsalepricetype> unitsalepricetypes = baseUnitSalePriceTypeDataService.listAll();
        return mapAll(unitsalepricetypes, UnitSalePriceTypeDto.class);
    }
}
