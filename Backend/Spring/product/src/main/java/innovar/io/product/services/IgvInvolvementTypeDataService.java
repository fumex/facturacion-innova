package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Igvinvolvementtype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IgvInvolvementTypeDataService extends RepositoryPostgressService<Igvinvolvementtype> {

    public IgvInvolvementTypeDataService(DSLContext dsl){super (dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Igvinvolvementtype.class);
    }

    @Override
    public innovar.io.product.models.tables.Igvinvolvementtype getTable() {
        return Tables.IGVINVOLVEMENTTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.IGVINVOLVEMENTTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.IGVINVOLVEMENTTYPE.ISACTIVE;
    }
}
