package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;

import innovar.io.core.controller.BaseController;
import innovar.io.product.baseBussinesService.ProductService;
import innovar.io.product.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

@RestController
@RequestMapping(ProductApplication.host)
public class ProductController extends BaseController {
    @Autowired
    ProductService productService;

    @PostMapping("/product")
    public AppResponse createProduct(@RequestBody() ProductDto productDetailDto){
        return Response(()-> productService.createProduct((productDetailDto)));
    }

    @PutMapping("/product/{id}")
    public AppResponse updateProduct(@PathVariable() String id, @RequestBody() ProductDto productDetailDto){
        return Response(()-> productService.updateProduct((id),(productDetailDto)));
    }

    @GetMapping("/product/{id}")
    public AppResponse getProduct(@PathVariable() String id){
        return Response(()-> productService.getProduct(id));
    }

    @GetMapping("/products")
    public AppResponse getAllProduct(){
        return Response(()-> productService.getAllProduct());
    }

    @GetMapping("/ListProducts")
    public AppResponse getListProduct(){
        return Response(()-> productService.getListProduct());
    }

    @DeleteMapping("/product/{id}")
    public AppResponse disabledProduct(@PathVariable() String id){
        return Response(()-> productService.disabledProduct(id));
    }

    //*******************************************
    @GetMapping("/productPrices/{code}")
    public AppResponse getListProductPrices(String code){
        return Response(()-> productService.getProductPriceDto(code));
    }
    @GetMapping("/ListProductsFull")
    public AppResponse getListProductFull(){
        return Response(()-> productService.listProductFull());
    }

    @GetMapping("/productFullBarCode/{barCode}")
    public AppResponse getProductFullBarCode(String barCode){
        return Response(()-> productService.getProductFullBarCode(barCode));
    }
    @GetMapping("/productFullId/{id}")
    public AppResponse getProductFullId(String id){
        return Response(()-> productService.getProductFullBarCode(id));
    }

}
