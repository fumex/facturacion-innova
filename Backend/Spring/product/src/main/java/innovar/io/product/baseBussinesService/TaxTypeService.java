package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.TaxTypeDto;
import innovar.io.product.models.tables.pojos.Taxtype;
import innovar.io.product.services.TaxTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class TaxTypeService extends BaseBussinesService {

    @Autowired
    TaxTypeDataService baseTaxTypeDataService;

    public TaxTypeDto createTaxType(TaxTypeDto taxTypeDto){
        Taxtype taxtype = map(taxTypeDto, Taxtype.class);
        List<Taxtype> taxtypes = baseTaxTypeDataService.findCode(taxTypeDto.getCode());
        if(taxtypes.size()>0){
            String id = taxtypes.get(0).getId();
            Taxtype taxtypeId = baseTaxTypeDataService.findById(id);
            taxtypeId.setActivateddate(new Timestamp(new Date().getTime()));
            baseTaxTypeDataService.update(id,taxtypeId);
            return map(taxtypeId, TaxTypeDto.class);
        }else{
            if(taxtype.isactive==null){
                taxtype.setIsactive(true);
            }
            taxtype.setActivateddate(new Timestamp(new Date().getTime()));
            taxtype.setInactivateddate(new Timestamp(new Date().getTime()));
            baseTaxTypeDataService.insert((taxtype));
            return map(taxtype, TaxTypeDto.class);
        }
    }

    public List<TaxTypeDto> createListTaxTypes(List<TaxTypeDto> taxTypeDtos){
        List<Taxtype> taxtypes = mapAll(taxTypeDtos, Taxtype.class);
        TaxTypeDto taxTypeDto = new TaxTypeDto();
        taxtypes.stream().forEach((p)->{
            taxTypeDto.setCode(p.getCode());
            taxTypeDto.setName(p.getName());
            taxTypeDto.setAbbreviation(p.getAbbreviation());
            taxTypeDto.setCodeinternational(p.getCodeinternational());
            taxTypeDto.setIsactive(p.getIsactive());
            createTaxType(taxTypeDto);
        });
        return mapAll(taxtypes,TaxTypeDto.class);
    }

    public TaxTypeDto updateTaxType(String id, TaxTypeDto taxTypeDto){
        Taxtype taxtype = map(taxTypeDto, Taxtype.class);
        Taxtype a = baseTaxTypeDataService.findById(id);
        taxtype.setActivateddate(a.getActivateddate());
        taxtype.setInactivateddate(a.getInactivateddate());
        baseTaxTypeDataService.update(id,taxtype);
        return map(taxtype, TaxTypeDto.class);
    }

    public TaxTypeDto getTaxType(String id){
        Taxtype taxtype = baseTaxTypeDataService.findById(id);
        return map(taxtype, TaxTypeDto.class);
    }

    public TaxTypeDto disableTaxType(String id){
        Taxtype taxtype = baseTaxTypeDataService.findById(id);
        taxtype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseTaxTypeDataService.update(id,taxtype);
        return map( baseTaxTypeDataService.disabled(id), TaxTypeDto.class);
    }

    public List<TaxTypeDto> getAllTaxType(){
        List<Taxtype> taxtypes = baseTaxTypeDataService.findIntoList();
        return mapAll(taxtypes, TaxTypeDto.class);
    }

    public List<TaxTypeDto> getListTaxType(){
        List<Taxtype> taxtypes = baseTaxTypeDataService.listAll();
        return mapAll(taxtypes, TaxTypeDto.class);
    }
}
