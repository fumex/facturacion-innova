/*
 * This file is generated by jOOQ.
 */
package innovar.io.product.models.tables;


import innovar.io.product.models.Indexes;
import innovar.io.product.models.Keys;
import innovar.io.product.models.Public;
import innovar.io.product.models.tables.records.ProductRecord;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import javax.annotation.Generated;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Product extends TableImpl<ProductRecord> {

    private static final long serialVersionUID = 1465605729;

    /**
     * The reference instance of <code>public.product</code>
     */
    public static final Product PRODUCT = new Product();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ProductRecord> getRecordType() {
        return ProductRecord.class;
    }

    /**
     * The column <code>public.product.id</code>.
     */
    public final TableField<ProductRecord, String> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.VARCHAR(24).nullable(false), this, "");

    /**
     * The column <code>public.product.barcode</code>.
     */
    public final TableField<ProductRecord, String> BARCODE = createField(DSL.name("barcode"), org.jooq.impl.SQLDataType.VARCHAR(25), this, "");

    /**
     * The column <code>public.product.code</code>.
     */
    public final TableField<ProductRecord, String> CODE = createField(DSL.name("code"), org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false), this, "");

    /**
     * The column <code>public.product.denomination</code>.
     */
    public final TableField<ProductRecord, String> DENOMINATION = createField(DSL.name("denomination"), org.jooq.impl.SQLDataType.VARCHAR(50).nullable(false), this, "");

    /**
     * The column <code>public.product.description</code>.
     */
    public final TableField<ProductRecord, String> DESCRIPTION = createField(DSL.name("description"), org.jooq.impl.SQLDataType.VARCHAR(250).nullable(false), this, "");

    /**
     * The column <code>public.product.stock</code>.
     */
    public final TableField<ProductRecord, Integer> STOCK = createField(DSL.name("stock"), org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(DSL.field("0", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.product.sunatcode</code>.
     */
    public final TableField<ProductRecord, String> SUNATCODE = createField(DSL.name("sunatcode"), org.jooq.impl.SQLDataType.VARCHAR(10), this, "");

    /**
     * The column <code>public.product.categorycode</code>.
     */
    public final TableField<ProductRecord, String> CATEGORYCODE = createField(DSL.name("categorycode"), org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false), this, "");

    /**
     * The column <code>public.product.createdon</code>.
     */
    public final TableField<ProductRecord, Timestamp> CREATEDON = createField(DSL.name("createdon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.product.createdby</code>.
     */
    public final TableField<ProductRecord, String> CREATEDBY = createField(DSL.name("createdby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.product.updatedon</code>.
     */
    public final TableField<ProductRecord, Timestamp> UPDATEDON = createField(DSL.name("updatedon"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.product.updatedby</code>.
     */
    public final TableField<ProductRecord, String> UPDATEDBY = createField(DSL.name("updatedby"), org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.product.isactive</code>.
     */
    public final TableField<ProductRecord, Boolean> ISACTIVE = createField(DSL.name("isactive"), org.jooq.impl.SQLDataType.BOOLEAN.defaultValue(DSL.field("true", org.jooq.impl.SQLDataType.BOOLEAN)), this, "");

    /**
     * The column <code>public.product.class</code>.
     */
    public final TableField<ProductRecord, String> CLASS = createField(DSL.name("class"), org.jooq.impl.SQLDataType.VARCHAR(200), this, "");

    /**
     * Create a <code>public.product</code> table reference
     */
    public Product() {
        this(DSL.name("product"), null);
    }

    /**
     * Create an aliased <code>public.product</code> table reference
     */
    public Product(String alias) {
        this(DSL.name(alias), PRODUCT);
    }

    /**
     * Create an aliased <code>public.product</code> table reference
     */
    public Product(Name alias) {
        this(alias, PRODUCT);
    }

    private Product(Name alias, Table<ProductRecord> aliased) {
        this(alias, aliased, null);
    }

    private Product(Name alias, Table<ProductRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Product(Table<O> child, ForeignKey<O, ProductRecord> key) {
        super(child, key, PRODUCT);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.PRODUCT_PKEY);
    }

    @Override
    public UniqueKey<ProductRecord> getPrimaryKey() {
        return Keys.PRODUCT_PKEY;
    }

    @Override
    public List<UniqueKey<ProductRecord>> getKeys() {
        return Arrays.<UniqueKey<ProductRecord>>asList(Keys.PRODUCT_PKEY);
    }

    @Override
    public List<ForeignKey<ProductRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<ProductRecord, ?>>asList(Keys.PRODUCT__PRODUCT_CATEGORYCODE_FKEY);
    }

    public Category category() {
        return new Category(this, Keys.PRODUCT__PRODUCT_CATEGORYCODE_FKEY);
    }

    @Override
    public Product as(String alias) {
        return new Product(DSL.name(alias), this);
    }

    @Override
    public Product as(Name alias) {
        return new Product(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Product rename(String name) {
        return new Product(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Product rename(Name name) {
        return new Product(name, null);
    }

    // -------------------------------------------------------------------------
    // Row14 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row14<String, String, String, String, String, Integer, String, String, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row14) super.fieldsRow();
    }
}
