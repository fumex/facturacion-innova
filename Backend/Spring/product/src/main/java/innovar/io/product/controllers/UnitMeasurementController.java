package innovar.io.product.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;

import innovar.io.product.baseBussinesService.UnitMeasurementService;
import innovar.io.product.dto.UnitMeasurementDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import innovar.io.product.ProductApplication;

import java.util.List;

@RestController
@RequestMapping(ProductApplication.host)
public class UnitMeasurementController extends BaseController {
    @Autowired
    UnitMeasurementService unitMeasurementService;

    @PostMapping("/unitmeasurement")
    public AppResponse createUnitMeasurement(@RequestBody() UnitMeasurementDto unitMeasurementDto) {
        return Response(() -> unitMeasurementService.createUnitMeasurement((unitMeasurementDto)));
    }

    @PostMapping("/unitmeasurementListCreate")
    public AppResponse createListUnitMeasurement(@RequestBody() List<UnitMeasurementDto> unitMeasurementDtos) {
        return Response(() -> unitMeasurementService.createListUnitMeasurements(unitMeasurementDtos));
    }

    @PutMapping("unitmeasurement/{id}")
    public AppResponse updateUnitMeasurement(@PathVariable() String id, @RequestBody() UnitMeasurementDto unitMeasurementDto) {
        return Response(() -> unitMeasurementService.updateUnitMeasurement((id), (unitMeasurementDto)));
    }

    @GetMapping("/unitmeasurement/{id}")
    public AppResponse getUnitMeasurement(@PathVariable() String id) {
        return Response(() -> unitMeasurementService.getUnitMeasurement(id));
    }

    @GetMapping("/unitmeasurements")
    public AppResponse getAllUnitMeasurement() {
        return Response(() -> unitMeasurementService.getAllUnitMeasurement());
    }

    @GetMapping("/listUnitmeasurement")
    public AppResponse getListUnitMeasurement() {
        return Response(() -> unitMeasurementService.getListUnitMeasurement());
    }

    @DeleteMapping("/unitmeasurement/{id}")
    public AppResponse disabledUnitMeasurement(@PathVariable() String id) {
        return Response(() -> unitMeasurementService.disableUnitMeasurement(id));
    }
}
