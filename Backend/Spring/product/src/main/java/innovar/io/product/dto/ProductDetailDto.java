package innovar.io.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProductDetailDto {
    public String     id;
    private String    batch;
    private String    brand;
    private String    color;
    private String    dimension;
    private String    model;
    private String    serie;
    private Date      expirationdate;
    private Date      productiondate;
    private String    productcode;
    private String    unitcode;
    public Boolean    isactive;
}
