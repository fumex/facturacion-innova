package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.IscInvolvementTypeDto;
import innovar.io.product.models.tables.pojos.Iscinvolvementtype;
import innovar.io.product.services.IscInvolvementTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class IscInvolvementTypeService extends BaseBussinesService {
    @Autowired
    IscInvolvementTypeDataService baseIscInvolvementTypeDataService;

    public IscInvolvementTypeDto createIscInvolvementType(IscInvolvementTypeDto iscInvolvementTypeDto){
        Iscinvolvementtype isc = map(iscInvolvementTypeDto, Iscinvolvementtype.class);
        List<Iscinvolvementtype> iscinvolvementtypes = baseIscInvolvementTypeDataService.findCode(iscInvolvementTypeDto.getCode());
        if(iscinvolvementtypes.size()>0){
            String id = iscinvolvementtypes.get(0).getId();
            Iscinvolvementtype iscinvolvementtype = baseIscInvolvementTypeDataService.findById(id);
            iscinvolvementtype.setActivateddate(new Timestamp(new Date().getTime()));
            baseIscInvolvementTypeDataService.update(id,iscinvolvementtype);
            return map(iscinvolvementtype, IscInvolvementTypeDto.class);
        }else{
            if(isc.isactive==null){
                isc.setIsactive(true);
            }
            isc.setActivateddate(new Timestamp(new Date().getTime()));
            isc.setInactivateddate(new Timestamp(new Date().getTime()));
            baseIscInvolvementTypeDataService.insert((isc));
            return map(isc, IscInvolvementTypeDto.class);
        }
    }

    public List<IscInvolvementTypeDto> createListIscInvolvementTypes(List<IscInvolvementTypeDto> iscInvolvementTypeDtos){
        List<Iscinvolvementtype> iscinvolvementtypes = mapAll(iscInvolvementTypeDtos, Iscinvolvementtype.class);
        IscInvolvementTypeDto iscInvolvementTypeDto = new IscInvolvementTypeDto();
        iscinvolvementtypes.stream().forEach((p)->{
            iscInvolvementTypeDto.setCode(p.getCode());
            iscInvolvementTypeDto.setName(p.getName());
            iscInvolvementTypeDto.setIsactive(p.getIsactive());
            iscInvolvementTypeDto.setTax(p.getTax());
            createIscInvolvementType(iscInvolvementTypeDto);
        });
        return mapAll(iscinvolvementtypes,IscInvolvementTypeDto.class);
    }

    public IscInvolvementTypeDto updateIscInvolvementType(String id, IscInvolvementTypeDto iscInvolvementTypeDto){
        Iscinvolvementtype iscinvolvementtype = map(iscInvolvementTypeDto, Iscinvolvementtype.class);
        Iscinvolvementtype a = baseIscInvolvementTypeDataService.findById(id);
        iscinvolvementtype.setActivateddate(a.getActivateddate());
        iscinvolvementtype.setInactivateddate(a.getInactivateddate());
        baseIscInvolvementTypeDataService.update(id,iscinvolvementtype);
        return map(iscinvolvementtype, IscInvolvementTypeDto.class);
    }

    public IscInvolvementTypeDto getIscInvolvementType(String id){
        Iscinvolvementtype iscinvolvementtype = baseIscInvolvementTypeDataService.findById(id);
        return map(iscinvolvementtype, IscInvolvementTypeDto.class);
    }

    public IscInvolvementTypeDto disableIscInvolvementType(String id){
        Iscinvolvementtype iscinvolvementtype = baseIscInvolvementTypeDataService.findById(id);
        iscinvolvementtype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseIscInvolvementTypeDataService.update(id,iscinvolvementtype);
        return map( baseIscInvolvementTypeDataService.disabled(id), IscInvolvementTypeDto.class);
    }

    public List<IscInvolvementTypeDto> getAllIscInvolvementType(){
        List<Iscinvolvementtype> iscinvolvementtypes = baseIscInvolvementTypeDataService.findIntoList();
        return mapAll(iscinvolvementtypes, IscInvolvementTypeDto.class);
    }

    public List<IscInvolvementTypeDto> getListIscInvolvementType(){
        List<Iscinvolvementtype> iscinvolvementtypes = baseIscInvolvementTypeDataService.listAll();
        return mapAll(iscinvolvementtypes, IscInvolvementTypeDto.class);
    }
}
