package innovar.io.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class IscInvolvementTypeDto {
    public String     id;
    private String    code;
    private String    name;
    private Double    tax;
    public Boolean    isactive;
}
