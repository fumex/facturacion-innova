package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Category;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryDataService extends RepositoryPostgressService<Category> {

    public CategoryDataService(DSLContext dsl){ super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Category.class);
    }

    @Override
    public innovar.io.product.models.tables.Category getTable() {
        return Tables.CATEGORY;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.CATEGORY.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.CATEGORY.ISACTIVE;
    }
}
