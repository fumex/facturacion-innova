package innovar.io.product.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.product.dto.DiscountChargeTypeDto;

import innovar.io.product.models.tables.pojos.Discountchargetype;
import innovar.io.product.services.DiscountChargeTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class DiscountChargeTypeService extends BaseBussinesService {
    @Autowired
    DiscountChargeTypeDataService baseDiscountChargeTypeDataService;

    public DiscountChargeTypeDto createDiscountChargeType(DiscountChargeTypeDto categoryDto){
        Discountchargetype discountchargetype = map(categoryDto, Discountchargetype.class);
        List<Discountchargetype> discountchargetypes = baseDiscountChargeTypeDataService.findCode(categoryDto.getCode());
        if(discountchargetypes.size()>0){
            String id = discountchargetypes.get(0).getId();
            Discountchargetype dataServiceById = baseDiscountChargeTypeDataService.findById(id);
            dataServiceById.setActivateddate(new Timestamp(new Date().getTime()));
            baseDiscountChargeTypeDataService.update(id,dataServiceById);
            return map(dataServiceById, DiscountChargeTypeDto.class);
        }else{
            if(discountchargetype.isactive==null){
                discountchargetype.setIsactive(true);
            }
            discountchargetype.setActivateddate(new Timestamp(new Date().getTime()));
            discountchargetype.setInactivateddate(new Timestamp(new Date().getTime()));
            baseDiscountChargeTypeDataService.insert((discountchargetype));
            return map(discountchargetype, DiscountChargeTypeDto.class);
        }
    }

    public List<DiscountChargeTypeDto> createListDiscountChargeTypes(List<DiscountChargeTypeDto> categoryDtos){
        List<Discountchargetype> discountchargetypes = mapAll(categoryDtos, Discountchargetype.class);
        DiscountChargeTypeDto discountChargeTypeDto = new DiscountChargeTypeDto();
        discountchargetypes.stream().forEach((p)->{
            discountChargeTypeDto.setCode(p.getCode());
            discountChargeTypeDto.setName(p.getName());
            discountChargeTypeDto.setIsactive(p.getIsactive());
            discountChargeTypeDto.setLevel(p.getLevel());
            createDiscountChargeType(discountChargeTypeDto);
        });
        return mapAll(discountchargetypes,DiscountChargeTypeDto.class);
    }

    public DiscountChargeTypeDto updateDiscountChargeType(String id, DiscountChargeTypeDto discountChargeTypeDto){
        Discountchargetype discountchargetype = map(discountChargeTypeDto, Discountchargetype.class);
        Discountchargetype a = baseDiscountChargeTypeDataService.findById(id);
        discountchargetype.setActivateddate(a.getActivateddate());
        discountchargetype.setInactivateddate(a.getInactivateddate());
        baseDiscountChargeTypeDataService.update(id,discountchargetype);
        return map(discountchargetype, DiscountChargeTypeDto.class);
    }

    public DiscountChargeTypeDto getDiscountChargeType(String id){
        Discountchargetype dataServiceById = baseDiscountChargeTypeDataService.findById(id);
        return map(dataServiceById, DiscountChargeTypeDto.class);
    }

    public DiscountChargeTypeDto disableDiscountChargeType(String id){
        Discountchargetype discountchargetype = baseDiscountChargeTypeDataService.findById(id);
        discountchargetype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseDiscountChargeTypeDataService.update(id,discountchargetype);
        return map( baseDiscountChargeTypeDataService.disabled(id), DiscountChargeTypeDto.class);
    }

    public List<DiscountChargeTypeDto> getAllDiscountChargeType(){
        List<Discountchargetype> discountchargetypes = baseDiscountChargeTypeDataService.findIntoList();
        return mapAll(discountchargetypes, DiscountChargeTypeDto.class);
    }

    public List<DiscountChargeTypeDto> getListDiscountChargeType(){
        List<Discountchargetype> discountchargetypes = baseDiscountChargeTypeDataService.listAll();
        return mapAll(discountchargetypes, DiscountChargeTypeDto.class);
    }

    public List<DiscountChargeTypeDto> listDiscountChargeTypeItem(){
        List<Discountchargetype> discountchargetypes = baseDiscountChargeTypeDataService.findDiscountChargeItem();
        return mapAll(discountchargetypes, DiscountChargeTypeDto.class);
    }
}
