package innovar.io.product.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.product.models.Tables;
import innovar.io.product.models.tables.pojos.Cointype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CoinTypeDataService extends RepositoryPostgressService<Cointype> {

    public CoinTypeDataService(DSLContext dsl){ super(dsl); }

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Cointype.class);
    }

    @Override
    public innovar.io.product.models.tables.Cointype getTable() {
        return Tables.COINTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.COINTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.COINTYPE.ISACTIVE;
    }
}
