package innovar.io.address.controllers;

import innovar.io.address.AddressApplication;
import innovar.io.address.baseBussinesService.ProvinceService;
import innovar.io.address.dto.ProvinceDto;
import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AddressApplication.host)
public class ProvinceController extends BaseController {
    @Autowired
    ProvinceService provinceService;
    @PostMapping("/province")
    public AppResponse createProvince(@RequestBody() ProvinceDto provinceDto){
        return Response(()-> provinceService.createProvince((provinceDto)));
    }

    @PostMapping("/provinceListCreate")
    public AppResponse createListProvince(@RequestBody() List<ProvinceDto> provinceDtos){
        return Response(()->provinceService.createListProvince(provinceDtos));
    }

    @PutMapping("/province/{id}")
    public AppResponse updateProvince(@PathVariable() String id, @RequestBody() ProvinceDto provinceDto){
        return Response(()-> provinceService.updateProvince((id),(provinceDto)));
    }

    @GetMapping("/province/{code}")
    public AppResponse getProvince(@PathVariable() String code){
        return Response(()-> provinceService.getProvince(code));
    }

    @GetMapping("/provinces")
    public AppResponse getAllProvinces(){
        return Response(()-> provinceService.getAllProvinces());
    }

    @GetMapping("/listprovinces")
    public AppResponse listProvinces(){
        return Response(()-> provinceService.listProvinces());
    }

    @DeleteMapping("/province/{id}")
    public AppResponse disabledProvince(@PathVariable() String id){
        return Response(()-> provinceService.disableProvince(id));
    }

    @GetMapping("/filterprovince/{code}")
    public AppResponse filterProvince(@PathVariable() String code){
        return Response(()->provinceService.filterProvinces(code));
    }



}
