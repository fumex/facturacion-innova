package innovar.io.address.controllers;

import innovar.io.address.AddressApplication;
import innovar.io.address.baseBussinesService.AddressPersonService;
import innovar.io.address.dto.AddressPersonDto;
import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AddressApplication.host)
public class AddressPersonController extends BaseController {
    @Autowired
    AddressPersonService addressPersonService;

    @PostMapping("/addressperson")
    public AppResponse createAddressPerson(@RequestBody() AddressPersonDto addressPersonDto){
        return Response(()-> addressPersonService.createAddressPerson((addressPersonDto)));
    }

    @PostMapping("/addresspersonListCreate")
    public AppResponse createListAddressPerson(@RequestBody() List<AddressPersonDto> addressPersonDtos){
        return Response(()->addressPersonService.createListAddressPerson(addressPersonDtos));
    }

    @PutMapping("/addressperson/{id}")
    public AppResponse updateAddressPerson(@PathVariable() String id, @RequestBody() AddressPersonDto addressPersonDto){
        return Response(()-> addressPersonService.updateAddressPerson((id),(addressPersonDto)));
    }

    @GetMapping("/addressperson/{id}")
    public AppResponse getAddressPerson(@PathVariable() String id){
        return Response(()-> addressPersonService.getAddressPerson(id));
    }

    @GetMapping("/addressesperson")
    public AppResponse getAllAddressesPersons(){
        return Response(()-> addressPersonService.getAllAddressPersons());
    }

    @DeleteMapping("/addressperson/{id}")
    public AppResponse disabledAddressPerson(@PathVariable() String id){
        return Response(()-> addressPersonService.disableAddressPerson(id));
    }
    @GetMapping("/findAddressPerson/{id}")
    public AppResponse findAddressPerson(@PathVariable() String id){
        return Response(()->addressPersonService.findAddressPerson(id));
    }
}
