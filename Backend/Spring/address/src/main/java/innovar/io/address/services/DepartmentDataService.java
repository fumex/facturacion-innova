package innovar.io.address.services;

import innovar.io.address.models.Tables;
import innovar.io.address.models.tables.pojos.Department;
import innovar.io.core.repositories.RepositoryPostgressService;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.jooq.impl.TableImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentDataService extends RepositoryPostgressService<Department> {

    public DepartmentDataService(DSLContext dsl) {
        super(dsl);
    }

    public List findCode(String code) {
        return find(Tables.DEPARTMENT.CODE.eq(code)).fetchInto(Department.class);
    }

    @Override
    public TableImpl<?> getTable() {
        return Tables.DEPARTMENT;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.DEPARTMENT.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.DEPARTMENT.ISACTIVE;
    }
}
