/*
 * This file is generated by jOOQ.
 */
package innovar.io.address.models.tables.pojos;


import innovar.io.core.models.BaseModel;

import javax.annotation.Generated;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.12"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Roadtype extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1743982329;

    private Integer   code;
    private String    name;
    private String    abbreviation;
    private Timestamp activateddate;
    private Timestamp inactivateddate;

    private String    class_;

    public Roadtype() {}

    public Roadtype(Roadtype value) {
        this.id = value.id;
        this.code = value.code;
        this.name = value.name;
        this.activateddate = value.activateddate;
        this.inactivateddate = value.inactivateddate;
        this.createdon = value.createdon;
        this.createdby = value.createdby;
        this.updatedon = value.updatedon;
        this.updatedby = value.updatedby;
        this.isactive = value.isactive;
        this.class_ = value.class_;
        this.abbreviation = value.abbreviation;
    }

    public Roadtype(
        String    id,
        Integer   code,
        String    name,
        Timestamp activateddate,
        Timestamp inactivateddate,
        Timestamp createdon,
        String    createdby,
        Timestamp updatedon,
        String    updatedby,
        Boolean   isactive,
        String    class_,
        String    abbreviation
    ) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.activateddate = activateddate;
        this.inactivateddate = inactivateddate;
        this.createdon = createdon;
        this.createdby = createdby;
        this.updatedon = updatedon;
        this.updatedby = updatedby;
        this.isactive = isactive;
        this.class_ = class_;
        this.abbreviation = abbreviation;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getActivateddate() {
        return this.activateddate;
    }

    public void setActivateddate(Timestamp activateddate) {
        this.activateddate = activateddate;
    }

    public Timestamp getInactivateddate() {
        return this.inactivateddate;
    }

    public void setInactivateddate(Timestamp inactivateddate) {
        this.inactivateddate = inactivateddate;
    }

    public Timestamp getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Timestamp createdon) {
        this.createdon = createdon;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Timestamp getUpdatedon() {
        return this.updatedon;
    }

    public void setUpdatedon(Timestamp updatedon) {
        this.updatedon = updatedon;
    }

    public String getUpdatedby() {
        return this.updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getClass_() {
        return this.class_;
    }

    public void setClass_(String class_) {
        this.class_ = class_;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Roadtype (");

        sb.append(id);
        sb.append(", ").append(code);
        sb.append(", ").append(name);
        sb.append(", ").append(activateddate);
        sb.append(", ").append(inactivateddate);
        sb.append(", ").append(createdon);
        sb.append(", ").append(createdby);
        sb.append(", ").append(updatedon);
        sb.append(", ").append(updatedby);
        sb.append(", ").append(isactive);
        sb.append(", ").append(class_);
        sb.append(", ").append(abbreviation);

        sb.append(")");
        return sb.toString();
    }
}
