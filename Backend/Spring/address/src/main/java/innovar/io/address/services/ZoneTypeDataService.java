package innovar.io.address.services;

import innovar.io.address.models.Tables;
import innovar.io.address.models.tables.pojos.Zonetype;
import innovar.io.core.repositories.RepositoryPostgressService;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZoneTypeDataService extends RepositoryPostgressService<Zonetype> {

    public ZoneTypeDataService(DSLContext dsl) {
        super(dsl);
    }

    public List findCode(String code) {
        return find(getTable().CODE.eq(Integer.parseInt(code))).fetchInto(Zonetype.class);
    }

    @Override
    public innovar.io.address.models.tables.Zonetype getTable() {
        return Tables.ZONETYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.ZONETYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.ZONETYPE.ISACTIVE;
    }
}
