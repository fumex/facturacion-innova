package innovar.io.address.services;

import innovar.io.address.models.Tables;
import innovar.io.address.models.tables.pojos.Addresstype;
import innovar.io.core.repositories.RepositoryPostgressService;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.jooq.impl.TableImpl;
import org.springframework.stereotype.Service;

@Service
public class AddressTypeDataService extends RepositoryPostgressService<Addresstype> {
    public AddressTypeDataService(DSLContext dsl) {
        super(dsl);
    }

    @Override
    public TableImpl<?> getTable() {
        return Tables.ADDRESSTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.ADDRESSTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.ADDRESSTYPE.ISACTIVE;
    }
}
