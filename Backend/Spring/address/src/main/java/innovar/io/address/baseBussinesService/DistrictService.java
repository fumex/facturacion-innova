package innovar.io.address.baseBussinesService;

import innovar.io.address.dto.DistrictDto;
import innovar.io.address.models.tables.pojos.District;
import innovar.io.address.services.DistrictDataService;
import innovar.io.address.services.ProvinceDataService;
import innovar.io.core.baseBusinessService.BaseBussinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class DistrictService  extends BaseBussinesService {
    @Autowired
    DistrictDataService baseDistrictDataService;
    ProvinceDataService baseProvinceDataService;
    public DistrictDto createDistrict(DistrictDto districtDto){
        District district = map(districtDto, District.class);
        List<District> districts = baseDistrictDataService.findCode(districtDto.code);
        if (districts.size() > 0){
            String id =  districts.get(0).getId();
            District district1 = baseDistrictDataService.findById(id);
            district1.setActivateddate(new Timestamp(new Date().getTime()));
            baseDistrictDataService.update(id, district1);
            return map(district1, DistrictDto.class);
        }else{
            if(district.isactive==null){
                district.setIsactive(true);
            }
            district.setActivateddate(new Timestamp(new Date().getTime()));
            district.setInactivateddate(new Timestamp(new Date().getTime()));
            baseDistrictDataService.insert((district));
            return map(district, DistrictDto.class);
        }
    }

    public List<DistrictDto> createListDistrict(List<DistrictDto> districtDtos){
        List<District> districts = mapAll(districtDtos, District.class);
        DistrictDto districtDto = new DistrictDto();
        districts.stream().forEach((p)->{
            districtDto.setCode(p.getCode());
            districtDto.setName(p.getName());
            districtDto.setCodeDepartment(p.getCodedepartment());
            districtDto.setCodeProvince(p.getCodeprovince());
            if(p.getIsactive()==null){
                districtDto.setIsactive(true);
            }else{
                districtDto.setIsactive(p.getIsactive());
            }
            createDistrict(districtDto);
        });
        return mapAll(districts,DistrictDto.class);
    }


    public List<DistrictDto> listDistricts(){
        List<District> districts = baseDistrictDataService.findIntoList();
        return mapAll(districts, DistrictDto.class);
    }
    public DistrictDto updateDistrict(String id, DistrictDto districtDto){
        District district = map(districtDto,District.class);
        District a = baseDistrictDataService.findById(id);
        district.setActivateddate(a.getActivateddate());
        district.setInactivateddate(a.getInactivateddate());
        if(district.isactive==null){
            district.setIsactive(true);
        }
        baseDistrictDataService.update(id,district);
        return map(district, DistrictDto.class);
    }

    public DistrictDto getDistrict(String code){
        List<District> districts = baseDistrictDataService.findCode(code);
        District district= districts.get(0);
        return map(district, DistrictDto.class);
    }

    public DistrictDto disableDistrict(String id){
        District district = baseDistrictDataService.findById(id);
        district.setInactivateddate(new Timestamp(new Date().getTime()));
        baseDistrictDataService.update(id,district);
        return map( baseDistrictDataService.disabled(id), DistrictDto.class);
    }

    public List<DistrictDto> getAllDistricts(){
        List<District> districts = baseDistrictDataService.findIntoList();
        return mapAll(districts,DistrictDto.class);
    }
    public List<DistrictDto> filterDistricts(String code){
        List<District> districts=baseDistrictDataService.findDistricts(code);
        return mapAll(districts,DistrictDto.class);
    }

}
