package innovar.io.address.controllers;

import innovar.io.address.AddressApplication;
import innovar.io.address.baseBussinesService.AddressService;
import innovar.io.address.dto.AddressDto;
import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import io.jsonwebtoken.SignatureAlgorithm;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import io.jsonwebtoken.Jwts;

@RestController
@RequestMapping(AddressApplication.host)
public class AddressController extends BaseController {
    @Autowired
    AddressService addressService;

    @PostMapping("/user/{username}/{pwd}")
    public AppResponse login(@PathVariable() String username, @PathVariable() String pwd) {
        return Response(()->  getJWTToken(username));
    }
    private String getJWTToken(String username) {
        String secretKey = "mySecretKey";
        List grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId(new ObjectId().toString())
                .setSubject(username)
                .claim("authorities", Arrays.asList("/**","/api/**"))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 6000000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }

    @PostMapping("/address")
    public AppResponse createAddress(@RequestBody() AddressDto addressDto){
        return Response(()-> addressService.createAddress((addressDto)));
    }

    @PutMapping("/address/{id}")
    public AppResponse updateAddress(@PathVariable() String id, @RequestBody() AddressDto addressDto){
        return Response(()-> addressService.updateAddress((id),(addressDto)));
    }

    @GetMapping("/address/{id}")
    public AppResponse getAddress(@PathVariable() String id){
        return Response(()-> addressService.getAddress(id));
    }

    @GetMapping("/addresses")
    public AppResponse getAllAddresses(){
        return Response(()-> addressService.getAllAddress());
    }

    @DeleteMapping("/address/{id}")
    public AppResponse disabledAddress(@PathVariable() String id){
        return Response(()-> addressService.disableAddress(id));
    }

}
