package innovar.io.address.baseBussinesService;

import innovar.io.address.dto.AddressDto;
import innovar.io.address.models.tables.pojos.Address;
import innovar.io.address.services.AddressDataService;
import innovar.io.core.baseBusinessService.BaseBussinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService extends BaseBussinesService {
    @Autowired
    AddressDataService baseAddressDataService;

    public AddressDto createAddress(AddressDto addressDto) {
        Address address = map(addressDto, Address.class);
        address.setDepartmentcode(addressDto.departmentcode);
        if(address.getIsactive()==null){
            address.setIsactive(true);
        }
        baseAddressDataService.insert(address);
        return map(address, AddressDto.class);
    }

    public AddressDto updateAddress(String id, AddressDto addressDto) {
        Address address = map(addressDto, Address.class);
        Address a = baseAddressDataService.findById(id);
        if(address.getIsactive()==null){
            address.setIsactive(true);
        }
        baseAddressDataService.update(id, address);
        return map(address, AddressDto.class);
    }

    public AddressDto getAddress(String id) {
        Address address = baseAddressDataService.findById(id);
        return map(address, AddressDto.class);
    }

    public AddressDto disableAddress(String id) {
        Address address = baseAddressDataService.findById(id);
        baseAddressDataService.update(id, address);
        return map(baseAddressDataService.disabled(id), AddressDto.class);
    }

    public List<AddressDto> getAllAddress() {
        List<Address> addresses = baseAddressDataService.findIntoList();
        return mapAll(addresses, AddressDto.class);
    }
}
