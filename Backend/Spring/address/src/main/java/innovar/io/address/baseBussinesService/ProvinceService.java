package innovar.io.address.baseBussinesService;


import innovar.io.address.dto.ProvinceDto;
import innovar.io.address.models.tables.pojos.Province;
import innovar.io.address.services.ProvinceDataService;
import innovar.io.core.baseBusinessService.BaseBussinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ProvinceService  extends BaseBussinesService {
    @Autowired
    ProvinceDataService baseProvinceDataService;

    public ProvinceDto createProvince(ProvinceDto provinceDto){
        Province province = map(provinceDto, Province.class);
        List<Province> provinces = baseProvinceDataService.findCode(provinceDto.code);
        if(provinces.size() > 0){
            String id = provinces.get(0).getId();
            Province province1 = baseProvinceDataService.findById(id);
            province1.setActivateddate(new Timestamp(new Date().getTime()));
            baseProvinceDataService.update(id, province1);
            return map(province1, ProvinceDto.class);
        }else{
            if(province.isactive==null){
                province.setIsactive(true);
            }
            province.setActivateddate(new Timestamp(new Date().getTime()));
            province.setInactivateddate(new Timestamp(new Date().getTime()));
            baseProvinceDataService.insert((province));
            return map(province, ProvinceDto.class);
        }
    }

    public List<ProvinceDto> createListProvince(List<ProvinceDto> provinceDtos){
        List<Province> provinces = mapAll(provinceDtos, Province.class);
        ProvinceDto provinceDto = new ProvinceDto();
        provinces.stream().forEach((p)->{
            provinceDto.setCode(p.getCode());
            provinceDto.setName(p.getName());
            provinceDto.setCodeDepartment(p.getCodedepartment());
            if(p.getIsactive()==null){
                provinceDto.setIsactive(true);
            }else{
                provinceDto.setIsactive(p.getIsactive());
            }
            createProvince(provinceDto);
        });
        return mapAll(provinces,ProvinceDto.class);
    }

    public List<ProvinceDto> listProvinces(){
        List<Province> provinces = baseProvinceDataService.findIntoList();
        return mapAll(provinces, ProvinceDto.class);
    }

    public ProvinceDto updateProvince(String id, ProvinceDto provinceDto){
        Province province = map(provinceDto,Province.class);
        Province a = baseProvinceDataService.findById(id);
        province.setActivateddate(a.getActivateddate());
        province.setInactivateddate(a.getInactivateddate());
        if(province.isactive==null){
            province.setIsactive(true);
        }
        baseProvinceDataService.update(id,province);
        return map(province, ProvinceDto.class);
    }

    public ProvinceDto getProvince(String code){
        List<Province> provinces = baseProvinceDataService.findCode(code);
        Province  province = provinces.get(0);
        return map(province, ProvinceDto.class);
    }

    public ProvinceDto disableProvince(String id){
        Province district = baseProvinceDataService.findById(id);
        district.setInactivateddate(new Timestamp(new Date().getTime()));
        baseProvinceDataService.update(id,district);
        return map( baseProvinceDataService.disabled(id), ProvinceDto.class);
    }

    public List<ProvinceDto> getAllProvinces(){
        List<Province> provinces = baseProvinceDataService.findIntoList();
        return mapAll(provinces, ProvinceDto.class);
    }

    public List<ProvinceDto> filterProvinces(String code){
        List<Province> provinces = baseProvinceDataService.findProvince(code);
        return mapAll(provinces, ProvinceDto.class);
    }


}
