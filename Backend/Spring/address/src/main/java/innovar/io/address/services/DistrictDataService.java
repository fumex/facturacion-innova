package innovar.io.address.services;

import innovar.io.address.models.Tables;
import innovar.io.address.models.tables.pojos.District;
import innovar.io.core.repositories.RepositoryPostgressService;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.jooq.impl.TableImpl;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DistrictDataService extends RepositoryPostgressService<District> {


    public DistrictDataService(DSLContext dsl) {
        super(dsl);
    }

    public List findCode(String code) {
        return find(Tables.DISTRICT.CODE.eq(code)).fetchInto(District.class);
    }

    public List findDistricts(String code) {
        String codeDepartment = code.substring(0, 2);
        return find(Tables.DISTRICT.CODEDEPARTMENT.eq(codeDepartment).and(Tables.DISTRICT.CODEPROVINCE.eq(code))).fetchInto(District.class);
    }

    @Override
    public TableImpl<?> getTable() {
        return Tables.DISTRICT;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.DISTRICT.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.DISTRICT.ISACTIVE;
    }
}
