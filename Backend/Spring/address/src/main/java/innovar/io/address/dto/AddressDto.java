package innovar.io.address.dto;

import innovar.io.core.models.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AddressDto {
    private String    id;
    public  String    departmentcode;
    private String    provincecode;
    private String    districtcode;
    private Integer   roadtypecode;
    private String    roadname;
    private Integer   zonetypecode;
    private String    zonename;
    private String    number;
    private String    apartment;
    private String    inside;
    private String    square;
    private String    lot;
    private Integer   km;
    private String    block;
    private String    stage;
    private String    reference;
    private String    addresstypecode;
    private Boolean isactive;
}
