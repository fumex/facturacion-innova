package innovar.io.address.baseBussinesService;

import innovar.io.address.dto.DepartmentDto;
import innovar.io.address.models.tables.pojos.Department;
import innovar.io.address.services.DepartmentDataService;
import innovar.io.core.baseBusinessService.BaseBussinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class DepartmentService extends BaseBussinesService {
    @Autowired
    DepartmentDataService baseDepartmentDataService;

    public DepartmentDto createDepartment(DepartmentDto departmentDto) {
        Department department = map(departmentDto, Department.class);
        List<Department> departments = baseDepartmentDataService.findCode(departmentDto.code);
        if (departments.size() > 0) {
            String id = departments.get(0).getId();
            Department department1 = baseDepartmentDataService.findById(id);
            department1.setActivateddate(new Timestamp(new Date().getTime()));
            return map(department1, DepartmentDto.class);
        } else {
            if(department.isactive==null){
                department.setIsactive(true);
            }
            department.setActivateddate(new Timestamp(new Date().getTime()));
            department.setInactivateddate(new Timestamp(new Date().getTime()));
            baseDepartmentDataService.insert((department));
            return map(department, DepartmentDto.class);
        }
    }

    public List<DepartmentDto> createListDepartment(List<DepartmentDto> departmentDtos) {
        List<Department> departments = mapAll(departmentDtos, Department.class);
        DepartmentDto departmentDto = new DepartmentDto();
        departments.stream().forEach((p) -> {
            departmentDto.setCode(p.getCode());
            departmentDto.setName(p.getName());
            if(p.getIsactive()==null){
                departmentDto.setIsactive(true);
            }else{
                departmentDto.setIsactive(p.getIsactive());
            }
            createDepartment(departmentDto);
        });
        return mapAll(departments, DepartmentDto.class);
    }

    public List<DepartmentDto> listDepartments() {
        List<Department> departments = baseDepartmentDataService.findIntoList();
        return mapAll(departments, DepartmentDto.class);
    }

    public DepartmentDto updateDepartment(String id, DepartmentDto departmentDto) {
        Department department = map(departmentDto, Department.class);
        Department a = baseDepartmentDataService.findById(id);
        department.setActivateddate(a.getActivateddate());
        department.setInactivateddate(a.getInactivateddate());
        if(department.isactive==null){
            department.setIsactive(true);
        }
        baseDepartmentDataService.update(id, department);
        return map(department, DepartmentDto.class);
    }

    public DepartmentDto getDepartment(String code) {
        List<Department> economicActivities = baseDepartmentDataService.findCode(code);
        Department department = economicActivities.get(0);
        return map(department, DepartmentDto.class);
    }

    public DepartmentDto disableDepartment(String id) {
        Department department = baseDepartmentDataService.findById(id);
        department.setInactivateddate(new Timestamp(new Date().getTime()));
        baseDepartmentDataService.update(id, department);
        return map(baseDepartmentDataService.disabled(id), DepartmentDto.class);
    }

    public List<DepartmentDto> getAllDepartments() {
        List<Department> departments = baseDepartmentDataService.findIntoList();
        return mapAll(departments, DepartmentDto.class);
    }
}
