package innovar.io.address.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AddressPersonDto {
    public String id;
    public String personId;
    public String addressId;
    private Boolean isactive;
}
