package innovar.io.address.controllers;

import innovar.io.address.AddressApplication;
import innovar.io.address.baseBussinesService.DepartmentService;
import innovar.io.address.dto.DepartmentDto;
import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AddressApplication.host)
public class DepartmentController extends BaseController {
    @Autowired
    DepartmentService departmentService;

    @PostMapping("/department")
    public AppResponse createDepartment(@RequestBody() DepartmentDto departmentDto){
        return Response(()-> departmentService.createDepartment((departmentDto)));
    }

    @PostMapping("/departmentListCreate")
    public AppResponse createListDepartment(@RequestBody() List<DepartmentDto> departmentDtos){
        return Response(()->departmentService.createListDepartment(departmentDtos));
    }

    @PutMapping("/department/{id}")
    public AppResponse updateDepartment(@PathVariable() String id, @RequestBody() DepartmentDto departmentDto){
        return Response(()-> departmentService.updateDepartment((id),(departmentDto)));
    }

    @GetMapping("/department/{code}")
    public AppResponse getDepartment(@PathVariable() String code){
        return Response(()-> departmentService.getDepartment(code));
    }

    @GetMapping("/departments")
    public AppResponse getAllDepartments(){
        return Response(()-> departmentService.getAllDepartments());
    }

    @GetMapping("/listdepartments")
    public AppResponse listDepartments(){
        return Response(()-> departmentService.listDepartments());
    }
    @DeleteMapping("/department/{id}")
    public AppResponse disabledDepartment(@PathVariable() String id){
        return Response(()-> departmentService.disableDepartment(id));
    }
}
