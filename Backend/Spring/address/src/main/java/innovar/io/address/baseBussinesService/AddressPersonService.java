package innovar.io.address.baseBussinesService;

import innovar.io.address.dto.AddressPersonDto;
import innovar.io.address.models.tables.pojos.Address;
import innovar.io.address.models.tables.pojos.Addressperson;
import innovar.io.address.services.AddressDataService;
import innovar.io.address.services.AddressPersonDataService;
import innovar.io.core.baseBusinessService.BaseBussinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressPersonService extends BaseBussinesService {
    @Autowired
    AddressPersonDataService baseAddressPersonDataService;
    @Autowired
    AddressDataService baseAddressDataService;

    public AddressPersonDto createAddressPerson(AddressPersonDto addressPersonDto){
        Addressperson address = map(addressPersonDto, Addressperson.class);
        address.setIsactive(true);
        baseAddressPersonDataService.insert((address));
        return map(address, AddressPersonDto.class);
    }

    public List<AddressPersonDto> createListAddressPerson(List<AddressPersonDto> phonePersonDtos){
        List<Addressperson> addressPersonList = mapAll(phonePersonDtos, Addressperson.class);
        AddressPersonDto addressPersonDto = new AddressPersonDto();
        addressPersonList.stream().forEach((p)->{
            addressPersonDto.setPersonId(p.getPersonid());
            addressPersonDto.setAddressId(p.getAddressid());
            if(p.getIsactive()==null){
                addressPersonDto.setIsactive(true);
            }else{
                addressPersonDto.setIsactive(p.getIsactive());
            }
            createAddressPerson(addressPersonDto);
        });
        return mapAll(addressPersonList, AddressPersonDto.class);
    }

    public AddressPersonDto updateAddressPerson(String id, AddressPersonDto addressPersonDto){
        Addressperson addressPerson = map(addressPersonDto,Addressperson.class);
        Addressperson a = baseAddressPersonDataService.findById(id);
        if(addressPerson.getIsactive()==null){
            addressPerson.setIsactive(true);
        }
        baseAddressPersonDataService.update(id,addressPerson);
        return map(addressPerson, AddressPersonDto.class);
    }

    public AddressPersonDto getAddressPerson(String id){
        Addressperson addressPerson = baseAddressPersonDataService.findById(id);
        return map(addressPerson, AddressPersonDto.class);
    }

    public AddressPersonDto disableAddressPerson(String id){
        Addressperson addressPerson = baseAddressPersonDataService.findById(id);
        baseAddressPersonDataService.update(id,addressPerson);
        return map( baseAddressPersonDataService.disabled(id), AddressPersonDto.class);
    }

    public List<AddressPersonDto> getAllAddressPersons(){
        List addressPersonList = baseAddressPersonDataService.findIntoList();
        return mapAll(addressPersonList, AddressPersonDto.class);
    }

    public List<Address> findAddressPerson(String idPerson){
        List<Addressperson> list = baseAddressPersonDataService.findAddressPerson(idPerson);
        List<Address> addresses = new ArrayList<Address>();
        list.stream().forEach((p)->{
            Address address = baseAddressDataService.findById(p.getAddressid());
            if(address.getIsactive()==true) {
                addresses.add(address);
            }
        });
        return mapAll(addresses,Address.class);
    }
}
