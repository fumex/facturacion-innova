package innovar.io.address.baseBussinesService;


import innovar.io.address.dto.RoadTypeDto;

import innovar.io.address.models.tables.pojos.Roadtype;
import innovar.io.address.services.RoadTypeDataService;
import innovar.io.core.baseBusinessService.BaseBussinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class RoadTypeService extends BaseBussinesService {
    @Autowired
    RoadTypeDataService baseTypeViaDataService;

    public RoadTypeDto createRoadType(RoadTypeDto typeViaDto) {
        Roadtype typeVia = map(typeViaDto, Roadtype.class);
        List<Roadtype> typeVias = baseTypeViaDataService.findCode(typeViaDto.code);
        if (typeVias.size() > 0) {
            String id = typeVias.get(0).getId();
            Roadtype typeVia1 = baseTypeViaDataService.findById(id);
            typeVia1.setActivateddate(new Timestamp(new Date().getTime()));
            baseTypeViaDataService.update(id, typeVia1);
            return map(typeVia1, RoadTypeDto.class);
        } else {
            if(typeVia.isactive==null){
                typeVia.setIsactive(true);
            }
            typeVia.setActivateddate(new Timestamp(new Date().getTime()));
            typeVia.setInactivateddate(new Timestamp(new Date().getTime()));
            baseTypeViaDataService.insert((typeVia));
            return map(typeVia, RoadTypeDto.class);
        }
    }

    public List<RoadTypeDto> createListRoadType(List<RoadTypeDto> roadTypeDtos) {
        List<Roadtype> roadTypes = mapAll(roadTypeDtos, Roadtype.class);
        RoadTypeDto roadTypeDto = new RoadTypeDto();
        roadTypes.stream().forEach((p) -> {
            roadTypeDto.setCode(p.getCode().toString());
            roadTypeDto.setName(p.getName());
            roadTypeDto.setAbbreviation(p.getAbbreviation());
            if(p.getIsactive()==null){
                roadTypeDto.setIsactive(true);
            }else{
                roadTypeDto.setIsactive(p.getIsactive());
            }
            createRoadType(roadTypeDto);
        });
        return mapAll(roadTypes, RoadTypeDto.class);
    }

    public RoadTypeDto updateRoadType(String id, RoadTypeDto typeViaDto) {
        Roadtype typeVia = map(typeViaDto, Roadtype.class);
        Roadtype a = baseTypeViaDataService.findById(id);
        typeVia.setActivateddate(a.getActivateddate());
        typeVia.setInactivateddate(a.getInactivateddate());
        if(typeVia.isactive==null){
            typeVia.setIsactive(true);
        }
        baseTypeViaDataService.update(id, typeVia);
        return map(typeVia, RoadTypeDto.class);
    }

    public RoadTypeDto getRoadType(String code) {
        List<Roadtype> typeVias = baseTypeViaDataService.findCode(code);
        Roadtype roadtype = typeVias.get(0);
        return map(roadtype, RoadTypeDto.class);
    }

    public RoadTypeDto disableRoadType(String id) {
        Roadtype typeVia = baseTypeViaDataService.findById(id);
        typeVia.setInactivateddate(new Timestamp(new Date().getTime()));
        baseTypeViaDataService.update(id, typeVia);
        return map(baseTypeViaDataService.disabled(id), RoadTypeDto.class);
    }

    public List<RoadTypeDto> getAllRoadTypes() {
        List<Roadtype> typeVias = baseTypeViaDataService.findIntoList();
        return mapAll(typeVias, RoadTypeDto.class);
    }
    public List<RoadTypeDto> getListRoadTypes(){
        List<Roadtype> roadtypes = baseTypeViaDataService.listAll();
        return mapAll(roadtypes, RoadTypeDto.class);
    }


}
