package innovar.io.address.services;

import innovar.io.address.models.Tables;
import innovar.io.address.models.tables.pojos.Address;
import innovar.io.address.models.tables.records.AddressRecord;
import innovar.io.core.repositories.RepositoryPostgressService;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

@Service
public class AddressDataService extends RepositoryPostgressService<Address> {

    public AddressDataService(DSLContext dsl) {
        super(dsl);
    }

    @Override
    public innovar.io.address.models.tables.Address getTable() {
        return Tables.ADDRESS;
    }

    @Override
    public TableField<AddressRecord, String> getId() {
        return (TableField<AddressRecord, String>) getTable().ID;
    }
    @Override
    public TableField<AddressRecord, Boolean> getIsActive() {
        return getTable().ISACTIVE;
    }
}
