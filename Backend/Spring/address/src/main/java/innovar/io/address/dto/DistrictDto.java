package innovar.io.address.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DistrictDto {
    public String id;
    public String code;
    public String name;
    public String codeDepartment;
    public String codeProvince;
    private Boolean isactive;
}
