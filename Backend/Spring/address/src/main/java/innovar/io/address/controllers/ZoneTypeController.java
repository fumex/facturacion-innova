package innovar.io.address.controllers;

import innovar.io.address.AddressApplication;
import innovar.io.address.baseBussinesService.ZoneTypeService;
import innovar.io.address.dto.ZoneTypeDto;
import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AddressApplication.host)
public class ZoneTypeController extends BaseController {
    @Autowired
    ZoneTypeService zoneTypeService;

    @PostMapping("/zonetype")
    public AppResponse createZoneType(@RequestBody() ZoneTypeDto zoneTypeDto){
        return Response(()-> zoneTypeService.createZoneType((zoneTypeDto)));
    }

    @PostMapping("/zonetypeListCreate")
    public AppResponse createListZoneType(@RequestBody() List<ZoneTypeDto> zoneTypeDtos){
        return Response(()->zoneTypeService.createListZoneType(zoneTypeDtos));
    }

    @PutMapping("/zonetype/{id}")
    public AppResponse updateZoneType(@PathVariable() String id, @RequestBody() ZoneTypeDto zoneTypeDto){
        return Response(()-> zoneTypeService.updateZoneType((id),(zoneTypeDto)));
    }

    @GetMapping("/zonetype/{code}")
    public AppResponse getZoneType(@PathVariable() String code){
        return Response(()-> zoneTypeService.getZoneType(code));
    }

    @GetMapping("/zonetypes")
    public AppResponse getAllZoneType(){
        return Response(()-> zoneTypeService.getAllZoneTypes());
    }



    @DeleteMapping("/zonetype/{id}")
    public AppResponse disabledZoneType(@PathVariable() String id){
        return Response(()-> zoneTypeService.disableZoneType(id));
    }
    @GetMapping("/ListZoneTypes")
    public AppResponse getListZoneTypes(){
        return Response(()-> zoneTypeService.getListZoneTypes());
    }

}
