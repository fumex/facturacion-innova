package innovar.io.address.baseBussinesService;


import innovar.io.address.dto.ZoneTypeDto;

import innovar.io.address.models.tables.pojos.Zonetype;
import innovar.io.address.services.ZoneTypeDataService;
import innovar.io.core.baseBusinessService.BaseBussinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ZoneTypeService extends BaseBussinesService {
    @Autowired
    ZoneTypeDataService baseZoneTypeDataService;

    public ZoneTypeDto createZoneType(ZoneTypeDto zoneTypeDto){
        Zonetype zonetype = map(zoneTypeDto, Zonetype.class);
        List<Zonetype> zoneTypes = baseZoneTypeDataService.findCode(zoneTypeDto.code);
        if(zoneTypes.size()>0){
            String id = zoneTypes.get(0).getId();
            Zonetype zoneType1 = baseZoneTypeDataService.findById(id);
            zoneType1.setActivateddate(new Timestamp(new Date().getTime()));
            baseZoneTypeDataService.update(id, zoneType1);
            return  map(zoneType1, ZoneTypeDto.class);
        }else{
            if(zonetype.isactive==null){
                zonetype.setIsactive(true);
            }
            zonetype.setActivateddate(new Timestamp(new Date().getTime()));
            zonetype.setInactivateddate(new Timestamp(new Date().getTime()));
            baseZoneTypeDataService.insert((zonetype));
            return map(zonetype, ZoneTypeDto.class);
        }
    }

    public List<ZoneTypeDto> createListZoneType(List<ZoneTypeDto> zoneTypeDtos){
        List<Zonetype> zoneTypes = mapAll(zoneTypeDtos, Zonetype.class);
        ZoneTypeDto zoneTypeDto = new ZoneTypeDto();
        zoneTypes.stream().forEach((p)->{
            zoneTypeDto.setCode(p.getCode().toString());
            zoneTypeDto.setName(p.getName());
            zoneTypeDto.setAbbreviation(p.getAbbreviation());
            if(p.getIsactive()==null){
                zoneTypeDto.setIsactive(true);
            }else{
                zoneTypeDto.setIsactive(p.getIsactive());
            }
            createZoneType(zoneTypeDto);
        });
        return mapAll(zoneTypes,ZoneTypeDto.class);
    }

    public ZoneTypeDto updateZoneType(String id, ZoneTypeDto zoneTypeDto){
        Zonetype zonetype = map(zoneTypeDto, Zonetype.class);
        Zonetype a = baseZoneTypeDataService.findById(id);
        zonetype.setActivateddate(a.getActivateddate());
        zonetype.setInactivateddate(a.getInactivateddate());
        if(zonetype.isactive==null){
            zonetype.setIsactive(true);
        }
        baseZoneTypeDataService.update(id,zonetype);
        return map(zonetype, ZoneTypeDto.class);
    }

    public ZoneTypeDto getZoneType(String code){
        List<Zonetype> zoneTypes = baseZoneTypeDataService.findCode(code);
        Zonetype zoneType= zoneTypes.get(0);
        return map(zoneType, ZoneTypeDto.class);
    }

    public ZoneTypeDto disableZoneType(String id){
        Zonetype Zonetype = baseZoneTypeDataService.findById(id);
        Zonetype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseZoneTypeDataService.update(id,Zonetype);
        return map( baseZoneTypeDataService.disabled(id), ZoneTypeDto.class);
    }

    public List<ZoneTypeDto> getAllZoneTypes(){
        List<Zonetype> zoneTypes = baseZoneTypeDataService.findIntoList();
        return mapAll(zoneTypes, ZoneTypeDto.class);
    }

    public List<ZoneTypeDto> getListZoneTypes(){
        List<Zonetype> zonetypes = baseZoneTypeDataService.listAll();
        return mapAll(zonetypes, ZoneTypeDto.class);
    }


}
