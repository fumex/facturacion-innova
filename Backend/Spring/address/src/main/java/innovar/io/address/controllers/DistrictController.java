package innovar.io.address.controllers;

import innovar.io.address.AddressApplication;
import innovar.io.address.baseBussinesService.DistrictService;
import innovar.io.address.dto.DistrictDto;
import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AddressApplication.host)
public class DistrictController extends BaseController {
    @Autowired
    DistrictService districtService;

    @PostMapping("/district")
    public AppResponse createDistrict(@RequestBody() DistrictDto districtDto){
        return Response(()-> districtService.createDistrict((districtDto)));
    }

    @PostMapping("/districtListCreate")
    public AppResponse createListDistrict(@RequestBody() List<DistrictDto> districtDtos){
        return Response(()->districtService.createListDistrict(districtDtos));
    }

    @PutMapping("/district/{id}")
    public AppResponse updateDistrict(@PathVariable() String id, @RequestBody() DistrictDto districtDto){
        return Response(()-> districtService.updateDistrict((id),(districtDto)));
    }

    @GetMapping("/district/{code}")
    public AppResponse getDistrict(@PathVariable() String code){
        return Response(()-> districtService.getDistrict(code));
    }

    @GetMapping("/districts")
    public AppResponse getAllDistricts(){
        return Response(()-> districtService.getAllDistricts());
    }

    @GetMapping("/listdistricts")
    public AppResponse listDistricts(){
        return Response(()-> districtService.listDistricts());
    }

    @DeleteMapping("/district/{id}")
    public AppResponse disabledDistrict(@PathVariable() String id){
        return Response(()-> districtService.disableDistrict(id));
    }
    @GetMapping("/filterdistricts/{code}")
    public AppResponse filterDistricts(@PathVariable() String code){
        return Response(()->districtService.filterDistricts(code));
    }
}
