package innovar.io.address.services;

import innovar.io.address.models.Tables;
import innovar.io.address.models.tables.pojos.Addressperson;
import innovar.io.core.repositories.RepositoryPostgressService;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.jooq.impl.TableImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressPersonDataService extends RepositoryPostgressService<Addressperson> {

    public AddressPersonDataService(DSLContext dsl) {
        super(dsl);
    }

    public List findAddressPerson(String personId) {
        return find(Tables.ADDRESSPERSON.PERSONID.eq(personId)).fetchInto(Addressperson.class);
    }

    @Override
    public Addressperson insert(Addressperson addressperson) {
        return super.insert(addressperson);
    }

    @Override
    public TableImpl<?> getTable() {
        return Tables.ADDRESSPERSON;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.ADDRESSPERSON.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.ADDRESSPERSON.ISACTIVE;
    }
}
