package innovar.io.address.services;

import innovar.io.address.models.Tables;
import innovar.io.address.models.tables.pojos.Province;
import innovar.io.core.repositories.RepositoryPostgressService;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProvinceDataService extends RepositoryPostgressService<Province> {

    public ProvinceDataService(DSLContext dsl) {
        super(dsl);
    }

    public List findCode(String code) {
        return find(getTable().CODE.eq(code)).fetchInto(Province.class);
    }

    public List findProvince(String codeDepartment) {
        return find(getTable().CODEDEPARTMENT.eq(codeDepartment)).orderBy(getTable().CODEDEPARTMENT.asc()).fetchInto(Province.class);
    }

    @Override
    public innovar.io.address.models.tables.Province getTable() {
        return Tables.PROVINCE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PROVINCE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PROVINCE.ISACTIVE;
    }
}
