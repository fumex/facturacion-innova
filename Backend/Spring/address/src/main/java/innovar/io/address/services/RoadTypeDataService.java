package innovar.io.address.services;

import innovar.io.address.models.Tables;
import innovar.io.address.models.tables.pojos.Roadtype;
import innovar.io.core.repositories.RepositoryPostgressService;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoadTypeDataService extends RepositoryPostgressService<Roadtype> {

    public RoadTypeDataService(DSLContext dsl) {
        super(dsl);
    }

    public List findCode(String code) {
        return find(getTable().CODE.eq(Integer.parseInt(code))).fetchInto(Roadtype.class);
    }

    @Override
    public innovar.io.address.models.tables.Roadtype getTable() {
        return Tables.ROADTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.ROADTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.ROADTYPE.ISACTIVE;
    }
}