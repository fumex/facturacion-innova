package innovar.io.address.controllers;

import innovar.io.address.AddressApplication;
import innovar.io.address.baseBussinesService.AddressTypeService;

import innovar.io.address.dto.AddressTypeDto;
import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(AddressApplication.host)
public class AddressTypeController extends BaseController {
    @Autowired
    AddressTypeService addressTypeService;

    @PostMapping("/addresstype")
    public AppResponse createAddressType(@RequestBody() AddressTypeDto addressTypeDto){
        return Response(()-> addressTypeService.createAddressType((addressTypeDto)));
    }

    @PutMapping("/addresstype/{id}")
    public AppResponse updateAddressType(@PathVariable() String id, @RequestBody() AddressTypeDto addressTypeDto){
        return Response(()-> addressTypeService.updateAddressType((id),(addressTypeDto)));
    }

    @GetMapping("/addresstype/{id}")
    public AppResponse getAddressType(@PathVariable() String id){
        return Response(()-> addressTypeService.getAddressType(id));
    }

    @GetMapping("/addressestypes")
    public AppResponse getAllAddressesTypes(){
        return Response(()-> addressTypeService.getAllAddressType());
    }

    @GetMapping("/ListAddressesTypes")
    public AppResponse getListAddressesTypes(){
        return Response(()-> addressTypeService.getListAddressType());
    }

    @DeleteMapping("/addresstype/{id}")
    public AppResponse disabledAddressType(@PathVariable() String id){
        return Response(()-> addressTypeService.disableAddressType(id));
    }
}
