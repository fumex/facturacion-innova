package innovar.io.address.controllers;

import innovar.io.address.AddressApplication;
import innovar.io.address.baseBussinesService.RoadTypeService;
import innovar.io.address.dto.RoadTypeDto;
import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AddressApplication.host)
public class RoadTypeController extends BaseController {
    @Autowired
    RoadTypeService roadTypeService;

    @PostMapping("/roadtype")
    public AppResponse createRoadType(@RequestBody() RoadTypeDto roadTypeDto){
        return Response(()-> roadTypeService.createRoadType((roadTypeDto)));
    }

    @PostMapping("/roadtypeListCreate")
    public AppResponse createListRoadType(@RequestBody() List<RoadTypeDto> roadTypeDtos){
        return Response(()->roadTypeService.createListRoadType(roadTypeDtos));
    }

    @PutMapping("/roadtype/{id}")
    public AppResponse updateRoadType(@PathVariable() String id, @RequestBody() RoadTypeDto roadTypeDto){
        return Response(()-> roadTypeService.updateRoadType((id),(roadTypeDto)));
    }

    @GetMapping("/roadtype/{code}")
    public AppResponse getRoadType(@PathVariable() String code){
        return Response(()-> roadTypeService.getRoadType(code));
    }

    @GetMapping("/roadtypes")
    public AppResponse getAllRoadTypes(){
        return Response(()-> roadTypeService.getAllRoadTypes());
    }

    @GetMapping("/ListRoadTypes")
    public AppResponse getListRoadTypes(){
        return Response(()-> roadTypeService.getListRoadTypes());
    }


    @DeleteMapping("/roadtype/{id}")
    public AppResponse disabledRoadType(@PathVariable() String id){
        return Response(()-> roadTypeService.disableRoadType(id));
    }
}

