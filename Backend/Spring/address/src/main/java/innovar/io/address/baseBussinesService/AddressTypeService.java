package innovar.io.address.baseBussinesService;

import innovar.io.address.dto.AddressTypeDto;
import innovar.io.address.models.tables.pojos.Addresstype;
import innovar.io.address.services.AddressTypeDataService;
import innovar.io.core.baseBusinessService.BaseBussinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressTypeService extends BaseBussinesService {
    @Autowired
    AddressTypeDataService baseAddresstypeDataService;

    public AddressTypeDto createAddressType(AddressTypeDto AddresstypeDto) {
        Addresstype addresstype = map(AddresstypeDto, Addresstype.class);
        if(addresstype.getIsactive()==null){
            addresstype.setIsactive(true);
        }
        baseAddresstypeDataService.insert((addresstype));
        return map(addresstype, AddressTypeDto.class);
    }

    public AddressTypeDto updateAddressType(String id, AddressTypeDto AddresstypeDto) {
        Addresstype addresstype = map(AddresstypeDto, Addresstype.class);
        Addresstype a = baseAddresstypeDataService.findById(id);
        if(addresstype.getIsactive()==null){
            addresstype.setIsactive(true);
        }
        baseAddresstypeDataService.update(id, addresstype);
        return map(addresstype, AddressTypeDto.class);
    }

    public AddressTypeDto getAddressType(String id) {
        Addresstype address = baseAddresstypeDataService.findById(id);
        return map(address, AddressTypeDto.class);
    }

    public AddressTypeDto disableAddressType(String id) {
        Addresstype Addresstype = baseAddresstypeDataService.findById(id);
        baseAddresstypeDataService.update(id, Addresstype);
        return map(baseAddresstypeDataService.disabled(id), AddressTypeDto.class);
    }

    public List<AddressTypeDto> getAllAddressType() {
        List<Addresstype> addressesType = baseAddresstypeDataService.findIntoList();
        return mapAll(addressesType, AddressTypeDto.class);
    }
    public List<AddressTypeDto> getListAddressType(){
        List<Addresstype> addressesType = baseAddresstypeDataService.listAll();
        return mapAll(addressesType, AddressTypeDto.class);
    }
}
