package innovar.io.address.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class RoadTypeDto {
    public String id;
    public String code;
    public String name;
    public String abbreviation;
    public Boolean isactive;
}
