package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;

import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.AdditionalInformationService;
import innovar.io.invoice.dto.AdditionalInformationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(InvoiceApplication.host)
public class AdditionalInformationController extends BaseController {
    @Autowired
    AdditionalInformationService additionalInformationService;

    @PostMapping("/additionalinformation")
    public AppResponse createAdditionalInformation(@RequestBody() AdditionalInformationDto additionalInformationDto){
        return Response(()-> additionalInformationService.createAdditionalInformation((additionalInformationDto)));
    }

    @PutMapping("/additionalinformation/{id}")
    public AppResponse updateAdditionalInformation(@PathVariable() String id, @RequestBody() AdditionalInformationDto referencedDocumentDto){
        return Response(()-> additionalInformationService.updateAdditionalInformation((id),(referencedDocumentDto)));
    }

    @GetMapping("/additionalinformation/{id}")
    public AppResponse getAdditionalInformation(@PathVariable() String id){
        return Response(()-> additionalInformationService.getAdditionalInformation(id));
    }

    @GetMapping("/additionalinformations")
    public AppResponse getAllAdditionalInformation(){
        return Response(()-> additionalInformationService.getAllAdditionalInformation());
    }

    @GetMapping("/ListAdditionalInformations")
    public AppResponse getListAdditionalInformation(){
        return Response(()-> additionalInformationService.getListAdditionalInformation());
    }

    @DeleteMapping("/additionalinformation/{id}")
    public AppResponse disabledAdditionalInformation(@PathVariable() String id){
        return Response(()-> additionalInformationService.disabledAdditionalInformation(id));
    }
}
