package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.invoice.dto.DiscountChargeTypeDto;

import innovar.io.invoice.models.tables.pojos.Discountchargetype;
import innovar.io.invoice.services.DiscountChargeTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class DiscountChargeTypeService extends BaseBussinesService {
    @Autowired
    DiscountChargeTypeDataService baseDiscountChargeType;

    public DiscountChargeTypeDto createDiscountChargeType(DiscountChargeTypeDto discountChargeTypeDto){
        Discountchargetype discountchargetype = map(discountChargeTypeDto, Discountchargetype.class);
        List<Discountchargetype> discountchargetypes = baseDiscountChargeType.findCode(discountChargeTypeDto.getCode());
        if(discountchargetypes.size()>0){
            String id = discountchargetypes.get(0).getId();
            Discountchargetype a = baseDiscountChargeType.findById(id);
            a.setActivateddate(new Timestamp(new Date().getTime()));
            baseDiscountChargeType.update(id,a);
            return map(a, DiscountChargeTypeDto.class);
        }else{
            if(discountchargetype.isactive==null){
                discountchargetype.setIsactive(true);
            }
            discountchargetype.setActivateddate(new Timestamp(new Date().getTime()));
            discountchargetype.setInactivateddate(new Timestamp(new Date().getTime()));
            baseDiscountChargeType.insert((discountchargetype));
            return map(discountchargetype, DiscountChargeTypeDto.class);
        }
    }

    public List<DiscountChargeTypeDto> createListDiscountChargeType(List<DiscountChargeTypeDto> discountChargeTypeDtos){
        List<Discountchargetype> discountchargetypes = mapAll(discountChargeTypeDtos, Discountchargetype.class);
        DiscountChargeTypeDto discountChargeTypeDto = new DiscountChargeTypeDto();
        discountchargetypes.stream().forEach((p)->{
            discountChargeTypeDto.setCode(p.getCode());
            discountChargeTypeDto.setName(p.getName());
            discountChargeTypeDto.setLevel(p.getLevel());
            if(p.getIsactive()==null){
                discountChargeTypeDto.setIsactive(true);
            }else{
                discountChargeTypeDto.setIsactive(p.getIsactive());
            }
            createDiscountChargeType(discountChargeTypeDto);
        });
        return mapAll(discountchargetypes,DiscountChargeTypeDto.class);
    }

    public DiscountChargeTypeDto updateDiscountChargeType(String id, DiscountChargeTypeDto debitNoteTypeDto){
        Discountchargetype debitnotetype = map(debitNoteTypeDto, Discountchargetype.class);
        Discountchargetype a = baseDiscountChargeType.findById(id);
        debitnotetype.setActivateddate(a.getActivateddate());
        debitnotetype.setInactivateddate(a.getInactivateddate());
        if(debitnotetype.isactive==null){
            debitnotetype.setIsactive(true);
        }
        baseDiscountChargeType.update(id,debitnotetype);
        return map(debitnotetype, DiscountChargeTypeDto.class);
    }

    public DiscountChargeTypeDto getDiscountChargeType(String id){
        Discountchargetype discountchargetype = baseDiscountChargeType.findById(id);
        return map(discountchargetype, DiscountChargeTypeDto.class);
    }

    public DiscountChargeTypeDto disableDiscountChargeType(String id){
        Discountchargetype discountchargetype = baseDiscountChargeType.findById(id);
        discountchargetype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseDiscountChargeType.update(id,discountchargetype);
        return map( baseDiscountChargeType.disabled(id), DiscountChargeTypeDto.class);
    }

    public List<DiscountChargeTypeDto> getAllDiscountChargeType(){
        List<Discountchargetype> discountchargetypes = baseDiscountChargeType.findIntoList();
        return mapAll(discountchargetypes, DiscountChargeTypeDto.class);
    }

    public List<DiscountChargeTypeDto> getListDiscountChargeType(){
        List<Discountchargetype> discountchargetypes = baseDiscountChargeType.listAll();
        return mapAll(discountchargetypes, DiscountChargeTypeDto.class);
    }
}
