package innovar.io.invoice.services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.dto.InvoiceDto;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Invoice;
import innovar.io.invoice.models.tables.pojos.Serie;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceDataService extends RepositoryPostgressService<Invoice> {

    public InvoiceDataService(DSLContext dsl){ super(dsl); }


    public List findInvoiceSerie(String serie){
        return find(getTable().SERIE.eq(serie)).fetchInto(InvoiceDto.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Invoice getTable() {
        return Tables.INVOICE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.INVOICE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.INVOICE.ISACTIVE;
    }
}
