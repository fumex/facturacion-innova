package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;

import innovar.io.invoice.baseBussinesService.DiscountChargeTypeService;
import innovar.io.invoice.dto.DiscountChargeTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class DiscountChargeTypeController extends BaseController {
    @Autowired
    DiscountChargeTypeService discountChargeTypeService;

    @PostMapping("/discountchargetype")
    public AppResponse createDiscountChargeType(@RequestBody() DiscountChargeTypeDto discountChargeTypeDto){
        return Response(()-> discountChargeTypeService.createDiscountChargeType((discountChargeTypeDto)));
    }
    @PostMapping("/discountchargetypeListCreate")
    public AppResponse createListDiscountChargeType(@RequestBody() List<DiscountChargeTypeDto> discountChargeTypeDtos){
        return Response(()-> discountChargeTypeService.createListDiscountChargeType(discountChargeTypeDtos));
    }

    @PutMapping("/discountchargetype/{id}")
    public AppResponse updateDiscountChargeType(@PathVariable() String id, @RequestBody() DiscountChargeTypeDto discountChargeTypeDto){
        return Response(()-> discountChargeTypeService.updateDiscountChargeType((id),(discountChargeTypeDto)));
    }

    @GetMapping("/discountchargetype/{id}")
    public AppResponse getDiscountChargeType(@PathVariable() String id){
        return Response(()-> discountChargeTypeService.getDiscountChargeType(id));
    }

    @GetMapping("/discountchargetypes")
    public AppResponse getAllDiscountChargeTypes(){
        return Response(()-> discountChargeTypeService.getAllDiscountChargeType());
    }

    @GetMapping("/ListDiscountchargetypes")
    public AppResponse getListDiscountChargeTypes(){
        return Response(()-> discountChargeTypeService.getListDiscountChargeType());
    }

    @DeleteMapping("/discountchargetype/{id}")
    public AppResponse disabledDiscountChargeType(@PathVariable() String id){
        return Response(()-> discountChargeTypeService.disableDiscountChargeType(id));
    }
}
