package innovar.io.invoice.services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Referenceddocumenttype;

import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReferencedDocumentTypeDataService extends RepositoryPostgressService<Referenceddocumenttype> {

    public ReferencedDocumentTypeDataService(DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Referenceddocumenttype.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Referenceddocumenttype getTable() {
        return Tables.REFERENCEDDOCUMENTTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.REFERENCEDDOCUMENTTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.REFERENCEDDOCUMENTTYPE.ISACTIVE;
    }
    /*public List<ReferencedDocumentType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, ReferencedDocumentType.class);
    }*/
}
