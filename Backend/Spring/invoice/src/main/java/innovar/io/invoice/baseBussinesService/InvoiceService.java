package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.invoice.dto.InvoiceDto;
import innovar.io.invoice.dto.SerieDto;
import innovar.io.invoice.models.tables.Serie;
import innovar.io.invoice.models.tables.pojos.Invoice;
import innovar.io.invoice.services.InvoiceDataService;
import innovar.io.invoice.services.SerieDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class InvoiceService extends BaseBussinesService {
    @Autowired
    InvoiceDataService invoiceDataService;
    @Autowired
    SerieDataService serieDataService;
    public InvoiceDto createInvoice(InvoiceDto invoiceDto){
        Invoice invoice = map(invoiceDto, Invoice.class);
        invoice.setEmissiondate(new Timestamp(new Date().getTime()));
        invoice.setIsactive(true);
        invoiceDataService.insert((invoice));
        return map(invoice, InvoiceDto.class);
    }

    public InvoiceDto updateInvoice(String id, InvoiceDto invoiceDto){
        Invoice invoice= map(invoiceDto,Invoice.class);
        invoice.setUpdatedon(new Timestamp(new Date().getTime()));
        if(invoice.isactive==null){
            invoice.setIsactive(true);
        }
        invoiceDataService.update(id, invoice);
        return  map(invoice, InvoiceDto.class);
    }
    public InvoiceDto getInvoice(String id){
        Invoice invoice= invoiceDataService.findById(id);
        return map(invoice, InvoiceDto.class);
    }

    public List<InvoiceDto> getAllInvoice(){
        List<Invoice> invoices= invoiceDataService.findIntoList();
        return  mapAll(invoices, InvoiceDto.class);
    }

    public List<InvoiceDto> getListInvoice(){
        List<Invoice> invoices= invoiceDataService.listAll();
        return  mapAll(invoices, InvoiceDto.class);
    }

    public InvoiceDto disabledInvoice(String id){
        Invoice invoice= invoiceDataService.findById(id);
        invoice.setUpdatedon(new Timestamp(new Date().getTime()));
        invoiceDataService.update(id, invoice);
        return map(invoiceDataService.disabled(id),InvoiceDto.class);
    }

    public int findLastInvoice(String serie){
        List<InvoiceDto> invoices = invoiceDataService.findInvoiceSerie(serie);
        if(invoices ==null || invoices.size()==0){
            List<SerieDto> series = serieDataService.findSeries(serie);
            return Integer.parseInt(series.get(0).getNumber());
        }
        return Integer.parseInt(invoices.get(invoices.size()-1).getNumber())+1;
    }
}
