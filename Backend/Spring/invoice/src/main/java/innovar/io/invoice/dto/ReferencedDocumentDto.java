package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ReferencedDocumentDto {
    private String    id;
    private String    invoicecode;
    private String    numberinvoicemodified;
    private String    serieinvoicemodified;
    private String    typeinvoicemodified;
    private String    numberremissionguide;
    private String    remissionguidecode;
    private String    documentreferencecode;
    private String    numberdocumentreference;
    private Boolean   isactive;
}
