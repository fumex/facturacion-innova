package innovar.io.invoice.services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Cointype;

import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoinTypeDataService extends RepositoryPostgressService<Cointype> {

    public CoinTypeDataService(DSLContext dsl){ super(dsl); }

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Cointype.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Cointype getTable() {
        return Tables.COINTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.COINTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.COINTYPE.ISACTIVE;
    }
    /*public List<CoinType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, CoinType.class);
    }*/
}
