/*
 * This file is generated by jOOQ.
 */
package innovar.io.invoice.models.tables.pojos;


import innovar.io.core.models.BaseModel;

import javax.annotation.Generated;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Referenceddocument extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1562106729;

    private String    invoicecode;
    private String    numberinvoicemodified;
    private String    serieinvoicemodified;
    private String    typeinvoicemodified;
    private String    numberremissionguide;
    private String    remissionguidecode;
    private String    documentreferencecode;
    private String    numberdocumentreference;

    private String    class_;

    public Referenceddocument() {}

    public Referenceddocument(Referenceddocument value) {
        this.id = value.id;
        this.invoicecode = value.invoicecode;
        this.numberinvoicemodified = value.numberinvoicemodified;
        this.serieinvoicemodified = value.serieinvoicemodified;
        this.typeinvoicemodified = value.typeinvoicemodified;
        this.numberremissionguide = value.numberremissionguide;
        this.remissionguidecode = value.remissionguidecode;
        this.documentreferencecode = value.documentreferencecode;
        this.numberdocumentreference = value.numberdocumentreference;
        this.createdon = value.createdon;
        this.createdby = value.createdby;
        this.updatedon = value.updatedon;
        this.updatedby = value.updatedby;
        this.isactive = value.isactive;
        this.class_ = value.class_;
    }

    public Referenceddocument(
        String    id,
        String    invoicecode,
        String    numberinvoicemodified,
        String    serieinvoicemodified,
        String    typeinvoicemodified,
        String    numberremissionguide,
        String    remissionguidecode,
        String    documentreferencecode,
        String    numberdocumentreference,
        Timestamp createdon,
        String    createdby,
        Timestamp updatedon,
        String    updatedby,
        Boolean   isactive,
        String    class_
    ) {
        this.id = id;
        this.invoicecode = invoicecode;
        this.numberinvoicemodified = numberinvoicemodified;
        this.serieinvoicemodified = serieinvoicemodified;
        this.typeinvoicemodified = typeinvoicemodified;
        this.numberremissionguide = numberremissionguide;
        this.remissionguidecode = remissionguidecode;
        this.documentreferencecode = documentreferencecode;
        this.numberdocumentreference = numberdocumentreference;
        this.createdon = createdon;
        this.createdby = createdby;
        this.updatedon = updatedon;
        this.updatedby = updatedby;
        this.isactive = isactive;
        this.class_ = class_;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoicecode() {
        return this.invoicecode;
    }

    public void setInvoicecode(String invoicecode) {
        this.invoicecode = invoicecode;
    }

    public String getNumberinvoicemodified() {
        return this.numberinvoicemodified;
    }

    public void setNumberinvoicemodified(String numberinvoicemodified) {
        this.numberinvoicemodified = numberinvoicemodified;
    }

    public String getSerieinvoicemodified() {
        return this.serieinvoicemodified;
    }

    public void setSerieinvoicemodified(String serieinvoicemodified) {
        this.serieinvoicemodified = serieinvoicemodified;
    }

    public String getTypeinvoicemodified() {
        return this.typeinvoicemodified;
    }

    public void setTypeinvoicemodified(String typeinvoicemodified) {
        this.typeinvoicemodified = typeinvoicemodified;
    }

    public String getNumberremissionguide() {
        return this.numberremissionguide;
    }

    public void setNumberremissionguide(String numberremissionguide) {
        this.numberremissionguide = numberremissionguide;
    }

    public String getRemissionguidecode() {
        return this.remissionguidecode;
    }

    public void setRemissionguidecode(String remissionguidecode) {
        this.remissionguidecode = remissionguidecode;
    }

    public String getDocumentreferencecode() {
        return this.documentreferencecode;
    }

    public void setDocumentreferencecode(String documentreferencecode) {
        this.documentreferencecode = documentreferencecode;
    }

    public String getNumberdocumentreference() {
        return this.numberdocumentreference;
    }

    public void setNumberdocumentreference(String numberdocumentreference) {
        this.numberdocumentreference = numberdocumentreference;
    }

    public Timestamp getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Timestamp createdon) {
        this.createdon = createdon;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Timestamp getUpdatedon() {
        return this.updatedon;
    }

    public void setUpdatedon(Timestamp updatedon) {
        this.updatedon = updatedon;
    }

    public String getUpdatedby() {
        return this.updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getClass_() {
        return this.class_;
    }

    public void setClass_(String class_) {
        this.class_ = class_;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Referenceddocument (");

        sb.append(id);
        sb.append(", ").append(invoicecode);
        sb.append(", ").append(numberinvoicemodified);
        sb.append(", ").append(serieinvoicemodified);
        sb.append(", ").append(typeinvoicemodified);
        sb.append(", ").append(numberremissionguide);
        sb.append(", ").append(remissionguidecode);
        sb.append(", ").append(documentreferencecode);
        sb.append(", ").append(numberdocumentreference);
        sb.append(", ").append(createdon);
        sb.append(", ").append(createdby);
        sb.append(", ").append(updatedon);
        sb.append(", ").append(updatedby);
        sb.append(", ").append(isactive);
        sb.append(", ").append(class_);

        sb.append(")");
        return sb.toString();
    }
}
