package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.invoice.dto.InvoiceDetailDto;
import innovar.io.invoice.models.tables.pojos.Invoicedetail;
import innovar.io.invoice.services.InvoiceDetailDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class InvoiceDetailService extends BaseBussinesService {
    @Autowired
    InvoiceDetailDataService baseInvoiceDetailDataService;

    public InvoiceDetailDto createInvoiceDetail(InvoiceDetailDto invoiceDto){
        Invoicedetail invoicedetail = map(invoiceDto, Invoicedetail.class);
        invoicedetail.setIsactive(true);
        baseInvoiceDetailDataService.insert((invoicedetail));
        return map(invoicedetail, InvoiceDetailDto.class);
    }

    public InvoiceDetailDto updateInvoiceDetail(String id, InvoiceDetailDto invoiceDto){
        Invoicedetail invoicedetail= map(invoiceDto,Invoicedetail.class);
        invoicedetail.setUpdatedon(new Timestamp(new Date().getTime()));
        if(invoicedetail.isactive==null){
            invoicedetail.setIsactive(true);
        }
        baseInvoiceDetailDataService.update(id, invoicedetail);
        return  map(invoicedetail, InvoiceDetailDto.class);
    }
    public InvoiceDetailDto getInvoiceDetail(String id){
        Invoicedetail invoicedetail= baseInvoiceDetailDataService.findById(id);
        return map(invoicedetail, InvoiceDetailDto.class);
    }

    public List<InvoiceDetailDto> getAllInvoiceDetail(){
        List<Invoicedetail> invoicedetails= baseInvoiceDetailDataService.findIntoList();
        return  mapAll(invoicedetails, InvoiceDetailDto.class);
    }

    public List<InvoiceDetailDto> getListInvoiceDetail(){
        List<Invoicedetail> invoicedetails= baseInvoiceDetailDataService.listAll();
        return  mapAll(invoicedetails, InvoiceDetailDto.class);
    }

    public InvoiceDetailDto disabledInvoiceDetail(String id){
        Invoicedetail invoice= baseInvoiceDetailDataService.findById(id);
        invoice.setUpdatedon(new Timestamp(new Date().getTime()));
        baseInvoiceDetailDataService.update(id, invoice);
        return map(baseInvoiceDetailDataService.disabled(id),InvoiceDetailDto.class);
    }
}
