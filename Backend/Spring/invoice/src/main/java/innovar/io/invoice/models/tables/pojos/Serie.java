/*
 * This file is generated by jOOQ.
 */
package innovar.io.invoice.models.tables.pojos;


import innovar.io.core.models.BaseModel;

import javax.annotation.Generated;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Serie extends BaseModel implements Serializable {

    private static final long serialVersionUID = 2026581827;

    private String    seriecode;
    private String    name;
    private String    number;
    private String    typedocumentcode;

    private String    class_;

    public Serie() {}

    public Serie(Serie value) {
        this.id = value.id;
        this.seriecode = value.seriecode;
        this.name = value.name;
        this.number = value.number;
        this.typedocumentcode = value.typedocumentcode;
        this.createdon = value.createdon;
        this.createdby = value.createdby;
        this.updatedon = value.updatedon;
        this.updatedby = value.updatedby;
        this.isactive = value.isactive;
        this.class_ = value.class_;
    }

    public Serie(
        String    id,
        String    seriecode,
        String    name,
        String    number,
        String    typedocumentcode,
        Timestamp createdon,
        String    createdby,
        Timestamp updatedon,
        String    updatedby,
        Boolean   isactive,
        String    class_
    ) {
        this.id = id;
        this.seriecode = seriecode;
        this.name = name;
        this.number = number;
        this.typedocumentcode = typedocumentcode;
        this.createdon = createdon;
        this.createdby = createdby;
        this.updatedon = updatedon;
        this.updatedby = updatedby;
        this.isactive = isactive;
        this.class_ = class_;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSeriecode() {
        return this.seriecode;
    }

    public void setSeriecode(String seriecode) {
        this.seriecode = seriecode;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTypedocumentcode() {
        return this.typedocumentcode;
    }

    public void setTypedocumentcode(String typedocumentcode) {
        this.typedocumentcode = typedocumentcode;
    }

    public Timestamp getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Timestamp createdon) {
        this.createdon = createdon;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Timestamp getUpdatedon() {
        return this.updatedon;
    }

    public void setUpdatedon(Timestamp updatedon) {
        this.updatedon = updatedon;
    }

    public String getUpdatedby() {
        return this.updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getClass_() {
        return this.class_;
    }

    public void setClass_(String class_) {
        this.class_ = class_;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Serie (");

        sb.append(id);
        sb.append(", ").append(seriecode);
        sb.append(", ").append(name);
        sb.append(", ").append(number);
        sb.append(", ").append(typedocumentcode);
        sb.append(", ").append(createdon);
        sb.append(", ").append(createdby);
        sb.append(", ").append(updatedon);
        sb.append(", ").append(updatedby);
        sb.append(", ").append(isactive);
        sb.append(", ").append(class_);

        sb.append(")");
        return sb.toString();
    }
}
