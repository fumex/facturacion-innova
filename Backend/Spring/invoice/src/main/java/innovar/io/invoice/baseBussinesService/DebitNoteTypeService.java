package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.invoice.dto.DebitNoteTypeDto;
import innovar.io.invoice.models.tables.pojos.Debitnotetype;
import innovar.io.invoice.services.DebitNoteTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class DebitNoteTypeService extends BaseBussinesService {

    @Autowired
    DebitNoteTypeDataService baseDebitNoteType;

    public DebitNoteTypeDto createDebitNoteType(DebitNoteTypeDto debitNoteTypeDto){
        Debitnotetype debitnotetype = map(debitNoteTypeDto, Debitnotetype.class);
        List<Debitnotetype> debitnotetypes = baseDebitNoteType.findCode(debitNoteTypeDto.getCode());
        if(debitnotetypes.size()>0){
            String id = debitnotetypes.get(0).getId();
            Debitnotetype a = baseDebitNoteType.findById(id);
            a.setActivateddate(new Timestamp(new Date().getTime()));
            baseDebitNoteType.update(id,a);
            return map(a, DebitNoteTypeDto.class);
        }else{
            if(debitnotetype.isactive==null){
                debitnotetype.setIsactive(true);
            }
            debitnotetype.setActivateddate(new Timestamp(new Date().getTime()));
            debitnotetype.setInactivateddate(new Timestamp(new Date().getTime()));
            baseDebitNoteType.insert((debitnotetype));
            return map(debitnotetype, DebitNoteTypeDto.class);
        }
    }

    public List<DebitNoteTypeDto> createListDebitNoteType(List<DebitNoteTypeDto> debitNoteTypeDtos){
        List<Debitnotetype> debitnotetypes = mapAll(debitNoteTypeDtos, Debitnotetype.class);
        DebitNoteTypeDto debitNoteTypeDto = new DebitNoteTypeDto();
        debitnotetypes.stream().forEach((p)->{
            debitNoteTypeDto.setCode(p.getCode());
            debitNoteTypeDto.setName(p.getName());
            debitNoteTypeDto.setLevel(p.getLevel());
            if(p.getIsactive()==null){
                debitNoteTypeDto.setIsactive(true);
            }else{
                debitNoteTypeDto.setIsactive(p.getIsactive());
            }
            createDebitNoteType(debitNoteTypeDto);
        });
        return mapAll(debitnotetypes,DebitNoteTypeDto.class);
    }

    public DebitNoteTypeDto updateDebitNoteType(String id, DebitNoteTypeDto debitNoteTypeDto){
        Debitnotetype debitnotetype = map(debitNoteTypeDto, Debitnotetype.class);
        Debitnotetype a = baseDebitNoteType.findById(id);
        debitnotetype.setActivateddate(a.getActivateddate());
        debitnotetype.setInactivateddate(a.getInactivateddate());
        if(debitnotetype.isactive==null){
            debitnotetype.setIsactive(true);
        }
        baseDebitNoteType.update(id,debitnotetype);
        return map(debitnotetype, DebitNoteTypeDto.class);
    }

    public DebitNoteTypeDto getDebitNoteType(String id){
        Debitnotetype debitnotetype = baseDebitNoteType.findById(id);
        return map(debitnotetype, DebitNoteTypeDto.class);
    }

    public DebitNoteTypeDto disableDebitNoteType(String id){
        Debitnotetype debitnotetype = baseDebitNoteType.findById(id);
        debitnotetype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseDebitNoteType.update(id,debitnotetype);
        return map( baseDebitNoteType.disabled(id), DebitNoteTypeDto.class);
    }

    public List<DebitNoteTypeDto> getAllDebitNoteType(){
        List<Debitnotetype> debitnotetypes = baseDebitNoteType.findIntoList();
        return mapAll(debitnotetypes, DebitNoteTypeDto.class);
    }

    public List<DebitNoteTypeDto> getListDebitNoteType(){
        List<Debitnotetype> debitnotetypes = baseDebitNoteType.listAll();
        return mapAll(debitnotetypes, DebitNoteTypeDto.class);
    }
}
