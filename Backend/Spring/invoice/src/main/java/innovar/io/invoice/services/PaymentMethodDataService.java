package innovar.io.invoice.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Paymentmethod;

import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentMethodDataService extends RepositoryPostgressService<Paymentmethod> {

    public PaymentMethodDataService(DSLContext dsl){ super(dsl); }

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Paymentmethod.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Paymentmethod getTable() {
        return Tables.PAYMENTMETHOD;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.PAYMENTMETHOD.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.PAYMENTMETHOD.ISACTIVE;
    }

    /*public List<PaymentMethod> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, PaymentMethod.class);
    }*/

}
