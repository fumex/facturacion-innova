package innovar.io.invoice.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Discountchargetype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountChargeTypeDataService extends RepositoryPostgressService<Discountchargetype> {

    public DiscountChargeTypeDataService(DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Discountchargetype.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Discountchargetype getTable() {
        return Tables.DISCOUNTCHARGETYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.DISCOUNTCHARGETYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.DISCOUNTCHARGETYPE.ISACTIVE;
    }
}
