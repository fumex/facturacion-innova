package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;

import innovar.io.invoice.baseBussinesService.InvoiceDetailService;
import innovar.io.invoice.dto.InvoiceDetailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(InvoiceApplication.host)
public class InvoiceDetailController extends BaseController {
    @Autowired
    InvoiceDetailService invoiceDetailService;

    @PostMapping("/invoicedetail")
    public AppResponse createInvoiceDetail(@RequestBody() InvoiceDetailDto invoiceDetailDto){
        return Response(()-> invoiceDetailService.createInvoiceDetail((invoiceDetailDto)));
    }

    @PutMapping("/invoicedetail/{id}")
    public AppResponse updateInvoiceDetail(@PathVariable() String id, @RequestBody() InvoiceDetailDto invoiceDetailDto){
        return Response(()-> invoiceDetailService.updateInvoiceDetail((id),(invoiceDetailDto)));
    }

    @GetMapping("/invoicedetail/{id}")
    public AppResponse getInvoiceDetail(@PathVariable() String id){
        return Response(()-> invoiceDetailService.getInvoiceDetail(id));
    }

    @GetMapping("/invoicedetails")
    public AppResponse getAllInvoiceDetails(){
        return Response(()-> invoiceDetailService.getAllInvoiceDetail());
    }

    @GetMapping("/ListInvoicedetails")
    public AppResponse getListInvoiceDetails(){
        return Response(()-> invoiceDetailService.getListInvoiceDetail());
    }

    @DeleteMapping("/invoicedetail/{id}")
    public AppResponse disabledInvoiceDetail(@PathVariable() String id){
        return Response(()-> invoiceDetailService.disabledInvoiceDetail(id));
    }
}
