package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.TaxConceptService;
import innovar.io.invoice.dto.TaxConceptDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class TaxConceptController extends BaseController {
    @Autowired
    TaxConceptService taxConceptService;

    @PostMapping("/taxconcept")
    public AppResponse createTaxConcept(@RequestBody() TaxConceptDto taxConceptDto){
        return Response(()-> taxConceptService.createTaxConcept((taxConceptDto)));
    }

    @PostMapping("/taxconceptListCreate")
    public AppResponse createListTaxConcept(@RequestBody() List<TaxConceptDto> taxConceptDtos){
        return Response(()-> taxConceptService.createListTaxConcept(taxConceptDtos));
    }

    @PutMapping("/taxconcept/{id}")
    public AppResponse updateTaxConcept(@PathVariable() String id, @RequestBody() TaxConceptDto taxConceptDto){
        return Response(()-> taxConceptService.updateTaxConcept((id),(taxConceptDto)));
    }

    @GetMapping("/taxconcept/{id}")
    public AppResponse getTaxConcept(@PathVariable() String id){
        return Response(()-> taxConceptService.getTaxConcept(id));
    }

    @GetMapping("/taxconcepts")
    public AppResponse getAllTaxConcept(){
        return Response(()-> taxConceptService.getAllTaxConcepts());
    }

    @GetMapping("/ListTaxconcepts")
    public AppResponse getListTaxConcept(){
        return Response(()-> taxConceptService.getListTaxConcepts());
    }

    @DeleteMapping("/taxconcept/{id}")
    public AppResponse disabledTaxConcept(@PathVariable() String id){
        return Response(()-> taxConceptService.disableTaxConcept(id));
    }
}
