package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class InvoiceDetailDto  {

    private String     id;
    private Integer    index;
    private Integer    quantity;
    private String     productcode;
    private String     platenumber;
    private BigDecimal salepriceitem;
    private BigDecimal igvamountitem;
    private BigDecimal iscamountitem;
    private BigDecimal ivapamountitem;
    private BigDecimal othertaxamountitem;
    private BigDecimal salevalueitem;
    private BigDecimal chargeamountitem;
    private String     invoicecode;
    private BigDecimal discountamountitem;
    private Boolean    isactive;

}
