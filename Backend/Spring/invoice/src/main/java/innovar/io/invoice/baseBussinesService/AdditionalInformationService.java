package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.AdditionalInformationDto;
import innovar.io.invoice.models.tables.pojos.Additionalinformation;
import innovar.io.invoice.services.AdditionalInformationDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class AdditionalInformationService  extends BaseBussinesService {
    @Autowired
    AdditionalInformationDataService baseAdditionalInformationDataService;

    public AdditionalInformationDto createAdditionalInformation(AdditionalInformationDto additionalInformationDto){
        Additionalinformation additionalInformation = map(additionalInformationDto, Additionalinformation.class);
        if(additionalInformation.isactive==null){
            additionalInformation.setIsactive(true);
        }
        baseAdditionalInformationDataService.insert((additionalInformation));
        return map(additionalInformation, AdditionalInformationDto.class);
    }

    public AdditionalInformationDto updateAdditionalInformation(String id, AdditionalInformationDto additionalInformationDto){
        Additionalinformation additionalInformation= map(additionalInformationDto,Additionalinformation.class);
        if(additionalInformation.isactive==null){
            additionalInformation.setIsactive(true);
        }
        additionalInformation.setUpdatedon(new Timestamp(new Date().getTime()));
        baseAdditionalInformationDataService.update(id, additionalInformation);
        return  map(additionalInformation, AdditionalInformationDto.class);
    }
    public AdditionalInformationDto getAdditionalInformation(String id){
        Additionalinformation additionalInformation= baseAdditionalInformationDataService.findById(id);
        return map(additionalInformation, AdditionalInformationDto.class);
    }

    public List<AdditionalInformationDto> getAllAdditionalInformation(){
        List<Additionalinformation> additionalInformation= baseAdditionalInformationDataService.findIntoList();
        return  mapAll(additionalInformation, AdditionalInformationDto.class);
    }

    public List<AdditionalInformationDto> getListAdditionalInformation(){
        List<Additionalinformation> additionalInformation= baseAdditionalInformationDataService.listAll();
        return  mapAll(additionalInformation, AdditionalInformationDto.class);
    }

    public AdditionalInformationDto disabledAdditionalInformation(String id){
        Additionalinformation informationDataServiceById= baseAdditionalInformationDataService.findById(id);
        informationDataServiceById.setUpdatedon(new Timestamp(new Date().getTime()));
        baseAdditionalInformationDataService.update(id, informationDataServiceById);
        return map(baseAdditionalInformationDataService.disabled(id),AdditionalInformationDto.class);
    }
}
