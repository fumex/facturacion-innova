package innovar.io.invoice.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Countrycode;
import innovar.io.invoice.models.tables.pojos.Creditnotetype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditNoteTypeDataService extends RepositoryPostgressService<Creditnotetype> {

    public CreditNoteTypeDataService(DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Creditnotetype.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Creditnotetype getTable() {
        return Tables.CREDITNOTETYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.CREDITNOTETYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.CREDITNOTETYPE.ISACTIVE;
    }
}
