package innovar.io.invoice.services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Operationtype;
;
import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperationTypeDataService extends RepositoryPostgressService<Operationtype> {

    public OperationTypeDataService(DSLContext dsl){ super(dsl); }

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Operationtype.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Operationtype getTable() {
        return Tables.OPERATIONTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.OPERATIONTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.OPERATIONTYPE.ISACTIVE;
    }
    /*public List<OperationType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, OperationType.class);
    }*/
}
