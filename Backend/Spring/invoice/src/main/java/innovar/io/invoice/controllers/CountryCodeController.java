package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.CountryCodeService;
import innovar.io.invoice.dto.CountryCodeDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@RequestMapping(InvoiceApplication.host)
public class CountryCodeController  extends BaseController {
    @Autowired
    CountryCodeService countryCodeService;

    @PostMapping("/countrycode")
    public AppResponse createCountryCode(@RequestBody() CountryCodeDto countryCodeDto){
        return Response(()-> countryCodeService.createCountryCode((countryCodeDto)));
    }
    @PostMapping("/countrycodeListCreate")
    public AppResponse createListCountryCode(@RequestBody() List<CountryCodeDto> countryCodeDtoList){
        return Response(()->countryCodeService.createListCountryCode(countryCodeDtoList));
    }

    @PutMapping("/countrycode/{id}")
    public AppResponse updateCountryCode(@PathVariable() String id, @RequestBody() CountryCodeDto countryCodeDto){
        return Response(()-> countryCodeService.updateCountryCode((id),(countryCodeDto)));
    }

    @GetMapping("/countrycode/{id}")
    public AppResponse getCountryCode(@PathVariable() String id){
        return Response(()-> countryCodeService.getCountryCode(id));
    }

    @GetMapping("/countrycodes")
    public AppResponse getAllCountryCodes(){
        return Response(()-> countryCodeService.getAllCountryCodes());
    }

    @GetMapping("/ListCountryCodes")
    public AppResponse getListCountryCodes(){
        return Response(()-> countryCodeService.getListCountryCodes());
    }

    @DeleteMapping("/countrycode/{id}")
    public AppResponse disabledCountryCode(@PathVariable() String id){
        return Response(()-> countryCodeService.disableCountryCode(id));
    }
}
