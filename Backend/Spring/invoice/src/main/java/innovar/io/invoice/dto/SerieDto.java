package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SerieDto {
    private String    id;
    private String    seriecode;
    private String    name;
    private String    number;
    private String    typedocumentcode;
    private Boolean   isactive;
}
