package innovar.io.invoice.services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Referenceddocument;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

@Service
public class ReferencedDocumentDataService extends RepositoryPostgressService<Referenceddocument> {

    public ReferencedDocumentDataService(DSLContext dsl){ super(dsl); }

    @Override
    public innovar.io.invoice.models.tables.Referenceddocument getTable() {
        return Tables.REFERENCEDDOCUMENT;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.REFERENCEDDOCUMENTTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.REFERENCEDDOCUMENTTYPE.ISACTIVE;
    }
}
