package innovar.io.invoice.services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Legendtype;

import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LegendTypeDataService extends RepositoryPostgressService<Legendtype> {

    public LegendTypeDataService(DSLContext dsl){ super(dsl); }

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Legendtype.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Legendtype getTable() {
        return Tables.LEGENDTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.LEGENDTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.LEGENDTYPE.ISACTIVE;
    }
    /*public List<LegendType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, LegendType.class);
    }*/
}
