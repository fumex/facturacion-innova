package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CountryCodeDto  {
    private String id;
    private String    code;
    private String    abbreviation2;
    private String    abbreviation3;
    private String    name;
    private Integer   number;
    private Boolean   isactive;
}