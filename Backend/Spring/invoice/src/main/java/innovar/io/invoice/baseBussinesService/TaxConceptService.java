package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.TaxConceptDto;
import innovar.io.invoice.models.tables.pojos.Taxconcept;
import innovar.io.invoice.services.TaxConceptDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class TaxConceptService extends BaseBussinesService {

    @Autowired
    TaxConceptDataService baseTaxConceptDataService;

    public TaxConceptDto createTaxConcept(TaxConceptDto taxConceptDto){
        Taxconcept taxConcept = map(taxConceptDto, Taxconcept.class);
        List<Taxconcept> taxConcepts = baseTaxConceptDataService.findCode(taxConceptDto.getCode());
        if(taxConcepts.size()>0){
            String id = taxConcepts.get(0).getId();
            Taxconcept taxConceptById = baseTaxConceptDataService.findById(id);
            taxConceptById.setActivateddate(new Timestamp(new Date().getTime()));
            baseTaxConceptDataService.update(id,taxConceptById);
            return map(taxConceptById, TaxConceptDto.class);
        }else{
            taxConcept.setActivateddate(new Timestamp(new Date().getTime()));
            taxConcept.setInactivateddate(new Timestamp(new Date().getTime()));
            if(taxConcept.getIsactive()==null){
                taxConcept.setIsactive(true);
            }
            baseTaxConceptDataService.insert((taxConcept));
            return map(taxConcept, TaxConceptDto.class);
        }
    }
    public List<TaxConceptDto> createListTaxConcept(List<TaxConceptDto> taxConceptDtos){
        List<Taxconcept> taxConcepts = mapAll(taxConceptDtos, Taxconcept.class);
        TaxConceptDto taxConceptDto = new TaxConceptDto();
        taxConcepts.stream().forEach((p)->{
            taxConceptDto.setCode(p.getCode());
            taxConceptDto.setName(p.getName());
            if(p.getIsactive()==null){
                taxConceptDto.setIsactive(true);
            }else{
                taxConceptDto.setIsactive(p.getIsactive());
            }
            createTaxConcept(taxConceptDto);
        });
        return mapAll(taxConcepts,TaxConceptDto.class);
    }

    public TaxConceptDto updateTaxConcept(String id, TaxConceptDto taxConceptDto){
        Taxconcept taxConcept = map(taxConceptDto, Taxconcept.class);
        Taxconcept a = baseTaxConceptDataService.findById(id);
        taxConcept.setActivateddate(a.getActivateddate());
        taxConcept.setInactivateddate(a.getInactivateddate());
        baseTaxConceptDataService.update(id,taxConcept);
        return map(taxConcept, TaxConceptDto.class);
    }

    public TaxConceptDto getTaxConcept(String id){
        Taxconcept taxConcept = baseTaxConceptDataService.findById(id);
        return map(taxConcept, TaxConceptDto.class);
    }

    public TaxConceptDto disableTaxConcept(String id){
        Taxconcept taxConcept = baseTaxConceptDataService.findById(id);
        taxConcept.setInactivateddate(new Timestamp(new Date().getTime()));
        baseTaxConceptDataService.update(id,taxConcept);
        return map( baseTaxConceptDataService.disabled(id), TaxConceptDto.class);
    }

    public List<TaxConceptDto> getAllTaxConcepts(){
        List<Taxconcept> taxTypes = baseTaxConceptDataService.findIntoList();
        return mapAll(taxTypes, TaxConceptDto.class);
    }

    public List<TaxConceptDto> getListTaxConcepts(){
        List<Taxconcept> taxTypes = baseTaxConceptDataService.listAll();
        return mapAll(taxTypes, TaxConceptDto.class);
    }
}

