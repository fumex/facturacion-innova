package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.InvoiceDocumentTypeDto;
import innovar.io.invoice.models.tables.pojos.Invoicedocumenttype;
import innovar.io.invoice.services.InvoiceDocumentTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class InvoiceDocumentTypeService extends BaseBussinesService {
    @Autowired
    InvoiceDocumentTypeDataService baseInvoiceDocumentTypeDataService;

    public InvoiceDocumentTypeDto createInvoiceDocumentType(InvoiceDocumentTypeDto invoiceDocumentTypeDto){
        Invoicedocumenttype invoiceDocumentType = map(invoiceDocumentTypeDto, Invoicedocumenttype.class);
        List<Invoicedocumenttype> invoiceDocumentTypes = baseInvoiceDocumentTypeDataService.findCode(invoiceDocumentTypeDto.getCode());
        if(invoiceDocumentTypes.size()>0){
            String id = invoiceDocumentTypes.get(0).getId();
            Invoicedocumenttype invoiceDocumentTypeDataServiceById = baseInvoiceDocumentTypeDataService.findById(id);
            invoiceDocumentTypeDataServiceById.setActivateddate(new Timestamp(new Date().getTime()));
            baseInvoiceDocumentTypeDataService.update(id,invoiceDocumentTypeDataServiceById);
            return map(invoiceDocumentTypeDataServiceById, InvoiceDocumentTypeDto.class);
        }else{
            if(invoiceDocumentType.isactive==null){
                invoiceDocumentType.setIsactive(true);
            }
            invoiceDocumentType.setActivateddate(new Timestamp(new Date().getTime()));
            invoiceDocumentType.setInactivateddate(new Timestamp(new Date().getTime()));
            baseInvoiceDocumentTypeDataService.insert((invoiceDocumentType));
            return map(invoiceDocumentType, InvoiceDocumentTypeDto.class);
        }
    }
    public List<InvoiceDocumentTypeDto> createListInvoiceDocumentType(List<InvoiceDocumentTypeDto> invoiceDocumentTypeDtos){
        List<Invoicedocumenttype> invoiceDocumentTypes = mapAll(invoiceDocumentTypeDtos, Invoicedocumenttype.class);
        InvoiceDocumentTypeDto invoiceDocumentTypeDto = new InvoiceDocumentTypeDto();
        invoiceDocumentTypes.stream().forEach((p)->{
            invoiceDocumentTypeDto.setCode(p.getCode());
            invoiceDocumentTypeDto.setName(p.getName());
            if(p.getIsactive()==null){
                invoiceDocumentTypeDto.setIsactive(true);
            }else{
                invoiceDocumentTypeDto.setIsactive(p.getIsactive());
            }
            createInvoiceDocumentType(invoiceDocumentTypeDto);
        });
        return mapAll(invoiceDocumentTypes,InvoiceDocumentTypeDto.class);
    }

    public InvoiceDocumentTypeDto updateInvoiceDocumentType(String id, InvoiceDocumentTypeDto invoiceDocumentTypeDto){
        Invoicedocumenttype invoiceDocumentType = map(invoiceDocumentTypeDto, Invoicedocumenttype.class);
        Invoicedocumenttype a = baseInvoiceDocumentTypeDataService.findById(id);
        invoiceDocumentType.setActivateddate(a.getActivateddate());
        invoiceDocumentType.setInactivateddate(a.getInactivateddate());
        if(invoiceDocumentType.isactive==null){
            invoiceDocumentType.setIsactive(true);
        }
        baseInvoiceDocumentTypeDataService.update(id,invoiceDocumentType);
        return map(invoiceDocumentType, InvoiceDocumentTypeDto.class);
    }

    public InvoiceDocumentTypeDto getInvoiceDocumentType(String id){
        Invoicedocumenttype invoiceDocumentType = baseInvoiceDocumentTypeDataService.findById(id);
        return map(invoiceDocumentType, InvoiceDocumentTypeDto.class);
    }

    public InvoiceDocumentTypeDto disableInvoiceDocumentType(String id){
        Invoicedocumenttype invoiceDocumentType = baseInvoiceDocumentTypeDataService.findById(id);
        invoiceDocumentType.setInactivateddate(new Timestamp(new Date().getTime()));
        baseInvoiceDocumentTypeDataService.update(id,invoiceDocumentType);
        return map( baseInvoiceDocumentTypeDataService.disabled(id), InvoiceDocumentTypeDto.class);
    }

    public List<InvoiceDocumentTypeDto> getAllInvoiceDocumentTypes(){
        List<Invoicedocumenttype> invoiceDocumentTypes = baseInvoiceDocumentTypeDataService.findIntoList();
        return mapAll(invoiceDocumentTypes, InvoiceDocumentTypeDto.class);
    }

    public List<InvoiceDocumentTypeDto> getListInvoiceDocumentTypes(){
        List<Invoicedocumenttype> invoiceDocumentTypes = baseInvoiceDocumentTypeDataService.listAll();
        return mapAll(invoiceDocumentTypes, InvoiceDocumentTypeDto.class);
    }
}

