package innovar.io.invoice.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Additionalinformation;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

@Service
public class AdditionalInformationDataService extends RepositoryPostgressService<Additionalinformation> {

    public AdditionalInformationDataService (DSLContext dsl){ super(dsl);}

    @Override
    public innovar.io.invoice.models.tables.Additionalinformation getTable() {
        return Tables.ADDITIONALINFORMATION;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.ADDITIONALINFORMATION.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.ADDITIONALINFORMATION.ISACTIVE;
    }
}
