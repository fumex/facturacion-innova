package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AdditionalInformationDto {
    public String id;
    private String     invoicecode;
    private String     numberpurchaseorder;
    private String     legendtypecode;
    private String     legendnote;
    private String     operationtypecode;
    private String     indicatorfise;
    private BigDecimal amountfise;
    private String     codefise;;
    public Boolean isactive;
}
