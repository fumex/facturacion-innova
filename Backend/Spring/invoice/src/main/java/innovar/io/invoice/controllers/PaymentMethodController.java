package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.PaymentMethodService;
import innovar.io.invoice.dto.PaymentMethodDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class PaymentMethodController extends BaseController {
    @Autowired
    PaymentMethodService paymentMethodService;

    @PostMapping("/paymentmethod")
    public AppResponse createPaymentMethod(@RequestBody() PaymentMethodDto paymentMethodDto){
        return Response(()-> paymentMethodService.createPaymentMethod((paymentMethodDto)));
    }

    @PostMapping("/paymentmethodListCreate")
    public AppResponse createListPaymentMethod(@RequestBody() List<PaymentMethodDto> paymentMethodDtos){
        return Response(()-> paymentMethodService.createListPaymentMethod(paymentMethodDtos));
    }

    @GetMapping("/paymentmethod/{id}")
    public AppResponse getPaymentMethod(@PathVariable() String id){
        return Response(()-> paymentMethodService.getPaymentMethod(id));
    }

    @PutMapping("/paymentmethod/{id}")
    public AppResponse updatePaymentMethod(@PathVariable() String id, @RequestBody() PaymentMethodDto paymentMethodDto){
        return Response(()-> paymentMethodService.updatePaymentMethod((id),(paymentMethodDto)));
    }

    @GetMapping("/paymentmethods")
    public AppResponse getAllPaymentMethods(){
        return Response(()-> paymentMethodService.getAllPaymentMethods());
    }

    @GetMapping("/ListPaymentMethods")
    public AppResponse getListPaymentMethods(){
        return Response(()-> paymentMethodService.getListPaymentMethods());
    }

    @DeleteMapping("/paymentmethod/{id}")
    public AppResponse disabledPaymentMethods(@PathVariable() String id){
        return Response(()-> paymentMethodService.disablePaymentMethod(id));
    }
}
