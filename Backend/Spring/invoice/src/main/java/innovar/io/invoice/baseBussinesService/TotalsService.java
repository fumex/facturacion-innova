package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.TotalsDto;
import innovar.io.invoice.models.tables.pojos.Totals;
import innovar.io.invoice.services.TotalsDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class TotalsService extends BaseBussinesService {
    @Autowired
    TotalsDataService totalsDataService;

    public TotalsDto createTotals(TotalsDto totalsDto){
        Totals total = map(totalsDto, Totals.class);
        if(total.getIsactive()==null){
            total.setIsactive(true);
        }
        totalsDataService.insert((total));
        return map(total, TotalsDto.class);
    }

    public TotalsDto updateTotals(String id, TotalsDto totalsDto){
        Totals total= map(totalsDto,Totals.class);
        total.setUpdatedon(new Timestamp(new Date().getTime()));
        if(total.getIsactive()==null){
            total.setIsactive(true);
        }
        totalsDataService.update(id, total);
        return  map(total, TotalsDto.class);
    }
    public TotalsDto getTotals(String id){
        Totals total= totalsDataService.findById(id);
        return map(total, TotalsDto.class);
    }

    public List<TotalsDto> getAllTotals(){
        List<Totals> totals= totalsDataService.findIntoList();
        return  mapAll(totals, TotalsDto.class);
    }

    public List<TotalsDto> getListTotals(){
        List<Totals> totals= totalsDataService.listAll();
        return  mapAll(totals, TotalsDto.class);
    }

    public TotalsDto disabledTotals(String id){
        Totals total= totalsDataService.findById(id);
        total.setUpdatedon(new Timestamp(new Date().getTime()));
        totalsDataService.update(id, total);
        return map(totalsDataService.disabled(id),TotalsDto.class);
    }
}
