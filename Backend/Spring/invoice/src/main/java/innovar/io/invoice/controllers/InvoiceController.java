package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;

import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.InvoiceService;
import innovar.io.invoice.dto.InvoiceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(InvoiceApplication.host)
public class InvoiceController extends BaseController {
    @Autowired
    InvoiceService invoiceService;

    @PostMapping("/invoice")
    public AppResponse createInvoice(@RequestBody() InvoiceDto invoiceDto){
        return Response(()-> invoiceService.createInvoice((invoiceDto)));
    }

    @PutMapping("/invoice/{id}")
    public AppResponse updateInvoice(@PathVariable() String id, @RequestBody() InvoiceDto invoiceDto){
        return Response(()-> invoiceService.updateInvoice((id),(invoiceDto)));
    }

    @GetMapping("/invoice/{id}")
    public AppResponse getInvoice(@PathVariable() String id){
        return Response(()-> invoiceService.getInvoice(id));
    }

    @GetMapping("/invoices")
    public AppResponse getAllInvoice(){
        return Response(()-> invoiceService.getAllInvoice());
    }

    @GetMapping("/ListInvoices")
    public AppResponse getListInvoice(){
        return Response(()-> invoiceService.getListInvoice());
    }

    @DeleteMapping("/invoice/{id}")
    public AppResponse disabledInvoice(@PathVariable() String id){
        return Response(()-> invoiceService.disabledInvoice(id));
    }

    @GetMapping("/findLastInvoice/{serie}")
    public AppResponse findLastInvoice(@PathVariable() String serie){
        return Response(()->invoiceService.findLastInvoice(serie));
    }
}
