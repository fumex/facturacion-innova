package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.OperationTypeService;
import innovar.io.invoice.dto.OperationTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class OperationTypeController extends BaseController {
    @Autowired
    OperationTypeService operationTypeService;

    @PostMapping("/operationtype")
    public AppResponse createOperationType(@RequestBody() OperationTypeDto operationTypeDto){
        return Response(()-> operationTypeService.createOperationType((operationTypeDto)));
    }

    @PostMapping("/operationtypeListCreate")
    public AppResponse createListTaxType(@RequestBody() List<OperationTypeDto> operationTypeDtos){
        return Response(()->operationTypeService.createListOperationType(operationTypeDtos));
    }

    @PutMapping("/operationtype/{id}")
    public AppResponse updateOperationType(@PathVariable() String id, @RequestBody() OperationTypeDto operationTypeDto){
        return Response(()-> operationTypeService.updateOperationType((id),(operationTypeDto)));
    }

    @GetMapping("/operationtype/{id}")
    public AppResponse getOperationType(@PathVariable() String id){
        return Response(()-> operationTypeService.getOperationType(id));
    }

    @GetMapping("/operationtypes")
    public AppResponse getAllOperationType(){
        return Response(()-> operationTypeService.getAllOperationTypes());
    }

    @DeleteMapping("/operationtype/{id}")
    public AppResponse disabledOperationType(@PathVariable() String id){
        return Response(()-> operationTypeService.disableOperationType(id));
    }

    @GetMapping("/ListOperationTypes")
    public AppResponse getListOperationType(){
        return Response(()-> operationTypeService.getListOperationTypes());
    }
}
