package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.CountryCodeDto;
import innovar.io.invoice.models.tables.pojos.Countrycode;
import innovar.io.invoice.services.CountryCodeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class CountryCodeService extends BaseBussinesService {
    @Autowired
    CountryCodeDataService baseCountryCodeDataService;

    public CountryCodeDto createCountryCode(CountryCodeDto countryCodeDto){
        Countrycode countryCode = map(countryCodeDto, Countrycode.class);
        List<Countrycode> countryCodes = baseCountryCodeDataService.findCode(countryCodeDto.getCode());
        if(countryCodes.size()>0){
            String id = countryCodes.get(0).getId();
            Countrycode countryCodeId = baseCountryCodeDataService.findById(id);
            countryCodeId.setActivateddate(new Timestamp(new Date().getTime()));
            baseCountryCodeDataService.update(id,countryCodeId);
            return map(countryCodeId, CountryCodeDto.class);
        }else{
            if(countryCode.isactive==null){
                countryCode.setIsactive(true);
            }
            countryCode.setActivateddate(new Timestamp(new Date().getTime()));
            countryCode.setInactivateddate(new Timestamp(new Date().getTime()));
            baseCountryCodeDataService.insert((countryCode));
            return map(countryCode, CountryCodeDto.class);
        }
    }
    public List<CountryCodeDto> createListCountryCode(List<CountryCodeDto> countryCodesDto){
        List<Countrycode> countryCodes = mapAll(countryCodesDto, Countrycode.class);
        CountryCodeDto countryCode = new CountryCodeDto();
        countryCodes.stream().forEach((p)->{
            countryCode.setCode(p.getCode());
            countryCode.setName(p.getName());
            countryCode.setAbbreviation2(p.getAbbreviation2());
            countryCode.setAbbreviation3(p.getAbbreviation3());
            countryCode.setNumber(p.getNumber());
            if(p.getIsactive()==null){
                countryCode.setIsactive(true);
            }else{
                countryCode.setIsactive(p.getIsactive());
            }
            createCountryCode(countryCode);
        });
        return mapAll(countryCodes,CountryCodeDto.class);
    }

    public CountryCodeDto updateCountryCode(String id, CountryCodeDto countryCodeDto){
        Countrycode typeVia = map(countryCodeDto, Countrycode.class);
        Countrycode a = baseCountryCodeDataService.findById(id);
        typeVia.setActivateddate(a.getActivateddate());
        typeVia.setInactivateddate(a.getInactivateddate());
        if(countryCodeDto.getIsactive()==null){
            countryCodeDto.setIsactive(true);
        }
        baseCountryCodeDataService.update(id,typeVia);
        return map(typeVia, CountryCodeDto.class);
    }

    public CountryCodeDto getCountryCode(String id){
        Countrycode countryCode = baseCountryCodeDataService.findById(id);
        return map(countryCode, CountryCodeDto.class);
    }

    public CountryCodeDto disableCountryCode(String id){
        Countrycode countryCode = baseCountryCodeDataService.findById(id);
        countryCode.setInactivateddate(new Timestamp(new Date().getTime()));
        baseCountryCodeDataService.update(id,countryCode);
        return map( baseCountryCodeDataService.disabled(id), CountryCodeDto.class);
    }

    public List<CountryCodeDto> getAllCountryCodes(){
        List<Countrycode> countryCodeList = baseCountryCodeDataService.findIntoList();
        return mapAll(countryCodeList, CountryCodeDto.class);
    }

    public List<CountryCodeDto> getListCountryCodes(){
        List<Countrycode> countryCodeList = baseCountryCodeDataService.listAll();
        return mapAll(countryCodeList, CountryCodeDto.class);
    }
}

