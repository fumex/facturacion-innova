package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.ReferencedDocumentDto;
import innovar.io.invoice.models.tables.pojos.Referenceddocument;
import innovar.io.invoice.services.ReferencedDocumentDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ReferencedDocumentService extends BaseBussinesService {
    @Autowired
    ReferencedDocumentDataService baseReferencedDocumentDataService;

    public ReferencedDocumentDto createReferencedDocument(ReferencedDocumentDto referencedDocumentDto){
        Referenceddocument referencedDocument = map(referencedDocumentDto, Referenceddocument.class);
        if(referencedDocument.getIsactive()==null){
            referencedDocument.setIsactive(true);
        }
        baseReferencedDocumentDataService.insert((referencedDocument));
        return map(referencedDocument, ReferencedDocumentDto.class);
    }

    public ReferencedDocumentDto updateReferencedDocument(String id, ReferencedDocumentDto referencedDocumentDto){
        Referenceddocument referencedDocument= map(referencedDocumentDto,Referenceddocument.class);
        referencedDocument.setUpdatedon(new Timestamp(new Date().getTime()));
        if(referencedDocument.getIsactive()==null){
            referencedDocument.setIsactive(true);
        }
        baseReferencedDocumentDataService.update(id, referencedDocument);
        return  map(referencedDocument, ReferencedDocumentDto.class);
    }
    public ReferencedDocumentDto getReferencedDocument(String id){
        Referenceddocument referencedDocument= baseReferencedDocumentDataService.findById(id);
        return map(referencedDocument, ReferencedDocumentDto.class);
    }

    public List<ReferencedDocumentDto> getAllReferencedDocuments(){
        List<Referenceddocument> referencedDocuments= baseReferencedDocumentDataService.findIntoList();
        return  mapAll(referencedDocuments, ReferencedDocumentDto.class);
    }

    public List<ReferencedDocumentDto> getListReferencedDocuments(){
        List<Referenceddocument> referencedDocuments= baseReferencedDocumentDataService.listAll();
        return  mapAll(referencedDocuments, ReferencedDocumentDto.class);
    }

    public ReferencedDocumentDto disabledReferencedDocument(String id){
        Referenceddocument referencedDocument= baseReferencedDocumentDataService.findById(id);
        referencedDocument.setUpdatedon(new Timestamp(new Date().getTime()));
        baseReferencedDocumentDataService.update(id, referencedDocument);
        return map(baseReferencedDocumentDataService.disabled(id),ReferencedDocumentDto.class);
    }
}
