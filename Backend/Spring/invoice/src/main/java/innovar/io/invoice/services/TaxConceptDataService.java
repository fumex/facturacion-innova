package innovar.io.invoice.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Taxconcept;

import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaxConceptDataService extends RepositoryPostgressService<Taxconcept> {

    public TaxConceptDataService(DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Taxconcept.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Taxconcept getTable() {
        return Tables.TAXCONCEPT;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.TAXCONCEPT.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.TAXCONCEPT.ISACTIVE;
    }

   /* public List<TaxConcept> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, TaxConcept.class);
    } */




}
