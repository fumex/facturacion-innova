package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.sql.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class InvoiceDto  {
    private String    id;
    private Date emissiondate;
    private Date      expirationdate;
    private String    reason;
    private String    serie;
    private String    number;
    private String    versiondocument;
    private String    versionubl;
    private String    companycode;
    private String    clientcode;
    private String    cointypecode;
    private String    documenttypecode;
    private String    creditnotecode;
    private String    debitnotecode;
    private Boolean   isactive;
}
