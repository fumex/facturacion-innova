package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;

import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.ReferencedDocumentService;
import innovar.io.invoice.dto.ReferencedDocumentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(InvoiceApplication.host)
public class ReferencedDocumentController extends BaseController {
    @Autowired
    ReferencedDocumentService referencedDocumentService;

    @PostMapping("/referenceddocument")
    public AppResponse createReferencedDocument(@RequestBody() ReferencedDocumentDto referencedDocumentDto){
        return Response(()-> referencedDocumentService.createReferencedDocument((referencedDocumentDto)));
    }

    @PutMapping("/referenceddocument/{id}")
    public AppResponse updateReferencedDocument(@PathVariable() String id, @RequestBody() ReferencedDocumentDto referencedDocumentDto){
        return Response(()-> referencedDocumentService.updateReferencedDocument((id),(referencedDocumentDto)));
    }

    @GetMapping("/referenceddocument/{id}")
    public AppResponse getReferencedDocument(@PathVariable() String id){
        return Response(()-> referencedDocumentService.getReferencedDocument(id));
    }

    @GetMapping("/referenceddocuments")
    public AppResponse getAllReferencedDocuments(){
        return Response(()-> referencedDocumentService.getAllReferencedDocuments());
    }

    @GetMapping("/ListReferenceddocuments")
    public AppResponse getListReferencedDocuments(){
        return Response(()-> referencedDocumentService.getListReferencedDocuments());
    }

    @DeleteMapping("/referenceddocument/{id}")
    public AppResponse disabledReferencedDocument(@PathVariable() String id){
        return Response(()-> referencedDocumentService.disabledReferencedDocument(id));
    }
}
