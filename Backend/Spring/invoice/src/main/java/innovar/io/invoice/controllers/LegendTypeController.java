package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.LegendTypeService;
import innovar.io.invoice.dto.LegendTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class LegendTypeController extends BaseController {
    @Autowired
    LegendTypeService legendTypeService;

    @PostMapping("/legendtype")
    public AppResponse createLegendType(@RequestBody() LegendTypeDto taxTypeDto){
        return Response(()-> legendTypeService.createLegendType((taxTypeDto)));
    }
    @PostMapping("/legendtypeListCreate")
    public AppResponse createListLegendTypes(@RequestBody() List<LegendTypeDto> legendTypeDtos){
        return Response(()->legendTypeService.createListLegendType(legendTypeDtos));
    }

    @PutMapping("/legendtype/{id}")
    public AppResponse updateLegendType(@PathVariable() String id, @RequestBody() LegendTypeDto legendTypeDto){
        return Response(()-> legendTypeService.updateLegendType((id),(legendTypeDto)));
    }

    @GetMapping("/legendtype/{id}")
    public AppResponse getLegendType(@PathVariable() String id){
        return Response(()-> legendTypeService.getLegendType(id));
    }

    @GetMapping("/legendtypes")
    public AppResponse getAllLegendTypes(){
        return Response(()-> legendTypeService.getAllLegendTypes());
    }

    @GetMapping("/ListLegendtypes")
    public AppResponse getListLegendTypes(){
        return Response(()-> legendTypeService.getListLegendTypes());
    }

    @DeleteMapping("/legendtype/{id}")
    public AppResponse disabledTaxType(@PathVariable() String id){
        return Response(()-> legendTypeService.disableLegendType(id));
    }
}
