package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.TotalsService;
import innovar.io.invoice.dto.TotalsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(InvoiceApplication.host)
public class TotalsController extends BaseController {
    @Autowired
    TotalsService totalsService;

    @PostMapping("/total")
    public AppResponse createTotal(@RequestBody() TotalsDto totalsDto){
        return Response(()-> totalsService.createTotals((totalsDto)));
    }

    @PutMapping("/total/{id}")
    public AppResponse updateTotal(@PathVariable() String id, @RequestBody() TotalsDto totalsDto){
        return Response(()-> totalsService.updateTotals((id),(totalsDto)));
    }

    @GetMapping("/total/{id}")
    public AppResponse getTotal(@PathVariable() String id){
        return Response(()-> totalsService.getTotals(id));
    }

    @GetMapping("/totals")
    public AppResponse getAllTotals(){
        return Response(()-> totalsService.getAllTotals());
    }

    @GetMapping("/ListTotals")
    public AppResponse getListTotals(){
        return Response(()-> totalsService.getListTotals());
    }

    @DeleteMapping("/total/{id}")
    public AppResponse disabledTotals(@PathVariable() String id){
        return Response(()-> totalsService.disabledTotals(id));
    }
}
