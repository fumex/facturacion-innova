package innovar.io.invoice.services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Countrycode;

import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryCodeDataService extends RepositoryPostgressService<Countrycode> {

    public CountryCodeDataService(DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Countrycode.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Countrycode getTable() {
        return Tables.COUNTRYCODE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.COUNTRYCODE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.COUNTRYCODE.ISACTIVE;
    }
   /* public List<CountryCode> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, CountryCode.class);
    }*/
}
