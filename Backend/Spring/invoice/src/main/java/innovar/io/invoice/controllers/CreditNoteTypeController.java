package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.CreditNoteTypeService;
import innovar.io.invoice.dto.CreditNoteTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class CreditNoteTypeController extends BaseController {
    @Autowired
    CreditNoteTypeService creditNoteTypeService;

    @PostMapping("/creditnotetype")
    public AppResponse createCreditNoteType(@RequestBody() CreditNoteTypeDto creditNoteTypeDto){
        return Response(()-> creditNoteTypeService.createCreditNoteType((creditNoteTypeDto)));
    }
    @PostMapping("/creditnotetypeListCreate")
    public AppResponse createListCreditNoteType(@RequestBody() List<CreditNoteTypeDto> creditNoteTypeDtos){
        return Response(()-> creditNoteTypeService.createListCreditNoteType(creditNoteTypeDtos));
    }

    @PutMapping("/creditnotetype/{id}")
    public AppResponse updateCreditNoteType(@PathVariable() String id, @RequestBody() CreditNoteTypeDto creditNoteTypeDto){
        return Response(()-> creditNoteTypeService.updateCreditNoteType((id),(creditNoteTypeDto)));
    }

    @GetMapping("/creditnotetype/{id}")
    public AppResponse getCreditNoteType(@PathVariable() String id){
        return Response(()-> creditNoteTypeService.getCreditNoteType(id));
    }

    @GetMapping("/creditnotetypes")
    public AppResponse getAllCreditNoteTypes(){
        return Response(()-> creditNoteTypeService.getAllCreditNoteType());
    }

    @GetMapping("/ListCreditnotetypes")
    public AppResponse getListCreditNoteTypes(){
        return Response(()-> creditNoteTypeService.getListCreditNoteType());
    }

    @DeleteMapping("/creditnotetype/{id}")
    public AppResponse disabledCreditNoteType(@PathVariable() String id){
        return Response(()-> creditNoteTypeService.disableCreditNoteType(id));
    }
}
