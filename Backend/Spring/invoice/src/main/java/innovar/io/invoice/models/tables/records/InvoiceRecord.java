/*
 * This file is generated by jOOQ.
 */
package innovar.io.invoice.models.tables.records;


import innovar.io.invoice.models.tables.Invoice;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record20;
import org.jooq.Row20;
import org.jooq.impl.UpdatableRecordImpl;

import javax.annotation.Generated;
import java.sql.Date;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class InvoiceRecord extends UpdatableRecordImpl<InvoiceRecord> implements Record20<String, Timestamp, Date, String, String, String, String, String, String, String, String, String, String, String, Timestamp, String, Timestamp, String, Boolean, String> {

    private static final long serialVersionUID = 1204336422;

    /**
     * Setter for <code>public.invoice.id</code>.
     */
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.invoice.id</code>.
     */
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>public.invoice.emissiondate</code>.
     */
    public void setEmissiondate(Timestamp value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.invoice.emissiondate</code>.
     */
    public Timestamp getEmissiondate() {
        return (Timestamp) get(1);
    }

    /**
     * Setter for <code>public.invoice.expirationdate</code>.
     */
    public void setExpirationdate(Date value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.invoice.expirationdate</code>.
     */
    public Date getExpirationdate() {
        return (Date) get(2);
    }

    /**
     * Setter for <code>public.invoice.reason</code>.
     */
    public void setReason(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.invoice.reason</code>.
     */
    public String getReason() {
        return (String) get(3);
    }

    /**
     * Setter for <code>public.invoice.serie</code>.
     */
    public void setSerie(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>public.invoice.serie</code>.
     */
    public String getSerie() {
        return (String) get(4);
    }

    /**
     * Setter for <code>public.invoice.number</code>.
     */
    public void setNumber(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>public.invoice.number</code>.
     */
    public String getNumber() {
        return (String) get(5);
    }

    /**
     * Setter for <code>public.invoice.versiondocument</code>.
     */
    public void setVersiondocument(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>public.invoice.versiondocument</code>.
     */
    public String getVersiondocument() {
        return (String) get(6);
    }

    /**
     * Setter for <code>public.invoice.versionubl</code>.
     */
    public void setVersionubl(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>public.invoice.versionubl</code>.
     */
    public String getVersionubl() {
        return (String) get(7);
    }

    /**
     * Setter for <code>public.invoice.companycode</code>.
     */
    public void setCompanycode(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>public.invoice.companycode</code>.
     */
    public String getCompanycode() {
        return (String) get(8);
    }

    /**
     * Setter for <code>public.invoice.clientcode</code>.
     */
    public void setClientcode(String value) {
        set(9, value);
    }

    /**
     * Getter for <code>public.invoice.clientcode</code>.
     */
    public String getClientcode() {
        return (String) get(9);
    }

    /**
     * Setter for <code>public.invoice.cointypecode</code>.
     */
    public void setCointypecode(String value) {
        set(10, value);
    }

    /**
     * Getter for <code>public.invoice.cointypecode</code>.
     */
    public String getCointypecode() {
        return (String) get(10);
    }

    /**
     * Setter for <code>public.invoice.documenttypecode</code>.
     */
    public void setDocumenttypecode(String value) {
        set(11, value);
    }

    /**
     * Getter for <code>public.invoice.documenttypecode</code>.
     */
    public String getDocumenttypecode() {
        return (String) get(11);
    }

    /**
     * Setter for <code>public.invoice.creditnotecode</code>.
     */
    public void setCreditnotecode(String value) {
        set(12, value);
    }

    /**
     * Getter for <code>public.invoice.creditnotecode</code>.
     */
    public String getCreditnotecode() {
        return (String) get(12);
    }

    /**
     * Setter for <code>public.invoice.debitnotecode</code>.
     */
    public void setDebitnotecode(String value) {
        set(13, value);
    }

    /**
     * Getter for <code>public.invoice.debitnotecode</code>.
     */
    public String getDebitnotecode() {
        return (String) get(13);
    }

    /**
     * Setter for <code>public.invoice.createdon</code>.
     */
    public void setCreatedon(Timestamp value) {
        set(14, value);
    }

    /**
     * Getter for <code>public.invoice.createdon</code>.
     */
    public Timestamp getCreatedon() {
        return (Timestamp) get(14);
    }

    /**
     * Setter for <code>public.invoice.createdby</code>.
     */
    public void setCreatedby(String value) {
        set(15, value);
    }

    /**
     * Getter for <code>public.invoice.createdby</code>.
     */
    public String getCreatedby() {
        return (String) get(15);
    }

    /**
     * Setter for <code>public.invoice.updatedon</code>.
     */
    public void setUpdatedon(Timestamp value) {
        set(16, value);
    }

    /**
     * Getter for <code>public.invoice.updatedon</code>.
     */
    public Timestamp getUpdatedon() {
        return (Timestamp) get(16);
    }

    /**
     * Setter for <code>public.invoice.updatedby</code>.
     */
    public void setUpdatedby(String value) {
        set(17, value);
    }

    /**
     * Getter for <code>public.invoice.updatedby</code>.
     */
    public String getUpdatedby() {
        return (String) get(17);
    }

    /**
     * Setter for <code>public.invoice.isactive</code>.
     */
    public void setIsactive(Boolean value) {
        set(18, value);
    }

    /**
     * Getter for <code>public.invoice.isactive</code>.
     */
    public Boolean getIsactive() {
        return (Boolean) get(18);
    }

    /**
     * Setter for <code>public.invoice.class</code>.
     */
    public void setClass_(String value) {
        set(19, value);
    }

    /**
     * Getter for <code>public.invoice.class</code>.
     */
    public String getClass_() {
        return (String) get(19);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record20 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row20<String, Timestamp, Date, String, String, String, String, String, String, String, String, String, String, String, Timestamp, String, Timestamp, String, Boolean, String> fieldsRow() {
        return (Row20) super.fieldsRow();
    }

    @Override
    public Row20<String, Timestamp, Date, String, String, String, String, String, String, String, String, String, String, String, Timestamp, String, Timestamp, String, Boolean, String> valuesRow() {
        return (Row20) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Invoice.INVOICE.ID;
    }

    @Override
    public Field<Timestamp> field2() {
        return Invoice.INVOICE.EMISSIONDATE;
    }

    @Override
    public Field<Date> field3() {
        return Invoice.INVOICE.EXPIRATIONDATE;
    }

    @Override
    public Field<String> field4() {
        return Invoice.INVOICE.REASON;
    }

    @Override
    public Field<String> field5() {
        return Invoice.INVOICE.SERIE;
    }

    @Override
    public Field<String> field6() {
        return Invoice.INVOICE.NUMBER;
    }

    @Override
    public Field<String> field7() {
        return Invoice.INVOICE.VERSIONDOCUMENT;
    }

    @Override
    public Field<String> field8() {
        return Invoice.INVOICE.VERSIONUBL;
    }

    @Override
    public Field<String> field9() {
        return Invoice.INVOICE.COMPANYCODE;
    }

    @Override
    public Field<String> field10() {
        return Invoice.INVOICE.CLIENTCODE;
    }

    @Override
    public Field<String> field11() {
        return Invoice.INVOICE.COINTYPECODE;
    }

    @Override
    public Field<String> field12() {
        return Invoice.INVOICE.DOCUMENTTYPECODE;
    }

    @Override
    public Field<String> field13() {
        return Invoice.INVOICE.CREDITNOTECODE;
    }

    @Override
    public Field<String> field14() {
        return Invoice.INVOICE.DEBITNOTECODE;
    }

    @Override
    public Field<Timestamp> field15() {
        return Invoice.INVOICE.CREATEDON;
    }

    @Override
    public Field<String> field16() {
        return Invoice.INVOICE.CREATEDBY;
    }

    @Override
    public Field<Timestamp> field17() {
        return Invoice.INVOICE.UPDATEDON;
    }

    @Override
    public Field<String> field18() {
        return Invoice.INVOICE.UPDATEDBY;
    }

    @Override
    public Field<Boolean> field19() {
        return Invoice.INVOICE.ISACTIVE;
    }

    @Override
    public Field<String> field20() {
        return Invoice.INVOICE.CLASS;
    }

    @Override
    public String component1() {
        return getId();
    }

    @Override
    public Timestamp component2() {
        return getEmissiondate();
    }

    @Override
    public Date component3() {
        return getExpirationdate();
    }

    @Override
    public String component4() {
        return getReason();
    }

    @Override
    public String component5() {
        return getSerie();
    }

    @Override
    public String component6() {
        return getNumber();
    }

    @Override
    public String component7() {
        return getVersiondocument();
    }

    @Override
    public String component8() {
        return getVersionubl();
    }

    @Override
    public String component9() {
        return getCompanycode();
    }

    @Override
    public String component10() {
        return getClientcode();
    }

    @Override
    public String component11() {
        return getCointypecode();
    }

    @Override
    public String component12() {
        return getDocumenttypecode();
    }

    @Override
    public String component13() {
        return getCreditnotecode();
    }

    @Override
    public String component14() {
        return getDebitnotecode();
    }

    @Override
    public Timestamp component15() {
        return getCreatedon();
    }

    @Override
    public String component16() {
        return getCreatedby();
    }

    @Override
    public Timestamp component17() {
        return getUpdatedon();
    }

    @Override
    public String component18() {
        return getUpdatedby();
    }

    @Override
    public Boolean component19() {
        return getIsactive();
    }

    @Override
    public String component20() {
        return getClass_();
    }

    @Override
    public String value1() {
        return getId();
    }

    @Override
    public Timestamp value2() {
        return getEmissiondate();
    }

    @Override
    public Date value3() {
        return getExpirationdate();
    }

    @Override
    public String value4() {
        return getReason();
    }

    @Override
    public String value5() {
        return getSerie();
    }

    @Override
    public String value6() {
        return getNumber();
    }

    @Override
    public String value7() {
        return getVersiondocument();
    }

    @Override
    public String value8() {
        return getVersionubl();
    }

    @Override
    public String value9() {
        return getCompanycode();
    }

    @Override
    public String value10() {
        return getClientcode();
    }

    @Override
    public String value11() {
        return getCointypecode();
    }

    @Override
    public String value12() {
        return getDocumenttypecode();
    }

    @Override
    public String value13() {
        return getCreditnotecode();
    }

    @Override
    public String value14() {
        return getDebitnotecode();
    }

    @Override
    public Timestamp value15() {
        return getCreatedon();
    }

    @Override
    public String value16() {
        return getCreatedby();
    }

    @Override
    public Timestamp value17() {
        return getUpdatedon();
    }

    @Override
    public String value18() {
        return getUpdatedby();
    }

    @Override
    public Boolean value19() {
        return getIsactive();
    }

    @Override
    public String value20() {
        return getClass_();
    }

    @Override
    public InvoiceRecord value1(String value) {
        setId(value);
        return this;
    }

    @Override
    public InvoiceRecord value2(Timestamp value) {
        setEmissiondate(value);
        return this;
    }

    @Override
    public InvoiceRecord value3(Date value) {
        setExpirationdate(value);
        return this;
    }

    @Override
    public InvoiceRecord value4(String value) {
        setReason(value);
        return this;
    }

    @Override
    public InvoiceRecord value5(String value) {
        setSerie(value);
        return this;
    }

    @Override
    public InvoiceRecord value6(String value) {
        setNumber(value);
        return this;
    }

    @Override
    public InvoiceRecord value7(String value) {
        setVersiondocument(value);
        return this;
    }

    @Override
    public InvoiceRecord value8(String value) {
        setVersionubl(value);
        return this;
    }

    @Override
    public InvoiceRecord value9(String value) {
        setCompanycode(value);
        return this;
    }

    @Override
    public InvoiceRecord value10(String value) {
        setClientcode(value);
        return this;
    }

    @Override
    public InvoiceRecord value11(String value) {
        setCointypecode(value);
        return this;
    }

    @Override
    public InvoiceRecord value12(String value) {
        setDocumenttypecode(value);
        return this;
    }

    @Override
    public InvoiceRecord value13(String value) {
        setCreditnotecode(value);
        return this;
    }

    @Override
    public InvoiceRecord value14(String value) {
        setDebitnotecode(value);
        return this;
    }

    @Override
    public InvoiceRecord value15(Timestamp value) {
        setCreatedon(value);
        return this;
    }

    @Override
    public InvoiceRecord value16(String value) {
        setCreatedby(value);
        return this;
    }

    @Override
    public InvoiceRecord value17(Timestamp value) {
        setUpdatedon(value);
        return this;
    }

    @Override
    public InvoiceRecord value18(String value) {
        setUpdatedby(value);
        return this;
    }

    @Override
    public InvoiceRecord value19(Boolean value) {
        setIsactive(value);
        return this;
    }

    @Override
    public InvoiceRecord value20(String value) {
        setClass_(value);
        return this;
    }

    @Override
    public InvoiceRecord values(String value1, Timestamp value2, Date value3, String value4, String value5, String value6, String value7, String value8, String value9, String value10, String value11, String value12, String value13, String value14, Timestamp value15, String value16, Timestamp value17, String value18, Boolean value19, String value20) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        value13(value13);
        value14(value14);
        value15(value15);
        value16(value16);
        value17(value17);
        value18(value18);
        value19(value19);
        value20(value20);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached InvoiceRecord
     */
    public InvoiceRecord() {
        super(Invoice.INVOICE);
    }

    /**
     * Create a detached, initialised InvoiceRecord
     */
    public InvoiceRecord(String id, Timestamp emissiondate, Date expirationdate, String reason, String serie, String number, String versiondocument, String versionubl, String companycode, String clientcode, String cointypecode, String documenttypecode, String creditnotecode, String debitnotecode, Timestamp createdon, String createdby, Timestamp updatedon, String updatedby, Boolean isactive, String class_) {
        super(Invoice.INVOICE);

        set(0, id);
        set(1, emissiondate);
        set(2, expirationdate);
        set(3, reason);
        set(4, serie);
        set(5, number);
        set(6, versiondocument);
        set(7, versionubl);
        set(8, companycode);
        set(9, clientcode);
        set(10, cointypecode);
        set(11, documenttypecode);
        set(12, creditnotecode);
        set(13, debitnotecode);
        set(14, createdon);
        set(15, createdby);
        set(16, updatedon);
        set(17, updatedby);
        set(18, isactive);
        set(19, class_);
    }
}
