/*
 * This file is generated by jOOQ.
 */
package innovar.io.invoice.models.tables.pojos;


import innovar.io.core.models.BaseModel;

import javax.annotation.Generated;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Additionalinformation extends BaseModel implements Serializable {

    private static final long serialVersionUID = -802171735;

    private String     invoicecode;
    private String     numberpurchaseorder;
    private String     legendtypecode;
    private String     legendnote;
    private String     operationtypecode;
    private String     indicatorfise;
    private BigDecimal amountfise;
    private String     codefise;

    private String     class_;

    public Additionalinformation() {}

    public Additionalinformation(Additionalinformation value) {
        this.id = value.id;
        this.invoicecode = value.invoicecode;
        this.numberpurchaseorder = value.numberpurchaseorder;
        this.legendtypecode = value.legendtypecode;
        this.legendnote = value.legendnote;
        this.operationtypecode = value.operationtypecode;
        this.indicatorfise = value.indicatorfise;
        this.amountfise = value.amountfise;
        this.codefise = value.codefise;
        this.createdon = value.createdon;
        this.createdby = value.createdby;
        this.updatedon = value.updatedon;
        this.updatedby = value.updatedby;
        this.isactive = value.isactive;
        this.class_ = value.class_;
    }

    public Additionalinformation(
        String     id,
        String     invoicecode,
        String     numberpurchaseorder,
        String     legendtypecode,
        String     legendnote,
        String     operationtypecode,
        String     indicatorfise,
        BigDecimal amountfise,
        String     codefise,
        Timestamp  createdon,
        String     createdby,
        Timestamp  updatedon,
        String     updatedby,
        Boolean    isactive,
        String     class_
    ) {
        this.id = id;
        this.invoicecode = invoicecode;
        this.numberpurchaseorder = numberpurchaseorder;
        this.legendtypecode = legendtypecode;
        this.legendnote = legendnote;
        this.operationtypecode = operationtypecode;
        this.indicatorfise = indicatorfise;
        this.amountfise = amountfise;
        this.codefise = codefise;
        this.createdon = createdon;
        this.createdby = createdby;
        this.updatedon = updatedon;
        this.updatedby = updatedby;
        this.isactive = isactive;
        this.class_ = class_;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoicecode() {
        return this.invoicecode;
    }

    public void setInvoicecode(String invoicecode) {
        this.invoicecode = invoicecode;
    }

    public String getNumberpurchaseorder() {
        return this.numberpurchaseorder;
    }

    public void setNumberpurchaseorder(String numberpurchaseorder) {
        this.numberpurchaseorder = numberpurchaseorder;
    }

    public String getLegendtypecode() {
        return this.legendtypecode;
    }

    public void setLegendtypecode(String legendtypecode) {
        this.legendtypecode = legendtypecode;
    }

    public String getLegendnote() {
        return this.legendnote;
    }

    public void setLegendnote(String legendnote) {
        this.legendnote = legendnote;
    }

    public String getOperationtypecode() {
        return this.operationtypecode;
    }

    public void setOperationtypecode(String operationtypecode) {
        this.operationtypecode = operationtypecode;
    }

    public String getIndicatorfise() {
        return this.indicatorfise;
    }

    public void setIndicatorfise(String indicatorfise) {
        this.indicatorfise = indicatorfise;
    }

    public BigDecimal getAmountfise() {
        return this.amountfise;
    }

    public void setAmountfise(BigDecimal amountfise) {
        this.amountfise = amountfise;
    }

    public String getCodefise() {
        return this.codefise;
    }

    public void setCodefise(String codefise) {
        this.codefise = codefise;
    }

    public Timestamp getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Timestamp createdon) {
        this.createdon = createdon;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Timestamp getUpdatedon() {
        return this.updatedon;
    }

    public void setUpdatedon(Timestamp updatedon) {
        this.updatedon = updatedon;
    }

    public String getUpdatedby() {
        return this.updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getClass_() {
        return this.class_;
    }

    public void setClass_(String class_) {
        this.class_ = class_;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Additionalinformation (");

        sb.append(id);
        sb.append(", ").append(invoicecode);
        sb.append(", ").append(numberpurchaseorder);
        sb.append(", ").append(legendtypecode);
        sb.append(", ").append(legendnote);
        sb.append(", ").append(operationtypecode);
        sb.append(", ").append(indicatorfise);
        sb.append(", ").append(amountfise);
        sb.append(", ").append(codefise);
        sb.append(", ").append(createdon);
        sb.append(", ").append(createdby);
        sb.append(", ").append(updatedon);
        sb.append(", ").append(updatedby);
        sb.append(", ").append(isactive);
        sb.append(", ").append(class_);

        sb.append(")");
        return sb.toString();
    }
}
