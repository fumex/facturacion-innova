package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CoinTypeDto  {
    private String id;
    private String    code;
    private String    name;
    private String    badge;
    private Integer   number;
    private Boolean isactive;
}