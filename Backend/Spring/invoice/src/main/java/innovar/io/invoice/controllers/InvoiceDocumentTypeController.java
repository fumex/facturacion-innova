package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.InvoiceDocumentTypeService;
import innovar.io.invoice.dto.InvoiceDocumentTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class InvoiceDocumentTypeController extends BaseController {
    @Autowired
    InvoiceDocumentTypeService invoiceDocumentTypeService;

    @PostMapping("/invoicedocumenttype")
    public AppResponse createInvoiceDocumentType(@RequestBody() InvoiceDocumentTypeDto invoiceDocumentTypeDto){
        return Response(()-> invoiceDocumentTypeService.createInvoiceDocumentType((invoiceDocumentTypeDto)));
    }
    @PostMapping("/invoicedocumenttypeListCreate")
    public AppResponse createListTaxType(@RequestBody() List<InvoiceDocumentTypeDto> invoiceDocumentTypeDtos){
        return Response(()->invoiceDocumentTypeService.createListInvoiceDocumentType(invoiceDocumentTypeDtos));
    }

    @PutMapping("/invoicedocumenttype/{id}")
    public AppResponse updateInvoiceDocumentType(@PathVariable() String id, @RequestBody() InvoiceDocumentTypeDto invoiceDocumentTypeDto){
        return Response(()-> invoiceDocumentTypeService.updateInvoiceDocumentType((id),(invoiceDocumentTypeDto)));
    }

    @GetMapping("/invoicedocumenttype/{id}")
    public AppResponse getInvoiceDocumentType(@PathVariable() String id){
        return Response(()-> invoiceDocumentTypeService.getInvoiceDocumentType(id));
    }

    @GetMapping("/invoicedocumenttypes")
    public AppResponse getAllInvoiceDocumentTypes(){
        return Response(()-> invoiceDocumentTypeService.getAllInvoiceDocumentTypes());
    }

    @GetMapping("/ListInvoiceDocumentTypes")
    public AppResponse getListInvoiceDocumentTypes(){
        return Response(()-> invoiceDocumentTypeService.getListInvoiceDocumentTypes());
    }

    @DeleteMapping("/invoicedocumenttype/{id}")
    public AppResponse disabledInvoiceDocumentType(@PathVariable() String id){
        return Response(()-> invoiceDocumentTypeService.disableInvoiceDocumentType(id));
    }
}