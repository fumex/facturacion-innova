package innovar.io.invoice.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Invoicedetail;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

@Service
public class InvoiceDetailDataService extends RepositoryPostgressService<Invoicedetail> {

    public InvoiceDetailDataService(DSLContext dsl){ super(dsl); }

    @Override
    public innovar.io.invoice.models.tables.Invoicedetail getTable() {
        return Tables.INVOICEDETAIL;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.INVOICEDETAIL.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.INVOICEDETAIL.ISACTIVE;
    }


}
