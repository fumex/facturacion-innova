package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TotalsDto {
    private String     id;
    private BigDecimal chargediscountbaseamount;
    private String     chargediscountcode;
    private BigDecimal chargediscountfactor;
    private Boolean    chargediscountindicator;
    private BigDecimal chargediscountporcentage;
    private BigDecimal globalchargeamount;
    private BigDecimal globaldiscountamount;
    private BigDecimal summationcharges;
    private BigDecimal summationdiscounts;
    private BigDecimal summationigv;
    private BigDecimal summationisc;
    private BigDecimal summationivap;
    private BigDecimal summationothertax;
    private BigDecimal totalamount;
    private BigDecimal totalsaleprice;
    private BigDecimal totalsalevalue;
    private BigDecimal totalsalevalueexport;
    private BigDecimal totalsalevalueoperationexoneradas;
    private BigDecimal totalsalevalueoperationgratuitas;
    private BigDecimal totalsalevalueoperationgravadas;
    private BigDecimal totalsalevalueoperationinafectas;
    private BigDecimal totaltaxamount;
    private String     invoicecode;;
    private Boolean    isactive;
}
