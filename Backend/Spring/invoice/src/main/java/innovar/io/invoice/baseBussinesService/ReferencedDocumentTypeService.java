package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.ReferencedDocumentTypeDto;
import innovar.io.invoice.models.tables.pojos.Referenceddocumenttype;
import innovar.io.invoice.services.ReferencedDocumentTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ReferencedDocumentTypeService extends BaseBussinesService {
    @Autowired
    ReferencedDocumentTypeDataService baseReferencedDocumentTypeDataService;

    public ReferencedDocumentTypeDto createReferencedDocumentType(ReferencedDocumentTypeDto referencedDocumentTypeDto){
        Referenceddocumenttype referencedDocumentType = map(referencedDocumentTypeDto, Referenceddocumenttype.class);
        List<Referenceddocumenttype> referencedDocumentTypes = baseReferencedDocumentTypeDataService.findCode(referencedDocumentTypeDto.getCode());
        if(referencedDocumentTypes.size()>0){
            String id = referencedDocumentTypes.get(0).getId();
            Referenceddocumenttype referencedDocumentTypeDataServiceById = baseReferencedDocumentTypeDataService.findById(id);
            referencedDocumentTypeDataServiceById.setActivateddate(new Timestamp(new Date().getTime()));
            baseReferencedDocumentTypeDataService.update(id,referencedDocumentTypeDataServiceById);
            return map(referencedDocumentTypeDataServiceById, ReferencedDocumentTypeDto.class);
        }else{
            if(referencedDocumentType.getIsactive()==null){
                referencedDocumentType.setIsactive(true);
            }
            referencedDocumentType.setActivateddate(new Timestamp(new Date().getTime()));
            referencedDocumentType.setInactivateddate(new Timestamp(new Date().getTime()));
            baseReferencedDocumentTypeDataService.insert((referencedDocumentType));
            return map(referencedDocumentType, ReferencedDocumentTypeDto.class);
        }
    }

    public List<ReferencedDocumentTypeDto> createListReferencedDocumentType(List<ReferencedDocumentTypeDto> referencedDocumentTypeDtos){
        List<Referenceddocumenttype> referencedDocumentTypes = mapAll(referencedDocumentTypeDtos, Referenceddocumenttype.class);
        ReferencedDocumentTypeDto referencedDocumentTypeDto = new ReferencedDocumentTypeDto();
        referencedDocumentTypes.stream().forEach((p)->{
            referencedDocumentTypeDto.setCode(p.getCode());
            referencedDocumentTypeDto.setName(p.getName());
            if(p.getIsactive()==null){
                referencedDocumentTypeDto.setIsactive(true);
            }else{
                referencedDocumentTypeDto.setIsactive(p.getIsactive());
            }
            createReferencedDocumentType(referencedDocumentTypeDto);
        });
        return mapAll(referencedDocumentTypes,ReferencedDocumentTypeDto.class);
    }

    public ReferencedDocumentTypeDto updateReferencedDocumentType(String id, ReferencedDocumentTypeDto referencedDocumentTypeDto){
        Referenceddocumenttype referencedDocumentType = map(referencedDocumentTypeDto, Referenceddocumenttype.class);
        Referenceddocumenttype a = baseReferencedDocumentTypeDataService.findById(id);
        referencedDocumentType.setActivateddate(a.getActivateddate());
        referencedDocumentType.setInactivateddate(a.getInactivateddate());
        if(referencedDocumentType.getIsactive()==null){
            referencedDocumentType.setIsactive(true);
        }
        baseReferencedDocumentTypeDataService.update(id,referencedDocumentType);
        return map(referencedDocumentType, ReferencedDocumentTypeDto.class);
    }

    public ReferencedDocumentTypeDto getReferencedDocumentType(String id){
        Referenceddocumenttype referencedDocumentType = baseReferencedDocumentTypeDataService.findById(id);
        return map(referencedDocumentType, ReferencedDocumentTypeDto.class);
    }

    public ReferencedDocumentTypeDto disableReferencedDocumentType(String id){
        Referenceddocumenttype referencedDocumentType = baseReferencedDocumentTypeDataService.findById(id);
        referencedDocumentType.setInactivateddate(new Timestamp(new Date().getTime()));
        baseReferencedDocumentTypeDataService.update(id,referencedDocumentType);
        return map( baseReferencedDocumentTypeDataService.disabled(id), ReferencedDocumentTypeDto.class);
    }

    public List<ReferencedDocumentTypeDto> getAllReferencedDocumentTypes(){
        List<Referenceddocumenttype> referencedDocumentTypes = baseReferencedDocumentTypeDataService.findIntoList();
        return mapAll(referencedDocumentTypes, ReferencedDocumentTypeDto.class);
    }

    public List<ReferencedDocumentTypeDto> getListReferencedDocumentTypes(){
        List<Referenceddocumenttype> referencedDocumentTypes = baseReferencedDocumentTypeDataService.listAll();
        return mapAll(referencedDocumentTypes, ReferencedDocumentTypeDto.class);
    }
}

