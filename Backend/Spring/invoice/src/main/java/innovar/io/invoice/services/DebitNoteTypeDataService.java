package innovar.io.invoice.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Debitnotetype;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DebitNoteTypeDataService extends RepositoryPostgressService<Debitnotetype> {

    public DebitNoteTypeDataService(DSLContext dsl){super(dsl);}

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Debitnotetype.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Debitnotetype getTable() {
        return Tables.DEBITNOTETYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.DEBITNOTETYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.DEBITNOTETYPE.ISACTIVE;
    }


}
