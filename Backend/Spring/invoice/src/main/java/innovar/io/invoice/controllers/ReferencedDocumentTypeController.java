package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.ReferencedDocumentTypeService;
import innovar.io.invoice.dto.ReferencedDocumentTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class ReferencedDocumentTypeController  extends BaseController {
    @Autowired
    ReferencedDocumentTypeService referencedDocumentTypeService;

    @PostMapping("/referenceddocumenttype")
    public AppResponse createReferencedDocumentType(@RequestBody() ReferencedDocumentTypeDto referencedDocumentTypeDto){
        return Response(()-> referencedDocumentTypeService.createReferencedDocumentType((referencedDocumentTypeDto)));
    }

    @PostMapping("/referenceddocumenttypeListCreate")
    public AppResponse createListReferencedDocumentType(@RequestBody() List<ReferencedDocumentTypeDto> referencedDocumentTypeDtos){
        return Response(()->referencedDocumentTypeService.createListReferencedDocumentType(referencedDocumentTypeDtos));
    }

    @GetMapping("/referenceddocumenttype/{id}")
    public AppResponse getReferencedDocumentType(@PathVariable() String id){
        return Response(()-> referencedDocumentTypeService.getReferencedDocumentType(id));
    }

    @PutMapping("/referenceddocumenttype/{id}")
    public AppResponse updateReferencedDocumentType(@PathVariable() String id, @RequestBody() ReferencedDocumentTypeDto referencedDocumentTypeDto){
        return Response(()-> referencedDocumentTypeService.updateReferencedDocumentType((id),(referencedDocumentTypeDto)));
    }

    @GetMapping("/referenceddocumenttypes")
    public AppResponse getAllReferencedDocumentTypes(){
        return Response(()-> referencedDocumentTypeService.getAllReferencedDocumentTypes());
    }

    @GetMapping("/ListReferencedDocumentTypes")
    public AppResponse getListReferencedDocumentTypes(){
        return Response(()-> referencedDocumentTypeService.getListReferencedDocumentTypes());
    }

    @DeleteMapping("/referenceddocumenttype/{id}")
    public AppResponse disabledReferencedDocumentType(@PathVariable() String id){
        return Response(()-> referencedDocumentTypeService.disableReferencedDocumentType(id));
    }
}
