package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.SerieService;
import innovar.io.invoice.dto.SerieDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(InvoiceApplication.host)
public class SerieController extends BaseController {
    @Autowired
    SerieService serieService;

    @PostMapping("/serie")
    public AppResponse createSerie(@RequestBody() SerieDto SerieDto){
        return Response(()-> serieService.createSerie((SerieDto)));
    }

    @PutMapping("/serie/{id}")
    public AppResponse updateSerie(@PathVariable() String id, @RequestBody() SerieDto serieDto){
        return Response(()-> serieService.updateSerie((id),(serieDto)));
    }

    @GetMapping("/serie/{id}")
    public AppResponse getSerie(@PathVariable() String id){
        return Response(()-> serieService.getSerie(id));
    }

    @GetMapping("/series")
    public AppResponse getAllSeries(){
        return Response(()-> serieService.getAllSeries());
    }

    @GetMapping("/findSeriesDocumentType/{code}")
    public AppResponse findSeriesDocumentTypes(@PathVariable() String code){
        return Response(()-> serieService.findSeriesDocumentType(code));
    }

    @GetMapping("/ListSeries")
    public AppResponse getListSeries(){
        return Response(()-> serieService.getListSeries());
    }

    @DeleteMapping("/serie/{id}")
    public AppResponse disabledSerie(@PathVariable() String id){
        return Response(()-> serieService.disabledSerie(id));
    }
}
