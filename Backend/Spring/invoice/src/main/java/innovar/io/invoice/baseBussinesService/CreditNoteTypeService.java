package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;

import innovar.io.invoice.dto.CreditNoteTypeDto;
import innovar.io.invoice.models.tables.pojos.Creditnotetype;
import innovar.io.invoice.services.CreditNoteTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class CreditNoteTypeService extends BaseBussinesService {
    @Autowired
    CreditNoteTypeDataService baseCreditNoteTypeDataService;

    public CreditNoteTypeDto createCreditNoteType(CreditNoteTypeDto creditNoteTypeDto){
        Creditnotetype creditnotetype = map(creditNoteTypeDto, Creditnotetype.class);
        List<Creditnotetype> creditnotetypes = baseCreditNoteTypeDataService.findCode(creditNoteTypeDto.getCode());
        if(creditnotetypes.size()>0){
            String id = creditnotetypes.get(0).getId();
            Creditnotetype a = baseCreditNoteTypeDataService.findById(id);
            a.setActivateddate(new Timestamp(new Date().getTime()));
            baseCreditNoteTypeDataService.update(id,a);
            return map(a, CreditNoteTypeDto.class);
        }else{
            if(creditnotetype.isactive==null){
                creditnotetype.setIsactive(true);
            }
            creditnotetype.setActivateddate(new Timestamp(new Date().getTime()));
            creditnotetype.setInactivateddate(new Timestamp(new Date().getTime()));
            baseCreditNoteTypeDataService.insert((creditnotetype));
            return map(creditnotetype, CreditNoteTypeDto.class);
        }
    }

    public List<CreditNoteTypeDto> createListCreditNoteType(List<CreditNoteTypeDto> creditNoteTypeDtos){
        List<Creditnotetype> creditnotetypes = mapAll(creditNoteTypeDtos, Creditnotetype.class);
        CreditNoteTypeDto creditNoteTypeDto = new CreditNoteTypeDto();
        creditnotetypes.stream().forEach((p)->{
            creditNoteTypeDto.setCode(p.getCode());
            creditNoteTypeDto.setName(p.getName());
            creditNoteTypeDto.setLevel(p.getLevel());
            if(p.getIsactive()==null){
                creditNoteTypeDto.setIsactive(true);
            }else{
                creditNoteTypeDto.setIsactive(p.getIsactive());
            }
            createCreditNoteType(creditNoteTypeDto);
        });
        return mapAll(creditnotetypes,CreditNoteTypeDto.class);
    }

    public CreditNoteTypeDto updateCreditNoteType(String id, CreditNoteTypeDto creditNoteTypeDto){
        Creditnotetype creditnotetype = map(creditNoteTypeDto, Creditnotetype.class);
        Creditnotetype a = baseCreditNoteTypeDataService.findById(id);
        creditnotetype.setActivateddate(a.getActivateddate());
        creditnotetype.setInactivateddate(a.getInactivateddate());
        if(creditnotetype.isactive==null){
            creditnotetype.setIsactive(true);
        }
        baseCreditNoteTypeDataService.update(id,creditnotetype);
        return map(creditnotetype, CreditNoteTypeDto.class);
    }

    public CreditNoteTypeDto getCreditNoteType(String id){
        Creditnotetype creditnotetype = baseCreditNoteTypeDataService.findById(id);
        return map(creditnotetype, CreditNoteTypeDto.class);
    }

    public CreditNoteTypeDto disableCreditNoteType(String id){
        Creditnotetype creditnotetype = baseCreditNoteTypeDataService.findById(id);
        creditnotetype.setInactivateddate(new Timestamp(new Date().getTime()));
        baseCreditNoteTypeDataService.update(id,creditnotetype);
        return map( baseCreditNoteTypeDataService.disabled(id), CreditNoteTypeDto.class);
    }

    public List<CreditNoteTypeDto> getAllCreditNoteType(){
        List<Creditnotetype> creditnotetypes = baseCreditNoteTypeDataService.findIntoList();
        return mapAll(creditnotetypes, CreditNoteTypeDto.class);
    }

    public List<CreditNoteTypeDto> getListCreditNoteType(){
        List<Creditnotetype> creditnotetypes = baseCreditNoteTypeDataService.listAll();
        return mapAll(creditnotetypes, CreditNoteTypeDto.class);
    }
}
