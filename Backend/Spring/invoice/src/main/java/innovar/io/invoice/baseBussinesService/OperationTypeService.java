package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.OperationTypeDto;
import innovar.io.invoice.models.tables.pojos.Operationtype;
import innovar.io.invoice.services.OperationTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class OperationTypeService extends BaseBussinesService {
    @Autowired
    OperationTypeDataService baseOperationTypeDataService;

    public OperationTypeDto createOperationType(OperationTypeDto operationTypeDto){
        Operationtype operationType = map(operationTypeDto, Operationtype.class);
        List<Operationtype> operationTypes = baseOperationTypeDataService.findCode(operationTypeDto.getCode());
        if(operationTypes.size()>0){
            String id = operationTypes.get(0).getId();
            Operationtype operationTypeDataServiceById = baseOperationTypeDataService.findById(id);
            operationTypeDataServiceById.setActivateddate(new Timestamp(new Date().getTime()));
            baseOperationTypeDataService.update(id,operationTypeDataServiceById);
            return map(operationTypeDataServiceById, OperationTypeDto.class);
        }else{
            if(operationType.isactive==null){
                operationType.setIsactive(true);
            }
            operationType.setActivateddate(new Timestamp(new Date().getTime()));
            operationType.setInactivateddate(new Timestamp(new Date().getTime()));
            baseOperationTypeDataService.insert((operationType));
            return map(operationType, OperationTypeDto.class);
        }
    }

    public List<OperationTypeDto> createListOperationType(List<OperationTypeDto> taxTypeDtos){
        List<Operationtype> operationTypes = mapAll(taxTypeDtos, Operationtype.class);
        OperationTypeDto operationTypeDto = new OperationTypeDto();
        operationTypes.stream().forEach((p)->{
            operationTypeDto.setCode(p.getCode());
            operationTypeDto.setName(p.getName());
            operationTypeDto.setAssociatedinvoicetype(p.getAssociatedinvoicetype());
            if(p.getIsactive()==null){
                operationTypeDto.setIsactive(true);
            }else{
                operationTypeDto.setIsactive(p.getIsactive());
            }
            createOperationType(operationTypeDto);
        });
        return mapAll(operationTypes,OperationTypeDto.class);
    }

    public OperationTypeDto updateOperationType(String id, OperationTypeDto operationTypeDto){
        Operationtype operationType = map(operationTypeDto, Operationtype.class);
        Operationtype a = baseOperationTypeDataService.findById(id);
        operationType.setActivateddate(a.getActivateddate());
        operationType.setInactivateddate(a.getInactivateddate());
        if(operationType.isactive==null){
            operationType.setIsactive(true);
        }
        baseOperationTypeDataService.update(id,operationType);
        return map(operationType, OperationTypeDto.class);
    }

    public OperationTypeDto getOperationType(String id){
        Operationtype operationType = baseOperationTypeDataService.findById(id);
        return map(operationType, OperationTypeDto.class);
    }

    public OperationTypeDto disableOperationType(String id){
        Operationtype operationType = baseOperationTypeDataService.findById(id);
        operationType.setInactivateddate(new Timestamp(new Date().getTime()));
        baseOperationTypeDataService.update(id,operationType);
        return map( baseOperationTypeDataService.disabled(id), OperationTypeDto.class);
    }

    public List<OperationTypeDto> getAllOperationTypes(){
        List<Operationtype> operationTypes = baseOperationTypeDataService.findIntoList();
        return mapAll(operationTypes, OperationTypeDto.class);
    }

    public List<OperationTypeDto> getListOperationTypes(){
        List<Operationtype> operationTypes = baseOperationTypeDataService.listAll();
        return mapAll(operationTypes, OperationTypeDto.class);
    }
}
