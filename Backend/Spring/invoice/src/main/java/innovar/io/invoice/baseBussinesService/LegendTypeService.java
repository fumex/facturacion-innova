package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.LegendTypeDto;
import innovar.io.invoice.models.tables.pojos.Legendtype;
import innovar.io.invoice.services.LegendTypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class LegendTypeService extends BaseBussinesService {
    @Autowired
    LegendTypeDataService baseLegendTypeDataService;

    public LegendTypeDto createLegendType(LegendTypeDto legendTypeDto){
        Legendtype legendType = map(legendTypeDto, Legendtype.class);
        List<Legendtype> legendTypes = baseLegendTypeDataService.findCode(legendTypeDto.getCode());
        if(legendTypes.size()>0){
            String id = legendTypes.get(0).getId();
            Legendtype legendTypeById = baseLegendTypeDataService.findById(id);
            legendTypeById.setActivateddate(new Timestamp(new Date().getTime()));
            baseLegendTypeDataService.update(id,legendTypeById);
            return map(legendTypeById, LegendTypeDto.class);
        }else{
            if(legendType.isactive==null){
                legendType.setIsactive(true);
            }
            legendType.setActivateddate(new Timestamp(new Date().getTime()));
            legendType.setInactivateddate(new Timestamp(new Date().getTime()));
            baseLegendTypeDataService.insert((legendType));
            return map(legendType, LegendTypeDto.class);
        }
    }

    public List<LegendTypeDto> createListLegendType(List<LegendTypeDto> legendTypeDtos){
        List<Legendtype> legendTypes = mapAll(legendTypeDtos, Legendtype.class);
        LegendTypeDto legendTypeDto = new LegendTypeDto();
        legendTypes.stream().forEach((p)->{
            legendTypeDto.setCode(p.getCode());
            legendTypeDto.setName(p.getName());
            if(p.getIsactive()==null){
                legendTypeDto.setIsactive(true);
            }else{
                legendTypeDto.setIsactive(p.getIsactive());
            }
            createLegendType(legendTypeDto);
        });
        return mapAll(legendTypes,LegendTypeDto.class);
    }

    public LegendTypeDto updateLegendType(String id, LegendTypeDto legendTypeDto){
        Legendtype legendType = map(legendTypeDto, Legendtype.class);
        Legendtype a = baseLegendTypeDataService.findById(id);
        legendType.setActivateddate(a.getActivateddate());
        legendType.setInactivateddate(a.getInactivateddate());
        if(legendType.isactive==null){
            legendType.setIsactive(true);
        }
        baseLegendTypeDataService.update(id,legendType);
        return map(legendType, LegendTypeDto.class);
    }

    public LegendTypeDto getLegendType(String id){
        Legendtype legendType = baseLegendTypeDataService.findById(id);
        return map(legendType, LegendTypeDto.class);
    }

    public LegendTypeDto disableLegendType(String id){
        Legendtype legendType = baseLegendTypeDataService.findById(id);
        legendType.setInactivateddate(new Timestamp(new Date().getTime()));
        baseLegendTypeDataService.update(id,legendType);
        return map( baseLegendTypeDataService.disabled(id), LegendTypeDto.class);
    }

    public List<LegendTypeDto> getAllLegendTypes(){
        List<Legendtype> legendTypes = baseLegendTypeDataService.findIntoList();
        return mapAll(legendTypes, LegendTypeDto.class);
    }

    public List<LegendTypeDto> getListLegendTypes(){
        List<Legendtype> legendTypes = baseLegendTypeDataService.listAll();
        return mapAll(legendTypes, LegendTypeDto.class);
    }

}
