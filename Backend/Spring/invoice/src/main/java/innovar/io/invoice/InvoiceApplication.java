package innovar.io.invoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "innovar.io")
@EntityScan(basePackages = "innovar.io")
public class InvoiceApplication {
    public static  final String host= "/api/invoice";
    public static void main(String[] args) {
        SpringApplication.run(InvoiceApplication.class, args);
    }

}
