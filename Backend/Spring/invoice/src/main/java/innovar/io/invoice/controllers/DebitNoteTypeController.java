package innovar.io.invoice.controllers;

import innovar.io.core.controller.AppResponse;
import innovar.io.core.controller.BaseController;
import innovar.io.invoice.InvoiceApplication;
import innovar.io.invoice.baseBussinesService.DebitNoteTypeService;
import innovar.io.invoice.dto.DebitNoteTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(InvoiceApplication.host)
public class DebitNoteTypeController extends BaseController {
    @Autowired
    DebitNoteTypeService debitNoteTypeService;

    @PostMapping("/debitnotetype")
    public AppResponse createDebitNoteType(@RequestBody() DebitNoteTypeDto debitNoteTypeDto){
        return Response(()-> debitNoteTypeService.createDebitNoteType((debitNoteTypeDto)));
    }
    @PostMapping("/debitnotetypeListCreate")
    public AppResponse createListDebitNoteType(@RequestBody() List<DebitNoteTypeDto> debitNoteTypeDtos){
        return Response(()-> debitNoteTypeService.createListDebitNoteType(debitNoteTypeDtos));
    }

    @PutMapping("/debitnotetype/{id}")
    public AppResponse updateDebitNoteType(@PathVariable() String id, @RequestBody() DebitNoteTypeDto debitNoteTypeDto){
        return Response(()-> debitNoteTypeService.updateDebitNoteType((id),(debitNoteTypeDto)));
    }

    @GetMapping("/debitnotetype/{id}")
    public AppResponse getDebitNoteType(@PathVariable() String id){
        return Response(()-> debitNoteTypeService.getDebitNoteType(id));
    }

    @GetMapping("/debitnotetypes")
    public AppResponse getAllDebitNoteTypes(){
        return Response(()-> debitNoteTypeService.getAllDebitNoteType());
    }

    @GetMapping("/ListDebitnotetypes")
    public AppResponse getListDebitNoteTypes(){
        return Response(()-> debitNoteTypeService.getListDebitNoteType());
    }

    @DeleteMapping("/debitnotetype/{id}")
    public AppResponse disabledDebitNoteType(@PathVariable() String id){
        return Response(()-> debitNoteTypeService.disableDebitNoteType(id));
    }
}
