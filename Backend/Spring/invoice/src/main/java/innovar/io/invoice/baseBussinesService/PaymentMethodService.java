package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.PaymentMethodDto;
import innovar.io.invoice.models.tables.pojos.Paymentmethod;
import innovar.io.invoice.services.PaymentMethodDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class PaymentMethodService extends BaseBussinesService {
    @Autowired
    PaymentMethodDataService basePaymentMethodDataService;

    public PaymentMethodDto createPaymentMethod(PaymentMethodDto paymentMethodDto){
        Paymentmethod paymentMethod = map(paymentMethodDto, Paymentmethod.class);
        List<Paymentmethod> paymentMethods = basePaymentMethodDataService.findCode(paymentMethodDto.getCode());
        if(paymentMethods.size()>0){
            String id = paymentMethods.get(0).getId();
            Paymentmethod methodDataServiceById = basePaymentMethodDataService.findById(id);
            methodDataServiceById.setActivateddate(new Timestamp(new Date().getTime()));
            basePaymentMethodDataService.update(id,methodDataServiceById);
            return map(methodDataServiceById, PaymentMethodDto.class);
        }else{
            if(paymentMethod.isactive==null){
                paymentMethod.setIsactive(true);
            }
            paymentMethod.setActivateddate(new Timestamp(new Date().getTime()));
            paymentMethod.setInactivateddate(new Timestamp(new Date().getTime()));
            basePaymentMethodDataService.insert((paymentMethod));
            return map(paymentMethod, PaymentMethodDto.class);
        }
    }

    public List<PaymentMethodDto> createListPaymentMethod(List<PaymentMethodDto> paymentMethodDtos){
        List<Paymentmethod> paymentMethods = mapAll(paymentMethodDtos, Paymentmethod.class);
        PaymentMethodDto paymentMethodDto = new PaymentMethodDto();
        paymentMethods.stream().forEach((p)->{
            paymentMethodDto.setCode(p.getCode());
            paymentMethodDto.setName(p.getName());
            if(p.getIsactive()==null){
                paymentMethodDto.setIsactive(true);
            }else{
                paymentMethodDto.setIsactive(p.getIsactive());
            }
            createPaymentMethod(paymentMethodDto);
        });
        return mapAll(paymentMethods,PaymentMethodDto.class);
    }

    public PaymentMethodDto updatePaymentMethod(String id, PaymentMethodDto paymentMethodDto){
        Paymentmethod paymentMethod = map(paymentMethodDto, Paymentmethod.class);
        Paymentmethod a = basePaymentMethodDataService.findById(id);
        paymentMethod.setActivateddate(a.getActivateddate());
        paymentMethod.setInactivateddate(a.getInactivateddate());
        if(paymentMethod.isactive==null){
            paymentMethod.setIsactive(true);
        }
        basePaymentMethodDataService.update(id,paymentMethod);
        return map(paymentMethod, PaymentMethodDto.class);
    }

    public PaymentMethodDto getPaymentMethod(String id){
        Paymentmethod methodDataServiceById = basePaymentMethodDataService.findById(id);
        return map(methodDataServiceById, PaymentMethodDto.class);
    }

    public PaymentMethodDto disablePaymentMethod(String id){
        Paymentmethod paymentMethod = basePaymentMethodDataService.findById(id);
        paymentMethod.setInactivateddate(new Timestamp(new Date().getTime()));
        basePaymentMethodDataService.update(id,paymentMethod);
        return map( basePaymentMethodDataService.disabled(id), PaymentMethodDto.class);
    }

    public List<PaymentMethodDto> getAllPaymentMethods(){
        List<Paymentmethod> paymentMethods = basePaymentMethodDataService.findIntoList();
        return mapAll(paymentMethods, PaymentMethodDto.class);
    }

    public List<PaymentMethodDto> getListPaymentMethods(){
        List<Paymentmethod> paymentMethods = basePaymentMethodDataService.listAll();
        return mapAll(paymentMethods, PaymentMethodDto.class);
    }
}
