package innovar.io.invoice.services;


import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Totals;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;

@Service
public class TotalsDataService extends RepositoryPostgressService<Totals> {

    public TotalsDataService(DSLContext dsl){ super(dsl);}

    @Override
    public innovar.io.invoice.models.tables.Totals getTable() {
        return Tables.TOTALS;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.TOTALS.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.TOTALS.ISACTIVE;
    }
}
