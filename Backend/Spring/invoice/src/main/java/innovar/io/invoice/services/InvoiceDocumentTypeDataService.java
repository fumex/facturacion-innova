package innovar.io.invoice.services;

import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Invoicedocumenttype;
;
import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceDocumentTypeDataService extends RepositoryPostgressService<Invoicedocumenttype> {

    public InvoiceDocumentTypeDataService(DSLContext dsl){ super(dsl); }

    public List findCode(String code){
        return find(getTable().CODE.eq(code)).fetchInto(Invoicedocumenttype.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Invoicedocumenttype getTable() {
        return Tables.INVOICEDOCUMENTTYPE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.INVOICEDOCUMENTTYPE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.INVOICEDOCUMENTTYPE.ISACTIVE;
    }
    /* public List<InvoiceDocumentType> findCode(String code){
        Query query= new Query();
        query.addCriteria(Criteria.where("code").is(code));
        return mongoTemplate.find(query, InvoiceDocumentType.class);
    }*/
}
