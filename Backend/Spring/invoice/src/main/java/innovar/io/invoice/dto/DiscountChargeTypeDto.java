package innovar.io.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DiscountChargeTypeDto {

    private String    id;
    private String    code;
    private String    name;
    private String    level;
    private Boolean   isactive;

}
