package innovar.io.invoice.services;



import innovar.io.core.repositories.RepositoryPostgressService;
import innovar.io.invoice.dto.SerieDto;
import innovar.io.invoice.models.Tables;
import innovar.io.invoice.models.tables.pojos.Serie;
import org.jooq.DSLContext;
import org.jooq.TableField;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SerieDataService extends RepositoryPostgressService<Serie> {

    public SerieDataService(DSLContext dsl){ super(dsl); }

    public List findSeries(String serie){
        return find(getTable().SERIECODE.eq(serie)).fetchInto(SerieDto.class);
    }

    public List findSeriesDocumentType(String code){
        return find(getTable().TYPEDOCUMENTCODE.eq(code).and(getTable().ISACTIVE.eq(true))).fetchInto(Serie.class);
    }

    @Override
    public innovar.io.invoice.models.tables.Serie getTable() {
        return Tables.SERIE;
    }

    @Override
    public TableField<?, String> getId() {
        return Tables.SERIE.ID;
    }

    @Override
    public TableField<?, Boolean> getIsActive() {
        return Tables.SERIE.ISACTIVE;
    }

}
