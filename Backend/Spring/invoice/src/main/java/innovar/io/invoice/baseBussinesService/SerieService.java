package innovar.io.invoice.baseBussinesService;

import innovar.io.core.baseBusinessService.BaseBussinesService;
import innovar.io.invoice.dto.SerieDto;
import innovar.io.invoice.models.tables.pojos.Serie;
import innovar.io.invoice.services.SerieDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class SerieService  extends BaseBussinesService {
    @Autowired
    SerieDataService baseSerieDataService;

    public SerieDto createSerie(SerieDto serieDto){
        Serie serie = map(serieDto,Serie.class);
        if(serie.getIsactive()==null){
            serie.setIsactive(true);
        }
        baseSerieDataService.insert((serie));
        return map(serie, SerieDto.class);
    }

    public SerieDto updateSerie(String id, SerieDto serieDto){
        Serie serie= map(serieDto,Serie.class);
        serie.setUpdatedon(new Timestamp(new Date().getTime()));
        if(serie.getIsactive()==null){
            serie.setIsactive(true);
        }
        baseSerieDataService.update(id, serie);
        return  map(serie, SerieDto.class);
    }
    public SerieDto getSerie(String id){
        Serie serie= baseSerieDataService.findById(id);
        return map(serie, SerieDto.class);
    }

    public List<SerieDto> getAllSeries(){
        List<Serie> series= baseSerieDataService.findIntoList();
        return  mapAll(series, SerieDto.class);
    }

    public List<SerieDto> getListSeries(){
        List<Serie> series= baseSerieDataService.listAll();
        return  mapAll(series, SerieDto.class);
    }

    public SerieDto disabledSerie(String id){
        Serie serie= baseSerieDataService.findById(id);
        serie.setUpdatedon(new Timestamp(new Date().getTime()));
        baseSerieDataService.update(id, serie);
        return map(baseSerieDataService.disabled(id),SerieDto.class);
    }
    public List<SerieDto> findSeriesDocumentType(String codeDocument){
        List<Serie> series = baseSerieDataService.findSeriesDocumentType(codeDocument);
        return mapAll(series, SerieDto.class);
    }
}
