package innovar.io.core.controller;

public interface AppMethod<T>  {
    public T execute();
}
