package innovar.io.core.controller;

import innovar.io.core.errorHandling.AppError;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AppResponse<T> {
    private T data;
    private AppError error;
}
