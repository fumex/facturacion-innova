package innovar.io.core.repositories;


import innovar.io.core.models.BaseModel;
import org.bson.types.ObjectId;
import org.jooq.*;
import org.jooq.impl.TableImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


@Service
public abstract class RepositoryPostgressService<T extends BaseModel>
//        implements IRepositoryMongoService<T>
{

    public final DSLContext dsl;

    public abstract TableImpl<?> getTable();

    public abstract TableField<?, String> getId();

    public abstract TableField<?, Boolean> getIsActive();

    public Class<T> inferedClass;

    public RepositoryPostgressService(DSLContext dsl) {
        this.dsl = dsl;
        this.inferedClass = getGenericClass();
    }


    public T insert(T t) {
        t.id = new ObjectId().toString();
        t.createdby = SecurityContextHolder.getContext().getAuthentication().getName();
        t.createdon = new Timestamp(new Date().getTime());
        t.updatedby = SecurityContextHolder.getContext().getAuthentication().getName();
        t.updatedon = new Timestamp(new Date().getTime());
        //t.isactive = true;
        Record record = dsl.newRecord(getTable(), t);
        dsl.insertInto(getTable()).set(record).execute();
        return t;
    }

    public T update(String id, T t) {
        T a = dsl.selectFrom(getTable()).where(getId().eq(id)).fetchOneInto(inferedClass);
        t.id =id;
        t.createdby = a.createdby;
        t.createdon = new Timestamp(new Date().getTime());
        t.updatedby = SecurityContextHolder.getContext().getAuthentication().getName();
        //t.isactive = (true);
        t.updatedon = new Timestamp(new Date().getTime());
        Record record = dsl.newRecord(getTable(), t);
        dsl.update(getTable()).set(record).where(getId().eq(id)).execute();
        return t;
    }

    public T disabled(String id) {
        T a = dsl.selectFrom(getTable()).where(getId().eq(id)).fetchOneInto(inferedClass);
        a.isactive = (false);
        a.updatedon = new Timestamp(new Date().getTime());
        a.updatedby = ("");
        Record record = dsl.newRecord(getTable(), a);
        dsl.update(getTable()).set(record).where(getId().eq(id)).execute();
        return a;

    }

    public T findById(String id) {
        return dsl.selectFrom(getTable())
                .where(getId().eq(id))
                .fetchOneInto(inferedClass);
    }

    protected SelectConditionStep find() {
        return dsl
                .selectFrom(getTable())
                .where(getIsActive().eq(true));
    }


    public List<T> findIntoList() {
        return dsl
                .selectFrom(getTable())
                .where(getIsActive().eq(true)).fetchInto(getGenericClass());
    }
    public List<T> listAll(){
        return  dsl
                .selectFrom(getTable()).fetchInto(getGenericClass());
    }
    protected SelectConditionStep FindInActives() {
        return dsl
                .selectFrom(getTable())
                .where(getIsActive().eq(false));
    }

    protected SelectConditionStep find(Condition query) {
        return dsl
                .selectFrom(getTable())
                .where()
                .and(query);
    }

    protected SelectConditionStep FindInActive() {
        return dsl
                .selectFrom(getTable())
                .where(getIsActive().eq(false));
    }

    protected SelectConditionStep FindInActives(Condition query) {
        return dsl
                .selectFrom(getTable())
                .where(getIsActive().eq(false))
                .and(query);
    }

    public Boolean Remove(String id) {
        return dsl
                .delete(getTable())
                .where(getIsActive().eq(true))
                .and(getId().eq(id)).execute() == 0;
    }


    private Class<T> getGenericClass() {
        if (inferedClass == null) {
            Type mySuperclass = getClass().getGenericSuperclass();
            Type tType = ((ParameterizedType) mySuperclass).getActualTypeArguments()[0];
            String className = tType.toString().split(" ")[1];
            try {
                inferedClass = (Class<T>) Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return inferedClass;
    }

}
