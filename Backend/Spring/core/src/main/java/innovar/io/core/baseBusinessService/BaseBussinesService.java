package innovar.io.core.baseBusinessService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class BaseBussinesService {

    @Autowired
    protected ModelMapper modelMapper;

    public <D, T> D map(final T entity, Class<D> outClass) {
        return modelMapper.map(entity, outClass);
    }

    public <D, T> List<D> mapAll(final Collection<T> entityLst, Class<D> outClass) {
        return entityLst.stream()
                .map(entity -> map(entity, outClass))
                .collect(Collectors.toList());
    }

}
