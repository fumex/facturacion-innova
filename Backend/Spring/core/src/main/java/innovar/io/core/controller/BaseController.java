package innovar.io.core.controller;

import innovar.io.core.errorHandling.AppError;

import java.util.Date;


public class BaseController {
    public <T> AppResponse Response(AppMethod<T> method){
        try{
            T resp = method.execute();
            return new AppResponse<T>(resp,null);
        } catch (Exception e){
            e.printStackTrace();
            return new AppResponse<T>(null, new AppError().setMessage(e.getMessage()).setDate(new Date()));
        }
    }
}
