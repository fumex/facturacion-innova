package innovar.io.core.models;

import java.sql.Timestamp;


public class BaseModel {
    public String id;
    public Timestamp createdon;
    public String createdby;
    public Timestamp updatedon;
    public String updatedby;
    public Boolean isactive;
}
