export const environment = {
  addressService: 'https://erp.innovarperu.com',
  personService: 'https://erp.innovarperu.com',
  invoiceService: 'https://erp.innovarperu.com',
  productService: 'https://erp.innovarperu.com',
  production: true
};
