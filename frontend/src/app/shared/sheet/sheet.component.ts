import { Component, OnInit, OnChanges, Input, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-sheet',
  templateUrl: './sheet.component.html',
  styles: []
})

export class SheetComponent implements OnInit, OnChanges {

  @Input()  rows: Array<any> = [];
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor() { this.displayedColumns = ['column1', 'column2', 'column n']; }

  ngOnInit() {
  }

  ngOnChanges(): void {
    this.createDatatable(this.rows);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createDatatable(data: Array<any>) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.assignColmnName(data);
  }

  assignColmnName( data: Array<any>): string[] {
    if (data.length !== 0) {
      const column = Object.keys(data[0]);
      this.displayedColumns = [];
      column.forEach(element => {
        this.displayedColumns.push( element.toString() );
      });
    }
    return this.displayedColumns;
  }

}
