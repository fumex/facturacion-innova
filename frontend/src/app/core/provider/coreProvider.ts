import { Injectable } from '@angular/core';

@Injectable()
export class CoreProvider {
    showMessageError(message: string): void {
        alert( message);
    }

    showMessageErrorUnexpected(): void {
        alert('Se a producido un error inesperado, consulte al administrador');
    }

    showMessage(message: string): void {
        alert(message);
    }

    showMessageOK(): void {
        alert('Se proceso de forma correcta');
    }

    getUrlBackEnd(): string {
        return 'http://192.168.1.4:8080/';
    }

    getUser(): string {
        return 'betzabe';
    }
}
