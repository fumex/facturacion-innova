import { PaymentMethodMaintenanceComponent } from './payment-method-maintenance/payment-method-maintenance.component';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { PaymentMethodDto } from './../../../services/invoice.service';
import { Parent } from 'src/app/core/class/Parent';
import { InvoiceService } from 'src/app/services/invoice.service';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { Component, OnInit, ViewChild } from '@angular/core';

import *  as XLSX from 'xlsx';
import { map, filter } from 'rxjs/operators';
@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styles: [],
  providers: [CoreProvider, InvoiceService]
})
export class PaymentMethodComponent extends Parent implements OnInit {
    listItem: Array<PaymentMethodDto> = [];
    listItemNew: Array<any> = [];
    dataSource: MatTableDataSource<PaymentMethodDto>;
    displayedColumns: string[] = ['code', 'name', 'status', 'button'];
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true})  sort: MatSort;

    target: DataTransfer = new DataTransfer();
  constructor(
    private formBuilder: FormBuilder,
    private invoiceService: InvoiceService,
    private CoreProvider: CoreProvider,
    private router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
    }
  }

  getItem() {
      this.invoiceService.listPaymentMethods().subscribe(data => {
          this.listItem = <Array<PaymentMethodDto>> (data.data);
          this.dataSource = new MatTableDataSource(this.listItem);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          console.log(data.data);
      }, error => {
          this.CoreProvider.showMessageErrorUnexpected();
      });
  }

  onFileChange(evt: any): Array<any> {
      this.target = (evt.target) as DataTransfer;
      if (this.target.files.length !== 1) {
          throw new Error('cannot use multiple fies');
      }
      const reader: FileReader =  new FileReader();
      reader.onload = (e: any) => {
          const bstr: string  = e.target.result;
          const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

          const ws: XLSX.WorkSheet = wb.Sheets['MEDIO DE PAGO']

          const json = XLSX.utils.sheet_to_json(ws);
          this.listItemNew = json.map(
              obj => {return{
                  code: obj['CODIGO'],
                  name: obj['NOMBRE']
              };
          });
      };
      reader.readAsBinaryString(this.target.files[0]);
      return this.listItemNew;
  }

  searchNewItem(list: Array<any>) {
      const temporalArray: Array<PaymentMethodDto> = JSON.parse(JSON.stringify(list));
      for (var i = temporalArray.length - 1 ; i >= 0; i--) {
          for (var j = 0; j < this.listItem.length; j++) {
              if ((temporalArray[i] !== undefined) && (String(temporalArray[i].code) === this.listItem[j].code.toString())) {
                  temporalArray.splice(i, 1);
              }
          }
      }
      return temporalArray;
  }

  inactivateItem(list: Array<any>) {
      const temporalArray: Array<PaymentMethodDto> = this.listItem;
      for (var i = temporalArray.length - 1; i >= 0; i--) {
          for (var j = 0; j < list.length; j++) {
              if ((temporalArray[i] !== undefined) && (temporalArray[i].code === list[j].code.toString())) {
                  temporalArray.splice(i, 1);
              }
          }
      }
      return temporalArray;
  }

  onSubmit() {
      let list =  this.searchNewItem(this.listItemNew);
      if ( list.length !== 0) {
          this.createPaymentMethodList(list);
      }
      let listDelete: Array<PaymentMethodDto> =  this.inactivateItem(this.listItemNew);
      if (listDelete.length !== 0) {
          console.log(listDelete);
          listDelete.forEach(element => {
              let paymentMethodDto: PaymentMethodDto =  new PaymentMethodDto();
              paymentMethodDto.id =  element.id;
              this.deletePaymentMethod(paymentMethodDto);
          });
      }
  }

  createPaymentMethodList(paymentMethodDtoListDto: Array<PaymentMethodDto>) {
      this.invoiceService.paymentmethodListCreate(paymentMethodDtoListDto).subscribe(
          data => {
              this.openSnackBar('Guardado', 'ok');
              this.getItem();
          },
          error => {
              console.log(error);
              this.openSnackBar('Error datos no corresponden', 'OK');
          }
      );
  }

  deletePaymentMethod(paymentMethodDto: PaymentMethodDto) {
    this.invoiceService.paymentmethodDelete(paymentMethodDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
        this.getItem();
        console.log('eliminado');
    });
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
            file : ['', Validators.required]
    });
  }

  returnDash() {
      this.router.navigate(['/dashboard']);
  }

  addItem() {
      let dialogRef =  this.dialog.open(PaymentMethodMaintenanceComponent, {
          width: '80%',
          disableClose: true,
          data: {
              operation: this.operationNew,
              info: null
          }
      });
      dialogRef.afterClosed().subscribe(result => {
          this.getItem();
      });
  }
  updateItem(item) {
    let dialogRef = this.dialog.open(PaymentMethodMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  deleteItem(item) {
    let dialogRef = this.dialog.open(PaymentMethodMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }


}
