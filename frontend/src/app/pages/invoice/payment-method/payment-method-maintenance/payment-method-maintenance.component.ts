import { CoreProvider } from './../../../../core/provider/coreProvider';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Parent } from './../../../../core/class/Parent';
import { InvoiceService, PaymentMethodDto } from './../../../../services/invoice.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-payment-method-maintenance',
  templateUrl: './payment-method-maintenance.component.html',
  styles: [],
  providers: [InvoiceService]
})
export class PaymentMethodMaintenanceComponent extends Parent implements OnInit {

  paymentMethodDto: PaymentMethodDto = new PaymentMethodDto();

  constructor(
    public dialogRef: MatDialogRef<PaymentMethodMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _invoiceService: InvoiceService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title){
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void{
    if (this.data['info'] !== null){
      var temp: PaymentMethodDto = <PaymentMethodDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else{
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string){
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.paymentMethodDto.code = info.code;
        this.paymentMethodDto.name = info.name;
        this.paymentMethodDto.isactive = info.isactive;
        this._invoiceService.paymentmethodPut(info.id, this.paymentMethodDto).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.paymentMethodDto.code = info.code;
        this.paymentMethodDto.name = info.name;
        this.paymentMethodDto.isactive = info.isactive;
        this._invoiceService.paymentmethodPost(this.paymentMethodDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.paymentMethodDto.id = this.form.value.id;
      this._invoiceService.paymentmethodDelete(this.paymentMethodDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
