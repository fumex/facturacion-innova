import { Component, OnInit } from '@angular/core';
import { Parent } from '../../../core/class/Parent';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { InvoiceService, OperationTypeDto, CoinTypeDto, SerieDto } from 'src/app/services/invoice.service';
import { AddressService, PersonService } from 'src/app/services/service.index';
import { DateAdapter, MAT_DATE_FORMATS, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/core/class/format-datepicker';
import { DocumentIdentityTypeDto, ClientDto } from 'src/app/services/person.service';
import { AddressDto } from 'src/app/services/address.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ItemComponent } from '../item/item.component';
import { ClientMaintenanceComponent } from '../../person/client/client-maintenance/client-maintenance.component';
import { Router } from '@angular/router';
import { InvoiceDto, InvoiceDetailDto, TotalsDto, AdditionalInformationDto } from '../../../services/invoice.service';

@Component({
  selector: 'app-factura',
  styleUrls: ['./factura.component.css'],
  templateUrl: './factura.component.html',
  providers: [CoreProvider, InvoiceService, AddressService, PersonService, {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}]
})

export class FacturaComponent extends Parent implements OnInit {
  invoice: InvoiceDto = new InvoiceDto();
  invoiceDetail: InvoiceDetailDto = new InvoiceDetailDto();
  invoiceTotal: TotalsDto = new TotalsDto();
  invoiceInfo: AdditionalInformationDto = new AdditionalInformationDto();

  coins: Array<CoinTypeDto>;
  documents: Array<DocumentIdentityTypeDto>;
  operations: Array<OperationTypeDto>;
  series: Array<SerieDto>;
  hint: string = '';
  client: ClientDto = new ClientDto();
  addressPerson: Array<AddressDto>;
  displayedColumns: string[] = ['number', 'description', 'unitsaleprice', 'quantity', 'salevalue', 'igv', 'amount', 'button'];
  items: Array<any> = [];
  dataSource: MatTableDataSource<any>;
  num: string = '';
  subIGV: number = 0;
  subISC: number = 0;
  subIVAP: number = 0;
  subOTH: number = 0;
  subCharge: number = 0;
  subDesct: number = 0;

  constructor(
    private formBuilder: FormBuilder,
    private addressService: AddressService,
    private invoiceService: InvoiceService,
    private personService: PersonService,
    private coreProvider: CoreProvider,
    private dialog: MatDialog,
    public router: Router,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getCoinTypes();
    this.getOperations();
    this.getSeries();
    this.getDocumentsId();
    this.getDetail();
  }

  openSnackBar(message: string, action?: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  buildForm() {
    this.form = this.formBuilder.group({
      id: 0,
      documenttypecode: ['01', Validators.required],
      operationtypecode: ['0101', Validators.required],
      cointypecode: ['PEN', Validators.required],
      serie: ['', Validators.required],
      emissiondate: [new Date()],
      expirationdate: [new Date()],
      versiondocument: ['2.0'],
      versionubl: ['2.1'],

      documentidcode: [{ value: '6', disabled: true}],
      documentidnumber: ['', [Validators.required, Validators.pattern('[0-9]*$')]],
      addressclient: [''],
      clientname: ['']
    });
  }

  getDetail() {
    this.dataSource = new MatTableDataSource(this.items);
  }

  getOperations() {
    this.invoiceService.operationtypes().subscribe(
      data => this.operations = data.data as Array<OperationTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getCoinTypes() {
    this.invoiceService.cointypes().subscribe(
      data => this.coins = data.data as Array<CoinTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getSeries() {
    this.invoiceService.findSeriesDocumentType(this.form.value.documenttypecode).subscribe(
      data => {
        if ((data.data !== undefined) && (data.data.length !== 0)) {
          this.series = data.data as Array<SerieDto>;
          this.form.controls.serie.setValue(data.data[0].seriecode);
          this.setNumber();
        } else {
          this.coreProvider.showMessageError('Ingrese o active series de comprobante');
          this.router.navigate([ '/serie']);
        }
      },
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getDocumentsId() {
    this.personService.documentidentitytypes().subscribe(
      data => {
        if (data.data !== null ) {
          this.documents = data.data as Array<DocumentIdentityTypeDto>;
        } else {
          this.coreProvider.showMessageError(data.error.message);
        }
      },
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getClient() {
    this.personService.findClientDocumentIdentity('6', this.form.value.documentidnumber).subscribe(
      data => {
        if (data.data.length === 0) {
          this.hint = 'Cliente no existe en la base de datos';
        } else {
          this.hint = 'Cliente encontrado';
          this.client = data.data[0] as ClientDto;
          this.form.controls.clientname.setValue(this.client.name);
          console.log(this.client);
          this.getAddressPerson();
          }
        },
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  addClient() {
    const dialogRef = this.dialog.open(ClientMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
  }

  cleanClient() {
    this.client = new ClientDto();
    this.form.controls.clientname.setValue('');
    this.addressPerson = [];
    this.form.controls.documentidnumber.reset();
  }

  getAddressPerson() {
    this.addressService.findAddressPerson(this.client.id).subscribe(
      data => { if (data.data.length !== 0) {
        console.log(data.data);
        this.addressPerson = data.data;
        this.form.controls.addressclient.setValue(data.data[0].id);
      } else {
        this.openSnackBar('Cliente no tiene direcciones guardadas');
      }
    },
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  setNumber() {
    this.invoiceService.findLastInvoice(this.form.value.serie).subscribe(
      data => {
        if (data.data !== undefined) {
          const temp = data.data;
          if (temp <= 99999999) {
            this.num = temp.toString().padStart(8, '0');
          } else {
            this.coreProvider.showMessage('Serie terminada, cambie de serie');
            this.form.value.serie.focus();
          }
        } else {
          this.coreProvider.showMessageError(data.error.message);
        }
      },
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  addItem(): void {
    const dialogRef = this.dialog.open(ItemComponent, {
      width: '65%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== true) {
        this.items.push(result);
        this.calculateTax(result.product.productPriceTaxDtos, result.quantity);
        this.calculateCD(result.product.productPriceChargeDiscountDtos, result.quantity);
        this.openSnackBar('Producto agregado');
        this.getDetail();
      }
    });
  }

  deleteItem(i) {
    this.reduceTax(this.items[i].product.productPriceTaxDtos, this.items[i].quantity);
    this.reduceCD(this.items[i].product.productPriceChargeDiscountDtos, this.items[i].quantity);
    this.items.splice(i, 1);
    this.getDetail();
  }

  getTotalCost() {
    return this.items.map(t => t.product.productPriceDto.unitsaleprice * t.quantity).reduce((acc, value) => acc + value, 0);
  }

  getOpGravada() {
    return this.items.map(t => t.product.productPriceDto.unitsalevalue * t.quantity).reduce((acc, value) => acc + value, 0);
  }

  calculateCD(array: Array<any>, quantity: number) {
    if (array !== null) {
      array.forEach(element => {
        switch (element.discountchargetypecode) {
          case '00':
            this.subDesct = (element.unitchargediscountamount * quantity) + this.subDesct;
            break;
          case '01':
            this.subDesct = (element.unitchargediscountamount * quantity) + this.subDesct;
            break;
          case '47':
            this.subCharge = (element.unitchargediscountamount * quantity) + this.subCharge;
            break;
          case '48':
            this.subCharge = (element.unitchargediscountamount * quantity) + this.subCharge;
            break;
        }
      });
    }
  }

  reduceCD(array: Array<any>, quantity: number) {
    if (array !== null) {
      array.forEach(element => {
        switch (element.discountchargetypecode) {
          case '00':
            this.subDesct = this.subDesct - (element.unitchargediscountamount * quantity);
            break;
          case '01':
            this.subDesct = this.subDesct - (element.unitchargediscountamount * quantity);
            break;
          case '47':
            this.subCharge = this.subCharge - (element.unitchargediscountamount * quantity);
            break;
          case '48':
            this.subCharge = this.subCharge - (element.unitchargediscountamount * quantity);
            break;
        }
      });
    }
  }

  calculateTax(array: Array<any>, quantity: number) {
    if (array !== null) {
      array.forEach(element => {
        const tax = element.taxtypecode;
        switch (tax) {
          case '1000':
            this.subIGV = (element.unittaxamount * quantity) + this.subIGV;
            break;
          case '1016':
            this.subIVAP = (element.unittaxamount * quantity) + this.subIVAP;
            break;
          case '2000':
            this.subISC = (element.unittaxamount * quantity) + this.subISC;
            break;
          case '9999':
            this.subOTH = (element.unittaxamount * quantity) + this.subOTH;
            break;
        }
      });
    }
  }

  reduceTax(array: Array<any>, quantity: number) {
    if (array !== null) {
      array.forEach(element => {
        const tax = element.taxtypecode;
        switch (tax) {
          case '1000':
            this.subIGV = this.subIGV - (element.unittaxamount * quantity);
            break;
          case '1016':
            this.subIVAP = this.subIVAP - (element.unittaxamount * quantity);
            break;
          case '2000':
            this.subISC = this.subISC - (element.unittaxamount * quantity);
            break;
          case '9999':
            this.subOTH = this.subOTH - (element.unittaxamount * quantity);
            break;
        }
      });
    }
  }

  setIGV(array, quantity: number) {
    if (array !== undefined) {
      return array.filter(t => t.taxtypecode === '1000').map( t2 => t2.unittaxamount * quantity).reduce((acc, value) => acc + value, 0);
    } else {
      return 0;
    }
  }

  setISC(array, quantity: number) {
    if (array !== undefined) {
      return array.filter(t => t.taxtypecode === '2000').map( t2 => t2.unittaxamount * quantity).reduce((acc, value) => acc + value, 0);
    } else {
      return 0;
    }
  }

  setIVAP(array, quantity: number) {
    if (array !== undefined) {
      return array.filter(t => t.taxtypecode === '1016').map( t2 => t2.unittaxamount * quantity).reduce((acc, value) => acc + value, 0);
    } else {
      return 0;
    }
  }

  setOTH(array, quantity: number) {
    if (array !== undefined) {
      return array.filter(t => t.taxtypecode === '9999').map( t2 => t2.unittaxamount * quantity).reduce((acc, value) => acc + value, 0);
    } else {
      return 0;
    }
  }

  setDesc(array, quantity: number) {
    if (array !== undefined) {
      return array.filter(t => ((t.discountchargetypecode === '00') || (t.discountchargetypecode === '01'))).map(
        t2 => t2.unitchargediscountamount * quantity).reduce((acc, value) => acc + value, 0);
    } else {
      return 0;
    }
  }

  setCharge(array, quantity: number) {
    if (array !== undefined) {
      return array.filter(t => ((t.discountchargetypecode === '47') || (t.discountchargetypecode === '48')) ).map(
        t2 => t2.unitchargediscountamount * quantity).reduce((acc, value) => acc + value, 0);
    } else {
      return 0;
    }
  }

  save() {
    this.invoice.clientcode = this.client.documentidnumber;
    this.invoice.cointypecode = this.form.value.cointypecode;
    this.invoice.companycode = 'EMPRESA 01';
    this.invoice.documenttypecode = this.form.value.documenttypecode;
    this.invoice.emissiondate = this.form.value.emissiondate;
    this.invoice.expirationdate = this.form.value.expirationdate;
    this.invoice.isactive = true;
    this.invoice.number = this.num;
    this.invoice.serie = this.form.value.serie;
    this.invoice.versiondocument = this.form.value.versiondocument;
    this.invoice.versionubl = this.form.value.versionubl;
    this.invoiceService.invoicePost(this.invoice).subscribe(
      data => { if (data.data !== null) {
        //  Información Adicional de comprobante
        this.invoiceInfo.invoicecode = data.data.id;
        this.invoiceInfo.operationtypecode = this.form.value.operationtypecode;
        this.invoiceService.additionalinformationPost(this.invoiceInfo).subscribe(
          info => { if (info.data !== null) {
              console.log('Información Adicional de comprobante agregado');
            } else {
            this.coreProvider.showMessageError(info.error.message);
          }}, error => this.coreProvider.showMessageErrorUnexpected()
        );
        // Totales de comprobante
        this.invoiceTotal.invoicecode = data.data.id;
        this.invoiceTotal.summationcharges = this.subCharge;
        this.invoiceTotal.summationdiscounts = this.subDesct;
        this.invoiceTotal.summationigv = this.subIGV;
        this.invoiceTotal.summationisc = this.subISC;
        this.invoiceTotal.summationivap = this.subIVAP;
        this.invoiceTotal.summationothertax = this.subOTH;
        this.invoiceTotal.totalamount = this.getOpGravada() + this.subCharge - this.subDesct;
        this.invoiceTotal.totalsaleprice = this.getTotalCost();
        this.invoiceTotal.totalsalevalue = this.getOpGravada();
        this.invoiceTotal.totaltaxamount = this.subIGV + this.subISC + this.subIVAP + this.subOTH;
        this.invoiceService.totalPost(this.invoiceTotal).subscribe(
          total => { if (total.data !== null) {
              console.log('Totales de comprobante agregados');
            } else {
            this.coreProvider.showMessageError(total.error.message);
          }}, error => this.coreProvider.showMessageErrorUnexpected()
        );
        // Detalles de comprobante
        this.invoiceDetail.invoicecode = data.data.id;
        this.items.forEach( (detail, index) => {
            this.invoiceDetail.index = index + 1;
            this.invoiceDetail.discountamountitem = this.setDesc(detail.product.productPriceChargeDiscountDtos, detail.quantity);
            this.invoiceDetail.chargeamountitem = this.setCharge(detail.product.productPriceChargeDiscountDtos, detail.quantity);
            this.invoiceDetail.igvamountitem = this.setIGV(detail.product.productPriceTaxDtos, detail.quantity);
            this.invoiceDetail.iscamountitem = this.setISC(detail.product.productPriceTaxDtos, detail.quantity);
            this.invoiceDetail.ivapamountitem = this.setIVAP(detail.product.productPriceTaxDtos, detail.quantity);
            this.invoiceDetail.othertaxamountitem = this.setOTH(detail.product.productPriceTaxDtos, detail.quantity);
            this.invoiceDetail.productcode = detail.product.code;
            this.invoiceDetail.quantity = detail.quantity;
            this.invoiceDetail.salepriceitem = detail.product.productPriceDto.unitsaleprice *  detail.quantity;
            this.invoiceDetail.salevalueitem = detail.product.productPriceDto.unitsalevalue *  detail.quantity;
            this.invoiceService.invoicedetailPost(this.invoiceDetail).subscribe(
              e => {
                if (e.data !== null) {
                  console.log('Detalles comprobante agregados');
                  this.openSnackBar('Detalles comprobante agregado');
                } else {
                this.coreProvider.showMessageError(e.error.message);
              }}, error =>  this.coreProvider.showMessageErrorUnexpected()
            );
        });
        this.returnDash();
      } else {
        this.coreProvider.showMessageError(data.error.message);
      }},
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  clean() {
    this.cleanClient();
    this.subIGV = 0;
    this.subISC = 0;
    this.subIVAP = 0;
    this.subOTH = 0;
    this.subCharge = 0;
    this.subDesct = 0;
    this.items = [];
    this.buildForm();
    this.getDetail();
    this.getSeries();
    this.setNumber();
  }


 /*
   getIGV() {
    return this.items.map(t => t.product.unitigvamount * t.quantity).reduce((acc, value) => acc + value, 0);
  }

  getISC() {
    if (this.items.productPriceDto !== null) {
      return this.items.map(t => t.product.unitiscamount * t.quantity).reduce((acc, value) => acc + value, 0);
    } else {
      return 0;
    }
  }

  getIVAP() {
    return this.items.map(t => t.product.unitivapamount * t.quantity).reduce((acc, value) => acc + value, 0);
  }

  getCargo() {
    return this.items.map(t => t.product.unitchargeamount * t.quantity).reduce((acc, value) => acc + value, 0);
  }

  getDescuento() {
    return this.items.map(t => t.product.unitdiscountamount * t.quantity).reduce((acc, value) => acc + value, 0);
  }

  getOtros() {
    return this.items.map(t => t.product.unitothertaxesamount * t.quantity).reduce((acc, value) => acc + value, 0);
  }

  MontoBase() {
    return (this.getOpGravada() + this.getCargo() - this.getDescuento());
  } */
  returnDash() {
    this.router.navigate(['/dashboard']);
  }
}

