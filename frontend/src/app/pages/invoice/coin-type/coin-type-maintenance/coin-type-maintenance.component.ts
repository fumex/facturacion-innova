import { CoreProvider } from './../../../../core/provider/coreProvider';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { Parent } from './../../../../core/class/Parent';
import { InvoiceService, CoinTypeDto } from './../../../../services/invoice.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-coin-type-maintenance',
  templateUrl: './coin-type-maintenance.component.html',
  styles: [],
  providers: [InvoiceService]
})
export class CoinTypeMaintenanceComponent extends Parent implements OnInit {

  coinTypeDto: CoinTypeDto = new CoinTypeDto();

  constructor(
    public dialogRef: MatDialogRef<CoinTypeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _invoiceService: InvoiceService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title){
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] !== null){
      var temp: CoinTypeDto = <CoinTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        badge: [{
          value: temp.badge,
          disabled: this.disabledEdit},
          Validators.required],
        number: [{
          value: temp.number,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else{
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        badge: ['', Validators.required],
        number: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string){
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          number: this.form.value.number,
          badge: this.form.value.badge,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.coinTypeDto.code = info.code;
        this.coinTypeDto.name = info.name;
        this.coinTypeDto.number = info.number;
        this.coinTypeDto.badge = info.badge;
        this.coinTypeDto.isactive = info.isactive;
        this._invoiceService.cointypePut(this.coinTypeDto, info.id).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.coinTypeDto.code = info.code;
        this.coinTypeDto.name = info.name;
        this.coinTypeDto.number = info.number;
        this.coinTypeDto.badge = info.badge;
        this.coinTypeDto.isactive = info.isactive;
        this._invoiceService.cointypePost(this.coinTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.coinTypeDto.id = this.form.value.id;
      this._invoiceService.cointypeDelete(this.coinTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
