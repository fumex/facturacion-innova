import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ReferencedDocumentTypeDto, InvoiceService } from '../../../../services/invoice.service';
import { Parent } from 'src/app/core/class/Parent';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-referenced-document-type-maintenance',
  templateUrl: './referenced-document-type-maintenance.component.html',
  styles: [],
  providers:[InvoiceService]
})
export class ReferencedDocumentTypeMaintenanceComponent extends Parent implements OnInit {

  referencedDocumentTypeDto: ReferencedDocumentTypeDto = new ReferencedDocumentTypeDto();
  constructor(
    public dialogRef:MatDialogRef<ReferencedDocumentTypeDto>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _invoiceService: InvoiceService,
    private _snackBar: MatSnackBar 
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: ReferencedDocumentTypeDto = <ReferencedDocumentTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.referencedDocumentTypeDto.code = info.code;
        this.referencedDocumentTypeDto.name = info.name;
        this.referencedDocumentTypeDto.isactive = info.isactive;
        this._invoiceService.referenceddocumenttypePut( info.id, this.referencedDocumentTypeDto).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.referencedDocumentTypeDto.code = info.code;
        this.referencedDocumentTypeDto.name = info.name;
        this.referencedDocumentTypeDto.isactive = info.isactive;
        this._invoiceService.referenceddocumenttypePost(this.referencedDocumentTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }
    if (this.title === this.operationDelete) {
      this.referencedDocumentTypeDto.id = this.form.value.id;
      this._invoiceService.referenceddocumenttypeDelete(this.referencedDocumentTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    } 
  }

}