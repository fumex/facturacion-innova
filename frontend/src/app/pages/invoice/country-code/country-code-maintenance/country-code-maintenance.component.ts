import { InvoiceModule } from '../../invoice.module';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CountryCodeDto, InvoiceService } from '../../../../services/invoice.service';
import { CoreProvider } from '../../../../core/provider/coreProvider';
import { Parent } from '../../../../core/class/Parent';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-country-code-maintenance',
  templateUrl: './country-code-maintenance.component.html',
  styles: [],
  providers: [InvoiceService]
})
export class CountryCodeMaintenanceComponent  extends Parent implements OnInit {
  countryCodeDto: CountryCodeDto = new CountryCodeDto();

  constructor(
    public dialogRef: MatDialogRef<CountryCodeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _invoiceService: InvoiceService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title){
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void{
    if (this.data['info'] !== null){
      var temp: CountryCodeDto = <CountryCodeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        abbreviation2: [{
          value: temp.abbreviation2,
          disabled: this.disabledEdit},
          Validators.required],
        abbreviation3: [{
          value: temp.abbreviation3,
          disabled: this.disabledEdit},
          Validators.required],
        number: [{
          value: temp.number,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else{
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        abbreviation2: ['', Validators.required],
        abbreviation3: ['', Validators.required],
        number: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string){
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          abbreviation2: this.form.value.abbreviation2,
          abbreviation3: this.form.value.abbreviation3,
          number: this.form.value.number,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.countryCodeDto.code = info.code;
        this.countryCodeDto.name = info.name;
        this.countryCodeDto.abbreviation2 = info.abbreviation2;
        this.countryCodeDto.abbreviation3 = info.abbreviation3;
        this.countryCodeDto.number = info.number;
        this.countryCodeDto.isactive = info.isactive;
        this._invoiceService.countrycodePut(this.countryCodeDto, info.id).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.countryCodeDto.code = info.code;
        this.countryCodeDto.name = info.name;
        this.countryCodeDto.abbreviation2 = info.abbreviation2;
        this.countryCodeDto.abbreviation3 = info.abbreviation3;
        this.countryCodeDto.number = info.number;
        this.countryCodeDto.isactive = info.isactive;
        this._invoiceService.countrycodePost(this.countryCodeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.countryCodeDto.id = this.form.value.id;
      this._invoiceService.countrycodeDelete(this.countryCodeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }
}
