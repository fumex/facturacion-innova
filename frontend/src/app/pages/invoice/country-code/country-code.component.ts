import { CountryCodeMaintenanceComponent } from './country-code-maintenance/country-code-maintenance.component';
import { element } from 'protractor';
import { FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { CountryCodeDto, InvoiceService } from '../../../services/invoice.service';


import { CoreProvider } from '../../../core/provider/coreProvider';
import { Parent } from '../../../core/class/Parent';
import { Component, OnInit , ViewChild } from '@angular/core';

import * as XLSX from 'xlsx';
import { map, filter } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'app-country-code',
    templateUrl: './country-code.component.html',
    styles: [],
    providers: [ CoreProvider , InvoiceService]
})
export class CountryCodeComponent extends Parent implements OnInit {

    listItem: Array<CountryCodeDto> = [];
    listItemNew: Array<any> = [];
    dataSource: MatTableDataSource<CountryCodeDto>;
    displayedColumns: string[] = ['code', 'name', 'abbreviation2', 'abbreviation3', 'number', 'status', 'button'];
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true})  sort: MatSort;

    target: DataTransfer = new DataTransfer();

    constructor(
        private formBuilder: FormBuilder,
        private invoiceService: InvoiceService,
        private CoreProvider: CoreProvider,
        private router: Router,
        public dialog: MatDialog,
        private _snackBar: MatSnackBar

    ) {
        super();
    }

    ngOnInit() {
        this.buildForm();
        this.getItem();
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    getItem() {
        this.invoiceService.listCountryCodes().subscribe(data => {
            this.listItem = <Array<CountryCodeDto>> (data.data);
            this.dataSource = new MatTableDataSource(this.listItem);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(data.data);
        }, error => {
            this.CoreProvider.showMessageErrorUnexpected();
        });
    }

    onFileChange(evt: any): Array<any> {
        this.target = (evt.target) as DataTransfer;
        if (this.target.files.length !== 1) {
            throw new Error('cannot use multiple fies');
        }
        const reader: FileReader =  new FileReader();
        reader.onload = (e: any) => {
            const bstr: string  = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

            const ws: XLSX.WorkSheet = wb.Sheets['CODIGO PAIS']

            const json = XLSX.utils.sheet_to_json(ws);
            this.listItemNew = json.map(
                obj => {return{
                    code: obj['CODIGO'],
                    name: obj['NOMBRE'],
                    abbreviation2: obj['ABREVIACION 2'],
                    abbreviation3: obj['ABREVIACION 3'],
                    number: obj['NUMERO']
                };
            });
        };
        reader.readAsBinaryString(this.target.files[0]);
        return this.listItemNew;
    }

    searchNewItem(list: Array<any>) {
        const temporalArray: Array<CountryCodeDto> = JSON.parse(JSON.stringify(list));
        for (var i = temporalArray.length - 1 ; i >= 0; i--) {
            for (var j = 0; j < this.listItem.length; j++) {
                if ((temporalArray[i] !== undefined) && (String(temporalArray[i].code) === this.listItem[j].code.toString())) {
                    temporalArray.splice(i, 1);
                }
            }
        }
        return temporalArray;
    }

    inactivateItem(list: Array<any>) {
        const temporalArray: Array<CountryCodeDto> = this.listItem;
        for (var i = temporalArray.length - 1; i >= 0; i--) {
            for (var j = 0; j < list.length; j++) {
                if ((temporalArray[i] !== undefined) && (temporalArray[i].code === list[j].code.toString())) {
                    temporalArray.splice(i, 1);
                }
            }
        }
        return temporalArray;
    }

    onSubmit() {
        let list: Array<CountryCodeDto> =  this.searchNewItem(this.listItemNew);
        if ( list.length !== 0) {
            this.createCountryCodeLis(list);
        }
        let listDelete: Array<CountryCodeDto> =  this.inactivateItem(this.listItemNew);
        if (listDelete.length !== 0) {
            console.log(listDelete);
            listDelete.forEach(element => {
                let countryCodeDto: CountryCodeDto =  new CountryCodeDto();
                countryCodeDto.id =  element.id;
                this.deleteCountryCode(countryCodeDto);
            });
        }

    }

    createCountryCodeLis(countryCodeLista: Array<CountryCodeDto>) {
        this.invoiceService.countrycodeListCreate(countryCodeLista).subscribe(
            data => {
                console.log(data);
                this.getItem();
            },
            error => {
                console.log(error);
            }
        );
    }
    createCountryCode(countryCodeDto: CountryCodeDto) {
        this.invoiceService.countrycodePost(countryCodeDto).subscribe(
            data => {
                console.log('Guardado')
            },
            error => {
                console.log(error);
                this.openSnackBar('Error datos no corresponden', 'OK');
            }
        );

    }

    deleteCountryCode(countrycodeDto: CountryCodeDto) {
        this.invoiceService.countrycodeDelete(countrycodeDto.id).subscribe(e => {
            this.openSnackBar('Eliminado', 'OK');
            this.getItem();
            console.log('eliminado');
        });
    }

    buildForm(): void {
        this.form = this.formBuilder.group({
            file : ['', Validators.required]
        });
    }

    returnDash() {
        this.router.navigate(['/dashboard']);
    }

    addItem() {
        let dialogRef =  this.dialog.open(CountryCodeMaintenanceComponent, {
            width: '80%',
            disableClose: true,
            data: {
                operation: this.operationNew,
                info: null
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            this.getItem();
        });
    }

    updateItem(item) {
        let dialogRef = this.dialog.open(CountryCodeMaintenanceComponent, {
          width: '80%',
          disableClose: true,
          data: {
            operation: this.operationUpdate,
            info: item
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.getItem();
        });
      }

      deleteItem(item) {
        let dialogRef = this.dialog.open(CountryCodeMaintenanceComponent, {
          width: '80%',
          disableClose: true,
          data: {
            operation: this.operationDelete,
            info: item
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.getItem();
        });
      }

      openSnackBar(message: string, action: string) {
        this._snackBar.open(message, action, {
          duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
        });
      }
}