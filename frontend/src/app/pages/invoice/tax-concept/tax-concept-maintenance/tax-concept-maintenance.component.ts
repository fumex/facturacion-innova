import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { TaxConceptDto } from './../../../../services/invoice.service';
import { Parent } from 'src/app/core/class/Parent';
import { InvoiceService } from 'src/app/services/invoice.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-tax-concept-maintenance',
  templateUrl: './tax-concept-maintenance.component.html',
  styles: [],
  providers: [InvoiceService]
})
export class TaxConceptMaintenanceComponent extends Parent implements OnInit {

  taxConceptDto: TaxConceptDto = new TaxConceptDto();

  constructor(
    public dialogRef: MatDialogRef<TaxConceptMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private invoiceService: InvoiceService,
    private snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] !== null) {
      var temp: TaxConceptDto = this.data['info'] as TaxConceptDto;
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      console.log(info);
      if (this.title === this.operationUpdate) {
        this.taxConceptDto.code = info.code;
        this.taxConceptDto.name = info.name;
        this.taxConceptDto.isactive = info.isactive;
        this.invoiceService.taxconceptPut(info.id, this.taxConceptDto).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.taxConceptDto.code = info.code;
        this.taxConceptDto.name = info.name;
        this.taxConceptDto.isactive = info.isactive;
        this.invoiceService.taxconceptPost(this.taxConceptDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.taxConceptDto.id = this.form.value.id;
      this.invoiceService.taxconceptDelete(this.taxConceptDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
