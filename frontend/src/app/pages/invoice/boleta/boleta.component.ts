import { Component, OnInit } from '@angular/core';
import { CoreProvider } from '../../../core/provider/coreProvider';
import { Parent } from 'src/app/core/class/Parent';
import { DateAdapter, MAT_DATE_FORMATS, MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/core/class/format-datepicker';
import { OperationTypeDto, InvoiceService, CoinTypeDto, SerieDto } from '../../../services/invoice.service';
import { FormBuilder, Validators } from '@angular/forms';
import { DocumentIdentityTypeDto, PersonService, ClientDto } from '../../../services/person.service';
import { ClientMaintenanceComponent } from '../../person/client/client-maintenance/client-maintenance.component';
import { AddressService, AddressDto } from '../../../services/address.service';
import { ItemComponent } from '../item/item.component';

export interface Detail {
  description: string;
  price: number;
  quantity: number;
  code: string;
}

@Component({
  selector: 'app-boleta',
  templateUrl: './boleta.component.html',
  styleUrls: ['boleta.component.css'],
  providers: [CoreProvider, InvoiceService, AddressService, PersonService, {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}]
})
export class BoletaComponent extends Parent implements OnInit {
  hint: string = '';
  serie: string = 'SSSS';
  operations: Array<OperationTypeDto>;
  coins: Array<CoinTypeDto>;
  series: Array<SerieDto>;
  documents: Array<DocumentIdentityTypeDto>;
  client: ClientDto = new ClientDto();
  addressPerson: Array<AddressDto>;

  displayedColumns: string[] = ['number', 'code', 'description', 'quantity', 'price', 'amount', 'button'];
  dataSource: MatTableDataSource<Detail>;
  items: Detail[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private addressService: AddressService,
    private invoiceService: InvoiceService,
    private personService: PersonService,
    private coreProvider: CoreProvider,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getCoinTypes();
    this.getOperations();
    this.getSeries();
    this.getDocumentsId();
    this.getDetail();
  }

  getDetail() {
    this.dataSource = new MatTableDataSource(this.items);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  buildForm() {
    this.form = this.formBuilder.group({
      id: 0,
      documentTypeCode: ['03', Validators.required],
      operationTypeCode: ['0101', Validators.required],
      coinCode: ['PEN', Validators.required],
      seriesCode: ['', Validators.required],
      emissionDate: [new Date()],
      expirationDate: [new Date()],
/*    versionDocument: [''],
      versionUBL: [''], */
      reason: [''],
      // ******************** CLIENTE *****************************
      documentIdCode: ['1'],
      documentIdNumber: ['', [Validators.required, Validators.pattern('[0-9]*$')]],
      addressClient:['']
    });
  }

  getOperations() {
    this.invoiceService.operationtypes().subscribe(
      data => this.operations = data.data as Array<OperationTypeDto>,
      error => this.coreProvider.showMessageError('Error Database ' + error)
    );
  }

  getCoinTypes() {
    this.invoiceService.cointypes().subscribe(
      data => this.coins = data.data as Array<CoinTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getSeries() {
    this.invoiceService.findSeriesDocumentType(this.form.value.documentTypeCode).subscribe(
      data => this.series = data.data as Array<SerieDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getDocumentsId() {
    this.personService.documentidentitytypes().subscribe(
      data => this.documents = data.data as Array<DocumentIdentityTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getClient() {
    this.personService.findClientDocumentIdentity(this.form.value.documentIdCode, this.form.value.documentIdNumber).subscribe(
      data => {
        if (data.data.length === 0) {
          this.hint = 'Cliente no existe en la base de datos';
        } else {
          this.hint = 'Cliente encontrado';
          this.client = data.data[0] as ClientDto;
          console.log(this.client);
          this.getAddressPerson();
          }
        },
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  addClient() {
    let dialogRef = this.dialog.open(ClientMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
  }

  cleanClient() {
    this.client = new ClientDto();
    this.addressPerson = [];
    this.form.controls.documentIdNumber.reset();
  }

  getAddressPerson() {
    this.addressService.findAddressPerson(this.client.id).subscribe(
      data => {
        this.addressPerson = data.data;
      },
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  setSerie() {
    this.serie = this.form.value.seriesCode;
  }

  addItem(): void {
    let dialogRef = this.dialog.open(ItemComponent, {
      width: '65%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== true) {
        this.items.push(result);
        this.getDetail();
        console.log(this.items);
        this.openSnackBar('Producto agregado', 'OK');
      }
    });
  }

  deleteItem(i) {
    this.items.splice(i, 1);
    this.getDetail();
  }

  getTotalCost() {
    return this.items.map(t => t.price * t.quantity).reduce((acc, value) => acc + value, 0);
  }

}
