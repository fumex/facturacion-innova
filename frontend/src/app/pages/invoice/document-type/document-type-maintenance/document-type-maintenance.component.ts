import { CoreProvider } from './../../../../core/provider/coreProvider';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { Parent } from './../../../../core/class/Parent';
import { InvoiceService, InvoiceDocumentTypeDto } from './../../../../services/invoice.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-document-type-maintenance',
  templateUrl: './document-type-maintenance.component.html',
  styles: [],
  providers: [InvoiceService]
})
export class DocumentTypeMaintenanceComponent extends Parent implements OnInit {

  invoiceDocumentTypeDto: InvoiceDocumentTypeDto = new InvoiceDocumentTypeDto();
  constructor(
    public dialogRef: MatDialogRef<DocumentTypeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _invoiceService: InvoiceService,
    private _snackBar: MatSnackBar

  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title){
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void{
    if (this.data['info'] !== null){
      var temp: InvoiceDocumentTypeDto = <InvoiceDocumentTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else{
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }
  openSnackBar(message: string, action: string){
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.invoiceDocumentTypeDto.code = info.code;
        this.invoiceDocumentTypeDto.name = info.name;
        this.invoiceDocumentTypeDto.isactive = info.isactive;
        this._invoiceService.invoicedocumenttypePut(info.id, this.invoiceDocumentTypeDto).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.invoiceDocumentTypeDto.code = info.code;
        this.invoiceDocumentTypeDto.name = info.name;
        this.invoiceDocumentTypeDto.isactive = info.isactive;
        this._invoiceService.invoicedocumenttypePost(this.invoiceDocumentTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.invoiceDocumentTypeDto.id = this.form.value.id;
      this._invoiceService.invoicedocumenttypeDelete(this.invoiceDocumentTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }


}
