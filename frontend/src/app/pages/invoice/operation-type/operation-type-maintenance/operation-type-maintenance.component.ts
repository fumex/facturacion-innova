import { CoreProvider } from './../../../../core/provider/coreProvider';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Parent } from './../../../../core/class/Parent';
import { InvoiceService, OperationTypeDto } from './../../../../services/invoice.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-operation-type-maintenance',
  templateUrl: './operation-type-maintenance.component.html',
  styles: [],
  providers: [InvoiceService]
})
export class OperationTypeMaintenanceComponent extends Parent implements OnInit {
  
  operationTypeDto: OperationTypeDto = new OperationTypeDto();

  constructor(
    public dialogRef: MatDialogRef<OperationTypeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _invoiceService: InvoiceService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title){
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void{
    if (this.data['info'] !== null){
      var temp: OperationTypeDto = this.data['info'] as OperationTypeDto;
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        associatedInvoiceType: [{
          value: temp.associatedinvoicetype,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else{
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        associatedInvoiceType: ['', Validators.required],
        isactive: [true, Validators.required],
      });
    }
  }

  openSnackBar(message: string, action: string){
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          associatedInvoiceType: this.form.value.associatedInvoiceType,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.operationTypeDto.code = info.code;
        this.operationTypeDto.name = info.name;
        this.operationTypeDto.associatedinvoicetype = info.associatedInvoiceType;
        this.operationTypeDto.isactive = info.isactive;
        this._invoiceService.operationtypePut(info.id, this.operationTypeDto).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.operationTypeDto.code = info.code;
        this.operationTypeDto.name = info.name;
        this.operationTypeDto.associatedinvoicetype = info.associatedInvoiceType;
        this.operationTypeDto.isactive = info.isactive;
        this._invoiceService.operationtypePost(this.operationTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.operationTypeDto.id = this.form.value.id;
      this._invoiceService.operationtypeDelete(this.operationTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
