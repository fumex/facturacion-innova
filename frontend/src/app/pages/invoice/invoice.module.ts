import { ReferencedDocumentTypeMaintenanceComponent } from './referenced-document-type/referenced-document-type-maintenance/referenced-document-type-maintenance.component';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { InvoiceService } from '../../services/invoice.service';
import { CountryCodeComponent } from './country-code/country-code.component';
import { AngularMaterialModule } from '../../shared/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CountryCodeMaintenanceComponent } from './country-code/country-code-maintenance/country-code-maintenance.component';
import { ReferencedDocumentTypeComponent } from './referenced-document-type/referenced-document-type.component';
import { OperationTypeComponent } from './operation-type/operation-type.component';
import { OperationTypeMaintenanceComponent } from './operation-type/operation-type-maintenance/operation-type-maintenance.component';
import { DocumentTypeComponent } from './document-type/document-type.component';
import { DocumentTypeMaintenanceComponent } from './document-type/document-type-maintenance/document-type-maintenance.component';
import { CoinTypeComponent } from './coin-type/coin-type.component';
import { CoinTypeMaintenanceComponent } from './coin-type/coin-type-maintenance/coin-type-maintenance.component';
import { TaxConceptComponent } from './tax-concept/tax-concept.component';
import { TaxConceptMaintenanceComponent } from './tax-concept/tax-concept-maintenance/tax-concept-maintenance.component';
import { LegendTypeComponent } from './legend-type/legend-type.component';
import { LegendTypeMaintenanceComponent } from './legend-type/legend-type-maintenance/legend-type-maintenance.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { PaymentMethodMaintenanceComponent } from './payment-method/payment-method-maintenance/payment-method-maintenance.component';
import { SerieComponent } from './serie/serie.component';
import { SerieMaintenanceComponent } from './serie/serie-maintenance/serie-maintenance.component';
import { BoletaComponent } from './boleta/boleta.component';
import { ItemComponent } from './item/item.component';
import { FacturaComponent } from './factura/factura.component';
import { CreditNoteTypeComponent } from './credit-note-type/credit-note-type.component';
import { CreditNoteTypeMaintenanceComponent } from './credit-note-type/credit-note-type-maintenance/credit-note-type-maintenance.component';
import { DebitNoteTypeComponent } from './debit-note-type/debit-note-type.component';
import { DebitNoteTypeMaintenanceComponent } from './debit-note-type/debit-note-type-maintenance/debit-note-type-maintenance.component';


@NgModule({
    declarations: [
        CountryCodeComponent,
        CountryCodeMaintenanceComponent,
        ReferencedDocumentTypeComponent,
        ReferencedDocumentTypeMaintenanceComponent,
        OperationTypeComponent,
        OperationTypeMaintenanceComponent,
        DocumentTypeComponent,
        DocumentTypeMaintenanceComponent,
        CoinTypeComponent,
        CoinTypeMaintenanceComponent,
        TaxConceptComponent,
        TaxConceptMaintenanceComponent,
        LegendTypeComponent,
        LegendTypeMaintenanceComponent,
        PaymentMethodComponent,
        PaymentMethodMaintenanceComponent,
        SerieComponent,
        SerieMaintenanceComponent,
        BoletaComponent,
        ItemComponent,
        FacturaComponent,
        CreditNoteTypeComponent,
        CreditNoteTypeMaintenanceComponent,
        DebitNoteTypeComponent,
        DebitNoteTypeMaintenanceComponent
    ],
    exports: [
        CountryCodeComponent,
        CountryCodeMaintenanceComponent,
        ReferencedDocumentTypeComponent,
        ReferencedDocumentTypeMaintenanceComponent,
        OperationTypeComponent,
        OperationTypeMaintenanceComponent,
        DocumentTypeComponent,
        DocumentTypeMaintenanceComponent,
        CoinTypeComponent,
        CoinTypeMaintenanceComponent,
        TaxConceptComponent,
        TaxConceptMaintenanceComponent,
        LegendTypeComponent,
        LegendTypeMaintenanceComponent,
        PaymentMethodComponent,
        PaymentMethodMaintenanceComponent,
        SerieMaintenanceComponent,
        ItemComponent,
        CreditNoteTypeMaintenanceComponent,
        DebitNoteTypeMaintenanceComponent
    ],
    entryComponents: [
        CountryCodeMaintenanceComponent,
        ReferencedDocumentTypeMaintenanceComponent,
        OperationTypeMaintenanceComponent,
        DocumentTypeMaintenanceComponent,
        CoinTypeMaintenanceComponent,
        TaxConceptMaintenanceComponent,
        LegendTypeMaintenanceComponent,
        PaymentMethodMaintenanceComponent,
        SerieMaintenanceComponent,
        ItemComponent,
        CreditNoteTypeMaintenanceComponent,
        DebitNoteTypeMaintenanceComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        AngularMaterialModule

    ],
    providers: [ CoreProvider, InvoiceService],
})
export class InvoiceModule {}