import { Component, OnInit, Inject } from '@angular/core';
import { InvoiceService, SerieDto, InvoiceDocumentTypeDto } from '../../../../services/invoice.service';
import { Parent } from '../../../../core/class/Parent';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { CoreProvider } from '../../../../core/provider/coreProvider';

@Component({
  selector: 'app-serie-maintenance',
  templateUrl: './serie-maintenance.component.html',
  providers: [CoreProvider, InvoiceService]
})

export class SerieMaintenanceComponent extends Parent implements OnInit {
  item: SerieDto = new SerieDto();
  invoiceTypes: Array<InvoiceDocumentTypeDto>;

  constructor(
    public dialogRef: MatDialogRef<SerieMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private invoiceService: InvoiceService,
    private coreProvider: CoreProvider,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data.operation;
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.getInvoiceType();
  }

  getInvoiceType() {
    this.invoiceService.invoicedocumenttypes().subscribe(
      data => { this.invoiceTypes = data.data; },
      error => { this.coreProvider.showMessageErrorUnexpected(); }
    );
  }

  buildForm(): void {
    if (this.data.info != null) {
      let temp: SerieDto = <SerieDto> this.data.info;
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.seriecode,
          disabled: this.disabledEdit},
          Validators.required],
        number: [{
          value: temp.number,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        typeDocumentCode: [{value: temp.typedocumentcode,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        number: ['00000001', Validators.required],
        name: ['', Validators.required],
        typeDocumentCode: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      let info = {
          id: this.form.value.id,
          code: this.form.value.code,
          number: this.form.value.number,
          name: this.form.value.name,
          typeDocumentCode: this.form.value.typeDocumentCode,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.item.seriecode = info.code;
        this.item.number = info.number;
        this.item.name = info.name;
        this.item.typedocumentcode = info.typeDocumentCode;
        this.item.isactive = info.isactive;
        this.invoiceService.seriePut(info.id, this.item).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.item.seriecode = info.code;
        this.item.number = info.number;
        this.item.name = info.name;
        this.item.typedocumentcode = info.typeDocumentCode;
        this.item.isactive = info.isactive;
        this.invoiceService.seriePost(this.item).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.item.id = this.form.value.id;
      this.invoiceService.serieDelete(this.item.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
