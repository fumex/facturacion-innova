import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreProvider } from '../../../core/provider/coreProvider';
import { InvoiceService, SerieDto } from '../../../services/invoice.service';
import { Parent } from '../../../core/class/Parent';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SerieMaintenanceComponent } from './serie-maintenance/serie-maintenance.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html',
  styles: [],
  providers: [CoreProvider, InvoiceService]
})
export class SerieComponent extends Parent implements OnInit {
  listItem: Array<SerieDto> = [];
  dataSource: MatTableDataSource<SerieDto>;
  displayedColumns: string[] = ['code', 'number', 'name', 'document', 'status', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private invoiceService: InvoiceService,
    private coreProvider: CoreProvider,
    public router: Router,
    public dialog: MatDialog,
  ) { super(); }

  ngOnInit() {
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem(): void {
    this.invoiceService.listSeries().
    subscribe(data => {
      this.listItem =  data.data as Array<SerieDto>;
      console.log(data.data);
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  addItem() {
    let dialogRef = this.dialog.open(SerieMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  viewItem(item){
    this.dialog.open(SerieMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationView,
        info: item
      }
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(SerieMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(SerieMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }

}
