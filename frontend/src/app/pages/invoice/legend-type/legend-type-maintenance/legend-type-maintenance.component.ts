import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { LegendTypeDto } from './../../../../services/invoice.service';
import { Parent } from 'src/app/core/class/Parent';
import { InvoiceService } from 'src/app/services/invoice.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-legend-type-maintenance',
  templateUrl: './legend-type-maintenance.component.html',
  styles: [],
  providers: [InvoiceService]
})
export class LegendTypeMaintenanceComponent extends Parent implements OnInit {

  legendTypeDto: LegendTypeDto = new LegendTypeDto();

  constructor(
    public dialogRef: MatDialogRef<LegendTypeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _invoiceService: InvoiceService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] !== null) {
      var temp: LegendTypeDto = <LegendTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.legendTypeDto.code = info.code;
        this.legendTypeDto.name = info.name;
        this.legendTypeDto.isactive = info.isactive;
        this._invoiceService.legendtypePut(info.id, this.legendTypeDto).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.legendTypeDto.code = info.code;
        this.legendTypeDto.name = info.name;
        this.legendTypeDto.isactive = info.isactive;
        this._invoiceService.legendtypePost(this.legendTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.legendTypeDto .id = this.form.value.id;
      this._invoiceService.legendtypeDelete(this.legendTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
