import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Parent } from '../../../core/class/Parent';
import { CoreProvider } from '../../../core/provider/coreProvider';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductService, ProductDto } from '../../../services/product.service';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styles: [],
  providers: [CoreProvider, ProductService]
})
export class ItemComponent extends Parent implements OnInit {

  listItem: Array<any> = [];
  dataSource = new MatTableDataSource<any>(this.listItem);
  selection = new SelectionModel<ProductDto>(true, []);
  displayedColumns: string[] = ['code', 'name', 'saleprice', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public dialogRef: MatDialogRef<ItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private coreProvider: CoreProvider,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.getItem();
    this.buildForm();
  }

  getItem(): void {
    this.productService.listProductsFull().
    subscribe(data => {
      this.listItem =  data.data as Array<any>;
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  buildForm() {
    this.form = this.formBuilder.group({
      quantity: [1, [Validators.required, Validators.pattern('[0-9]*$')]]
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    this.dialogRef.close(this.selection.selected);
    console.log(this.selection.selected);
  }

  addItem(element) {
    var info = {
      product: element,
      quantity: this.form.value.quantity,
    }
    this.dialogRef.close(info);
    console.log(info);
  }
  // ***************************************************************
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: ProductDto): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
  // ***************************************************************
}
