import { Component, OnInit, Inject } from '@angular/core';
import { CoreProvider } from '../../../core/provider/coreProvider';
import { PersonService, PhoneDto, TelephoneTypeDto } from '../../../services/person.service';
import { Parent } from '../../../core/class/Parent';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html',
  providers: [CoreProvider, PersonService]
})
export class PhoneComponent extends Parent implements OnInit {
  phoneDto: PhoneDto = new PhoneDto();
  phoneDto2: any = '';
  phonetypes: Array<TelephoneTypeDto>;
  constructor(
    public dialogRef: MatDialogRef<PhoneComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private personService: PersonService,
    private _snackBar: MatSnackBar,
    private coreProvider: CoreProvider
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.getTelephoneTypes();
  }

  buildForm() {
    if (this.data['info'] != null) {
      var temp: PhoneDto = <PhoneDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        number: [{value: temp.number, disabled: this.disabledEdit},
          Validators.required],
        extension: [{value: temp.extension, disabled: this.disabledEdit}],
        telephoneTypeCode: [{value: temp.telephonetypecode, disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        number: ['', Validators.required],
        extension: [''],
        telephoneTypeCode: ['T01', Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  getTelephoneTypes() {
    this.personService.telephonetypes().subscribe(
      data => { this.phonetypes = data.data; },
      error => { this.coreProvider.showMessageErrorUnexpected(); }
    );
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          number: this.form.value.number,
          extension: this.form.value.extension,
          telephoneTypeCode: this.form.value.telephoneTypeCode,
      };
      if (this.title === this.operationUpdate) {
        this.phoneDto.number = info.number,
        this.phoneDto.extension = info.extension,
        this.phoneDto.telephonetypecode = info.telephoneTypeCode,
        this.personService.phonePut(info.id, this.phoneDto).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.phoneDto.number = info.number,
        this.phoneDto.extension = info.extension,
        this.phoneDto.telephonetypecode = info.telephoneTypeCode,
        this.personService.phonePost(this.phoneDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            info.id = data.data.id;
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
        });
      }
      this.dialogRef.close(info);
    }
    if (this.title === this.operationDelete) {
      this.phoneDto.id = this.form.value.id;
      this.personService.phoneDelete(this.phoneDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }
  // tarea termianda
}
