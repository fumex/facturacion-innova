import { Component, OnInit, Inject } from '@angular/core';
import { CoreProvider } from '../../../../core/provider/coreProvider';
import { PersonService} from '../../../../services/service.index';
import { Parent } from 'src/app/core/class/Parent';
import { TaxpayerTypeDto } from '../../../../services/person.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-type-taxpayer-maintenance',
  templateUrl: './type-taxpayer-maintenance.component.html',
  styles: [],
  providers: [CoreProvider]
})
export class TypeTaxpayerMaintenanceComponent extends Parent implements OnInit {
  typeTaxpayerDto: TaxpayerTypeDto = new TaxpayerTypeDto();
  constructor(
    public dialogRef: MatDialogRef<TypeTaxpayerMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private PersonService: PersonService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: TaxpayerTypeDto = <TaxpayerTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.typeTaxpayerDto.code = info.code;
        this.typeTaxpayerDto.name = info.name;
        this.typeTaxpayerDto.isactive = info.isactive;
        this.PersonService.taxpayertypePut(info.id, this.typeTaxpayerDto).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.typeTaxpayerDto.code = info.code;
        this.typeTaxpayerDto.name = info.name;
        this.typeTaxpayerDto.isactive = info.isactive;
        this.PersonService.taxpayertypePost(this.typeTaxpayerDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.typeTaxpayerDto.id = this.form.value.id;
      this.PersonService.taxpayertypeDelete(this.typeTaxpayerDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }
}
