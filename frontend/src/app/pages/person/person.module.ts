import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EconomicActivityComponent } from './economic-activity/economic-activity.component';
import { RucStateComponent } from './ruc-state/ruc-state.component';
import { PatternComponent } from './pattern/pattern.component';
import { TypeTaxpayerComponent } from './type-taxpayer/type-taxpayer.component';
import { TypeDocumentIdentityComponent } from './type-document-identity/type-document-identity.component';
import { TaxRegimeComponent } from './tax-regime/tax-regime.component';
import { EconomicActivityMaintenanceComponent } from './economic-activity/economic-activity-maintenance/economic-activity-maintenance.component';
import { PatternMaintenanceComponent } from './pattern/pattern-maintenance/pattern-maintenance.component';
import { RucStateMaintenanceComponent } from './ruc-state/ruc-state-maintenance/ruc-state-maintenance.component';
import { TaxRegimeMaintenanceComponent } from './tax-regime/tax-regime-maintenance/tax-regime-maintenance.component';
import { TypeDocumentIdentityMaintenanceComponent } from './type-document-identity/type-document-identity-maintenance/type-document-identity-maintenance.component';
import { TypeTaxpayerMaintenanceComponent } from './type-taxpayer/type-taxpayer-maintenance/type-taxpayer-maintenance.component';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/shared/angular-material.module';
import { CompanyComponent } from './company/company.component';
import { PersonService } from 'src/app/services/service.index';
import { FiscalAddressConditionComponent } from './fiscal-address-condition/fiscal-address-condition.component';
import { FiscalAddressConditionMaintenanceComponent } from './fiscal-address-condition/fiscal-address-condition-maintenance/fiscal-address-condition-maintenance.component';
import { TelephoneTypeComponent } from './telephone-type/telephone-type.component';
import { TelephoneTypeMaintenanceComponent } from './telephone-type/telephone-type-maintenance/telephone-type-maintenance.component';
import { PhoneComponent } from './phone/phone.component';
import { ClientComponent } from './client/client.component';
import { ClientMaintenanceComponent } from './client/client-maintenance/client-maintenance.component';
import { ProviderComponent } from './provider/provider.component';
import { ProvidertMaintenanceComponent } from './provider/providert-maintenance/providert-maintenance.component';



@NgModule({
  declarations: [
    EconomicActivityComponent,
    RucStateComponent,
    PatternComponent,
    TypeTaxpayerComponent,
    TypeDocumentIdentityComponent,
    TaxRegimeComponent,
    EconomicActivityMaintenanceComponent,
    PatternMaintenanceComponent,
    RucStateMaintenanceComponent,
    TaxRegimeMaintenanceComponent,
    TypeDocumentIdentityMaintenanceComponent,
    TypeTaxpayerMaintenanceComponent,
    CompanyComponent,
    FiscalAddressConditionComponent,
    FiscalAddressConditionMaintenanceComponent,
    TelephoneTypeComponent,
    TelephoneTypeMaintenanceComponent,
    PhoneComponent,
    ClientComponent,
    ClientMaintenanceComponent,
    ProviderComponent,
    ProvidertMaintenanceComponent
  ],
  exports: [
    EconomicActivityMaintenanceComponent,
    PatternMaintenanceComponent,
    RucStateMaintenanceComponent,
    TaxRegimeMaintenanceComponent,
    TypeDocumentIdentityMaintenanceComponent,
    TypeTaxpayerMaintenanceComponent,
    FiscalAddressConditionMaintenanceComponent,
    TelephoneTypeMaintenanceComponent,
    PhoneComponent,
    ClientMaintenanceComponent,
    ProvidertMaintenanceComponent
  ],
  entryComponents: [
    EconomicActivityMaintenanceComponent,
    PatternMaintenanceComponent,
    RucStateMaintenanceComponent,
    TaxRegimeMaintenanceComponent,
    TypeDocumentIdentityMaintenanceComponent,
    TypeTaxpayerMaintenanceComponent,
    FiscalAddressConditionMaintenanceComponent,
    TelephoneTypeMaintenanceComponent,
    PhoneComponent,
    ClientMaintenanceComponent,
    ProvidertMaintenanceComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    AngularMaterialModule
  ],
  providers: [PersonService]
})
export class PersonModule { }
