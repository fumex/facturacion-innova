import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { PersonService } from 'src/app/services/service.index';
import { Parent } from 'src/app/core/class/Parent';
import { TelephoneTypeDto } from 'src/app/services/person.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TelephoneTypeMaintenanceComponent } from './telephone-type-maintenance/telephone-type-maintenance.component';

@Component({
  selector: 'app-telephone-type',
  templateUrl: './telephone-type.component.html',
  styles: [],
  providers: [CoreProvider, PersonService]
})
export class TelephoneTypeComponent extends Parent implements OnInit {

  listItem: Array<TelephoneTypeDto> = [];
  dataSource: MatTableDataSource<TelephoneTypeDto>;
  displayedColumns: string[] = ['code', 'name', 'status', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  target: DataTransfer = new DataTransfer();

  constructor(
    private formBuilder: FormBuilder,
    private personService: PersonService,
    private coreProvider: CoreProvider,
    private router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.getItem();
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem(): void {
    this.personService.listTelephoneTypes().
    subscribe(data => {
      this.listItem =  data.data as Array<TelephoneTypeDto>;
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, () => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  addItem() {
    let dialogRef = this.dialog.open(TelephoneTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(TelephoneTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(TelephoneTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }
}
