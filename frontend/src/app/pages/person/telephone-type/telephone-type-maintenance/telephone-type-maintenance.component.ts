import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

import { PersonService } from '../../../../services/service.index';
import { TelephoneTypeDto } from 'src/app/services/person.service';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { Parent } from 'src/app/core/class/Parent';

@Component({
  selector: 'app-telephone-type-maintenance',
  templateUrl: './telephone-type-maintenance.component.html',
  styles: [],
  providers: [CoreProvider]
})
export class TelephoneTypeMaintenanceComponent extends Parent implements OnInit {

  telephoneTypeDto: TelephoneTypeDto =  new TelephoneTypeDto();
  constructor(
    public dialogRef: MatDialogRef<TelephoneTypeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _personService: PersonService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.newCode();
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: TelephoneTypeDto = <TelephoneTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.telephoneTypeDto.code = info.code;
        this.telephoneTypeDto.name = info.name;
        this.telephoneTypeDto.isactive = info.isactive;
        this._personService.telephonetypePut(info.id, this.telephoneTypeDto).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
          }, error => console.log(error));
      } else {
        delete info.id;
        this.telephoneTypeDto.code = info.code;
        this.telephoneTypeDto.name = info.name;
        this.telephoneTypeDto.isactive = info.isactive;
        this._personService.telephonetypePost(this.telephoneTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }
    if (this.title === this.operationDelete) {
      this.telephoneTypeDto.id = this.form.value.id;
      this._personService.telephonetypeDelete(this.telephoneTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    } 
  }

  newCode() {
    this._personService.listTelephoneTypes().subscribe( data => {
      if (data.data !== null) {
        let length = data.data.length;
        console.log(data.data.length);
        if ( (length === 0) || (length < 9)) {
          this.form.controls.code.setValue('000' + (length + 1));
        } else {
          this.form.controls.code.setValue('00' + (length + 1));
        }
      } else { this.coreProvider.showMessageError(data.error.message); }},
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

}
