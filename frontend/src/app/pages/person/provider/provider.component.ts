import { Component, OnInit, ViewChild } from '@angular/core';
import { Parent } from '../../../core/class/Parent';
import { ProvidertMaintenanceComponent } from './providert-maintenance/providert-maintenance.component';
import { ProviderDto, PersonService } from 'src/app/services/person.service';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styles: [],
  providers: [CoreProvider, PersonService]
})
export class ProviderComponent extends Parent  implements OnInit {

  listItem: Array<ProviderDto> = [];
  dataSource: MatTableDataSource<ProviderDto>;
  displayedColumns: string[] = ['document', 'name', 'type', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  target: DataTransfer = new DataTransfer();

  constructor(
    private personService: PersonService,
    private coreProvider: CoreProvider,
    private router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem(): void {
    this.personService.listProviders().
    subscribe(data => {
      this.listItem =  data.data as Array<ProviderDto>;
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, () => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  addItem() {
    let dialogRef = this.dialog.open(ProvidertMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(ProvidertMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(ProvidertMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  viewItem(item) {
    this.dialog.open(ProvidertMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: { 
        operation: this.operationView,
        info: item
      }
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }
}
