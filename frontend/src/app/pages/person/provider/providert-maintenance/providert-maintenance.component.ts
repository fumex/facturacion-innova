import { Component, OnInit, Inject } from '@angular/core';
import { Parent } from '../../../../core/class/Parent';
import {  ProviderDto, PersonService, DocumentIdentityTypeDto, TaxpayerTypeDto,
          PhoneDto, PhonePersonDto } from '../../../../services/person.service';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { AddressService } from 'src/app/services/service.index';
import { AddressDto, AddressPersonDto } from 'src/app/services/address.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ClientMaintenanceComponent } from '../../client/client-maintenance/client-maintenance.component';
import { FormBuilder, Validators } from '@angular/forms';
import { AddressComponent } from 'src/app/pages/address/address.component';
import { PhoneComponent } from '../../phone/phone.component';

@Component({
  selector: 'app-providert-maintenance',
  templateUrl: './providert-maintenance.component.html',
  styles: [],
  providers: [CoreProvider, PersonService, AddressService]
})
export class ProvidertMaintenanceComponent extends Parent implements OnInit {
  documents: Array<DocumentIdentityTypeDto>;
  taxpayers: Array<TaxpayerTypeDto>;
  telephones: Array<PhoneDto> = [];
  addresses: Array<AddressDto> = [];
  addressPerson: AddressPersonDto = new AddressPersonDto();
  phonePerson: PhonePersonDto = new PhonePersonDto();
  providerCompany: boolean = false;
  provider: ProviderDto = new ProviderDto();

  constructor(
    public dialogRef: MatDialogRef<ClientMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private addressService: AddressService,
    private personService: PersonService,
    private dialog: MatDialog
  ) { super(); }

  ngOnInit() {
    this.title = this.data.operation;
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.getTypeDocuments();
    this.getTaxPayers();
  }

  buildForm(): void {
    if (this.data.info != null) {
      let temp: ProviderDto = <ProviderDto> this.data.info;
      const [lastname1, lastname2, names] = temp.name.split('-');
      this.form = this.formBuilder.group({
        id: [temp.id],
        documentidcode: [{
          value: temp.documentidcode,
          disabled: this.disabledEdit},
          Validators.required],
        documentidnumber: [{
          value: temp.documentidnumber,
          disabled: this.disabledEdit},
          Validators.required],
        razon: [{
            value: temp.name,
            disabled: this.disabledEdit}],
        lastname1: [{
          value: lastname1,
          disabled: this.disabledEdit}],
        lastname2: [{
          value: lastname2,
          disabled: this.disabledEdit}],
        names: [{
          value: names,
          disabled: this.disabledEdit}],
        tradename: [{
          value: temp.tradename,
          disabled: this.disabledEdit}],
        foreigntradeactivityflag: [{
          value: temp.foreigntradeactivityflag,
          disabled: this.disabledEdit}, Validators.required],
        taxpayertypecode: [{
          value: temp.taxpayertypecode,
          disabled: this.disabledEdit},
          Validators.required],
        providertype: [{
          value: temp.providertype,
          disabled: this.disabledEdit},
          Validators.required],
      });
      this.getName(temp.documentidcode);
      this.personService.findPhonesPerson(temp.id).subscribe(
        data => {
          console.log(data);
          this.telephones = data.data;
        }, error => console.log('No hay telefonos', error)
      );
      this.addressService.findAddressPerson(temp.id).subscribe(
        data => {
          console.log(data);
          this.addresses = data.data;
        }, error => console.log('No hay telefonos', error)
      );
    } else {
      this.form = this.formBuilder.group({
        id: 0,
        documentidcode: ['1', Validators.required],
        documentidnumber: ['', [Validators.required, Validators.pattern('[0-9]*$')]],
        razon: [''],
        names: [''],
        lastname1: [''],
        lastname2: [''],
        tradename: [''],
        foreigntradeactivityflag: [false, Validators.required],
        taxpayertypecode: ['1', Validators.required],
        providertype: ['P', Validators.required]
      });
    }
  }

  changeName(): void {
    if (this.form.value.documentidcode === '6') {
      this.providerCompany = true;
      this.form.controls.providertype.setValue('C');
      this.form.controls.taxpayertypecode.setValue('2');
      console.log('Persona Juridica');
    } else {
        this.providerCompany = false;
        this.form.controls.providertype.setValue('P');
        this.form.controls.taxpayertypecode.setValue('1');
        console.log('Persona Natural');
      }
  }

  getTypeDocuments(): void {
    this.personService.documentidentitytypes().
    subscribe(data => {
      this.documents = data.data;
    });
  }

  getTaxPayers(): void {
    this.personService.taxpayertypes().
    subscribe(data => {
      this.taxpayers = data.data as Array<TaxpayerTypeDto>;
    });
  }

  setAddress(): void {
    const dialogRef = this.dialog.open(AddressComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data.roadName !== undefined) {
        this.addresses.push(data as AddressDto);
      }
    });
  }

  deleteAddress(element, i) {
    const dialogRef = this.dialog.open(AddressComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== true) {
      this.addresses.splice(i, 1); }
    });
  }

  setPhone(): void {
    const dialogRef = this.dialog.open(PhoneComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data.number !== undefined) {
        this.telephones.push(data as PhoneDto);
      }
    });
  }

  deletePhone(element, i) {
    const dialogRef = this.dialog.open(PhoneComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== true) {
        this.telephones.splice(i, 1);
      }
    });
  }

  addAddressPerson(id) {
    this.addressPerson.personId = id;
    this.addresses.forEach(
      data => {
        this.addressPerson.addressId = data.id;
        this.addressService.addresspersonPost(this.addressPerson).subscribe(
          e => {
            console.log('Guardando direccion de Empresa');
            console.log(e);
          }, error => console.log(error));
      },
      error => console.log(error)
    );
  }

  addPhonePerson(id) {
    this.phonePerson.personid = id;
    this.telephones.forEach(
      data => {
        this.phonePerson.phoneid = data.id;
        this.personService.phonepersonPost(this.phonePerson).subscribe(
          e => {
            console.log('Guardando telefono de Empresa');
            console.log(e);
          }, error => console.log(error));
      },
      error => console.log(error)
    );
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      let info = {
        id: this.form.value.id,
        documentidcode: this.form.value.documentidcode,
        documentidnumber: this.form.value.documentidnumber,
        name: this.setName(this.form.value.documentidcode),
        tradename: this.form.value.tradename,
        foreigntradeactivityflag: this.form.value.foreigntradeactivityflag,
        taxpayertypecode: this.form.value.taxpayertypecode,
        providertype: this.form.value.providertype
      };
      console.log(info);
      if (this.title === this.operationUpdate) {
        this.provider.documentidcode = info.documentidcode,
        this.provider.documentidnumber = info.documentidnumber,
        this.provider.foreigntradeactivityflag = info.foreigntradeactivityflag,
        this.provider.name = info.name,
        this.provider.taxpayertypecode = info.taxpayertypecode,
        this.provider.tradename = info.tradename,
        this.provider.providertype = info.providertype,
        this.personService.providerPut(info.id, this.provider).subscribe(e => {
          /* this.addAddressPerson(e.data.id);
          this.addPhonePerson(e.data.id); */
          console.log('Actualizado', 'OK');
        }, error => console.log(error));
        } else {
        delete info.id;
        this.provider.id = this.form.value.id,
        this.provider.documentidcode = info.documentidcode,
        this.provider.documentidnumber = info.documentidnumber,
        this.provider.foreigntradeactivityflag = info.foreigntradeactivityflag,
        this.provider.name = info.name,
        this.provider.taxpayertypecode = info.taxpayertypecode,
        this.provider.tradename = info.tradename,
        this.provider.providertype = info.providertype,
        this.personService.providerPost(this.provider).subscribe(
          data => {
            console.log('Guardado', 'OK');
            this.addAddressPerson(data.data.id);
            this.addPhonePerson(data.data.id);
          },
          error => {
            console.log(error);
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.provider.id = this.form.value.id;
      this.personService.providerDelete(this.provider.id).subscribe(e => {
        console.log('Elimando', 'OK');
        // this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  setName(code): string {
    if (code === '6') {
      return this.form.value.razon;
    } else {
      return this.form.value.lastname1 + '-' + this.form.value.lastname2 + '-' + this.form.value.names;
    }
  }

  getName(type) {
    if (type === '6') {
      this.providerCompany = true;
      this.form.controls.lastname1.reset();
      this.form.controls.lastname2.reset();
      this.form.controls.names.reset();
    } else {
      this.providerCompany = false;
      this.form.controls.razon.reset();
    }
  }

}
