import { Component, OnInit, Inject } from '@angular/core';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { PersonService} from '../../../../services/service.index';
import { Parent } from '../../../../core/class/Parent';
import { PadronDto } from '../../../../services/person.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-pattern-maintenance',
  templateUrl: './pattern-maintenance.component.html',
  styles: [],
  providers: [CoreProvider]
})
export class PatternMaintenanceComponent extends Parent implements OnInit {
  patternDto: PadronDto = new PadronDto();

  constructor(
    public dialogRef: MatDialogRef<PatternMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private patternService: PersonService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.newCode();
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: PadronDto = this.data.info as PadronDto;
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.patternDto.code = info.code;
        this.patternDto.name = info.name;
        this.patternDto.isactive = info.isactive;
        this.patternService.padronPut(info.id, this.patternDto)  .subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.patternDto.code = info.code;
        this.patternDto.name = info.name;
        this.patternDto.isactive = info.isactive;
        this.patternService.padronPost(this.patternDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {

      this.patternDto.id = this.form.value.id;
      this.patternService.padronDelete(this.patternDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  newCode() {
    this.patternService.listPadrones().subscribe( data => {
      if (data.data !== null) {
        let length = data.data.length;
        console.log(data.data.length);
        if ( (length === 0) || (length < 9)) {
          this.form.controls.code.setValue('PDR0' + (length + 1));
        } else {
          this.form.controls.code.setValue('PDR' + (length + 1));
        }
      } else { this.coreProvider.showMessageError(data.error.message); }},
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

}
