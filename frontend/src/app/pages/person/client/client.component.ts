import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreProvider } from '../../../core/provider/coreProvider';
import { PersonService, ClientDto } from '../../../services/person.service';
import { Parent } from '../../../core/class/Parent';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ClientMaintenanceComponent } from './client-maintenance/client-maintenance.component';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styles: [],
  providers: [CoreProvider, PersonService]
})
export class ClientComponent extends Parent implements OnInit {

  listItem: Array<ClientDto> = [];
  dataSource: MatTableDataSource<ClientDto>;
  displayedColumns: string[] = ['document', 'name', 'type', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  target: DataTransfer = new DataTransfer();

  constructor(
    private formBuilder: FormBuilder,
    private personService: PersonService,
    private coreProvider: CoreProvider,
    private router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem(): void {
    this.personService.listClients().
    subscribe(data => {
      this.listItem =  data.data as Array<ClientDto>;
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, () => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  addItem() {
    let dialogRef = this.dialog.open(ClientMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log(this.getItem());
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(ClientMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(ClientMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  viewItem(item) {
    this.dialog.open(ClientMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationView,
        info: item
      }
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }
}
