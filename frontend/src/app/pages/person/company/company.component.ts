import { Component, OnInit } from '@angular/core';
import { Parent } from '../../../core/class/Parent';
import { FormBuilder, Validators } from '@angular/forms';
import { CoreProvider } from '../../../core/provider/coreProvider';
import { AddressService, PersonService } from '../../../services/service.index';
import { DocumentIdentityTypeDto,
         EconomicActivityDto,
         TaxRegimeDto,
         PadronDto,
         TaxpayerTypeDto,
         RucStateDto,
         CompanyDto} from '../../../services/person.service';
import { MatTableDataSource, MatDialog, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import {
          EconomicActivityMaintenanceComponent
       } from '../economic-activity/economic-activity-maintenance/economic-activity-maintenance.component';
import { AddressComponent } from '../../address/address.component';
import { PatternMaintenanceComponent } from '../pattern/pattern-maintenance/pattern-maintenance.component';
import { PhoneComponent } from '../phone/phone.component';
import { FiscalAddressConditionTypeDto, PhoneDto, PhonePersonDto,
  EconomicActivityCompanyDto, PadronPersonDto } from '../../../services/person.service';
import { AddressDto, AddressPersonDto } from '../../../services/address.service';
import { AppDateAdapter, APP_DATE_FORMATS } from '../../../core/class/format-datepicker';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css'],
  providers: [CoreProvider, AddressService, PersonService, {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}]
})

export class CompanyComponent extends Parent implements OnInit {

  addressPersons: Array<AddressPersonDto> = [];
  addressPerson: AddressPersonDto = new AddressPersonDto();
  phonePerson: PhonePersonDto = new PhonePersonDto();
  activityPerson: EconomicActivityCompanyDto = new EconomicActivityCompanyDto();
  padronPerson: PadronPersonDto = new PadronPersonDto();

  economicActivities: Array<EconomicActivityDto>;
  conditions: Array<FiscalAddressConditionTypeDto>;
  documents: Array<DocumentIdentityTypeDto>;
  patterns: Array<PadronDto>;
  states: Array<RucStateDto>;
  taxregimes: Array<TaxRegimeDto>;
  taxpayers: Array<TaxpayerTypeDto>;

  telephones: Array<PhoneDto> = [];
  addresses: Array<AddressDto> = [];
  activities: Array<EconomicActivityDto>;

  isLinear = false;

  displayColumns: string[] = ['select', 'name'];
  dataSource = new MatTableDataSource<PadronDto>(this.patterns);
  selection = new SelectionModel<PadronDto>(true, []);

  companyDto: CompanyDto = new CompanyDto();

  constructor(
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private adressService: AddressService,
    private personService: PersonService,
    private dialog: MatDialog
  ) { super(); }

  ngOnInit() {
    this.title = localStorage.getItem('operation');
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }

    this.buildForm();
    this.getTypeDocuments();
    this.getEconomicActivities();
    this.getTaxRegimes();
    this.getPatterns();
    this.getTaxPayers();
    this.getRUCStates();
    this.getFicalAddress();
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      id: 0,
      activitystartdate: [new Date(), Validators.required],
      documentidcode: ['6',  Validators.required],
      documentidnumber: ['', [Validators.required, Validators.pattern('[0-9]*$')]],
      electronictransmitterdate: [new Date(), Validators.required],
      fiscaladdressconditionstypecode: ['CD1', Validators.required],
      foreigntradeactivityflag: [false, Validators.required],
      name: ['Empresa 1', Validators.required],
      rucstatuscode: ['ER1', Validators.required],
      taxregimecode: ['RT2', Validators.required],
      taxpayertypecode: ['39', Validators.required],
      tradename: [''],
      activityEconomic: ['', Validators.required],
    });
  }

  /* buildForm(): void {
    let temp: CompanyDto;
    if (this.title !== this.operationNew) {
      temp = <CompanyDto>JSON.parse(localStorage.getItem("company"));
    } else {
      temp = new CompanyDto();
      temp.id = '';
      temp.typeDocumentIdentity = '6';
      temp.startDateActivities = new Date();
      temp.electronicTransmitter = new Date();
      temp.foreignTradeActivity = 'SN';
    }
    this.form = this.formBuilder.group({
      // idEntry: [temp.id],
      typeDocumentIdentity: [{value: temp.typeDocumentIdentity, disabled: true}, Validators.required],
      numberIdentity: [{value: temp.numberIdentity, disabled: this.disabledEdit}, Validators.required],
      name: [{value: temp.name, disabled: this.disabledEdit}, Validators.required],
      tradename: [{value: temp.tradename, disabled: this.disabledEdit}],
      address: [{value: temp.address, disabled: true}, Validators.required],
      addressAnnexedEstablishments: [{value: temp.addressAnnexedEstablishments, disabled: this.disabledEdit}],
      activityEconomic: [{value: temp.activityEconomic, disabled: this.disabledEdit}, Validators.required],
      foreignTradeActivity: [{value: temp.foreignTradeActivity, disabled: this.disabledEdit}, Validators.required],
      startDateActivities: [{value: temp.startDateActivities, disabled: this.disabledEdit}, Validators.required],
      electronicTransmitter: [{value: temp.electronicTransmitter, disabled: this.disabledEdit}, Validators.required],
      phone: [{value: temp.phone, disabled: this.disabledEdit}],
      mobilePhone: [{value: temp.mobilePhone, disabled: this.disabledEdit}],
      patterns: [{value: temp.patterns, disabled: this.disabledEdit}],
      regimeTax: [{value: temp.regimeTax, disabled: this.disabledEdit}, Validators.required],
      typeContributor: [{value: temp.typeContributor, disabled: this.disabledEdit}, Validators.required],
      rucStatus: [{value: temp.rucStatus, disabled: this.disabledEdit}, Validators.required],
      fiscalAddressConditions: [{value: temp.fiscalAddressConditions, disabled: this.disabledEdit}, Validators.required],
    });
  } */

  /* validateList() {
    if (this.addresses !== null || this.telephones !== null) {
      console.log(this.selection.isEmpty());
      this.form.controls.list.setValue(true);
    }
  } */

  getTypeDocuments(): void {
    this.personService.documentidentitytypes().
    subscribe(data => {
      this.documents = data.data;
    });
  }

  getFicalAddress(): void {
    this.personService.fiscaladdressconditiontypes().
    subscribe(data => {
      this.conditions = data.data;
    });
  }

  getEconomicActivities(): void {
    this.personService.economicactivities().
    subscribe(data => {
      this.economicActivities = data.data as Array<EconomicActivityDto>;
      this.economicActivities.sort(function(a, b) {
        var nameA = a.code.toLowerCase(), nameB = b.code.toLowerCase();
        if (nameA < nameB)
            return -1;
        if (nameA > nameB)
            return 1;
        return 0;
      });
    });
  }

  addActivity(): void {
    let dialogRef = this.dialog.open(EconomicActivityMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getEconomicActivities();
    });
  }

  listEconomicActivities() {
    this.activities = this.form.value.activityEconomic;
  }

  getTaxRegimes(): void {
    this.personService.taxregimes().
    subscribe(data => {
      this.taxregimes = data.data as Array<TaxRegimeDto>;
      this.taxregimes.sort(function(a, b) {
        var nameA = a.code.toLowerCase(), nameB = b.code.toLowerCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
    });
  }

  getPatterns(): void {
    this.personService.padrones().
    subscribe(data => {
      this.patterns = data.data as Array<PadronDto>;
      this.dataSource.data = this.patterns;
    });
  }

  addPatterns(): void {
    let dialogRef = this.dialog.open(PatternMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getPatterns();
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: PadronDto): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  getTaxPayers(): void {
    this.personService.taxpayertypes().
    subscribe(data => {
      this.taxpayers = data.data as Array<TaxpayerTypeDto>;
    });
  }

  getRUCStates(): void {
    this.personService.rucstates().
    subscribe(data => {
      this.states = data.data;
    });
  }

  setAddress(): void {
    let dialogRef = this.dialog.open(AddressComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data.roadName !== undefined) {
        this.addresses.push(data as AddressDto);
      }
    });
  }

  deleteAddress(element, i) {
    let dialogRef = this.dialog.open(AddressComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.addresses.splice(i, 1);
    });
  }

  save() {
    this.companyDto.activitystartdate = this.form.value.activitystartdate,
    this.companyDto.documentidcode = this.form.value.documentidcode,
    this.companyDto.documentidnumber = this.form.value.documentidnumber,
    this.companyDto.electronictransmitterdate = this.form.value.electronictransmitterdate,
    this.companyDto.fiscaladdressconditionstypecode = this.form.value.fiscaladdressconditionstypecode,
    this.companyDto.foreigntradeactivityflag = this.form.value.foreigntradeactivityflag,
    this.companyDto.name = this.form.value.name,
    this.companyDto.rucstatuscode = this.form.value.rucstatuscode,
    this.companyDto.taxregimecode = this.form.value.taxregimecode,
    this.companyDto.taxpayertypecode = this.form.value.taxpayertypecode,
    this.companyDto.tradename = this.form.value.tradename,
    this.personService.companyPost(this.companyDto).subscribe(
      data => {
        console.log('Guardado', 'OK');
        console.log(data.data);
        this.addAddressPerson(data.data.id);
        this.addPhonePerson(data.data.id);
        this.addActivityPerson(data.data.id);
        this.addPadronPerson(data.data.id);
        // this.clean();
      },
      error => {
        console.log(error);
      });

/*     if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
        id: this.form.value.id,
        activitystartdate: this.form.value.activitystartdate,
        documentidcode: this.form.value.documentidcode,
        documentidnumber: this.form.value.documentidnumber,
        electronictransmitterdate: this.form.value.electronictransmitterdate,
        fiscaladdressconditionstypecode: this.form.value.fiscaladdressconditionstypecode,
        foreigntradeactivityflag: this.form.value.foreigntradeactivityflag,
        name: this.form.value.name,
        rucstatuscode: this.form.value.rucstatuscode,
        taxregimecode: this.form.value.taxregimecode,
        taxpayertypecode: this.form.value.taxpayertypecode,
        tradename: this.form.value.tradename,
      };
      console.log(info);
      if (this.title === this.operationUpdate) {
        this.companyDto.activitystartdate = info.activitystartdate,
        this.companyDto.documentidcode = info.documentidcode,
        this.companyDto.documentidnumber = info.documentidnumber,
        this.companyDto.electronictransmitterdate = info.electronictransmitterdate,
        this.companyDto.fiscaladdressconditionstypecode = info.fiscaladdressconditionstypecode,
        this.companyDto.foreigntradeactivityflag = info.foreigntradeactivityflag,
        this.companyDto.name = info.name,
        this.companyDto.rucstatuscode = info.rucstatuscode,
        this.companyDto.taxregimecode = info.taxregimecode,
        this.companyDto.taxpayertypecode = info.taxpayertypecode,
        this.companyDto.tradename = info.tradename,
        /* this.companyDto.address = info.address;
        this.companyDto.activityEconomic = info.activityEconomic;
        this.companyDto.phone = info.phone;
        this.companyDto.patterns = info.patterns;
        this.personService.companyPut(this.companyDto, info.id).subscribe(e => {
          console.log('Actualizado', 'OK');
        }, error => console.log(error));
        } else {
        delete info.id;
        this.companyDto.id = this.form.value.id,
        this.companyDto.activitystartdate = this.form.value.activitystartdate,
        this.companyDto.documentidcode = this.form.value.documentidcode,
        this.companyDto.documentidnumber = this.form.value.documentidnumber,
        this.companyDto.electronictransmitterdate = this.form.value.electronicTransmitter,
        this.companyDto.fiscaladdressconditionstypecode = this.form.value.fiscaladdressconditionstypecode,
        this.companyDto.foreigntradeactivityflag = this.form.value.foreigntradeactivityflag,
        this.companyDto.name = this.form.value.name,
        this.companyDto.rucstatuscode = this.form.value.rucstatuscode,
        this.companyDto.taxregimecode = this.form.value.taxregimecode,
        this.companyDto.taxpayertypecode = this.form.value.taxpayertypecode,
        this.companyDto.tradename = this.form.value.tradename,

        this.personService.companyPost(this.companyDto).subscribe(
          data => {
            console.log('Guardado', 'OK');
          },
          error => {
            console.log(error);
          });
        }
      }
    if (this.title === this.operationDelete) {
      this.companyDto.id = this.form.value.id;
      this.personService.companyDelete(this.companyDto.id).subscribe(e => {
        console.log('Guardado', 'OK');
        // this.openSnackBar('Eliminado', 'OK');
      });
    } */
  }

  clean() {
    this.buildForm();
    this.activities = [];
    this.telephones = [];
    this.addresses = [];
    this.selection.clear();
  }

  setPhone(): void {
    let dialogRef = this.dialog.open(PhoneComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data.number !== undefined) {
        this.telephones.push(data as PhoneDto);
      }
    });
  }

  deletePhone(element, i) {
    let dialogRef = this.dialog.open(PhoneComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.telephones.splice(i, 1);
    });
  }

  addAddressPerson(id) {
    this.addressPerson.personId = id;
    this.addresses.forEach(
      data => {
        this.addressPerson.addressId = data.id;
        this.adressService.addresspersonPost(this.addressPerson).subscribe(
          e => {
            console.log('Guardando direccion de Empresa');
            console.log(e);
          }, error => console.log(error));
      },
      error => console.log(error)
    );
    /* this.addresses.forEach(
      data => {
        this.addressPerson.id = '';
        this.addressPerson.addressId = data.id;
        this.addressPersons.push(this.addressPerson);
      },
      error => console.log(error)
    );

    this.adressService.addresspersonListCreate(this.addressPersons).subscribe(
      data => {
        console.log('***********************');
        console.log(data);
        console.log('***********************');
    },
    error => console.log(error)); */
  }

  addPhonePerson(id) {
    this.phonePerson.personid = id;
    this.telephones.forEach(
      data => {
        this.phonePerson.phoneid = data.id;
        this.personService.phonepersonPost(this.phonePerson).subscribe(
          e => {
            console.log('Guardando telefono de Empresa');
            console.log(e);
          }, error => console.log(error));
      },
      error => console.log(error)
    );
  }

  addActivityPerson(id) {
    this.activityPerson.companyid = id;
    this.activities.forEach(
      data => {
        this.activityPerson.economicactivityid = data.id;
        this.personService.economicactivitycompanyPost(this.activityPerson).subscribe(
          data => {
            console.log('Guardando actividad de Empresa');
            console.log(data);
          }, error => console.log(error));
      },
      error => console.log(error)
    );
  }

  addPadronPerson(id) {
    this.padronPerson.companyid = id;
    this.selection.selected.forEach(
      data => {
        this.padronPerson.padronid = data.id;
        this.personService.padronpersonPost(this.padronPerson).subscribe(
          e => {
            console.log('Guardando padron de Empresa');
            console.log(e);
          }, error => console.log(error));
      },
      error => console.log(error)
    );
  }
}
