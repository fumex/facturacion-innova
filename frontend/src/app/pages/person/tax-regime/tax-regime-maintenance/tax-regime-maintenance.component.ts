import { Component, OnInit, Inject } from '@angular/core';
import { CoreProvider } from '../../../../core/provider/coreProvider';
import { PersonService} from '../../../../services/service.index';
import { Parent } from 'src/app/core/class/Parent';
import { TaxRegimeDto } from 'src/app/services/person.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-tax-regime-maintenance',
  templateUrl: './tax-regime-maintenance.component.html',
  styles: [],
  providers: [CoreProvider]
})
export class TaxRegimeMaintenanceComponent extends Parent implements OnInit {
  taxRegimeDto: TaxRegimeDto = new TaxRegimeDto();

  constructor(
    public dialogRef: MatDialogRef<TaxRegimeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private PersonService: PersonService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.newCode();
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: TaxRegimeDto = <TaxRegimeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        abbreviation: [{
            value: temp.abbreviation,
            disabled: this.disabledEdit},
            Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        abbreviation: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          abbreviation: this.form.value.abbreviation,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.taxRegimeDto.code = info.code;
        this.taxRegimeDto.name = info.name;
        this.taxRegimeDto.abbreviation = info.abbreviation;
        this.taxRegimeDto.isactive = info.isactive;
        this.PersonService.taxregimePut(info.id, this.taxRegimeDto).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.taxRegimeDto.code = info.code;
        this.taxRegimeDto.name = info.name;
        this.taxRegimeDto.abbreviation = info.abbreviation;
        this.taxRegimeDto.isactive = info.isactive;
        this.PersonService.taxregimePost(this.taxRegimeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.taxRegimeDto.id = this.form.value.id;
      this.PersonService.taxregimeDelete(this.taxRegimeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  newCode() {
    this.PersonService.listTaxRegimes().subscribe( data => {
      if (data.data !== null) {
        let length = data.data.length;
        console.log(data.data.length);
        if ( (length === 0) || (length < 9)) {
          this.form.controls.code.setValue('RT0' + (length + 1));
        } else {
          this.form.controls.code.setValue('RT' + (length + 1));
        }
      } else { this.coreProvider.showMessageError(data.error.message); }},
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }
}
