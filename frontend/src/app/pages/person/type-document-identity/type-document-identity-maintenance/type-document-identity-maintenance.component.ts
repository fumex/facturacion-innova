import { Component, OnInit, Inject } from '@angular/core';
import { CoreProvider } from '../../../../core/provider/coreProvider';
import { Parent } from '../../../../core/class/Parent';
import { DocumentIdentityTypeDto, PersonService } from 'src/app/services/person.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-type-document-identity-maintenance',
  templateUrl: './type-document-identity-maintenance.component.html',
  styles: [],
  providers: [CoreProvider]
})
export class TypeDocumentIdentityMaintenanceComponent extends Parent implements OnInit {
  documentIdentity: DocumentIdentityTypeDto = new DocumentIdentityTypeDto();

  constructor(
    public dialogRef: MatDialogRef<TypeDocumentIdentityMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private PersonService: PersonService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: DocumentIdentityTypeDto = <DocumentIdentityTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          // abbreviation: this.form.value.abbreviation,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.documentIdentity.code = info.code;
        this.documentIdentity.name = info.name;
        this.documentIdentity.isactive = info.isactive;
        this.PersonService.documentidentitytypePut(info.id, this.documentIdentity).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.documentIdentity.code = info.code;
        this.documentIdentity.name = info.name;
        this.documentIdentity.isactive = info.isactive;
        this.PersonService.documentidentitytypePost(this.documentIdentity).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.documentIdentity.id = this.form.value.id;
      this.PersonService.documentidentitytypeDelete(this.documentIdentity.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
