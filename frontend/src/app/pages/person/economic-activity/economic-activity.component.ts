import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators, NgControl } from '@angular/forms';
import { CoreProvider } from '../../../core/provider/coreProvider';
import { PersonService} from '../../../services/service.index';
import { EconomicActivityDto } from '../../../services/person.service';

import { Parent } from 'src/app/core/class/Parent';

import * as XLSX from 'xlsx';
import { map, filter } from 'rxjs/operators';
import { Router } from '@angular/router';

import { EconomicActivityMaintenanceComponent } from './economic-activity-maintenance/economic-activity-maintenance.component';

@Component({
  selector: 'app-economic-activity',
  templateUrl: './economic-activity.component.html',
  styles: [],
  providers: [CoreProvider, PersonService]
})
export class EconomicActivityComponent extends Parent implements OnInit {

  listItem: Array<EconomicActivityDto> = [];
  listItemNew: Array<any> = [];
  dataSource: MatTableDataSource<EconomicActivityDto>;
  displayedColumns: string[] = ['code', 'name', 'status', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  target: DataTransfer = new DataTransfer();

  constructor( private formBuilder: FormBuilder,
               private personService: PersonService,
               private coreProvider: CoreProvider,
               private router: Router,
               public dialog: MatDialog,
               private _snackBar: MatSnackBar
              ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem() {
    this.personService.listEconomicActivities().subscribe(data => {
      this.listItem =  <Array<EconomicActivityDto>>(data.data);
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      console.log(data.data);
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  onFileChange(evt: any): Array<any> {
    /* wire up file reader */
    this.target = (evt.target) as DataTransfer;
    if (this.target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      /* grab first sheet */
      // const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets['ACTIVIDAD ECONOMICA'];
      /* save data */
      const json =  XLSX.utils.sheet_to_json(ws);
      this.listItemNew = json.map(
        obj => {return {
        code: obj['CODIGO'],
        name: obj['NOMBRE']
      }; });
    };
    reader.readAsBinaryString(this.target.files[0]);
    return this.listItemNew;
  }

  searchNewItem(list: Array<any>) {
    const temporalArray: Array<EconomicActivityDto> = JSON.parse(JSON.stringify(list));
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < this.listItem.length; j++) {
        if ((temporalArray[i] !== undefined) && ( String(temporalArray[i].code) === this.listItem[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  inactivateItem(list: Array<any>) {
    const temporalArray: Array<EconomicActivityDto> = this.listItem;
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < list.length; j++) {
        if ((temporalArray[i] !== undefined) && ( temporalArray[i].code === list[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  onSubmit() {
    let list = this.searchNewItem(this.listItemNew);
    if (list.length !== 0) {
      this.createEconomicActivityList(list);
    }
    let listDelete: Array<EconomicActivityDto> = this.inactivateItem(this.listItemNew);
    if (listDelete.length !== 0) {
      console.log(listDelete);
      listDelete.forEach( element => {
        let economicActivityDto: EconomicActivityDto = new EconomicActivityDto();
        economicActivityDto.id = element.id;
        this.deleteEconomicActivity(economicActivityDto);
      });
    }
  }

  createEconomicActivityList(economicActivityListDto:Array<EconomicActivityDto>) {
    this.personService.economicactivityListCreate(economicActivityListDto).subscribe(
      data => {
        this.openSnackBar('Guardado', 'OK');
        this.getItem();
      },
      error => {
        console.log(error);
        this.openSnackBar('Error datos no corresponde', 'OK');
      }
    );
  }

  deleteEconomicActivity(economicActivityDto: EconomicActivityDto) {
    this.personService.economicactivityDelete(economicActivityDto.id).subscribe(e => {
      this.openSnackBar('Eliminado', 'OK');
      console.log('eliminado');
      this.getItem();
    });
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      file: ['', Validators.required]
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }

  addItem() {
    let dialogRef = this.dialog.open(EconomicActivityMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  updateItem(item) {
    let dialogRef = this.dialog.open(EconomicActivityMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  deleteItem(item) {
    let dialogRef = this.dialog.open(EconomicActivityMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }
}

