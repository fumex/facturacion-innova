import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

import { PersonService} from '../../../../services/service.index';
import { EconomicActivityDto } from 'src/app/services/person.service';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { Parent } from '../../../../core/class/Parent';

@Component  ({
  selector: 'app-economic-activity-maintenance',
  templateUrl: './economic-activity-maintenance.component.html',
  styles: [],
  providers: [CoreProvider]
})
export class EconomicActivityMaintenanceComponent extends Parent implements OnInit {

  economicActivityDto: EconomicActivityDto = new EconomicActivityDto();

  constructor(
    public dialogRef: MatDialogRef<EconomicActivityMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _personService: PersonService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: EconomicActivityDto = <EconomicActivityDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.economicActivityDto.code = info.code;
        this.economicActivityDto.name = info.name;
        this.economicActivityDto.isactive = info.isactive;
        this._personService.economicactivityPut(this.economicActivityDto, info.id).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.economicActivityDto.code = info.code;
        this.economicActivityDto.name = info.name;
        this.economicActivityDto.isactive = info.isactive;
        this._personService.economicactivityPost(this.economicActivityDto).subscribe(
          data => {
            //this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => 
            console.log(error))
            this.openSnackBar('Codigo existente','OK');
      }
    }
    if (this.title === this.operationDelete) {
      this.economicActivityDto.id = this.form.value.id;
      this._personService.economicactivityDelete(this.economicActivityDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }
}
