import { Component, OnInit, Inject } from '@angular/core';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { Parent } from 'src/app/core/class/Parent';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { PersonService } from 'src/app/services/service.index';
import { FiscalAddressConditionTypeDto } from 'src/app/services/person.service';

@Component({
  selector: 'app-fiscal-address-condition-maintenance',
  templateUrl: './fiscal-address-condition-maintenance.component.html',
  styles: [],
  providers: [CoreProvider]
})
export class FiscalAddressConditionMaintenanceComponent extends Parent implements OnInit {

  fiscalAddressDto: FiscalAddressConditionTypeDto = new FiscalAddressConditionTypeDto();

  constructor(
    public dialogRef: MatDialogRef<FiscalAddressConditionMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private _personService: PersonService,
    private _snackBar: MatSnackBar
  ) { super();}

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.newCode();
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: FiscalAddressConditionTypeDto = <FiscalAddressConditionTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.fiscalAddressDto.code = info.code;
        this.fiscalAddressDto.name = info.name;
        this.fiscalAddressDto.isactive = info.isactive;
        this._personService.fiscaladdressconditiontypePut(this.fiscalAddressDto, info.id).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.fiscalAddressDto.code = info.code;
        this.fiscalAddressDto.name = info.name;
        this.fiscalAddressDto.isactive = info.isactive;
        this._personService.fiscaladdressconditiontypePost(this.fiscalAddressDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }
    if (this.title === this.operationDelete) {
      this.fiscalAddressDto.id = this.form.value.id;
      this._personService.fiscaladdressconditiontypeDelete(this.fiscalAddressDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  newCode() {
    this._personService.listFiscalAddressConditionTypes().subscribe( data => {
      if (data.data !== null) {
        let length = data.data.length;
        console.log(data.data.length);
        if ( (length === 0) || (length < 9)) {
          this.form.controls.code.setValue('CD0' + (length + 1));
        } else {
          this.form.controls.code.setValue('CD' + (length + 1));
        }
      } else { this.coreProvider.showMessageError(data.error.message); }},
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }
}
