import { Component, OnInit, ViewChild } from '@angular/core';
import { Parent } from 'src/app/core/class/Parent';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { PersonService } from 'src/app/services/service.index';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FiscalAddressConditionTypeDto } from 'src/app/services/person.service';
import { FiscalAddressConditionMaintenanceComponent } from './fiscal-address-condition-maintenance/fiscal-address-condition-maintenance.component';

import * as XLSX from 'xlsx';

@Component({
  selector: 'app-fiscal-address-condition',
  templateUrl: './fiscal-address-condition.component.html',
  styles: [],
  providers: [CoreProvider, PersonService]
})
export class FiscalAddressConditionComponent extends Parent implements OnInit {

  listItem: Array<FiscalAddressConditionTypeDto> = [];
  listItemNew: Array<any> = [];
  dataSource: MatTableDataSource<FiscalAddressConditionTypeDto>;
  displayedColumns: string[] = ['code', 'name', 'status', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  target: DataTransfer = new DataTransfer();

  constructor(
      private formBuilder: FormBuilder,
      private personService: PersonService,
      private coreProvider: CoreProvider,
      private router: Router,
      public dialog: MatDialog,
      private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem() {
    this.personService.listFiscalAddressConditionTypes().subscribe(data => {
      this.listItem =  <Array<FiscalAddressConditionTypeDto>>(data.data);
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      console.log(data.data);
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  onFileChange(evt: any): Array<any> {
    /* wire up file reader */
    this.target = (evt.target) as DataTransfer;
    if (this.target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      /* grab first sheet */
      // const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets['T CONDICION DEL DOMICILIO'];
      /* save data */
      const json =  XLSX.utils.sheet_to_json(ws);
      this.listItemNew = json.map(
        obj => {return {
        code: obj['CODIGO'],
        name: obj['NOMBRE']
      }; });
    };
    reader.readAsBinaryString(this.target.files[0]);
    return this.listItemNew;
  }

  searchNewItem(list: Array<any>) {
    const temporalArray: Array<FiscalAddressConditionTypeDto> = JSON.parse(JSON.stringify(list));
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < this.listItem.length; j++) {
        if ((temporalArray[i] !== undefined) && ( String(temporalArray[i].code) === this.listItem[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  inactivateItem(list: Array<any>) {
    const temporalArray: Array<FiscalAddressConditionTypeDto> = this.listItem;
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < list.length; j++) {
        if ((temporalArray[i] !== undefined) && ( temporalArray[i].code === list[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  onSubmit() {
    let list = this.searchNewItem(this.listItemNew);
    if (list.length !== 0) {
      this.createFiscalAddressList(list);
    }
    let listDelete: Array<FiscalAddressConditionTypeDto> = this.inactivateItem(this.listItemNew);
    if (listDelete.length !== 0) {
      console.log(listDelete);
      listDelete.forEach( element => {
        let fiscalAddressDto: FiscalAddressConditionTypeDto = new FiscalAddressConditionTypeDto();
        fiscalAddressDto.id = element.id;
        this.deleteFiscalAddress(fiscalAddressDto);
      });
    }
  }

  createFiscalAddressList(fiscalAddressListDto: Array<FiscalAddressConditionTypeDto>) {
    this.personService.fiscaladdressconditiontypeListCreate(fiscalAddressListDto).subscribe(
      data => {
        this.openSnackBar('Guardado', 'OK');
        this.getItem();
      },
      error => {
        console.log(error);
        this.openSnackBar('Error datos no corresponde', 'OK');
      }
    );  
  }

  deleteFiscalAddress(fiscalAddressDto: FiscalAddressConditionTypeDto) {
    this.personService.fiscaladdressconditiontypeDelete(fiscalAddressDto.id).subscribe(e => {
      this.openSnackBar('Eliminado', 'OK');
      console.log('eliminado');
      this.getItem();
    });
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      file: ['', Validators.required]
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }

  addItem() {
    let dialogRef = this.dialog.open(FiscalAddressConditionMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  updateItem(item) {
    let dialogRef = this.dialog.open(FiscalAddressConditionMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  deleteItem(item) {
    let dialogRef = this.dialog.open(FiscalAddressConditionMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

}
