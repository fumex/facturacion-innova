import { InvoiceModule } from './invoice/invoice.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { PagesRoutingModule } from './pages-routing.module';
import { AngularMaterialModule } from '../shared/angular-material.module';
import { CommonModule } from '@angular/common';
import { AddressModule } from './address/address.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PersonModule } from './person/person.module';
import {ProductModule} from './product/product.module';

@NgModule({
    declarations: [
        DashboardComponent,
        PagesComponent,
        AccountSettingsComponent
    ],
    exports: [
        DashboardComponent,
        PagesComponent
    ],
    imports: [
        SharedModule,
        PagesRoutingModule,
        FormsModule,
        CommonModule,
        AngularMaterialModule,
        ReactiveFormsModule,
        AddressModule,
        PersonModule,
        InvoiceModule,
        ProductModule
    ]
})
export class PagesModule { }
