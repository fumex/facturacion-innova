import { Component, OnInit, ViewChild } from '@angular/core';
import { Parent } from '../../core/class/Parent';
import { FormBuilder, FormArray, Validators, FormGroup } from '@angular/forms';

import { CoreProvider } from '../../core/provider/coreProvider';
import { InvoiceService, InvoiceDto } from '../../services/invoice.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: [],
  providers: [CoreProvider, InvoiceService]
})
export class DashboardComponent extends Parent implements OnInit {
  invoices: Array<InvoiceDto>;
  dataSource: MatTableDataSource<InvoiceDto>;
  displayedColumns: string[] = ['serie', 'emissiondate', 'clientcode', 'cointypecode', 'status'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true})  sort: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private invoiceService: InvoiceService
  ) { super(); }

  ngOnInit() {
    this.getItem();
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      address: [''],
      phones: this.formBuilder.array([this.formBuilder.group({phone: ['']})])
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem() {
    this.invoiceService.listInvoices().subscribe(
      data => {
          this.invoices = <Array<InvoiceDto>> (data.data);
          this.dataSource = new MatTableDataSource(this.invoices);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          console.log(data.data);
      }, error => {
          this.coreProvider.showMessageErrorUnexpected();
    });
  }

}
