import { Component, OnInit, Inject } from '@angular/core';

import { SettingsService } from '../../services/service.index';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: []
})
export class AccountSettingsComponent implements OnInit {

  // tslint:disable-next-line: variable-name
  constructor( public _settings: SettingsService ) {}

  ngOnInit() {
    this.setCheck();
  }

  switchColor(theme: string, link: any) {
    this.applyCheck(link);
    this._settings.applyTheme(theme);
  }

  applyCheck(link: any) {
    // tslint:disable-next-line: prefer-const
    let selectors: any = document.getElementsByClassName('selector');
    // tslint:disable-next-line: prefer-const
    for (let ref of selectors) {
      ref.classList.remove('working');
    }
    link.classList.add('working');
  }
  setCheck() {
    // tslint:disable-next-line: prefer-const
    let selectors: any = document.getElementsByClassName('selector');
    // tslint:disable-next-line: prefer-const
    let theme = this._settings.settings.theme;
    // tslint:disable-next-line: prefer-const
    for (let ref of selectors) {
      if ( ref.getAttribute('data-theme') === theme) {
        ref.classList.add('working');
      }
    }
  }
}
