import { Component, OnInit, Inject } from '@angular/core';
import { Parent } from 'src/app/core/class/Parent';
import { AddressTypeDto, AddressService } from 'src/app/services/address.service';

import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-address-type-maintenance',
  templateUrl: './address-type-maintenance.component.html',
  styles: [],
  providers: [AddressService]
})
export class AddressTypeMaintenanceComponent  extends Parent implements OnInit {
  item: AddressTypeDto = new AddressTypeDto();
  constructor(
    public dialogRef: MatDialogRef<AddressTypeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private addressService: AddressService,
    private _snackBar: MatSnackBar
  ) { super() }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: AddressTypeDto = <AddressTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        description: [{value: temp.description,  disabled: this.disabledEdit}]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        description: [''],
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          description: this.form.value.description
      };
      if (this.title === this.operationUpdate) {
        this.item.code = info.code;
        this.item.name = info.name;
        this.item.description = info.description;
        this.addressService.addresstypePut(this.item, info.id).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.item.code = info.code;
        this.item.name = info.name;
        this.item.description = info.description;
        this.addressService.addresstypePost(this.item).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.item.id = this.form.value.id;
      this.addressService.addresstypeDelete(this.item.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
