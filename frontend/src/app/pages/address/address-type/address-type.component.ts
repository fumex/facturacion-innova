import { Component, OnInit, ViewChild } from '@angular/core';
import { AddressTypeDto } from 'src/app/services/address.service';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { AddressService } from '../../../services/service.index';
import { Parent } from 'src/app/core/class/Parent';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { AddressTypeMaintenanceComponent } from './address-type-maintenance/address-type-maintenance.component';

@Component({
  selector: 'app-address-type',
  templateUrl: './address-type.component.html',
  styles: [],
  providers: [CoreProvider, AddressService]
})
export class AddressTypeComponent extends Parent implements OnInit {
  listItem: Array<AddressTypeDto> = [];
  dataSource: MatTableDataSource<AddressTypeDto>;
  displayedColumns: string[] = ['code', 'name', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private adressService: AddressService,
    private coreProvider: CoreProvider,
    public router: Router,
    public dialog: MatDialog,
  ) { super(); }

  ngOnInit() {
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem(): void {
    this.adressService.listAddressesTypes().
    subscribe(data => {
      this.listItem =  data.data as Array<AddressTypeDto>;
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, () => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  addItem() {
    let dialogRef = this.dialog.open(AddressTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  viewItem(item){
    this.dialog.open(AddressTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationView,
        info: item
      }
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(AddressTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(AddressTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }
}
