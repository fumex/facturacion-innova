import { Component, OnInit, Inject } from '@angular/core';
import { AddressService } from '../../../../services/service.index';
import { Parent } from 'src/app/core/class/Parent';

import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { RoadTypeDto } from '../../../../services/address.service';

@Component({
  selector: 'app-type-road-maintenance',
  templateUrl: './type-road-maintenance.component.html',
  styles: [],
  providers: [AddressService]
})
export class TypeRoadMaintenanceComponent  extends Parent implements OnInit {
  roadTypeDto: RoadTypeDto = new RoadTypeDto();
  constructor(
    public dialogRef: MatDialogRef<TypeRoadMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private adressService: AddressService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: RoadTypeDto = <RoadTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        abbreviation: [{
          value: temp.abbreviation,
          disabled: this.disabledEdit},
          Validators.required]
          
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        abbreviation: ['', Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          abbreviation:this.form.value.abbreviation,
      };
      if (this.title === this.operationUpdate) {
        this.roadTypeDto.code = info.code;
        this.roadTypeDto.name = info.name;
        this.roadTypeDto.abbreviation = info.abbreviation;
        this.adressService.roadtypePut(info.id, this.roadTypeDto).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.roadTypeDto.code = info.code;
        this.roadTypeDto.name = info.name;
        this.roadTypeDto.abbreviation = info.abbreviation;
        this.adressService.roadtypePost(this.roadTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'CLOSE');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.roadTypeDto.id = this.form.value.id;
      this.adressService.roadtypeDelete(this.roadTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
