import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { Parent } from '../../../core/class/Parent';
import { AddressService } from '../../../services/service.index';
import { DepartmentDto } from '../../../services/address.service';

import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import * as XLSX from 'xlsx';
import { DepartmentMaintenanceComponent } from './department-maintenance/department-maintenance.component';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styles: [],
  providers: [CoreProvider, AddressService]
})
export class DepartmentComponent extends Parent implements OnInit {
  listItem: Array<DepartmentDto> = [];
  listItemNew: Array<any> = [];
  dataSource: MatTableDataSource<DepartmentDto>;
  displayedColumns: string[] = ['code', 'name', 'button'];

  target: DataTransfer = new DataTransfer();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    private adressService: AddressService,
    private coreProvider: CoreProvider,
    public router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      file: ['', Validators.required]
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  getItem(): void {
    this.adressService.listdepartments().
    subscribe(data => {
      this.listItem =  data.data as Array<DepartmentDto>;
      this.listItem.sort(function(a, b) {
        var nameA=a.name.toLowerCase(), nameB = b.name.toLowerCase();
        if (nameA < nameB)
            return -1;
        if (nameA > nameB)
            return 1;
        return 0;
      });
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, () => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  onFileChange(evt: any): Array<any> {
    this.target = (evt.target) as DataTransfer;
    if (this.target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      // const wsname: string = wb.SheetNames[3];
      const ws: XLSX.WorkSheet = wb.Sheets['DEPARTAMENTO'];
      const json =  XLSX.utils.sheet_to_json(ws);
      this.listItemNew = json.map(
        obj => {return {
        code: obj['CODIGO'],
        name: obj['NOMBRE']
      }; });
    };
    reader.readAsBinaryString(this.target.files[0]);
    return this.listItemNew;
  }

  searchNewItem(list: Array<any>) {
    const temporalArray: Array<DepartmentDto> = JSON.parse(JSON.stringify(list));
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < this.listItem.length; j++) {
        if ((temporalArray[i] !== undefined) && ( String(temporalArray[i].code) === this.listItem[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  inactivateItem(list: Array<any>) {
    const temporalArray: Array<DepartmentDto> = this.listItem;
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < list.length; j++) {
        if ((temporalArray[i] !== undefined) && ( temporalArray[i].code === list[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  onSubmit() {
    let list = this.searchNewItem(this.listItemNew);
    if (list.length !== 0) {
      this.createDepartmentList(list);
    }
    let listDelete: Array<DepartmentDto> = this.inactivateItem(this.listItemNew);
    if (listDelete.length !== 0) {
      console.log(listDelete);
      listDelete.forEach( element => {
        let departmentDto: DepartmentDto = new DepartmentDto();
        departmentDto.id = element.id;
        this.deleteDepartment(departmentDto);
      });
    }
  }

  createDepartmentList(departmentDtoList:Array<DepartmentDto>) {
    this.adressService.departmentListCreate(departmentDtoList).subscribe(
      () => {
        this.openSnackBar('Guardado', 'OK');
        this.getItem();
      },
      error => {
        console.log(error);
        this.openSnackBar('Error datos no corresponde', 'OK');
      }
    );
  }

  deleteDepartment(departmentDto: DepartmentDto) {
    this.adressService.departmentDelete(departmentDto.id).subscribe(data => {
      this.openSnackBar('Eliminado', 'OK');
      console.log('eliminado');
      this.getItem();
    });
  }

  addItem() {
    let dialogRef = this.dialog.open(DepartmentMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(DepartmentMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(DepartmentMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getItem();
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }

}
