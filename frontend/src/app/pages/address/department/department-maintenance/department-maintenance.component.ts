import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { AddressService} from '../../../../services/service.index';
import { Parent } from 'src/app/core/class/Parent';
import { DepartmentDto } from '../../../../services/address.service';

import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-department-maintenance',
  templateUrl: './department-maintenance.component.html',
  styles: [],
  providers: [AddressService]
})
export class DepartmentMaintenanceComponent extends Parent implements OnInit {
  departmentDto: DepartmentDto = new DepartmentDto();
  newCode: string;
  departments: Array<DepartmentDto>;

  constructor(
    public dialogRef: MatDialogRef<DepartmentMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private adressService: AddressService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        this.setnewCode();
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: DepartmentDto = <DepartmentDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
      };
      if (this.title === this.operationUpdate) {
        this.departmentDto.code = info.code;
        this.departmentDto.name = info.name;
        this.adressService.departmentPut(this.departmentDto, info.id).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.departmentDto.code = info.code;
        this.departmentDto.name = info.name;
        this.adressService.departmentPost(this.departmentDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.departmentDto.id = this.form.value.id;
      this.adressService.departmentDelete(this.departmentDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  setnewCode() {
    this.adressService.listdepartments().subscribe( data => {
      this.departments = data.data as Array<DepartmentDto>;
      if (this.departments.length < 9) {
        this.newCode = '0' + String(this.departments.length + 1);
      } else {
        this.newCode = String(this.departments.length + 1);
      }
      this.form.controls.code.setValue(this.newCode);
    });
  }
}
