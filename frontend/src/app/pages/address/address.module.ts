import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentComponent } from './department/department.component';
import { ProvinceComponent } from './province/province.component';
import { DistrictComponent } from './district/district.component';
import { DepartmentMaintenanceComponent } from './department/department-maintenance/department-maintenance.component';
import { ProvinceMaintenanceComponent } from './province/province-maintenance/province-maintenance.component';
import { DistrictMaintenanceComponent } from './district/district-maintenance/district-maintenance.component';
import { AngularMaterialModule } from '../../shared/angular-material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { TypeRoadComponent } from './type-road/type-road.component';
import { TypeRoadMaintenanceComponent } from './type-road/type-road-maintenance/type-road-maintenance.component';
import { TypeZoneComponent } from './type-zone/type-zone.component';
import { TypeZoneMaintenanceComponent } from './type-zone/type-zone-maintenance/type-zone-maintenance.component';
import { AddressComponent } from './address.component';
import { AddressTypeComponent } from './address-type/address-type.component';
import { AddressTypeMaintenanceComponent } from './address-type/address-type-maintenance/address-type-maintenance.component';


@NgModule({
  declarations: [
    DepartmentComponent,
    ProvinceComponent,
    DistrictComponent,
    DepartmentMaintenanceComponent,
    ProvinceMaintenanceComponent,
    DistrictMaintenanceComponent,
    TypeRoadComponent,
    TypeZoneComponent,
    TypeRoadMaintenanceComponent,
    TypeZoneMaintenanceComponent,
    AddressComponent,
    AddressTypeComponent,
    AddressTypeMaintenanceComponent
  ],
  exports: [
    DepartmentMaintenanceComponent,
    ProvinceMaintenanceComponent,
    DistrictMaintenanceComponent,
    TypeRoadMaintenanceComponent,
    TypeZoneMaintenanceComponent,
    AddressTypeMaintenanceComponent,
    AddressComponent
  ],
  entryComponents: [
    DepartmentMaintenanceComponent,
    ProvinceMaintenanceComponent,
    DistrictMaintenanceComponent,
    TypeRoadMaintenanceComponent,
    TypeZoneMaintenanceComponent,
    AddressTypeMaintenanceComponent,
    AddressComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    SharedModule,
    FormsModule
  ]
})
export class AddressModule { }
