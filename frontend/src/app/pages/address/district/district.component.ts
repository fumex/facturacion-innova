import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreProvider } from '../../../core/provider/coreProvider';
import { AddressService } from '../../../services/service.index';
import { Parent } from '../../../core/class/Parent';

import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import * as XLSX from 'xlsx';
import { DistrictMaintenanceComponent } from './district-maintenance/district-maintenance.component';
import { DistrictDto } from '../../../services/address.service';
@Component({
  selector: 'app-district',
  templateUrl: './district.component.html',
  styles: [],
  providers: [CoreProvider, AddressService]
})
export class DistrictComponent extends Parent implements OnInit {
  listItem: Array<DistrictDto> = [];
  listItemNew: Array<any> = [];
  dataSource: MatTableDataSource<DistrictDto>;
  displayedColumns: string[] = ['code', 'department', 'province', 'name', 'button'];

  target: DataTransfer = new DataTransfer();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    private adressService: AddressService,
    private coreProvider: CoreProvider,
    public router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      file: ['', Validators.required]
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  getItem(): void {
    this.adressService.listdistricts().
    subscribe(data => {
      this.listItem =  data.data as Array<DistrictDto>;
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  onFileChange(evt: any): Array<any> {
    this.target = (evt.target) as DataTransfer;
    if (this.target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      // const wsname: string = wb.SheetNames[3];
      const ws: XLSX.WorkSheet = wb.Sheets['DISTRITO'];
      const json =  XLSX.utils.sheet_to_json(ws);
      this.listItemNew = json.map(
        obj => {return {
        code: obj['CODIGO'],
        name: obj['NOMBRE']
      }; });
    };
    reader.readAsBinaryString(this.target.files[0]);
    return this.listItemNew;
  }

  searchNewItem(list: Array<any>) {
    const temporalArray: Array<DistrictDto> = JSON.parse(JSON.stringify(list));
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < this.listItem.length; j++) {
        if ((temporalArray[i] !== undefined) && ( String(temporalArray[i].code) === this.listItem[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  inactivateItem(list: Array<any>) {
    const temporalArray: Array<DistrictDto> = this.listItem;
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < list.length; j++) {
        if ((temporalArray[i] !== undefined) && ( temporalArray[i].code === list[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  onSubmit() {
    let list = this.searchNewItem(this.listItemNew);
    let districtListDto: Array<DistrictDto> = [];
    if (list.length !== 0) {
      console.log(list);
      list.forEach( element => {
        let districtDto: DistrictDto = new DistrictDto();
        districtDto.id = '0';
        districtDto.code = element.code.toString();
        districtDto.codeDepartment = districtDto.code.substr(0, 2);
        districtDto.codeProvince = districtDto.code.substr(0, 4);
        districtDto.name = element.name.toString();
        districtListDto.push(districtDto);
      });
      this.createDistrictList(districtListDto);
    }

    let listDelete: Array<DistrictDto> = this.inactivateItem(this.listItemNew);
    if (listDelete.length !== 0) {
      console.log(listDelete);
      listDelete.forEach( element => {
        let districtDto: DistrictDto = new DistrictDto();
        districtDto.id = element.id;
        this.deleteDistrict(districtDto);
      });
    }
  }

  createDistrictList(districtListDto: Array<DistrictDto>) {
    this.adressService.districtListCreate(districtListDto).subscribe(
      data => {
        this.openSnackBar('Guardado', 'OK');
        this.getItem();
      },
      error => {
        console.log(error);
        this.openSnackBar('Error datos no corresponde', 'OK');
    });
  }

  deleteDistrict(districtDto: DistrictDto) {
    this.adressService.districtDelete(districtDto.id).subscribe(e => {
      this.openSnackBar('Eliminado', 'OK');
      console.log('eliminado');
      this.getItem();
    });
  }

  addItem() {
    let dialogRef = this.dialog.open(DistrictMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(DistrictMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(DistrictMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    console.log(element);

    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  viewItem(item){
    this.dialog.open(DistrictMaintenanceComponent, {
      width: '80%',
      disableClose: true, 
      data: { 
        operation: this.operationView,
        info: item
      }
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }

}
