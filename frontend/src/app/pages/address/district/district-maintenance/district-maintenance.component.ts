import { Component, OnInit, Inject } from '@angular/core';
import { AddressService} from '../../../../services/service.index';
import { Parent } from 'src/app/core/class/Parent';
import { DistrictDto, ProvinceDto, DepartmentDto } from '../../../../services/address.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { CoreProvider } from '../../../../core/provider/coreProvider';

@Component({
  selector: 'app-district-maintenance',
  templateUrl: './district-maintenance.component.html',
  styles: [],
  providers: [CoreProvider, AddressService]
})
export class DistrictMaintenanceComponent extends Parent implements OnInit {
  districtDto: DistrictDto = new DistrictDto();
  departments: Array<DepartmentDto>;
  provinces: Array<ProvinceDto>;
  districts: Array<DistrictDto>;
  newCode: string;
  constructor(
    public dialogRef: MatDialogRef<DistrictMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private adressService: AddressService,
    private _snackBar: MatSnackBar,
    private coreProvider: CoreProvider
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew: 
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.getDepartments();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: DistrictDto = <DistrictDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        codeDepartment: [{
            value: temp.codeDepartment,
            disabled: this.disabledEdit},
            Validators.required],
        codeProvince: [{
          value: temp.codeProvince,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        codeDepartment: ['', Validators.required],
        codeProvince: ['', Validators.required],
        name: ['', Validators.required]
      });
    }
  }

  getDepartments(): void {
    this.adressService.departments().
    subscribe(data => {
      this.departments =  data.data as Array<DepartmentDto>;
      this.departments.sort(function(a, b) {
        var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase();
        if (nameA < nameB) 
            return -1; 
        if (nameA > nameB)
            return 1;
        return 0;
      });
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
    this.adressService.provinces().subscribe(
      data => {
        this.provinces =  data.data as Array<ProvinceDto>;
      }, error => {
        this.coreProvider.showMessageErrorUnexpected();
      });
  }

  getProvinces(): void {
    this.adressService.filterprovince(this.form.value.codeDepartment).
    subscribe(data => {
      this.provinces =  data.data as Array<ProvinceDto>;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          codeDepartment: this.form.value.codeDepartment,
          codeProvince: this.form.value.codeProvince,
          name: this.form.value.name,
      };
      if (this.title === this.operationUpdate) {
        this.districtDto.code = info.code;
        this.districtDto.codeDepartment = info.codeDepartment;
        this.districtDto.codeProvince = info.codeProvince;
        this.districtDto.name = info.name;
        this.adressService.districtPut(this.districtDto, info.id).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.districtDto.code = info.code;
        this.districtDto.codeDepartment = info.codeDepartment;
        this.districtDto.codeProvince = info.codeProvince;
        this.districtDto.name = info.name;
        this.adressService.districtPost(this.districtDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.districtDto.id = this.form.value.id;
      this.adressService.districtDelete(this.districtDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  setCode() {
    let province = this.form.value.codeProvince;
    this.adressService.filterdistricts(province).
    subscribe(data => {
      this.districts = data.data as Array<DistrictDto>;
      this.districts.sort(function(a, b) {
        var nameA = a.code.toLowerCase(), nameB = b.code.toLowerCase();
        if (nameA < nameB) 
            return -1; 
        if (nameA > nameB)
            return 1;
        return 0;
      });
      if (this.districts.length === 0) {
        this.newCode = province + '0' + String(this.districts.length + 1);
      } else {
        let district = Number(this.districts[this.districts.length - 1].code.substr(-2)) + 1;
        if (district <= 9) {
          this.newCode = province + '0' + district;
        } else {
          this.newCode = province + district;
        }
      }
      this.form.controls.code.setValue(this.newCode);
    });
  }

}
