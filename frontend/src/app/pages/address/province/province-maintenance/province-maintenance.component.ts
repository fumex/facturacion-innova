import { Component, OnInit, Inject } from '@angular/core';
import { AddressService} from '../../../../services/service.index';
import { Parent } from 'src/app/core/class/Parent';
import { ProvinceDto, DepartmentDto } from '../../../../services/address.service';

import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { CoreProvider } from '../../../../core/provider/coreProvider';

@Component({
  selector: 'app-province-maintenance',
  templateUrl: './province-maintenance.component.html',
  styles: [],
  providers: [CoreProvider, AddressService]
})
export class ProvinceMaintenanceComponent extends Parent implements OnInit {
  provinceDto: ProvinceDto = new ProvinceDto();
  departments: Array<DepartmentDto>;
  provinces: Array<ProvinceDto>;
  newCode: string;

  constructor( public dialogRef: MatDialogRef<ProvinceMaintenanceComponent>,
               @Inject(MAT_DIALOG_DATA) public data,
               private formBuilder: FormBuilder,
               private adressService: AddressService,
               private _snackBar: MatSnackBar,
               private coreProvider: CoreProvider ) {
      super();
    }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        // this.setCode();
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.getDepartment();
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: ProvinceDto = <ProvinceDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        codeDepartment: [{
          value: temp.codeDepartment,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        codeDepartment: ['', Validators.required],
        name: ['', Validators.required]
      });
    }
  }

  getDepartment() {
    this.adressService.departments().
    subscribe(data => {
      this.departments =  data.data as Array<DepartmentDto>;
      this.departments.sort(function(a, b) {
        var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase();
        if (nameA < nameB) 
            return -1; 
        if (nameA > nameB)
            return 1;
        return 0;
      });
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          codeDepartment: this.form.value.codeDepartment,
          name: this.form.value.name,
      };
      if (this.title === this.operationUpdate) {
        this.provinceDto.code = info.code;
        this.provinceDto.codeDepartment = info.codeDepartment;
        this.provinceDto.name = info.name;
        this.adressService.provincePut(info.id, this.provinceDto).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.provinceDto.code = info.code;
        this.provinceDto.codeDepartment = info.codeDepartment;
        this.provinceDto.name = info.name;
        this.adressService.provincePost(this.provinceDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.provinceDto.id = this.form.value.id;
      this.adressService.provinceDelete(this.provinceDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  setCode() {
    let departmet = this.form.value.codeDepartment;
    this.adressService.filterprovince(departmet).
    subscribe(data => {
      this.provinces = data.data as Array<ProvinceDto>;
      this.provinces.sort(function(a, b) {
        var nameA = a.code.toLowerCase(), nameB = b.code.toLowerCase();
        if (nameA < nameB) 
            return -1; 
        if (nameA > nameB)
            return 1;
        return 0;
      });
      if (this.provinces.length === 0) {
        this.newCode = departmet + '0' + String(this.provinces.length + 1);
      } else {
        let province = Number(this.provinces[this.provinces.length - 1].code.substr(-2)) + 1;
        if (province <= 9) {
          this.newCode = departmet + '0' + province;
        } else {
          this.newCode = departmet + province;
        }
      }
      this.form.controls.code.setValue(this.newCode);
    });
  }

}
