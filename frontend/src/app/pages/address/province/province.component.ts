import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { Parent } from '../../../core/class/Parent';
import { AddressService } from '../../../services/service.index';
import { ProvinceDto } from '../../../services/address.service';

import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import * as XLSX from 'xlsx';
import { ProvinceMaintenanceComponent } from './province-maintenance/province-maintenance.component';

@Component({
  selector: 'app-province',
  templateUrl: './province.component.html',
  styles: [],
  providers: [CoreProvider, AddressService]
})
export class ProvinceComponent extends Parent implements OnInit {
  listItem: Array<ProvinceDto> = [];
  listItemNew: Array<any> = [];
  dataSource: MatTableDataSource<ProvinceDto>;
  displayedColumns: string[] = ['code', 'department', 'name', 'button'];

  target: DataTransfer = new DataTransfer();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    private adressService: AddressService,
    private coreProvider: CoreProvider,
    public router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      file: ['', Validators.required]
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  getItem(): void {
    this.adressService.listprovinces().
    subscribe(data => {
      this.listItem =  data.data as Array<ProvinceDto>;
      this.listItem.sort( function(a, b) {
        var nameA = a.code.toLowerCase(), nameB = b.code.toLowerCase();
        if (nameA < nameB) 
            return -1; 
        if (nameA > nameB)
            return 1;
        return 0;
      });
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  onFileChange(evt: any): Array<any> {
    this.target = (evt.target) as DataTransfer;
    if (this.target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      const ws: XLSX.WorkSheet = wb.Sheets['PROVINCIA'];
      const json =  XLSX.utils.sheet_to_json(ws);
      this.listItemNew = json.map(
        obj => {return {
        code: obj['CODIGO'],
        name: obj['NOMBRE']
      }; });
    };
    reader.readAsBinaryString(this.target.files[0]);
    return this.listItemNew;
  }

  searchNewItem(list: Array<any>) {
    const temporalArray: Array<ProvinceDto> = JSON.parse(JSON.stringify(list));
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < this.listItem.length; j++) {
        if ((temporalArray[i] !== undefined) && ( String(temporalArray[i].code) === this.listItem[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  inactivateItem(list: Array<any>) {
    const temporalArray: Array<ProvinceDto> = this.listItem;
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < list.length; j++) {
        if ((temporalArray[i] !== undefined) && ( temporalArray[i].code === list[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    return temporalArray;
  }

  onSubmit() {
    let list = this.searchNewItem(this.listItemNew);
    let provinceListDto:Array<ProvinceDto> = [];
    if (list.length !== 0) {
      list.forEach( element => {
        let provinceDto: ProvinceDto = new ProvinceDto();
        provinceDto.id = '0';
        provinceDto.code = element.code.toString();
        provinceDto.codeDepartment = provinceDto.code.substr(0, 2);
        provinceDto.name = element.name.toString();
        provinceListDto.push(provinceDto);
      });
      this.createProvinceList(provinceListDto);
    }

    let listDelete: Array<ProvinceDto> = this.inactivateItem(this.listItemNew);
    if (listDelete.length !== 0) {
      console.log(listDelete);
      listDelete.forEach( element => {
        let provinceDto: ProvinceDto = new ProvinceDto();
        provinceDto.id = element.id;
        this.deleteProvince(provinceDto);
      });
    }
  }

  createProvinceList(provinceListDto: Array<ProvinceDto>) {
    this.adressService.provinceListCreate(provinceListDto).subscribe(
      data => {
        this.openSnackBar('Guardado', 'OK');
        this.getItem();
      },
      error => {
        console.log(error);
        this.openSnackBar('Error datos no corresponde', 'OK');
    });
  }

  deleteProvince(provinceDto: ProvinceDto) {
    this.adressService.provinceDelete(provinceDto.id).subscribe(e => {
      this.openSnackBar('Eliminado', 'OK');
      console.log('eliminado');
      this.getItem();
    });
  }

  addItem() {
    let dialogRef = this.dialog.open(ProvinceMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(ProvinceMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(ProvinceMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  viewItem(item){
    this.dialog.open(ProvinceMaintenanceComponent, {
      width: '60%',
      disableClose: true, 
      data: { 
        operation: this.operationView,
        info: item
      }
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }
}
