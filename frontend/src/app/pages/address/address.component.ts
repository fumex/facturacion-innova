import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { CoreProvider } from '../../core/provider/coreProvider';
import { AddressService } from '../../services/service.index';
import { DepartmentDto, ProvinceDto, DistrictDto,
         AddressDto, ZoneTypeDto, RoadTypeDto, AddressTypeDto } from '../../services/address.service';
import { Parent } from '../../core/class/Parent';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators, AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  providers: [CoreProvider, AddressService]
})

export class AddressComponent extends Parent implements OnInit {
  addressDto: AddressDto = new AddressDto();
  addresses: Array<AddressTypeDto>;
  departments: Array<DepartmentDto>;
  provinces: Array<ProvinceDto>;
  districts: Array<DistrictDto>;
  roads: Array<RoadTypeDto>;
  zones: Array<ZoneTypeDto>;

  constructor(
    public dialogRef: MatDialogRef<AddressComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private addressService: AddressService,
    private _snackBar: MatSnackBar,
    private coreProvider: CoreProvider
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.getDepartments();
    this.getRoads();
    this.getZones();
    this.getAddresses();
    this.form.setValidators([
      this.oneOfControlRequired(
        this.form.get('number'),
        this.form.get('square'),
        this.form.get('km'),
        this.form.get('block')
    )]);
  }

  buildForm(): void {
    if (this.data['info'] != null) {
      var temp: AddressDto = <AddressDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        addresstypecode: [{
          value: temp.addresstypecode,
          disabled: this.disabledEdit}, Validators.required],
        departmentcode: [{
          value: temp.departmentcode,
          disabled: this.disabledEdit}, Validators.required],
        provincecode: [{
          value: temp.provincecode,
          disabled: this.disabledEdit}, Validators.required],
        districtcode: [{
          value: temp.districtcode,
          disabled: this.disabledEdit}, Validators.required],
        ubigeo: [{
          value: temp.districtcode,
          disabled: true}],
        roadtypecode: [{
          value: temp.roadtypecode,
          disabled: this.disabledEdit}, Validators.required],
        roadname: [{
          value: temp.roadname,
          disabled: this.disabledEdit}, Validators.required],
        number: [{
          value: temp.number,
          disabled: this.disabledEdit}],
        apartment: [{
          value: temp.apartment,
          disabled: this.disabledEdit}],
        inside: [{
          value: temp.inside,
          disabled: this.disabledEdit}],
        square: [[{
          value: temp.square,
          disabled: this.disabledEdit}]],
        lot: [{
          value: temp.lot,
          disabled: this.disabledEdit}],
        km: [[{
          value: temp.km,
          disabled: this.disabledEdit}]],
        block: [[{
          value: temp.block,
          disabled: this.disabledEdit}]],
        stage: [{
          value: temp.stage,
          disabled: this.disabledEdit}],
        zonetypecode: [{
          value: temp.zonetypecode,
          disabled: this.disabledEdit}, Validators.required],
        zonename: [[{
          value: temp.zonename,
          disabled: this.disabledEdit}], Validators.required],
        reference: [{
          value: temp.reference,
          disabled: this.disabledEdit}],
      });
      this.form.patchValue(temp);
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        addresstypecode: ['D03', Validators.required],
        apartment: [''],
        block: [''],
        departmentcode: ['', Validators.required],
        districtcode: ['', Validators.required],
        inside: [''],
        km: [''],
        lot: [''],
        number: [''],
        provincecode: ['', Validators.required],
        reference: [''],
        roadname: ['', Validators.required],
        roadtypecode: ['', Validators.required],
        square: [''],
        stage: [''],
        zonename: ['',  Validators.required],
        zonetypecode: ['', Validators.required],
        ubigeo: [{value: '000000', disabled: true}],
      });
    }
  }

  oneOfControlRequired(...controls: AbstractControl[]): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      for (const aControl of controls) {
        if (!Validators.required(aControl)) {
          return null;
        }
      }
      return { oneOfRequired: true };
    };
  }

  getDepartments(): void {
    this.addressService.departments().
    subscribe(data => {
      this.departments =  data.data as Array<DepartmentDto>;
      this.departments.sort(function(a, b) {
        var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase();
        if (nameA < nameB)
            return -1; 
        if (nameA > nameB)
            return 1;
        return 0;
      });
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  getProvinces(): void {
    this.addressService.filterprovince(this.form.value.departmentcode).
    subscribe(data => {
      this.provinces =  data.data as Array<ProvinceDto>;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  getUbigeo(): void {
    this.form.controls.ubigeo.setValue(this.form.value.districtcode);
  }

  getDistricts(): void {
    this.addressService.filterdistricts(this.form.value.provincecode).
    subscribe(data => {
      this.districts =  data.data as Array<DistrictDto>;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  getRoads(): void {
    this.addressService.roadtypes().
    subscribe(data => {
      this.roads =  data.data as Array<RoadTypeDto>;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  getZones(): void {
    this.addressService.zonetypes().
    subscribe(data => {
      this.zones =  data.data as Array<ZoneTypeDto>;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  getAddresses(): void {
    this.addressService.addressestypes().subscribe(
      data => { 
        this.addresses = data.data as Array<AddressTypeDto>;
      },
      error => { this.coreProvider.showMessageErrorUnexpected(); }
    );
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          addresstypecode: this.form.value.addresstypecode,
          departmentcode: this.form.value.departmentcode,
          provincecode: this.form.value.provincecode,
          districtcode: this.form.value.districtcode,
          roadtypecode: this.form.value.roadtypecode,
          roadname: this.form.value.roadname,
          number: this.form.value.number,
          apartment: this.form.value.apartment,
          inside: this.form.value.inside,
          square: this.form.value.square,
          lot: this.form.value.lot,
          km: this.form.value.km,
          block: this.form.value.block,
          stage: this.form.value.stage,
          zonetypecode: this.form.value.zonetypecode,
          zonename: this.form.value.zonename,
          reference: this.form.value.reference
      };
      console.log(info);
      if (this.title === this.operationUpdate) {
        this.addressDto.addresstypecode = info.addresstypecode,
        this.addressDto.departmentcode = info.departmentcode,
        this.addressDto.provincecode = info.provincecode,
        this.addressDto.districtcode = info.districtcode,
        this.addressDto.roadtypecode = info.roadtypecode,
        this.addressDto.roadname = info.roadname,
        this.addressDto.number = info.number,
        this.addressDto.apartment = info.apartment,
        this.addressDto.inside = info.inside,
        this.addressDto.square = info.square,
        this.addressDto.lot = info.lot,
        this.addressDto.km = info.km,
        this.addressDto.block = info.block,
        this.addressDto.stage = info.stage,
        this.addressDto.zonetypecode = info.zonetypecode,
        this.addressDto.zonename = info.zonename,
        this.addressDto.reference = info.reference,
        this.addressService.addressPut(this.addressDto, info.id).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.addressDto.addresstypecode = info.addresstypecode,
        this.addressDto.departmentcode = info.departmentcode,
        this.addressDto.provincecode = info.provincecode,
        this.addressDto.districtcode = info.districtcode,
        this.addressDto.roadtypecode = info.roadtypecode,
        this.addressDto.roadname = info.roadname,
        this.addressDto.number = info.number,
        this.addressDto.apartment = info.apartment,
        this.addressDto.inside = info.inside,
        this.addressDto.square = info.square,
        this.addressDto.lot = info.lot,
        this.addressDto.km = info.km,
        this.addressDto.block = info.block,
        this.addressDto.stage = info.stage,
        this.addressDto.zonetypecode = info.zonetypecode,
        this.addressDto.zonename = info.zonename,
        this.addressDto.reference = info.reference;
        this.addressService.addressPost(this.addressDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            info.id = data.data.id;
          },
          error => {
            console.log(error);
            this.openSnackBar('Actualizado', 'OK');
        });
        this.dialogRef.close(info);
      }
    }
    if (this.title === this.operationDelete) {
      this.addressDto.id = this.form.value.id;
      this.addressService.addressDelete(this.addressDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }
}

