import { PaymentMethodComponent } from './invoice/payment-method/payment-method.component';
import { CountryCodeComponent } from './invoice/country-code/country-code.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { EconomicActivityComponent } from './person/economic-activity/economic-activity.component';
import { PatternComponent } from './person/pattern/pattern.component';
import { RucStateComponent } from './person/ruc-state/ruc-state.component';
import { TaxRegimeComponent } from './person/tax-regime/tax-regime.component';
import { TypeDocumentIdentityComponent } from './person/type-document-identity/type-document-identity.component';
import { TypeTaxpayerComponent } from './person/type-taxpayer/type-taxpayer.component';
import { DepartmentComponent } from './address/department/department.component';
import { ProvinceComponent } from './address/province/province.component';
import { DistrictComponent } from './address/district/district.component';
import { TypeRoadComponent } from './address/type-road/type-road.component';
import { TypeZoneComponent } from './address/type-zone/type-zone.component';
import { CompanyComponent } from './person/company/company.component';
import { AddressTypeComponent } from './address/address-type/address-type.component';
import { FiscalAddressConditionComponent } from './person/fiscal-address-condition/fiscal-address-condition.component';
import { TelephoneTypeComponent } from './person/telephone-type/telephone-type.component';
import { ReferencedDocumentTypeComponent } from './invoice/referenced-document-type/referenced-document-type.component';
import { OperationTypeComponent } from './invoice/operation-type/operation-type.component';
import { DocumentTypeComponent } from './invoice/document-type/document-type.component';
import { CoinTypeComponent } from './invoice/coin-type/coin-type.component';
import { TaxConceptComponent } from './invoice/tax-concept/tax-concept.component';
import { LegendTypeComponent } from './invoice/legend-type/legend-type.component';
import { ClientComponent } from './person/client/client.component';
import { ProviderComponent } from './person/provider/provider.component';
import { SerieComponent } from './invoice/serie/serie.component';
import { BoletaComponent } from './invoice/boleta/boleta.component';
import { TaxTypeComponent } from './product/tax-type/tax-type.component';
import { DiscountChargeTypeComponent } from './product/discount-charge-type/discount-charge-type.component';
import { CategoryComponent } from './product/category/category.component';
import { SunatCodeComponent } from './product/sunat-code/sunat-code.component';
import { IGVInvolvementTypeComponent } from './product/igvinvolvement-type/igvinvolvement-type.component';
import { ISCInvolvementTypeComponent } from './product/iscinvolvement-type/iscinvolvement-type.component';
import { UnitSalePriceTypeComponent } from './product/unit-sale-price-type/unit-sale-price-type.component';
import { UnitMeasurementComponent } from './product/unit-measurement/unit-measurement.component';
import { ProductMaintenanceComponent } from './product/item/product-maintenance/product-maintenance.component';
import { ProductSearchComponent } from './product/item/product-search/product-search.component';
import { CoinComponent } from './product/coin/coin.component';
import { FacturaComponent } from './invoice/factura/factura.component';
import { CreditNoteTypeComponent } from './invoice/credit-note-type/credit-note-type.component';
import { DebitNoteTypeComponent } from './invoice/debit-note-type/debit-note-type.component';

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
            { path: 'account-settings', component: AccountSettingsComponent, data: { title: 'Configuración de temas' } },
            { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard' } },
            { path: 'economic-activity', component: EconomicActivityComponent, data: { title: 'Actividad Económica' } },
            { path: 'pattern', component: PatternComponent, data: { title: 'Padrones' } },
            { path: 'ruc-state', component: RucStateComponent, data: { title: 'Estado de RUC' } },
            { path: 'tax-regime', component: TaxRegimeComponent, data: { title: 'Régimen Tributario' } },
            { path: 'document-identity-type', component: TypeDocumentIdentityComponent, data: { title: 'Documento de Identidad' } },
            { path: 'taxpayer-type', component: TypeTaxpayerComponent, data: { title: 'Tipo de Contribuyente' } },
            { path: 'deparment', component: DepartmentComponent, data: { title: 'Departamento' } },
            { path: 'province', component: ProvinceComponent, data: { title: 'Provincia' } },
            { path: 'district', component: DistrictComponent, data: { title: 'Distrito' } },
            { path: 'road-type', component: TypeRoadComponent, data: { title: 'Tipo de Vía' } },
            { path: 'zone-type', component: TypeZoneComponent, data: { title: 'Tipo de Zona' } },
            { path: 'address-type', component: AddressTypeComponent, data: { title: 'Tipo de Dirección' } },
            { path: 'company', component: CompanyComponent, data: { title: 'Empresa' }},
            { path: 'fiscal-address-condition', component: FiscalAddressConditionComponent,
                data: { title: 'Condicion de Domicilio Fiscal' } },
            { path: 'telephone-type', component: TelephoneTypeComponent, data: { title: 'Tipo de Teléfono' } },
            { path: 'country-code', component: CountryCodeComponent, data: { title: 'Codigo de Pais' } },
            { path: 'referenced-document-type', component: ReferencedDocumentTypeComponent,
            data: { title: 'Tipo de Documento de Referencia' } },
            { path: 'operation-type', component: OperationTypeComponent, data: { title: 'Tipo de Operacion' } },
            { path: 'invoice-document-type', component: DocumentTypeComponent, data: { title: 'Tipo de Documento de Comprobante' } },
            { path: 'coin-type', component: CoinTypeComponent, data: { title: 'Tipo de Moneda' } },
            { path: 'tax-concept', component: TaxConceptComponent, data: { title: 'Tipo Concepto Tributario' } },
            { path: 'payment-method', component: PaymentMethodComponent, data: { title: 'Medio de Pago' } },
            { path: 'legend-type', component: LegendTypeComponent, data: { title: 'Leyendas' } },
            { path: 'client', component: ClientComponent, data: { title: 'Registro de Clientes' } },
            { path: 'provider', component: ProviderComponent, data: { title: 'Registro de Proveedores' } },
            { path: 'serie', component: SerieComponent, data: { title: 'Serie' } },
            { path: 'boleta', component: BoletaComponent, data: { title: 'Boleta de Venta Electrónica' } },
            { path: 'factura', component: FacturaComponent, data: { title: 'FACTURA ELECTRÓNICA' } },
            { path: 'credit-note-type', component: CreditNoteTypeComponent, data: { title: 'Tipo de Nota de Credito' } },
            { path: 'debit-note-type', component: DebitNoteTypeComponent, data: { title: 'Tipo de Nota de Debito' } },


            { path: 'category', component: CategoryComponent, data: { title: 'Categoria' } },
            { path: 'sunat-code', component: SunatCodeComponent, data: { title: 'Codigo SUNAT' } },
            { path: 'coin-type-product', component: CoinComponent, data: { title: 'Moneda Producto' } },
            { path: 'igv-involvement-type', component: IGVInvolvementTypeComponent, data: { title: 'Tipo de Afectación IGV' } },
            { path: 'isc-involvement-type', component: ISCInvolvementTypeComponent, data: { title: 'Tipo de Afectación ISC' } },
            { path: 'charge-discount-type', component: DiscountChargeTypeComponent, data: { title: 'Tipo de Cargo o Descuento' } },
            { path: 'unit-sale-price-type', component: UnitSalePriceTypeComponent, data: { title: 'Tipo de Precio de Venta Unitario' } },
            { path: 'tax-type', component: TaxTypeComponent, data: { title: 'Tipo de Tributo' } },
            { path: 'unit-measurement', component: UnitMeasurementComponent, data: { title: 'Unidad de Medida' } },
            { path: 'product-maintenance', component: ProductMaintenanceComponent, data: { title: 'Producto' } },
            { path: 'product', component: ProductSearchComponent, data: { title: 'Producto' } },


        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
