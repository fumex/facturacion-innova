import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Parent } from 'src/app/core/class/Parent';
import { Component, OnInit, Inject } from '@angular/core';
import { ProductService, DiscountChargeTypeDto } from '../../../../services/product.service';

@Component({
  selector: 'app-discount-charge-type-maintenance',
  templateUrl: './discount-charge-type-maintenance.component.html',
  styles: [],
  providers: [CoreProvider, ProductService]
})
export class DiscountChargeTypeMaintenanceComponent extends Parent implements OnInit {

  discountChargeTypeDto: DiscountChargeTypeDto = new DiscountChargeTypeDto();

  constructor(
    public dialogRef: MatDialogRef<DiscountChargeTypeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private productService: ProductService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title){
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }
  buildForm(): void{
    if (this.data['info'] !== null){
      var temp: DiscountChargeTypeDto = <DiscountChargeTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        level: [{
          value: temp.level,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else{
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        level: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string){
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          level: this.form.value.level,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.discountChargeTypeDto.code = info.code;
        this.discountChargeTypeDto.name = info.name;
        this.discountChargeTypeDto.level = info.level;
        this.discountChargeTypeDto.isactive = info.isactive;
        this.productService.discountchargetypePut(this.discountChargeTypeDto, info.id).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.discountChargeTypeDto.code = info.code;
        this.discountChargeTypeDto.name = info.name;
        this.discountChargeTypeDto.level = info.level;
        this.discountChargeTypeDto.isactive = info.isactive;
        this.productService.discountchargetypePost(this.discountChargeTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.discountChargeTypeDto.id = this.form.value.id;
      this.productService.discountchargetypeDelete(this.discountChargeTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
