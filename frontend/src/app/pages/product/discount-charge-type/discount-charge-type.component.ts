import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Parent } from 'src/app/core/class/Parent';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { Component, OnInit, ViewChild } from '@angular/core';

import *  as XLSX from 'xlsx';
import { map, filter } from 'rxjs/operators';
import { DiscountChargeTypeMaintenanceComponent } from './discount-charge-type-maintenance/discount-charge-type-maintenance.component';
import { DiscountChargeTypeDto, ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-discount-charge-type',
  templateUrl: './discount-charge-type.component.html',
  styles: [],
  providers: [CoreProvider, ProductService]
})
export class DiscountChargeTypeComponent extends Parent implements OnInit {

    listItem: Array<DiscountChargeTypeDto> = [];
    listItemNew: Array<any> = [];
    dataSource: MatTableDataSource<DiscountChargeTypeDto>;
    displayedColumns: string[] = ['code', 'name', 'level', 'status', 'button'];
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true})  sort: MatSort;

    target: DataTransfer = new DataTransfer();

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private CoreProvider: CoreProvider,
    private router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
    }
  }

  getItem() {
      this.productService.listDiscountChargeType().subscribe(data => {
          this.listItem = <Array<DiscountChargeTypeDto>> (data.data);
          this.dataSource = new MatTableDataSource(this.listItem);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          console.log(data.data);
      }, error => {
          this.CoreProvider.showMessageErrorUnexpected();
      });
  }

  onFileChange(evt: any): Array<any> {
      this.target = (evt.target) as DataTransfer;
      if (this.target.files.length !== 1) {
          throw new Error('cannot use multiple fies');
      }
      const reader: FileReader =  new FileReader();
      reader.onload = (e: any) => {
          const bstr: string  = e.target.result;
          const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
          const ws: XLSX.WorkSheet = wb.Sheets['CAT53CodCargosDscto'];
          const json = XLSX.utils.sheet_to_json(ws);
          this.listItemNew = json.map(
              obj => {return{
                code: obj['CODIGO'],
                name: obj['NOMBRE'],
                level: obj['NIVEL'],
                isactive: obj['ESTADO']
              };
          });
      };
      reader.readAsBinaryString(this.target.files[0]);
      return this.listItemNew;
  }
  
  searchNewItem(list: Array<any>) {
      const temporalArray: Array<DiscountChargeTypeDto> = JSON.parse(JSON.stringify(list));
      for (var i = temporalArray.length - 1 ; i >= 0; i--) {
          for (var j = 0; j < this.listItem.length; j++) {
              if ((temporalArray[i] !== undefined) && (String(temporalArray[i].code) === this.listItem[j].code.toString())) {
                  temporalArray.splice(i, 1);
              }
          }
      }
      return temporalArray;
  }

  inactivateItem(list: Array<any>) {
      const temporalArray: Array<DiscountChargeTypeDto> = this.listItem;
      for (var i = temporalArray.length - 1; i >= 0; i--) {
          for (var j = 0; j < list.length; j++) {
              if ((temporalArray[i] !== undefined) && (temporalArray[i].code === list[j].code.toString())) {
                  temporalArray.splice(i, 1);
              }
          }
      }
      return temporalArray;
  }

  onSubmit() {
      let list =  this.searchNewItem(this.listItemNew);
      if ( list.length !== 0) {
          this.createDiscountChargeTypeList(list);
      }
      let listDelete: Array<DiscountChargeTypeDto> =  this.inactivateItem(this.listItemNew);
      if (listDelete.length !== 0) {
          console.log(listDelete);
          listDelete.forEach(element => {
              let discountChargeTypeDto: DiscountChargeTypeDto =  new DiscountChargeTypeDto();
              discountChargeTypeDto.id =  element.id;
              this.deleteDiscountChargeType(discountChargeTypeDto);
          });
      }
  }

  createDiscountChargeTypeList(discountChargeTypeListDto: Array<DiscountChargeTypeDto>) {
      this.productService.discountchargetypeListCreate(discountChargeTypeListDto).subscribe(
          data => {
              this.openSnackBar('Guardado', 'ok');
              this.getItem();
          },
          error => {
              console.log(error);
              this.openSnackBar('Error datos no corresponden', 'OK');
          }
      );
  }

  deleteDiscountChargeType(discountChargeTypeDto: DiscountChargeTypeDto) {
      this.productService.discountchargetypeDelete(discountChargeTypeDto.id).subscribe(e => {
          this.openSnackBar('Eliminado', 'OK');
          this.getItem();
          console.log('eliminado');
      });
  }

  buildForm(): void {
      this.form = this.formBuilder.group({
          file : ['', Validators.required]
      });
  }

  returnDash() {
    this.router.navigate(['/dashboard']);
  }

  addItem() {
      let dialogRef =  this.dialog.open(DiscountChargeTypeMaintenanceComponent, {
          width: '80%',
          disableClose: true,
          data: {
              operation: this.operationNew,
              info: null
          }
      });
      dialogRef.afterClosed().subscribe(result => {
          this.getItem();
      });
  }
  updateItem(item) {
    let dialogRef = this.dialog.open(DiscountChargeTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  deleteItem(item) {
    let dialogRef = this.dialog.open(DiscountChargeTypeMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

}
