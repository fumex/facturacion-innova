import { Component, OnInit, ViewChild } from '@angular/core';
import { Parent } from '../../../core/class/Parent';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { ProductService, UnitSalePriceTypeDto } from 'src/app/services/product.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UnitSalePriceMaintenanceComponent } from './unit-sale-price-maintenance/unit-sale-price-maintenance.component';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-unit-sale-price-type',
  templateUrl: './unit-sale-price-type.component.html',
  styles: [],
  providers: [CoreProvider, ProductService]
})
export class UnitSalePriceTypeComponent extends Parent implements OnInit {

  listItem: Array<UnitSalePriceTypeDto> = [];
  listItemNew: Array<any> = [];
  dataSource: MatTableDataSource<UnitSalePriceTypeDto>;
  displayedColumns: string[] = ['code', 'name', 'status', 'button'];
  target: DataTransfer = new DataTransfer();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private coreProvider: CoreProvider,
    public router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.buildForm();
    this.getItem();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getItem(): void {
    this.productService.listUnitsalepricetype().
    subscribe(data => {
      this.listItem =  data.data as Array<UnitSalePriceTypeDto>;
      this.dataSource = new MatTableDataSource(this.listItem);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, error => {
      this.coreProvider.showMessageErrorUnexpected();
    });
  }

  onFileChange(evt: any): Array<any> {
    this.target = (evt.target) as DataTransfer;
    if (this.target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      const ws: XLSX.WorkSheet = wb.Sheets['CAT16TPrecioVentaUnit'];
      const json =  XLSX.utils.sheet_to_json(ws);
      this.listItemNew = json.map(
        obj => {return {
        code: obj['CODIGO'],
        name: obj['NOMBRE'],
        isactive: obj['ESTADO']
      }; });
    };
    reader.readAsBinaryString(this.target.files[0]);
    return this.listItemNew;
  }

  searchNewItem(list: Array<any>) {
    const temporalArray: Array<UnitSalePriceTypeDto> = JSON.parse(JSON.stringify(list));
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < this.listItem.length; j++) {
        if ((temporalArray[i] !== undefined) && ( String(temporalArray[i].code) === this.listItem[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    this.getItem();
    return temporalArray;
  }

  inactivateItem(list: Array<any>) {
    const temporalArray: Array<UnitSalePriceTypeDto> = this.listItem;
    for (var i =  temporalArray.length - 1; i >= 0; i--) {
      for (var j = 0; j < list.length; j++) {
        if ((temporalArray[i] !== undefined) && ( temporalArray[i].code === list[j].code.toString())) {
          temporalArray.splice(i, 1);
        }
      }
    }
    this.getItem();
    return temporalArray;
  }

  onSubmit() {
    let list = this.searchNewItem(this.listItemNew);
    if (list.length !== 0) {
      this.createItemList(list);
    }

    let listDelete: Array<UnitSalePriceTypeDto> = this.inactivateItem(this.listItemNew);
    if (listDelete.length !== 0) {
      console.log(listDelete);
      listDelete.forEach( element => {
        let itemDto: UnitSalePriceTypeDto = new UnitSalePriceTypeDto();
        itemDto.id = element.id;
        this.deleteItemList(itemDto);
      });
    }
    this.getItem();
  }

  createItemList(itemListDto: Array<UnitSalePriceTypeDto>) {
    console.log(itemListDto);
    this.productService.unitsalepricetypeListCreate(itemListDto).subscribe(
      data => {
        console.log(data);
        this.openSnackBar('Guardado', 'OK');
        this.getItem();
      },
      error => {
        console.log(error);
        this.openSnackBar('Error datos no corresponde', 'OK');
      });
  }

  deleteItemList(itemDto: UnitSalePriceTypeDto) {
    this.productService.unitsalepricetypeDelete(itemDto.id).subscribe(e => {
      this.openSnackBar('Eliminado', 'OK');
      console.log('eliminado');
      this.getItem();
    });
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      file: ['', Validators.required]
    });
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }

  addItem() {
    let dialogRef = this.dialog.open(UnitSalePriceMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  updateItem(element) {
    let dialogRef = this.dialog.open(UnitSalePriceMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationUpdate,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  deleteItem(element) {
    let dialogRef = this.dialog.open(UnitSalePriceMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationDelete,
        info: element
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getItem();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

}
