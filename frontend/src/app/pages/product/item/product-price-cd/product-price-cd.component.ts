import { Component, OnInit, Inject } from '@angular/core';
import { Parent } from '../../../../core/class/Parent';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { ProductService, DiscountChargeTypeDto } from 'src/app/services/product.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-price-cd',
  templateUrl: './product-price-cd.component.html',
  providers: [CoreProvider, ProductService]
})
export class ProductPriceCDComponent extends Parent implements OnInit {
  discountchargetypes: Array<DiscountChargeTypeDto>;

  constructor(
    public dialogRef: MatDialogRef<ProductPriceCDComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private productService: ProductService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data.operation;
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.getDiscountchargetypes();
  }

  buildForm(): void {
    if (this.data.info != null) {
      var temp = this.data.info;
      this.form = this.formBuilder.group({
        isactive: [{value: temp.isactive, disabled: this.disabledEdit}, Validators.required],
        discountchargetypecode: [{value: temp.isactive, disabled: this.disabledEdit}, Validators.required],
        unitchargediscountamount: [{value: temp.igvinvolvementtypecode, disabled: this.disabledEdit}, Validators.required],
      });
    } else {
      this.form = this.formBuilder.group({
        isactive: [true, Validators.required],
        discountchargetypecode: ['', Validators.required],
        name: [''],
        unitchargediscountamount: [0, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  getDiscountchargetypes() {
    this.productService.listDiscountChargeTypeItem().subscribe(
      data => this.discountchargetypes = data.data as Array<DiscountChargeTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  amountType(): number {
    if ((this.form.value.discountchargetypecode.code === '00')||(this.form.value.discountchargetypecode.code === '01')) {
      return this.form.value.unitchargediscountamount * -1;
    } else {
      return this.form.value.unitchargediscountamount;
    }
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
        discountchargetypecode: this.form.value.discountchargetypecode.code,
        name: this.form.value.discountchargetypecode.name,
        unitchargediscountamount: this.amountType(),
        isactive: this.form.value.isactive
      };
      this.dialogRef.close(info);
      this.openSnackBar('Agregado a producto', 'OK');
    }
  }
}
