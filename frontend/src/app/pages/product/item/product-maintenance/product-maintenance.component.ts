import { filter } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { CoreProvider } from '../../../../core/provider/coreProvider';
import { ProductService, ProductDto, CategoryDto, TaxTypeDto,
  UnitMeasurementDto, UnitSalePriceTypeDto, DiscountChargeTypeDto,
  IgvInvolvementTypeDto, IscInvolvementTypeDto, CoinTypeDto } from '../../../../services/product.service';
import { Parent } from '../../../../core/class/Parent';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ProductPriceTaxComponent } from '../product-price-tax/product-price-tax.component';
import { ProductPriceCDComponent } from '../product-price-cd/product-price-cd.component';
import { ProductPriceDto, ProductDetailDto, ProductPriceTaxDto, ProductPriceChargeDiscountDto } from '../../../../services/product.service';
import { CategoryMaintenanceComponent } from '../../category/category-maintenance/category-maintenance.component';


@Component({
  selector: 'app-product-maintenance',
  templateUrl: './product-maintenance.component.html',
  providers: [CoreProvider, ProductService]
})
export class ProductMaintenanceComponent extends Parent implements OnInit {

  item: ProductDto = new ProductDto();
  itemDetail: ProductDetailDto = new ProductDetailDto();
  itemPrice: ProductPriceDto = new ProductPriceDto();
  itemPriceTax: ProductPriceTaxDto = new ProductPriceTaxDto();
  itemPriceCD: ProductPriceChargeDiscountDto = new ProductPriceChargeDiscountDto();

  categories: Array<CategoryDto>;
  measurementunits: Array<UnitMeasurementDto>;
  unitsalepricetypes: Array<UnitSalePriceTypeDto>;
  coins: Array<CoinTypeDto>;
  tempTaxes: Array<any> = [];
  tempCD: Array<any> = [];
  price: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private productService: ProductService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { super(); }

  ngOnInit() {
    this.title = localStorage.getItem('productOperation');
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.initForm();
  }

  buildForm(): void {
    let temp: ProductDto;
    if (this.title !== this.operationNew) {
      temp = JSON.parse(localStorage.getItem('product')) as ProductDto;
    } else {
      temp = new ProductDto();
      temp.id = '0';
      temp.stock = 1;
      temp.isactive = true;
    }
    this.form = this.formBuilder.group({
      id: [temp.id],
      code: [{value: temp.code, disabled: this.disabledEdit}, Validators.required],
      barcode: [{value: temp.barcode, disabled: this.disabledEdit}],
      sunatcode: [{value: temp.sunatcode, disabled: this.disabledEdit}],
      denomination: [{value: temp.denomination, disabled: this.disabledEdit}, Validators.required],
      description: [{value: temp.description, disabled: this.disabledEdit}],
      stock: [{value: temp.stock, disabled: this.disabledEdit}, Validators.required],
      categorycode: [{value: temp.categorycode, disabled: this.disabledEdit}, Validators.required],
      isactive: [{value: temp.isactive, disabled: this.disabledEdit}, Validators.required],

      unitsalevalue: [{value: 0, disabled: this.disabledEdit}, Validators.required],
      unitbaseamount: [{value: 0, disabled: this.disabledEdit}],
      unitsaleprice: [{value: 0, disabled: this.disabledEdit}, Validators.required],
      unitsalepricetypecode: [{value: '01', disabled: this.disabledEdit}, Validators.required],
      coincode: [{value: 'PEN', disabled: this.disabledEdit}, Validators.required],

      unitcode: [{value: '', disabled: this.disabledEdit}, Validators.required],
      unitigvamount: [{value: 0, disabled: this.disabledEdit}]
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
        id: this.form.value.id,
        code: this.form.value.code,
        barcode: this.form.value.barcode,
        sunatcode: this.form.value.sunatcode,
        denomination: this.form.value.denomination,
        description: this.form.value.description,
        stock: this.form.value.stock,
        categorycode: this.form.value.categorycode,
        isactive: this.form.value.isactive,

        unitsaleprice: this.form.value.unitsaleprice,
        unitbaseamount: this.form.value.unitbaseamount,
        unitsalevalue: this.form.value.unitsalevalue,
        unitsalepricetypecode: this.form.value.unitsalepricetypecode,
        coincode: this.form.value.coincode,

        unitcode: this.form.value.unitcode,
        unitigvamount: this.form.value.unitigvamount
      };
      // console.log(info);
      if (this.title === this.operationUpdate) {
        this.item.barcode = info.barcode,
        this.item.categorycode = info.categorycode,
        this.item.code = info.code,
        this.item.denomination = info.denomination,
        this.item.description = info.description,
        this.item.stock = info.stock,
        this.item.sunatcode = info.sunatcode,
        this.item.isactive = info.isactive,
        // this.item.unitcode = info.unitcode,
        this.itemPrice.unitsalevalue = info.unitsalevalue,
        this.itemPrice.unitbaseamount = info.unitbaseamount,
        this.itemPrice.unitsaleprice = info.unitsaleprice,
        this.itemPrice.unitsalepricetypecode = info.unitsalepricetypecode,
        this.itemPrice.coincode = info.coincode,

        this.productService.productPut(info.id, this.item).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
          this.returnDash();
          console.log(e);
        }, error => console.log(error));
      } else {
        delete info.id;
        this.item.barcode = info.barcode,
        this.item.categorycode = info.categorycode,
        this.item.code = info.code,
        this.item.denomination = info.denomination,
        this.item.description = info.description,
        this.item.stock = info.stock,
        this.item.sunatcode = info.sunatcode,
        this.item.isactive = info.isactive,
        // this.item.unitcode = info.unitcode,
        this.itemPrice.unitsalevalue = info.unitsalevalue,
        this.itemPrice.unitbaseamount = info.unitbaseamount,
        this.itemPrice.unitsaleprice = info.unitsaleprice,
        this.itemPrice.unitsalepricetypecode = info.unitsalepricetypecode,
        this.itemPrice.coincode = info.coincode,
        this.productService.productPost(this.item).subscribe(
          data => {
            if (data.data !== null) {
              this.itemPrice.productcode = data.data.id;
              this.productService.productpricePost(this.itemPrice).subscribe(
                e => { if (e.data !== null) {
                  if (this.tempCD !== null) {
                    this.tempCD.forEach(cd => {
                      this.itemPriceCD.idproductprice = e.data.id;
                      this.itemPriceCD.discountchargetypecode = cd.discountchargetypecode;
                      this.itemPriceCD.unitchargediscountamount = cd.unitchargediscountamount;
                      this.itemPriceCD.isactivate = cd.isactivate;
                      this.productService.productpricechargediscountPost(this.itemPriceCD).subscribe(
                        datacd => {
                          if (datacd !== null) {
                            this.itemPriceCD = null;
                            console.log('Cargo/Descuento agregados ..... ');
                          } else { this.coreProvider.showMessageError(datacd.error.message); }
                        },
                        error => this.coreProvider.showMessageErrorUnexpected());
                    });
                  } else { console.log('No hay Cargos/Descuent que agregar..... '); }
                  if (this.tempTaxes !== null) {
                    this.tempTaxes.forEach(tax => {
                      this.itemPriceTax.idproductprice = e.data.id;
                      this.itemPriceTax.taxtypecode = tax.taxtypecode;
                      this.itemPriceTax.igvinvolvementtypecode = tax.igvinvolvementtypecode;
                      this.itemPriceTax.iscinvolvementtypecode = tax.iscinvolvementtypecode;
                      this.itemPriceTax.unittaxamount = tax.unittaxamount;
                      this.itemPriceTax.isactive = tax.isactivate;
                      this.productService.productpricetaxPost(this.itemPriceTax).subscribe(
                        datatax => {
                          if (datatax !== null) {
                            console.log('Impuestos agregados ..... ');
                          } else { this.coreProvider.showMessageError(datatax.error.message); }
                        },
                        error => this.coreProvider.showMessageErrorUnexpected());
                    });
                  } else { console.log('No hay impuestos que agregar..... '); }
                  console.log('Precio agregado..... ');
                  this.openSnackBar('Producto agregado', '0K');
                  this.returnDash();
                } else {
                  this.coreProvider.showMessageError(e.error.message);
                }},
                error => this.coreProvider.showMessageErrorUnexpected());
              console.log('Producto agregado.....', data.data);
            } else {
              this.coreProvider.showMessageError(data.error.message);
            }
          },
          error => this.coreProvider.showMessageErrorUnexpected());
      }
    }
    if (this.title === this.operationDelete) {
      this.item.id = this.form.value.id;
      this.productService.productDelete(this.item.id).subscribe(e => {
        this.returnDash();
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  getCategories() {
    this.productService.categories().subscribe(
      data => this.categories = data.data as Array<CategoryDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getMeasurementunits() {
    this.productService.unitmeasurements().subscribe(
      data => this.measurementunits = data.data as Array<UnitMeasurementDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getUnitsalepricetypes() {
    this.productService.unitsalepricetypes().subscribe(
      data => this.unitsalepricetypes = data.data as Array<UnitSalePriceTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getCoins() {
    this.productService.cointypes().subscribe(
      data => this.coins = data.data as Array<CoinTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  setBaseAmount() {
    this.price = true;
    let additional = this.tempCD.map(t => t.unitchargediscountamount).reduce((acc, value) => acc + value, 0);
    this.form.controls.unitbaseamount.setValue(this.form.value.unitsalevalue +  additional);
  }

  calculateIGV() {
    let temp = this.form.value.unitbaseamount +  this.calculateISC();
    this.form.controls.unitigvamount.setValue(temp * 0.18);
  }

  recalculateIGV() {
    let found = this.tempTaxes.find(element => element.taxabbreviation === 'ISC');
    console.log(found);
    if (found !== undefined) {
      let tempIGV = this.tempTaxes.find(e => e.taxabbreviation === 'IGV');
      if (tempIGV !== undefined) {
         tempIGV.unittaxamount =  (this.form.value.unitbaseamount + found.unittaxamount)  * 0.18;
      }
    } else {
      let tempIGV2 = this.tempTaxes.find(element => element.taxabbreviation === 'IGV');
      if (tempIGV2 !== undefined) {
         tempIGV2.unittaxamount =  this.form.value.unitbaseamount * 0.18;
         this.form.controls.unitigvamount.setValue(tempIGV2.unittaxamount)
      }
    }
  }

  calculateISC(): number {
    if (this.tempTaxes.length !== 0) {
      let found2 = this.tempTaxes.find(result => (result.taxabbreviation === 'ISC') && (result.taxtypecode === '2000'));
      if (found2 !== undefined) {
        return found2.unittaxamount as number;
      }
    } else {
      return 0;
    }
  }

  calculateSalePrice() {
    let temp = this.tempTaxes.map(t => t.unittaxamount as number).reduce((acc, value) => acc + value, 0);
    this.form.controls.unitsaleprice.setValue(this.form.value.unitbaseamount + temp);
  }

  initForm() {
    this.getCategories();
    this.getMeasurementunits();
    this.getUnitsalepricetypes();
    this.getCoins();
    this.calculateISC();
  }

  clean() {
    this.buildForm();
    this.categories = [];
    this.measurementunits = [];
    this.tempCD = [];
    this.tempTaxes = [];
    this.price = false;
  }

  returnDash() {
    this.router.navigate([ '/product']);
  }

  addChargeDiscount(): void {
    let dialogRef = this.dialog.open(ProductPriceCDComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data !== true) {
        this.tempCD.push(data);
      }
      this.setBaseAmount();
      console.log(data);
    });
  }

  deleteCD(i) {
    this.tempCD.splice(i, 1);
    this.setBaseAmount();
  }

  addTax(): void {
    let dialogRef = this.dialog.open(ProductPriceTaxComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: this.form.value.unitbaseamount + this.calculateISC()
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data !== true) {
        console.log(data);
        this.tempTaxes.push(data);
        if (data.taxabbreviation === 'ISC') {
          console.log('Recaculando igv *****************************');
          this.recalculateIGV();
        }
      }
      this.calculateSalePrice();
    });
  }

  deleteTax(i) {
    this.tempTaxes.splice(i, 1);
    this.recalculateIGV();
    this.calculateSalePrice();
  }

  addCategory() {
    let dialogRef = this.dialog.open(CategoryMaintenanceComponent, {
      width: '80%',
      disableClose: true,
      data: {
        operation: this.operationNew,
        info: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getCategories();
    });
  }

  applyFilter(filterValue: string) {
   
  }

}
