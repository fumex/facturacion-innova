import { Component, OnInit, Inject } from '@angular/core';
import { Parent } from '../../../../core/class/Parent';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductService, IgvInvolvementTypeDto, IscInvolvementTypeDto, TaxTypeDto } from '../../../../services/product.service';

@Component({
  selector: 'app-product-price-tax',
  templateUrl: './product-price-tax.component.html',
  providers: [CoreProvider, ProductService]
})
export class ProductPriceTaxComponent extends Parent implements OnInit {
  igvinvolvementtypes: Array<IgvInvolvementTypeDto>;
  iscinvolvementtypes: Array<IscInvolvementTypeDto>;
  taxes: Array<TaxTypeDto>;
  taxigv: boolean = false;
  taxisc: boolean = false;
  unitbaseamount: number = 0;

  constructor(
    public dialogRef: MatDialogRef<ProductPriceTaxComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private productService: ProductService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data.operation;
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
    this.initForm();
  }

  buildForm(): void {
    if (this.data.info != null) {
      this.unitbaseamount = this.data.info as number;
    } else {
      this.unitbaseamount = 0;
    }
    this.form = this.formBuilder.group({
      isactive: [true, Validators.required],
      igvinvolvementtypecode: [''],
      iscinvolvementtypecode: [' '],
      taxabbreviation: [''],
      taxtypecode: ['', Validators.required],
      unittaxamount: [0, Validators.required]
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  getIgvinvolvementtypes() {
    this.productService.igvinvolvementtypes().subscribe(
      data => this.igvinvolvementtypes = data.data as Array<IgvInvolvementTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getIscinvolvementtypes() {
    this.productService.iscinvolvementtypes().subscribe(
      data => this.iscinvolvementtypes = data.data as Array<IscInvolvementTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  getTaxes() {
    this.productService.taxtypes().subscribe(
      data => this.taxes = data.data as Array<TaxTypeDto>,
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  changeTax() {
    if (this.form.value.taxtypecode.code === '1000') {
      this.taxigv = true;
      this.taxisc = false;
      this.form.controls.igvinvolvementtypecode.setValue('10');
      this.form.controls.iscinvolvementtypecode.setValue(null);
      this.form.controls.unittaxamount.setValue(this.unitbaseamount * 0.18);
      console.log('IGV');
    } else if (this.form.value.taxtypecode.code === '2000') {
      this.taxigv = false;
      this.taxisc = true;
      this.form.controls.igvinvolvementtypecode.setValue(null);
      this.form.controls.iscinvolvementtypecode.setValue('01');
      console.log('ISC');
    } else {
        this.taxigv = false;
        this.taxisc = false;
        this.form.controls.igvinvolvementtypecode.setValue(null);
        this.form.controls.iscinvolvementtypecode.setValue(null);
        console.log('OTHER TAX');
    }
  }

  initForm() {
    this.getIgvinvolvementtypes();
    this.getIscinvolvementtypes();
    this.getTaxes();
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
        igvinvolvementtypecode: this.form.value.igvinvolvementtypecode,
        iscinvolvementtypecode: this.form.value.iscinvolvementtypecode,
        taxtypecode: this.form.value.taxtypecode.code,
        taxabbreviation: this.form.value.taxtypecode.abbreviation,
        unittaxamount: this.form.value.unittaxamount,
        isactive: this.form.value.isactive
      };
      this.dialogRef.close(info);
      this.openSnackBar('Tributo agregado a producto', 'OK');
    }
  }
}
