import { Component, OnInit, ViewChild } from '@angular/core';
import { Parent } from '../../../../core/class/Parent';
import { CoreProvider } from '../../../../core/provider/coreProvider';
import { ProductService, ProductDto, ProductPriceDto } from '../../../../services/product.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styles: [],
  providers: [CoreProvider, ProductService]
})
export class ProductSearchComponent extends Parent implements OnInit {
  listItemFull: Array<any> = [];
  listItem: Array<ProductDto> = [];
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['code', 'name', 'stock', 'saleprice', 'isactive', 'button'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private coreProvider: CoreProvider,
    public router: Router,
    public dialog: MatDialog
  ) { super(); }

  ngOnInit() {
    this.getItemFull();
  }

  getItemFull(): void {
    this.productService.listProductsFull().subscribe(
      data => {
        if ((data.data !== null) || (data.data !== undefined)) {
          this.listItemFull = data.data;
          this.dataSource = new MatTableDataSource(this.listItemFull);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          console.log(this.listItemFull);
        } else {
          this.coreProvider.showMessageError(data.error.message);
        }
      },
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

 filterByProperty(prop, value) {
    var filtered = [];
    var array = this.dataSource.data;
    for (var i = 0; i < array.length; i++) {
        var obj = array[i].productDto;
        for (var key in obj) {
            if (typeof(obj[key] === 'object')) {
                var item = obj[key];
                if (item[prop] === value) {
                    filtered.push(item);
                }
            }
        }
    }
    return filtered;
  }


  updateItem(item) {
    this.router.navigate(['/product-maintenance']);
    localStorage.setItem('productOperation', this.operationUpdate);
    localStorage.setItem('product', JSON.stringify(item));
  }

  viewItem(item) {
    this.router.navigate(['/product-maintenance']);
    localStorage.setItem('productOperation', this.operationView);
    localStorage.setItem('product', JSON.stringify(item));
  }

  deleteItem(item) {
    this.router.navigate(['/product-maintenance']);
    localStorage.setItem('productOperation', this.operationDelete);
    localStorage.setItem('product', JSON.stringify(item));
  }

  addItem() {
    this.router.navigate(['/product-maintenance']);
    localStorage.setItem('productOperation', this.operationNew);
    localStorage.setItem('product', '');
  }

  returnDash() {
    this.router.navigate([ '/dashboard']);
  }
}
