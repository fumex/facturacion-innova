import { Component, OnInit, Inject } from '@angular/core';
import { Parent } from '../../../../core/class/Parent';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { ProductService } from 'src/app/services/product.service';
import { IscInvolvementTypeDto } from '../../../../services/product.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-iscinvolvement-maintenance',
  templateUrl: './iscinvolvement-maintenance.component.html',
  styles: [],
  providers: [CoreProvider, ProductService]
})
export class ISCInvolvementMaintenanceComponent extends Parent implements OnInit {

  item: IscInvolvementTypeDto = new IscInvolvementTypeDto();
  constructor(
    public dialogRef: MatDialogRef<ISCInvolvementMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data.operation;
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data.info != null) {
      var temp: IscInvolvementTypeDto = this.data.info as IscInvolvementTypeDto;
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        tax: [{
          value: temp.tax,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        tax: [0.50, Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          tax: this.form.value.tax,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.item.code = info.code;
        this.item.name = info.name;
        this.item.tax = info.tax;
        this.item.isactive = info.isactive;
        this.productService.iscinvolvementtypePut(info.id, this.item).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.item.code = info.code;
        this.item.name = info.name;
        this.item.tax = info.tax;
        this.item.isactive = info.isactive;
        this.productService.iscinvolvementtypePost(this.item).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar(error, 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.item.id = this.form.value.id;
      this.productService.iscinvolvementtypeDelete(this.item.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
