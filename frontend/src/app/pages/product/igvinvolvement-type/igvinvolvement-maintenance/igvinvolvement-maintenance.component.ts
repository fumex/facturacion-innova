import { Component, OnInit, Inject } from '@angular/core';
import { Parent } from '../../../../core/class/Parent';
import { IgvInvolvementTypeDto, ProductService } from '../../../../services/product.service';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-igvinvolvement-maintenance',
  templateUrl: './igvinvolvement-maintenance.component.html',
  styles: [],
  providers: [CoreProvider, ProductService]
})
export class IGVInvolvementMaintenanceComponent extends Parent implements OnInit {

  item: IgvInvolvementTypeDto = new IgvInvolvementTypeDto();
  constructor(
    public dialogRef: MatDialogRef<IGVInvolvementMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data.operation;
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data.info != null) {
      var temp: IgvInvolvementTypeDto = this.data.info as IgvInvolvementTypeDto;
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        taxcode: [{
          value: temp.taxcode,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        taxcode: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          taxcode: this.form.value.taxcode,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.item.code = info.code;
        this.item.name = info.name;
        this.item.taxcode = info.taxcode;
        this.item.isactive = info.isactive;
        this.productService.igvinvolvementtypePut(info.id, this.item).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.item.code = info.code;
        this.item.name = info.name;
        this.item.taxcode = info.taxcode;
        this.item.isactive = info.isactive;
        this.productService.igvinvolvementtypePost(this.item).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar(error, 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.item.id = this.form.value.id;
      this.productService.igvinvolvementtypeDelete(this.item.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
