import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxTypeComponent } from './tax-type/tax-type.component';
import { TaxTypeMaintenanceComponent } from './tax-type/tax-type-maintenance/tax-type-maintenance.component';
import { DiscountChargeTypeComponent } from './discount-charge-type/discount-charge-type.component';
import {
  DiscountChargeTypeMaintenanceComponent
} from './discount-charge-type/discount-charge-type-maintenance/discount-charge-type-maintenance.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/shared/angular-material.module';
import { CoreProvider } from 'src/app/core/provider/coreProvider';
import { UnitMeasurementComponent } from './unit-measurement/unit-measurement.component';
import { UnitMeasurementMaintenanceComponent
} from './unit-measurement/unit-measurement-maintenance/unit-measurement-maintenance.component';
import { IGVInvolvementTypeComponent } from './igvinvolvement-type/igvinvolvement-type.component';
import { IGVInvolvementMaintenanceComponent } from './igvinvolvement-type/igvinvolvement-maintenance/igvinvolvement-maintenance.component';
import { ISCInvolvementTypeComponent } from './iscinvolvement-type/iscinvolvement-type.component';
import { ISCInvolvementMaintenanceComponent } from './iscinvolvement-type/iscinvolvement-maintenance/iscinvolvement-maintenance.component';
import { CategoryComponent } from './category/category.component';
import { CategoryMaintenanceComponent } from './category/category-maintenance/category-maintenance.component';
import { UnitSalePriceTypeComponent } from './unit-sale-price-type/unit-sale-price-type.component';
import { UnitSalePriceMaintenanceComponent } from './unit-sale-price-type/unit-sale-price-maintenance/unit-sale-price-maintenance.component';
import { SunatCodeComponent } from './sunat-code/sunat-code.component';
import { SunatCodeMaintenanceComponent } from './sunat-code/sunat-code-maintenance/sunat-code-maintenance.component';
import { ProductMaintenanceComponent } from './item/product-maintenance/product-maintenance.component';
import { ProductSearchComponent } from './item/product-search/product-search.component';
import { CoinComponent } from './coin/coin.component';
import { CoinMaintenanceComponent } from './coin/coin-maintenance/coin-maintenance.component';
import { ProductPriceTaxComponent } from './item/product-price-tax/product-price-tax.component';
import { ProductPriceCDComponent } from './item/product-price-cd/product-price-cd.component';



@NgModule({
  declarations: [
    TaxTypeComponent,
    TaxTypeMaintenanceComponent,
    DiscountChargeTypeComponent,
    DiscountChargeTypeMaintenanceComponent,
    UnitMeasurementComponent,
    UnitMeasurementMaintenanceComponent,
    IGVInvolvementTypeComponent,
    IGVInvolvementMaintenanceComponent,
    ISCInvolvementTypeComponent,
    ISCInvolvementMaintenanceComponent,
    CategoryComponent,
    CategoryMaintenanceComponent,
    UnitSalePriceTypeComponent,
    UnitSalePriceMaintenanceComponent,
    SunatCodeComponent,
    SunatCodeMaintenanceComponent,
    ProductMaintenanceComponent,
    ProductSearchComponent,
    CoinComponent,
    CoinMaintenanceComponent,
    ProductPriceTaxComponent,
    ProductPriceCDComponent
  ],
  exports: [
    TaxTypeMaintenanceComponent,
    DiscountChargeTypeMaintenanceComponent,
    UnitMeasurementMaintenanceComponent,
    IGVInvolvementMaintenanceComponent,
    ISCInvolvementMaintenanceComponent,
    CategoryMaintenanceComponent,
    UnitSalePriceMaintenanceComponent,
    SunatCodeMaintenanceComponent,
    CoinMaintenanceComponent,
    ProductPriceTaxComponent,
    ProductPriceCDComponent
  ],
  entryComponents: [
    TaxTypeMaintenanceComponent,
    DiscountChargeTypeMaintenanceComponent,
    UnitMeasurementMaintenanceComponent,
    IGVInvolvementMaintenanceComponent,
    ISCInvolvementMaintenanceComponent,
    CategoryMaintenanceComponent,
    UnitSalePriceMaintenanceComponent,
    SunatCodeMaintenanceComponent,
    CoinMaintenanceComponent,
    ProductPriceTaxComponent,
    ProductPriceCDComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    AngularMaterialModule
  ],
  providers: [ CoreProvider ],
})
export class ProductModule { }
