import { Component, OnInit, Inject } from '@angular/core';
import { Parent } from '../../../../core/class/Parent';
import { ProductService, CoinTypeDto } from '../../../../services/product.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { CoreProvider } from 'src/app/core/provider/coreProvider';

@Component({
  selector: 'app-coin-maintenance',
  templateUrl: './coin-maintenance.component.html',
  styles: [],
  providers: [ProductService]
})
export class CoinMaintenanceComponent extends Parent implements OnInit {

  coinTypeDto: CoinTypeDto = new CoinTypeDto();

  constructor(
    public dialogRef: MatDialogRef<CoinMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private productService: ProductService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title){
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] !== null){
      var temp: CoinTypeDto = <CoinTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        badge: [{
          value: temp.badge,
          disabled: this.disabledEdit},
          Validators.required],
        number: [{
          value: temp.number,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else{
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        badge: ['', Validators.required],
        number: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string){
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          number: this.form.value.number,
          badge: this.form.value.badge,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.coinTypeDto.code = info.code;
        this.coinTypeDto.name = info.name;
        this.coinTypeDto.number = info.number;
        this.coinTypeDto.badge = info.badge;
        this.coinTypeDto.isactive = info.isactive;
        this.productService.cointypePut(this.coinTypeDto, info.id).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.coinTypeDto.code = info.code;
        this.coinTypeDto.name = info.name;
        this.coinTypeDto.number = info.number;
        this.coinTypeDto.badge = info.badge;
        this.coinTypeDto.isactive = info.isactive;
        this.productService.cointypePost(this.coinTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.coinTypeDto.id = this.form.value.id;
      this.productService.cointypeDelete(this.coinTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }
}
