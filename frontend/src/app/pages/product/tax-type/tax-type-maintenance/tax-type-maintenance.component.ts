import { CoreProvider } from './../../../../core/provider/coreProvider';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Parent } from './../../../../core/class/Parent';
import { Component, OnInit, Inject } from '@angular/core';
import { ProductService, TaxTypeDto } from '../../../../services/product.service';


@Component({
  selector: 'app-tax-type-maintenance',
  templateUrl: './tax-type-maintenance.component.html',
  styles: [],
  providers: [CoreProvider, ProductService]
})
export class TaxTypeMaintenanceComponent extends Parent implements OnInit {
  taxTypeDto: TaxTypeDto = new TaxTypeDto();
  constructor(
    public dialogRef: MatDialogRef<TaxTypeMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private productService: ProductService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data['operation'];
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
          this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data['info'] !== null) {
      var temp: TaxTypeDto = <TaxTypeDto> this.data['info'];
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        codeinternational: [{
          value: temp.codeinternational,
          disabled: this.disabledEdit},
          Validators.required],
        abbreviation: [{
          value: temp.abbreviation,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        codeinternational: ['', Validators.required],
        abbreviation: ['AA', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          codeinternational:  this.form.value.codeinternational,
          abbreviation:  this.form.value.abbreviation,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.taxTypeDto.code = info.code;
        this.taxTypeDto.name = info.name;
        this.taxTypeDto.codeinternational = info.codeinternational;
        this.taxTypeDto.abbreviation = info.abbreviation;
        this.taxTypeDto.isactive = info.isactive;
        this.productService.taxtypePut(info.id, this.taxTypeDto).subscribe(
          data => {
            this.openSnackBar('Actualizado', 'OK');
            this.dialogRef.close();
            console.log(data.data);
          }, error => console.log(error));
      } else {
        delete info.id;
        this.taxTypeDto.code = info.code;
        this.taxTypeDto.name = info.name;
        this.taxTypeDto.codeinternational = info.codeinternational;
        this.taxTypeDto.abbreviation = info.abbreviation;
        this.taxTypeDto.isactive = info.isactive;
        this.productService.taxtypePost(this.taxTypeDto).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
            this.dialogRef.close();
          },
          error => console.log(error));
      }
    }

    if (this.title === this.operationDelete) {
      this.taxTypeDto.id = this.form.value.id;
      this.productService.taxtypeDelete(this.taxTypeDto.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

}
