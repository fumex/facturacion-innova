import { Component, OnInit, Inject } from '@angular/core';
import { CoreProvider } from '../../../../core/provider/coreProvider';
import { Parent } from '../../../../core/class/Parent';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductService, CategoryDto } from '../../../../services/product.service';

@Component({
  selector: 'app-category-maintenance',
  templateUrl: './category-maintenance.component.html',
  providers: [CoreProvider, ProductService]
})
export class CategoryMaintenanceComponent extends Parent implements OnInit {

  item: CategoryDto = new CategoryDto();

  constructor(
    public dialogRef: MatDialogRef<CategoryMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private coreProvider: CoreProvider,
    private productService: ProductService,
    private _snackBar: MatSnackBar
  ) { super(); }

  ngOnInit() {
    this.title = this.data.operation;
    switch (this.title) {
      case this.operationDelete:
        this.disabledEdit = true;
        break;
      case this.operationView:
        this.disabledEdit = true;
        break;
      case this.operationNew:
        this.newCode();
        this.disabledEdit = false;
        break;
      case this.operationUpdate:
        this.disabledEdit = false;
        break;
      default:
        this.disabledEdit = true;
    }
    this.buildForm();
  }

  buildForm(): void {
    if (this.data.info != null) {
      var temp: CategoryDto = this.data.info as CategoryDto;
      this.form = this.formBuilder.group({
        id: [temp.id],
        code: [{
          value: temp.code,
          disabled: this.disabledEdit},
          Validators.required],
        name: [{
          value: temp.name,
          disabled: this.disabledEdit},
          Validators.required],
        isactive: [{
          value: temp.isactive,
          disabled: this.disabledEdit},
          Validators.required]
      });
    } else {
      this.form = this.formBuilder.group({
        id: [0],
        code: ['', Validators.required],
        name: ['', Validators.required],
        isactive: [true, Validators.required]
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'right'
    });
  }

  save() {
    if (this.title === this.operationNew || this.title === this.operationUpdate) {
      var info = {
          id: this.form.value.id,
          code: this.form.value.code,
          name: this.form.value.name,
          isactive: this.form.value.isactive
      };
      if (this.title === this.operationUpdate) {
        this.item.code = info.code;
        this.item.name = info.name;
        this.item.isactive = info.isactive;
        this.productService.categoryPut(this.item, info.id).subscribe(e => {
          this.openSnackBar('Actualizado', 'OK');
        }, error => console.log(error));
      } else {
        delete info.id;
        this.item.code = info.code;
        this.item.name = info.name;
        this.item.isactive = info.isactive;
        this.productService.categoryPost(this.item).subscribe(
          data => {
            this.openSnackBar('Guardado', 'OK');
          },
          error => {
            console.log(error);
            this.openSnackBar(error, 'OK');
          });
      }
    }
    if (this.title === this.operationDelete) {
      this.item.id = this.form.value.id;
      this.productService.categoryDelete(this.item.id).subscribe(e => {
        this.openSnackBar('Eliminado', 'OK');
      });
    }
  }

  newCode() {
    this.productService.listCategory().subscribe( data => {
      if (data.data !== null) {
        let length = data.data.length;
        console.log(data.data.length);
        if ( (length === 0) || (length < 9)) {
          this.form.controls.code.setValue('CAT0' + (length + 1));
        } else {
          this.form.controls.code.setValue('CAT' + (length + 1));
        }
      } else { this.coreProvider.showMessageError(data.error.message); }},
      error => this.coreProvider.showMessageErrorUnexpected()
    );
  }

}
