import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { PagesModule } from './pages/pages.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
// services
import { ServiceModule } from './services/service.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as PersonApiService from './services/person.service';
import * as AddressApiService from './services/address.service';
import * as InvoiceApiService from './services/invoice.service';
import * as ProductService from './services/product.service';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    PagesModule,
    AppRoutingModule,
    ServiceModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [PersonApiService.PersonService, { provide: PersonApiService.API_BASE_URL, useValue: environment.personService },
              AddressApiService.AddressService, { provide: AddressApiService.API_BASE_URL, useValue: environment.addressService },
              InvoiceApiService.InvoiceService, { provide: InvoiceApiService.API_BASE_URL, useValue: environment.invoiceService },
              ProductService.ProductService, { provide: ProductService.API_BASE_URL, useValue: environment.productService }],
  bootstrap: [AppComponent]
})
export class AppModule { }
