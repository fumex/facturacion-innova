import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonService,
         SettingsService,
         SharedService,
         SidebarService,
         AddressService} from './service.index';

@NgModule({
  declarations: [],
  providers: [
    SettingsService,
    SharedService,
    SidebarService,
    PersonService,
    AddressService
  ],
  imports: [
    CommonModule
  ]
})
export class ServiceModule { }
