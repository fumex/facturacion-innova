import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  settings: Settings = {
    themeUrl: 'assets/css/colors/megna.css',
    theme: 'megna'
  };

  constructor() {
    this.getSettings();
  }

  saveSettings() {
    localStorage.setItem('ajustes', JSON.stringify(this.settings));
  }

  getSettings() {
    if ( localStorage.getItem('ajustes') ) {
      this.settings = JSON.parse( localStorage.getItem('ajustes') );
      this.applyTheme(this.settings.theme);
    } else {
      this.applyTheme(this.settings.theme);
    }
  }

  applyTheme( theme: string ) {
    const url = `assets/css/colors/${theme}.css`;
    document.getElementById('theme').setAttribute('href', url);
    this.settings.themeUrl = url;
    this.settings.theme = theme;
    this.saveSettings();
  }
}

interface Settings {
  themeUrl: string;
  theme: string;
}
