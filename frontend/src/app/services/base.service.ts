import { HttpResponseBase } from '@angular/common/http'; // ignore
import { Observable } from 'rxjs';

// import { RequestOptionsArgs } from '@angular/http';

export class BaseService {
  protected transformOptions(options: any) {
    // TODO: Change options if required
    // const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // if (currentUser && currentUser.token) {
    //   options.headers.append('Authorization', `Bearer ${currentUser.token}`);
    // }
    return Promise.resolve(options);
  }

  protected transformResult(
    url: string,
    response: HttpResponseBase,
    processor: (response: HttpResponseBase) => any
  ): Observable<any> {
    return processor(response);
  }
}

export default {
  BaseService
};
