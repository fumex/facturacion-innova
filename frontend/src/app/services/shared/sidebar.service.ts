import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  menu: any = [
    {
      title: 'Persona',
      icon: 'mdi mdi-account-settings-variant',
      submenu: [
        // { title: 'Dashboard', url: '/dashboard' },
        { title: 'Actividad Económica', url: '/economic-activity' },
        { title: 'Estado de RUC', url: '/ruc-state' },
        { title: 'Condicion de Domicilio Fiscal', url: '/fiscal-address-condition' },
        { title: 'Padrones', url: '/pattern' },
        { title: 'Tipo de Contribuyente', url: '/taxpayer-type' },
        { title: 'Tipo de Documento de Identidad', url: '/document-identity-type' },
        { title: 'Tipo de Teléfono', url: '/telephone-type' },
        { title: 'Régimen Tributario', url: '/tax-regime' }
      ]
    },
    {
      title: 'Dirección',
      icon: 'mdi mdi-bank',
      submenu: [
        { title: 'Departamento', url: '/deparment' },
        { title: 'Provincia', url: '/province' },
        { title: 'Distrito', url: '/district' },
        { title: 'Tipo de Dirección', url: '/address-type' },
        { title: 'Tipo de Vía', url: '/road-type' },
        { title: 'Tipo de Zona', url: '/zone-type' }
      ]
    },
    {
      title: 'Comprobante',
      icon: 'mdi mdi-coin',
      submenu: [
        { title: 'Código de País', url: '/country-code' },
        { title: 'Medio de Pago', url: '/payment-method'},
        { title: 'Serie', url: '/serie'},
        { title: 'Tipo de Concepto Tributario', url: '/tax-concept'},
        { title: 'Tipo de Documento Comprobante', url: '/invoice-document-type' },
        { title: 'Tipo de Documento de Referencia', url: '/referenced-document-type' },
        { title: 'Tipo de Leyenda', url: '/legend-type'},
        { title: 'Tipo de Moneda', url: '/coin-type' },
        { title: 'Tipo de Operacion', url: '/operation-type' },
        { title: 'Tipo de Nota de Credito', url: '/credit-note-type' },
        { title: 'Tipo de Nota de Debito', url: '/debit-note-type' },
      ]
    },
    {
      title: 'Producto',
      icon: 'mdi mdi-archive',
      submenu: [
        { title: 'Categoria', url: '/category'},
        { title: 'Codigo SUNAT', url: '/sunat-code'},
        { title: 'Moneda', url: '/coin-type-product' },
        { title: 'Tipo de Afectación IGV', url: '/igv-involvement-type'},
        { title: 'Tipo de Afectación ISC', url: '/isc-involvement-type'},
        { title: 'Tipo de Cargo/Descuento', url: '/charge-discount-type'},
        { title: 'Tipo de Precio de Venta Unitario', url: '/unit-sale-price-type'},
        { title: 'Tipo de Tributo', url: '/tax-type'},
        { title: 'Unidad de Medida', url: '/unit-measurement'},
      ]
    },
  ];
  menu2: any = [
    {
      title: 'Girar Comprobante',
        icon: 'mdi mdi-cash-usd',
        submenu: [
          { title: 'Boleta Electrónica', url: '/boleta' },
          { title: 'Factura Electrónica', url: '/factura' }
        ]
      },
    {
      title: 'Registros',
      icon: 'mdi mdi-domain',
      submenu: [
        { title: 'Empresa', url: '/company' },
        { title: 'Clientes', url: '/client' },
        { title: 'Proveedores', url: '/provider' },
      ]
    },
    {
      title: 'Almacen',
      icon: 'mdi mdi-archive',
      submenu: [
        { title: 'Productos', url: '/product' },
      ]
    },
  ];
  constructor() { }
}
